output "azure_bot_service_noaccess" {
  value       = "${azurerm_role_definition.azure_bot_service_noaccess_definition}"
  description = "NoAccess Definition for Azure Bot Service."
}

output "azure_bot_service_readonly" {
  value       = "${azurerm_role_definition.azure_bot_service_readonly_definition}"
  description = "ReadOnly Definition for Azure Bot Service."
}

output "azure_bot_service_fullaccess" {
  value       = "${azurerm_role_definition.azure_bot_service_fullaccess_definition}"
  description = "FullAccess Definition for Azure Bot Service."
}

output "azure_databricks_noaccess" {
  value       = "${azurerm_role_definition.azure_databricks_noaccess_definition}"
  description = "NoAccess Definition for Azure Databricks."
}

output "azure_databricks_readonly" {
  value       = "${azurerm_role_definition.azure_databricks_readonly_definition}"
  description = "ReadOnly Definition for Azure Databricks."
}

output "azure_databricks_fullaccess" {
  value       = "${azurerm_role_definition.azure_databricks_fullaccess_definition}"
  description = "FullAccess Definition for Azure Databricks."
}

output "azure_search_noaccess" {
  value       = "${azurerm_role_definition.azure_search_noaccess_definition}"
  description = "NoAccess Definition for Azure Search."
}

output "azure_search_readonly" {
  value       = "${azurerm_role_definition.azure_search_readonly_definition}"
  description = "ReadOnly Definition for Azure Search."
}

output "azure_search_fullaccess" {
  value       = "${azurerm_role_definition.azure_search_fullaccess_definition}"
  description = "FullAccess Definition for Azure Search."
}

output "cognitive_services_noaccess" {
  value       = "${azurerm_role_definition.cognitive_services_noaccess_definition}"
  description = "NoAccess Definition for Cognitive Services."
}

output "cognitive_services_readonly" {
  value       = "${azurerm_role_definition.cognitive_services_readonly_definition}"
  description = "ReadOnly Definition for Cognitive Services."
}

output "cognitive_services_fullaccess" {
  value       = "${azurerm_role_definition.cognitive_services_fullaccess_definition}"
  description = "FullAccess Definition for Cognitive Services."
}

output "computer_vision_noaccess" {
  value       = "${azurerm_role_definition.computer_vision_noaccess_definition}"
  description = "NoAccess Definition for Computer Vision."
}

output "computer_vision_readonly" {
  value       = "${azurerm_role_definition.computer_vision_readonly_definition}"
  description = "ReadOnly Definition for Computer Vision."
}

output "computer_vision_fullaccess" {
  value       = "${azurerm_role_definition.computer_vision_fullaccess_definition}"
  description = "FullAccess Definition for Computer Vision."
}

output "content_moderator_noaccess" {
  value       = "${azurerm_role_definition.content_moderator_noaccess_definition}"
  description = "NoAccess Definition for Content moderator."
}

output "content_moderator_readonly" {
  value       = "${azurerm_role_definition.content_moderator_readonly_definition}"
  description = "ReadOnly Definition for Content moderator."
}

output "content_moderator_fullaccess" {
  value       = "${azurerm_role_definition.content_moderator_fullaccess_definition}"
  description = "FullAccess Definition for Content moderator."
}

output "face_noaccess" {
  value       = "${azurerm_role_definition.face_noaccess_definition}"
  description = "NoAccess Definition for Face."
}

output "face_readonly" {
  value       = "${azurerm_role_definition.face_readonly_definition}"
  description = "ReadOnly Definition for Face."
}

output "face_fullaccess" {
  value       = "${azurerm_role_definition.face_fullaccess_definition}"
  description = "FullAccess Definition for Face."
}

output "azure_machine_learning_service_noaccess" {
  value       = "${azurerm_role_definition.azure_machine_learning_service_noaccess_definition}"
  description = "NoAccess Definition for Azure Machine Learning service."
}

output "azure_machine_learning_service_readonly" {
  value       = "${azurerm_role_definition.azure_machine_learning_service_readonly_definition}"
  description = "ReadOnly Definition for Azure Machine Learning service."
}

output "azure_machine_learning_service_fullaccess" {
  value       = "${azurerm_role_definition.azure_machine_learning_service_fullaccess_definition}"
  description = "FullAccess Definition for Azure Machine Learning service."
}

output "machine_learning_studio_noaccess" {
  value       = "${azurerm_role_definition.machine_learning_studio_noaccess_definition}"
  description = "NoAccess Definition for Machine Learning Studio."
}

output "machine_learning_studio_readonly" {
  value       = "${azurerm_role_definition.machine_learning_studio_readonly_definition}"
  description = "ReadOnly Definition for Machine Learning Studio."
}

output "machine_learning_studio_fullaccess" {
  value       = "${azurerm_role_definition.machine_learning_studio_fullaccess_definition}"
  description = "FullAccess Definition for Machine Learning Studio."
}

output "language_understanding_noaccess" {
  value       = "${azurerm_role_definition.language_understanding_noaccess_definition}"
  description = "NoAccess Definition for Language Understanding."
}

output "language_understanding_readonly" {
  value       = "${azurerm_role_definition.language_understanding_readonly_definition}"
  description = "ReadOnly Definition for Language Understanding."
}

output "language_understanding_fullaccess" {
  value       = "${azurerm_role_definition.language_understanding_fullaccess_definition}"
  description = "FullAccess Definition for Language Understanding."
}

output "text_analytics_noaccess" {
  value       = "${azurerm_role_definition.text_analytics_noaccess_definition}"
  description = "NoAccess Definition for Text Analytics."
}

output "text_analytics_readonly" {
  value       = "${azurerm_role_definition.text_analytics_readonly_definition}"
  description = "ReadOnly Definition for Text Analytics."
}

output "text_analytics_fullaccess" {
  value       = "${azurerm_role_definition.text_analytics_fullaccess_definition}"
  description = "FullAccess Definition for Text Analytics."
}

output "azure_stream_analytics_noaccess" {
  value       = "${azurerm_role_definition.azure_stream_analytics_noaccess_definition}"
  description = "NoAccess Definition for Azure Stream Analytics."
}

output "azure_stream_analytics_readonly" {
  value       = "${azurerm_role_definition.azure_stream_analytics_readonly_definition}"
  description = "ReadOnly Definition for Azure Stream Analytics."
}

output "azure_stream_analytics_fullaccess" {
  value       = "${azurerm_role_definition.azure_stream_analytics_fullaccess_definition}"
  description = "FullAccess Definition for Azure Stream Analytics."
}

output "hdinsight_noaccess" {
  value       = "${azurerm_role_definition.hdinsight_noaccess_definition}"
  description = "NoAccess Definition for HDInsight."
}

output "hdinsight_readonly" {
  value       = "${azurerm_role_definition.hdinsight_readonly_definition}"
  description = "ReadOnly Definition for HDInsight."
}

output "hdinsight_fullaccess" {
  value       = "${azurerm_role_definition.hdinsight_fullaccess_definition}"
  description = "FullAccess Definition for HDInsight."
}

output "data_factory_noaccess" {
  value       = "${azurerm_role_definition.data_factory_noaccess_definition}"
  description = "NoAccess Definition for Data Factory."
}

output "data_factory_readonly" {
  value       = "${azurerm_role_definition.data_factory_readonly_definition}"
  description = "ReadOnly Definition for Data Factory."
}

output "data_factory_fullaccess" {
  value       = "${azurerm_role_definition.data_factory_fullaccess_definition}"
  description = "FullAccess Definition for Data Factory."
}

output "data_lake_analytics_noaccess" {
  value       = "${azurerm_role_definition.data_lake_analytics_noaccess_definition}"
  description = "NoAccess Definition for Data Lake Analytics."
}

output "data_lake_analytics_readonly" {
  value       = "${azurerm_role_definition.data_lake_analytics_readonly_definition}"
  description = "ReadOnly Definition for Data Lake Analytics."
}

output "data_lake_analytics_fullaccess" {
  value       = "${azurerm_role_definition.data_lake_analytics_fullaccess_definition}"
  description = "FullAccess Definition for Data Lake Analytics."
}

output "event_hubs_noaccess" {
  value       = "${azurerm_role_definition.event_hubs_noaccess_definition}"
  description = "NoAccess Definition for Event Hubs."
}

output "event_hubs_readonly" {
  value       = "${azurerm_role_definition.event_hubs_readonly_definition}"
  description = "ReadOnly Definition for Event Hubs."
}

output "event_hubs_fullaccess" {
  value       = "${azurerm_role_definition.event_hubs_fullaccess_definition}"
  description = "FullAccess Definition for Event Hubs."
}

output "azure_analysis_services_noaccess" {
  value       = "${azurerm_role_definition.azure_analysis_services_noaccess_definition}"
  description = "NoAccess Definition for Azure Analysis Services."
}

output "azure_analysis_services_readonly" {
  value       = "${azurerm_role_definition.azure_analysis_services_readonly_definition}"
  description = "ReadOnly Definition for Azure Analysis Services."
}

output "azure_analysis_services_fullaccess" {
  value       = "${azurerm_role_definition.azure_analysis_services_fullaccess_definition}"
  description = "FullAccess Definition for Azure Analysis Services."
}

output "data_catalog_noaccess" {
  value       = "${azurerm_role_definition.data_catalog_noaccess_definition}"
  description = "NoAccess Definition for Data Catalog."
}

output "data_catalog_readonly" {
  value       = "${azurerm_role_definition.data_catalog_readonly_definition}"
  description = "ReadOnly Definition for Data Catalog."
}

output "data_catalog_fullaccess" {
  value       = "${azurerm_role_definition.data_catalog_fullaccess_definition}"
  description = "FullAccess Definition for Data Catalog."
}

output "azure_data_explorer_noaccess" {
  value       = "${azurerm_role_definition.azure_data_explorer_noaccess_definition}"
  description = "NoAccess Definition for Azure Data Explorer."
}

output "azure_data_explorer_readonly" {
  value       = "${azurerm_role_definition.azure_data_explorer_readonly_definition}"
  description = "ReadOnly Definition for Azure Data Explorer."
}

output "azure_data_explorer_fullaccess" {
  value       = "${azurerm_role_definition.azure_data_explorer_fullaccess_definition}"
  description = "FullAccess Definition for Azure Data Explorer."
}

output "azure_blockchain_service_noaccess" {
  value       = "${azurerm_role_definition.azure_blockchain_service_noaccess_definition}"
  description = "NoAccess Definition for Azure Blockchain Service."
}

output "azure_blockchain_service_readonly" {
  value       = "${azurerm_role_definition.azure_blockchain_service_readonly_definition}"
  description = "ReadOnly Definition for Azure Blockchain Service."
}

output "azure_blockchain_service_fullaccess" {
  value       = "${azurerm_role_definition.azure_blockchain_service_fullaccess_definition}"
  description = "FullAccess Definition for Azure Blockchain Service."
}

output "virtual_machines_noaccess" {
  value       = "${azurerm_role_definition.virtual_machines_noaccess_definition}"
  description = "NoAccess Definition for Virtual Machines."
}

output "virtual_machines_readonly" {
  value       = "${azurerm_role_definition.virtual_machines_readonly_definition}"
  description = "ReadOnly Definition for Virtual Machines."
}

output "virtual_machines_fullaccess" {
  value       = "${azurerm_role_definition.virtual_machines_fullaccess_definition}"
  description = "FullAccess Definition for Virtual Machines."
}

output "batch_noaccess" {
  value       = "${azurerm_role_definition.batch_noaccess_definition}"
  description = "NoAccess Definition for Batch."
}

output "batch_readonly" {
  value       = "${azurerm_role_definition.batch_readonly_definition}"
  description = "ReadOnly Definition for Batch."
}

output "batch_fullaccess" {
  value       = "${azurerm_role_definition.batch_fullaccess_definition}"
  description = "FullAccess Definition for Batch."
}

output "sap_hana_on_azure_large_instances_noaccess" {
  value       = "${azurerm_role_definition.sap_hana_on_azure_large_instances_noaccess_definition}"
  description = "NoAccess Definition for SAP HANA on Azure Large Instances."
}

output "sap_hana_on_azure_large_instances_readonly" {
  value       = "${azurerm_role_definition.sap_hana_on_azure_large_instances_readonly_definition}"
  description = "ReadOnly Definition for SAP HANA on Azure Large Instances."
}

output "sap_hana_on_azure_large_instances_fullaccess" {
  value       = "${azurerm_role_definition.sap_hana_on_azure_large_instances_fullaccess_definition}"
  description = "FullAccess Definition for SAP HANA on Azure Large Instances."
}

output "virtual_machine_scale_sets_noaccess" {
  value       = "${azurerm_role_definition.virtual_machine_scale_sets_noaccess_definition}"
  description = "NoAccess Definition for Virtual Machine Scale Sets."
}

output "virtual_machine_scale_sets_readonly" {
  value       = "${azurerm_role_definition.virtual_machine_scale_sets_readonly_definition}"
  description = "ReadOnly Definition for Virtual Machine Scale Sets."
}

output "virtual_machine_scale_sets_fullaccess" {
  value       = "${azurerm_role_definition.virtual_machine_scale_sets_fullaccess_definition}"
  description = "FullAccess Definition for Virtual Machine Scale Sets."
}

output "azure_vmware_solution_by_cloudsimple_noaccess" {
  value       = "${azurerm_role_definition.azure_vmware_solution_by_cloudsimple_noaccess_definition}"
  description = "NoAccess Definition for Azure VMware Solution by CloudSimple."
}

output "azure_vmware_solution_by_cloudsimple_readonly" {
  value       = "${azurerm_role_definition.azure_vmware_solution_by_cloudsimple_readonly_definition}"
  description = "ReadOnly Definition for Azure VMware Solution by CloudSimple."
}

output "azure_vmware_solution_by_cloudsimple_fullaccess" {
  value       = "${azurerm_role_definition.azure_vmware_solution_by_cloudsimple_fullaccess_definition}"
  description = "FullAccess Definition for Azure VMware Solution by CloudSimple."
}

output "azure_kubernetes_service_noaccess" {
  value       = "${azurerm_role_definition.azure_kubernetes_service_noaccess_definition}"
  description = "NoAccess Definition for Azure Kubernetes Service (AKS)."
}

output "azure_kubernetes_service_readonly" {
  value       = "${azurerm_role_definition.azure_kubernetes_service_readonly_definition}"
  description = "ReadOnly Definition for Azure Kubernetes Service (AKS)."
}

output "azure_kubernetes_service_fullaccess" {
  value       = "${azurerm_role_definition.azure_kubernetes_service_fullaccess_definition}"
  description = "FullAccess Definition for Azure Kubernetes Service (AKS)."
}

output "service_fabric_noaccess" {
  value       = "${azurerm_role_definition.service_fabric_noaccess_definition}"
  description = "NoAccess Definition for Service Fabric."
}

output "service_fabric_readonly" {
  value       = "${azurerm_role_definition.service_fabric_readonly_definition}"
  description = "ReadOnly Definition for Service Fabric."
}

output "service_fabric_fullaccess" {
  value       = "${azurerm_role_definition.service_fabric_fullaccess_definition}"
  description = "FullAccess Definition for Service Fabric."
}

output "container_instances_noaccess" {
  value       = "${azurerm_role_definition.container_instances_noaccess_definition}"
  description = "NoAccess Definition for Container Instances."
}

output "container_instances_readonly" {
  value       = "${azurerm_role_definition.container_instances_readonly_definition}"
  description = "ReadOnly Definition for Container Instances."
}

output "container_instances_fullaccess" {
  value       = "${azurerm_role_definition.container_instances_fullaccess_definition}"
  description = "FullAccess Definition for Container Instances."
}

output "container_registry_noaccess" {
  value       = "${azurerm_role_definition.container_registry_noaccess_definition}"
  description = "NoAccess Definition for Container Registry."
}

output "container_registry_readonly" {
  value       = "${azurerm_role_definition.container_registry_readonly_definition}"
  description = "ReadOnly Definition for Container Registry."
}

output "container_registry_fullaccess" {
  value       = "${azurerm_role_definition.container_registry_fullaccess_definition}"
  description = "FullAccess Definition for Container Registry."
}

output "azure_red_hat_openshift_noaccess" {
  value       = "${azurerm_role_definition.azure_red_hat_openshift_noaccess_definition}"
  description = "NoAccess Definition for Azure Red Hat OpenShift."
}

output "azure_red_hat_openshift_readonly" {
  value       = "${azurerm_role_definition.azure_red_hat_openshift_readonly_definition}"
  description = "ReadOnly Definition for Azure Red Hat OpenShift."
}

output "azure_red_hat_openshift_fullaccess" {
  value       = "${azurerm_role_definition.azure_red_hat_openshift_fullaccess_definition}"
  description = "FullAccess Definition for Azure Red Hat OpenShift."
}

output "azure_sql_database_noaccess" {
  value       = "${azurerm_role_definition.azure_sql_database_noaccess_definition}"
  description = "NoAccess Definition for Azure SQL Database."
}

output "azure_sql_database_readonly" {
  value       = "${azurerm_role_definition.azure_sql_database_readonly_definition}"
  description = "ReadOnly Definition for Azure SQL Database."
}

output "azure_sql_database_fullaccess" {
  value       = "${azurerm_role_definition.azure_sql_database_fullaccess_definition}"
  description = "FullAccess Definition for Azure SQL Database."
}

output "azure_cosmos_db_noaccess" {
  value       = "${azurerm_role_definition.azure_cosmos_db_noaccess_definition}"
  description = "NoAccess Definition for Azure Cosmos DB."
}

output "azure_cosmos_db_readonly" {
  value       = "${azurerm_role_definition.azure_cosmos_db_readonly_definition}"
  description = "ReadOnly Definition for Azure Cosmos DB."
}

output "azure_cosmos_db_fullaccess" {
  value       = "${azurerm_role_definition.azure_cosmos_db_fullaccess_definition}"
  description = "FullAccess Definition for Azure Cosmos DB."
}

output "azure_cache_for_redis_noaccess" {
  value       = "${azurerm_role_definition.azure_cache_for_redis_noaccess_definition}"
  description = "NoAccess Definition for Azure Cache for Redis."
}

output "azure_cache_for_redis_readonly" {
  value       = "${azurerm_role_definition.azure_cache_for_redis_readonly_definition}"
  description = "ReadOnly Definition for Azure Cache for Redis."
}

output "azure_cache_for_redis_fullaccess" {
  value       = "${azurerm_role_definition.azure_cache_for_redis_fullaccess_definition}"
  description = "FullAccess Definition for Azure Cache for Redis."
}

output "azure_database_for_postgresql_noaccess" {
  value       = "${azurerm_role_definition.azure_database_for_postgresql_noaccess_definition}"
  description = "NoAccess Definition for Azure Database for PostgreSQL."
}

output "azure_database_for_postgresql_readonly" {
  value       = "${azurerm_role_definition.azure_database_for_postgresql_readonly_definition}"
  description = "ReadOnly Definition for Azure Database for PostgreSQL."
}

output "azure_database_for_postgresql_fullaccess" {
  value       = "${azurerm_role_definition.azure_database_for_postgresql_fullaccess_definition}"
  description = "FullAccess Definition for Azure Database for PostgreSQL."
}

output "azure_database_for_mariadb_noaccess" {
  value       = "${azurerm_role_definition.azure_database_for_mariadb_noaccess_definition}"
  description = "NoAccess Definition for Azure Database for MariaDB."
}

output "azure_database_for_mariadb_readonly" {
  value       = "${azurerm_role_definition.azure_database_for_mariadb_readonly_definition}"
  description = "ReadOnly Definition for Azure Database for MariaDB."
}

output "azure_database_for_mariadb_fullaccess" {
  value       = "${azurerm_role_definition.azure_database_for_mariadb_fullaccess_definition}"
  description = "FullAccess Definition for Azure Database for MariaDB."
}

output "azure_database_for_mysql_noaccess" {
  value       = "${azurerm_role_definition.azure_database_for_mysql_noaccess_definition}"
  description = "NoAccess Definition for Azure Database for MySQL."
}

output "azure_database_for_mysql_readonly" {
  value       = "${azurerm_role_definition.azure_database_for_mysql_readonly_definition}"
  description = "ReadOnly Definition for Azure Database for MySQL."
}

output "azure_database_for_mysql_fullaccess" {
  value       = "${azurerm_role_definition.azure_database_for_mysql_fullaccess_definition}"
  description = "FullAccess Definition for Azure Database for MySQL."
}

output "azure_lab_services_noaccess" {
  value       = "${azurerm_role_definition.azure_lab_services_noaccess_definition}"
  description = "NoAccess Definition for Azure Lab Services."
}

output "azure_lab_services_readonly" {
  value       = "${azurerm_role_definition.azure_lab_services_readonly_definition}"
  description = "ReadOnly Definition for Azure Lab Services."
}

output "azure_lab_services_fullaccess" {
  value       = "${azurerm_role_definition.azure_lab_services_fullaccess_definition}"
  description = "FullAccess Definition for Azure Lab Services."
}

output "azure_devops_noaccess" {
  value       = "${azurerm_role_definition.azure_devops_noaccess_definition}"
  description = "NoAccess Definition for Azure DevOps."
}

output "azure_devops_readonly" {
  value       = "${azurerm_role_definition.azure_devops_readonly_definition}"
  description = "ReadOnly Definition for Azure DevOps."
}

output "azure_devops_fullaccess" {
  value       = "${azurerm_role_definition.azure_devops_fullaccess_definition}"
  description = "FullAccess Definition for Azure DevOps."
}

output "azure_devtest_labs_noaccess" {
  value       = "${azurerm_role_definition.azure_devtest_labs_noaccess_definition}"
  description = "NoAccess Definition for Azure DevTest Labs."
}

output "azure_devtest_labs_readonly" {
  value       = "${azurerm_role_definition.azure_devtest_labs_readonly_definition}"
  description = "ReadOnly Definition for Azure DevTest Labs."
}

output "azure_devtest_labs_fullaccess" {
  value       = "${azurerm_role_definition.azure_devtest_labs_fullaccess_definition}"
  description = "FullAccess Definition for Azure DevTest Labs."
}

output "azure_active_directory_noaccess" {
  value       = "${azurerm_role_definition.azure_active_directory_noaccess_definition}"
  description = "NoAccess Definition for Azure Active Directory."
}

output "azure_active_directory_readonly" {
  value       = "${azurerm_role_definition.azure_active_directory_readonly_definition}"
  description = "ReadOnly Definition for Azure Active Directory."
}

output "azure_active_directory_fullaccess" {
  value       = "${azurerm_role_definition.azure_active_directory_fullaccess_definition}"
  description = "FullAccess Definition for Azure Active Directory."
}

output "azure_active_directory_domain_services_noaccess" {
  value       = "${azurerm_role_definition.azure_active_directory_domain_services_noaccess_definition}"
  description = "NoAccess Definition for Azure Active Directory Domain Services."
}

output "azure_active_directory_domain_services_readonly" {
  value       = "${azurerm_role_definition.azure_active_directory_domain_services_readonly_definition}"
  description = "ReadOnly Definition for Azure Active Directory Domain Services."
}

output "azure_active_directory_domain_services_fullaccess" {
  value       = "${azurerm_role_definition.azure_active_directory_domain_services_fullaccess_definition}"
  description = "FullAccess Definition for Azure Active Directory Domain Services."
}

output "event_grid_noaccess" {
  value       = "${azurerm_role_definition.event_grid_noaccess_definition}"
  description = "NoAccess Definition for Event Grid."
}

output "event_grid_readonly" {
  value       = "${azurerm_role_definition.event_grid_readonly_definition}"
  description = "ReadOnly Definition for Event Grid."
}

output "event_grid_fullaccess" {
  value       = "${azurerm_role_definition.event_grid_fullaccess_definition}"
  description = "FullAccess Definition for Event Grid."
}

output "logic_apps_noaccess" {
  value       = "${azurerm_role_definition.logic_apps_noaccess_definition}"
  description = "NoAccess Definition for Logic Apps."
}

output "logic_apps_readonly" {
  value       = "${azurerm_role_definition.logic_apps_readonly_definition}"
  description = "ReadOnly Definition for Logic Apps."
}

output "logic_apps_fullaccess" {
  value       = "${azurerm_role_definition.logic_apps_fullaccess_definition}"
  description = "FullAccess Definition for Logic Apps."
}

output "api_management_noaccess" {
  value       = "${azurerm_role_definition.api_management_noaccess_definition}"
  description = "NoAccess Definition for API Management."
}

output "api_management_readonly" {
  value       = "${azurerm_role_definition.api_management_readonly_definition}"
  description = "ReadOnly Definition for API Management."
}

output "api_management_fullaccess" {
  value       = "${azurerm_role_definition.api_management_fullaccess_definition}"
  description = "FullAccess Definition for API Management."
}

output "service_bus_noaccess" {
  value       = "${azurerm_role_definition.service_bus_noaccess_definition}"
  description = "NoAccess Definition for Service Bus."
}

output "service_bus_readonly" {
  value       = "${azurerm_role_definition.service_bus_readonly_definition}"
  description = "ReadOnly Definition for Service Bus."
}

output "service_bus_fullaccess" {
  value       = "${azurerm_role_definition.service_bus_fullaccess_definition}"
  description = "FullAccess Definition for Service Bus."
}

output "azure_iot_hub_noaccess" {
  value       = "${azurerm_role_definition.azure_iot_hub_noaccess_definition}"
  description = "NoAccess Definition for Azure IoT Hub."
}

output "azure_iot_hub_readonly" {
  value       = "${azurerm_role_definition.azure_iot_hub_readonly_definition}"
  description = "ReadOnly Definition for Azure IoT Hub."
}

output "azure_iot_hub_fullaccess" {
  value       = "${azurerm_role_definition.azure_iot_hub_fullaccess_definition}"
  description = "FullAccess Definition for Azure IoT Hub."
}

output "azure_iot_central_noaccess" {
  value       = "${azurerm_role_definition.azure_iot_central_noaccess_definition}"
  description = "NoAccess Definition for Azure IoT Central."
}

output "azure_iot_central_readonly" {
  value       = "${azurerm_role_definition.azure_iot_central_readonly_definition}"
  description = "ReadOnly Definition for Azure IoT Central."
}

output "azure_iot_central_fullaccess" {
  value       = "${azurerm_role_definition.azure_iot_central_fullaccess_definition}"
  description = "FullAccess Definition for Azure IoT Central."
}

output "azure_time_series_insights_noaccess" {
  value       = "${azurerm_role_definition.azure_time_series_insights_noaccess_definition}"
  description = "NoAccess Definition for Azure Time Series Insights."
}

output "azure_time_series_insights_readonly" {
  value       = "${azurerm_role_definition.azure_time_series_insights_readonly_definition}"
  description = "ReadOnly Definition for Azure Time Series Insights."
}

output "azure_time_series_insights_fullaccess" {
  value       = "${azurerm_role_definition.azure_time_series_insights_fullaccess_definition}"
  description = "FullAccess Definition for Azure Time Series Insights."
}

output "azure_maps_noaccess" {
  value       = "${azurerm_role_definition.azure_maps_noaccess_definition}"
  description = "NoAccess Definition for Azure Maps."
}

output "azure_maps_readonly" {
  value       = "${azurerm_role_definition.azure_maps_readonly_definition}"
  description = "ReadOnly Definition for Azure Maps."
}

output "azure_maps_fullaccess" {
  value       = "${azurerm_role_definition.azure_maps_fullaccess_definition}"
  description = "FullAccess Definition for Azure Maps."
}

output "windows_10_iot_core_services_noaccess" {
  value       = "${azurerm_role_definition.windows_10_iot_core_services_noaccess_definition}"
  description = "NoAccess Definition for Windows 10 IoT Core Services."
}

output "windows_10_iot_core_services_readonly" {
  value       = "${azurerm_role_definition.windows_10_iot_core_services_readonly_definition}"
  description = "ReadOnly Definition for Windows 10 IoT Core Services."
}

output "windows_10_iot_core_services_fullaccess" {
  value       = "${azurerm_role_definition.windows_10_iot_core_services_fullaccess_definition}"
  description = "FullAccess Definition for Windows 10 IoT Core Services."
}

output "notification_hubs_noaccess" {
  value       = "${azurerm_role_definition.notification_hubs_noaccess_definition}"
  description = "NoAccess Definition for Notification Hubs."
}

output "notification_hubs_readonly" {
  value       = "${azurerm_role_definition.notification_hubs_readonly_definition}"
  description = "ReadOnly Definition for Notification Hubs."
}

output "notification_hubs_fullaccess" {
  value       = "${azurerm_role_definition.notification_hubs_fullaccess_definition}"
  description = "FullAccess Definition for Notification Hubs."
}

output "azure_advisor_noaccess" {
  value       = "${azurerm_role_definition.azure_advisor_noaccess_definition}"
  description = "NoAccess Definition for Azure Advisor."
}

output "azure_advisor_readonly" {
  value       = "${azurerm_role_definition.azure_advisor_readonly_definition}"
  description = "ReadOnly Definition for Azure Advisor."
}

output "azure_advisor_fullaccess" {
  value       = "${azurerm_role_definition.azure_advisor_fullaccess_definition}"
  description = "FullAccess Definition for Azure Advisor."
}

output "scheduler_noaccess" {
  value       = "${azurerm_role_definition.scheduler_noaccess_definition}"
  description = "NoAccess Definition for Scheduler."
}

output "scheduler_readonly" {
  value       = "${azurerm_role_definition.scheduler_readonly_definition}"
  description = "ReadOnly Definition for Scheduler."
}

output "scheduler_fullaccess" {
  value       = "${azurerm_role_definition.scheduler_fullaccess_definition}"
  description = "FullAccess Definition for Scheduler."
}

output "automation_noaccess" {
  value       = "${azurerm_role_definition.automation_noaccess_definition}"
  description = "NoAccess Definition for Automation."
}

output "automation_readonly" {
  value       = "${azurerm_role_definition.automation_readonly_definition}"
  description = "ReadOnly Definition for Automation."
}

output "automation_fullaccess" {
  value       = "${azurerm_role_definition.automation_fullaccess_definition}"
  description = "FullAccess Definition for Automation."
}

output "azure_monitor_noaccess" {
  value       = "${azurerm_role_definition.azure_monitor_noaccess_definition}"
  description = "NoAccess Definition for Azure Monitor."
}

output "azure_monitor_readonly" {
  value       = "${azurerm_role_definition.azure_monitor_readonly_definition}"
  description = "ReadOnly Definition for Azure Monitor."
}

output "azure_monitor_fullaccess" {
  value       = "${azurerm_role_definition.azure_monitor_fullaccess_definition}"
  description = "FullAccess Definition for Azure Monitor."
}

output "azure_service_health_noaccess" {
  value       = "${azurerm_role_definition.azure_service_health_noaccess_definition}"
  description = "NoAccess Definition for Azure Service Health."
}

output "azure_service_health_readonly" {
  value       = "${azurerm_role_definition.azure_service_health_readonly_definition}"
  description = "ReadOnly Definition for Azure Service Health."
}

output "azure_service_health_fullaccess" {
  value       = "${azurerm_role_definition.azure_service_health_fullaccess_definition}"
  description = "FullAccess Definition for Azure Service Health."
}

output "microsoft_azure_portal_noaccess" {
  value       = "${azurerm_role_definition.microsoft_azure_portal_noaccess_definition}"
  description = "NoAccess Definition for Microsoft Azure portal."
}

output "microsoft_azure_portal_readonly" {
  value       = "${azurerm_role_definition.microsoft_azure_portal_readonly_definition}"
  description = "ReadOnly Definition for Microsoft Azure portal."
}

output "microsoft_azure_portal_fullaccess" {
  value       = "${azurerm_role_definition.microsoft_azure_portal_fullaccess_definition}"
  description = "FullAccess Definition for Microsoft Azure portal."
}

output "azure_resource_manager_noaccess" {
  value       = "${azurerm_role_definition.azure_resource_manager_noaccess_definition}"
  description = "NoAccess Definition for Azure Resource Manager."
}

output "azure_resource_manager_readonly" {
  value       = "${azurerm_role_definition.azure_resource_manager_readonly_definition}"
  description = "ReadOnly Definition for Azure Resource Manager."
}

output "azure_resource_manager_fullaccess" {
  value       = "${azurerm_role_definition.azure_resource_manager_fullaccess_definition}"
  description = "FullAccess Definition for Azure Resource Manager."
}

output "azure_policy_noaccess" {
  value       = "${azurerm_role_definition.azure_policy_noaccess_definition}"
  description = "NoAccess Definition for Azure Policy."
}

output "azure_policy_readonly" {
  value       = "${azurerm_role_definition.azure_policy_readonly_definition}"
  description = "ReadOnly Definition for Azure Policy."
}

output "azure_policy_fullaccess" {
  value       = "${azurerm_role_definition.azure_policy_fullaccess_definition}"
  description = "FullAccess Definition for Azure Policy."
}

output "cost_management_noaccess" {
  value       = "${azurerm_role_definition.cost_management_noaccess_definition}"
  description = "NoAccess Definition for Cost management."
}

output "cost_management_readonly" {
  value       = "${azurerm_role_definition.cost_management_readonly_definition}"
  description = "ReadOnly Definition for Cost management."
}

output "cost_management_fullaccess" {
  value       = "${azurerm_role_definition.cost_management_fullaccess_definition}"
  description = "FullAccess Definition for Cost management."
}

output "azure_managed_applications_noaccess" {
  value       = "${azurerm_role_definition.azure_managed_applications_noaccess_definition}"
  description = "NoAccess Definition for Azure Managed Applications."
}

output "azure_managed_applications_readonly" {
  value       = "${azurerm_role_definition.azure_managed_applications_readonly_definition}"
  description = "ReadOnly Definition for Azure Managed Applications."
}

output "azure_managed_applications_fullaccess" {
  value       = "${azurerm_role_definition.azure_managed_applications_fullaccess_definition}"
  description = "FullAccess Definition for Azure Managed Applications."
}

output "azure_blueprints_noaccess" {
  value       = "${azurerm_role_definition.azure_blueprints_noaccess_definition}"
  description = "NoAccess Definition for Azure Blueprints."
}

output "azure_blueprints_readonly" {
  value       = "${azurerm_role_definition.azure_blueprints_readonly_definition}"
  description = "ReadOnly Definition for Azure Blueprints."
}

output "azure_blueprints_fullaccess" {
  value       = "${azurerm_role_definition.azure_blueprints_fullaccess_definition}"
  description = "FullAccess Definition for Azure Blueprints."
}

output "content_delivery_network_noaccess" {
  value       = "${azurerm_role_definition.content_delivery_network_noaccess_definition}"
  description = "NoAccess Definition for Content Delivery Network."
}

output "content_delivery_network_readonly" {
  value       = "${azurerm_role_definition.content_delivery_network_readonly_definition}"
  description = "ReadOnly Definition for Content Delivery Network."
}

output "content_delivery_network_fullaccess" {
  value       = "${azurerm_role_definition.content_delivery_network_fullaccess_definition}"
  description = "FullAccess Definition for Content Delivery Network."
}

output "media_services_noaccess" {
  value       = "${azurerm_role_definition.media_services_noaccess_definition}"
  description = "NoAccess Definition for Media Services."
}

output "media_services_readonly" {
  value       = "${azurerm_role_definition.media_services_readonly_definition}"
  description = "ReadOnly Definition for Media Services."
}

output "media_services_fullaccess" {
  value       = "${azurerm_role_definition.media_services_fullaccess_definition}"
  description = "FullAccess Definition for Media Services."
}

output "azure_stack_noaccess" {
  value       = "${azurerm_role_definition.azure_stack_noaccess_definition}"
  description = "NoAccess Definition for Azure Stack."
}

output "azure_stack_readonly" {
  value       = "${azurerm_role_definition.azure_stack_readonly_definition}"
  description = "ReadOnly Definition for Azure Stack."
}

output "azure_stack_fullaccess" {
  value       = "${azurerm_role_definition.azure_stack_fullaccess_definition}"
  description = "FullAccess Definition for Azure Stack."
}

output "azure_site_recovery_noaccess" {
  value       = "${azurerm_role_definition.azure_site_recovery_noaccess_definition}"
  description = "NoAccess Definition for Azure Site Recovery."
}

output "azure_site_recovery_readonly" {
  value       = "${azurerm_role_definition.azure_site_recovery_readonly_definition}"
  description = "ReadOnly Definition for Azure Site Recovery."
}

output "azure_site_recovery_fullaccess" {
  value       = "${azurerm_role_definition.azure_site_recovery_fullaccess_definition}"
  description = "FullAccess Definition for Azure Site Recovery."
}

output "azure_database_migration_service_noaccess" {
  value       = "${azurerm_role_definition.azure_database_migration_service_noaccess_definition}"
  description = "NoAccess Definition for Azure Database Migration Service."
}

output "azure_database_migration_service_readonly" {
  value       = "${azurerm_role_definition.azure_database_migration_service_readonly_definition}"
  description = "ReadOnly Definition for Azure Database Migration Service."
}

output "azure_database_migration_service_fullaccess" {
  value       = "${azurerm_role_definition.azure_database_migration_service_fullaccess_definition}"
  description = "FullAccess Definition for Azure Database Migration Service."
}

output "azure_migrate_noaccess" {
  value       = "${azurerm_role_definition.azure_migrate_noaccess_definition}"
  description = "NoAccess Definition for Azure Migrate."
}

output "azure_migrate_readonly" {
  value       = "${azurerm_role_definition.azure_migrate_readonly_definition}"
  description = "ReadOnly Definition for Azure Migrate."
}

output "azure_migrate_fullaccess" {
  value       = "${azurerm_role_definition.azure_migrate_fullaccess_definition}"
  description = "FullAccess Definition for Azure Migrate."
}

output "spatial_anchors_noaccess" {
  value       = "${azurerm_role_definition.spatial_anchors_noaccess_definition}"
  description = "NoAccess Definition for Spatial anchors."
}

output "spatial_anchors_readonly" {
  value       = "${azurerm_role_definition.spatial_anchors_readonly_definition}"
  description = "ReadOnly Definition for Spatial anchors."
}

output "spatial_anchors_fullaccess" {
  value       = "${azurerm_role_definition.spatial_anchors_fullaccess_definition}"
  description = "FullAccess Definition for Spatial anchors."
}

output "visual_studio_app_centre_noaccess" {
  value       = "${azurerm_role_definition.visual_studio_app_centre_noaccess_definition}"
  description = "NoAccess Definition for Visual Studio App Centre."
}

output "visual_studio_app_centre_readonly" {
  value       = "${azurerm_role_definition.visual_studio_app_centre_readonly_definition}"
  description = "ReadOnly Definition for Visual Studio App Centre."
}

output "visual_studio_app_centre_fullaccess" {
  value       = "${azurerm_role_definition.visual_studio_app_centre_fullaccess_definition}"
  description = "FullAccess Definition for Visual Studio App Centre."
}

output "expressroute_noaccess" {
  value       = "${azurerm_role_definition.expressroute_noaccess_definition}"
  description = "NoAccess Definition for ExpressRoute."
}

output "expressroute_readonly" {
  value       = "${azurerm_role_definition.expressroute_readonly_definition}"
  description = "ReadOnly Definition for ExpressRoute."
}

output "expressroute_fullaccess" {
  value       = "${azurerm_role_definition.expressroute_fullaccess_definition}"
  description = "FullAccess Definition for ExpressRoute."
}

output "azure_dns_noaccess" {
  value       = "${azurerm_role_definition.azure_dns_noaccess_definition}"
  description = "NoAccess Definition for Azure DNS."
}

output "azure_dns_readonly" {
  value       = "${azurerm_role_definition.azure_dns_readonly_definition}"
  description = "ReadOnly Definition for Azure DNS."
}

output "azure_dns_fullaccess" {
  value       = "${azurerm_role_definition.azure_dns_fullaccess_definition}"
  description = "FullAccess Definition for Azure DNS."
}

output "virtual_network_noaccess" {
  value       = "${azurerm_role_definition.virtual_network_noaccess_definition}"
  description = "NoAccess Definition for Virtual Network."
}

output "virtual_network_readonly" {
  value       = "${azurerm_role_definition.virtual_network_readonly_definition}"
  description = "ReadOnly Definition for Virtual Network."
}

output "virtual_network_fullaccess" {
  value       = "${azurerm_role_definition.virtual_network_fullaccess_definition}"
  description = "FullAccess Definition for Virtual Network."
}

output "traffic_manager_noaccess" {
  value       = "${azurerm_role_definition.traffic_manager_noaccess_definition}"
  description = "NoAccess Definition for Traffic Manager."
}

output "traffic_manager_readonly" {
  value       = "${azurerm_role_definition.traffic_manager_readonly_definition}"
  description = "ReadOnly Definition for Traffic Manager."
}

output "traffic_manager_fullaccess" {
  value       = "${azurerm_role_definition.traffic_manager_fullaccess_definition}"
  description = "FullAccess Definition for Traffic Manager."
}

output "load_balancer_noaccess" {
  value       = "${azurerm_role_definition.load_balancer_noaccess_definition}"
  description = "NoAccess Definition for Load Balancer."
}

output "load_balancer_readonly" {
  value       = "${azurerm_role_definition.load_balancer_readonly_definition}"
  description = "ReadOnly Definition for Load Balancer."
}

output "load_balancer_fullaccess" {
  value       = "${azurerm_role_definition.load_balancer_fullaccess_definition}"
  description = "FullAccess Definition for Load Balancer."
}

output "vpn_gateway_noaccess" {
  value       = "${azurerm_role_definition.vpn_gateway_noaccess_definition}"
  description = "NoAccess Definition for VPN Gateway."
}

output "vpn_gateway_readonly" {
  value       = "${azurerm_role_definition.vpn_gateway_readonly_definition}"
  description = "ReadOnly Definition for VPN Gateway."
}

output "vpn_gateway_fullaccess" {
  value       = "${azurerm_role_definition.vpn_gateway_fullaccess_definition}"
  description = "FullAccess Definition for VPN Gateway."
}

output "application_gateway_noaccess" {
  value       = "${azurerm_role_definition.application_gateway_noaccess_definition}"
  description = "NoAccess Definition for Application Gateway."
}

output "application_gateway_readonly" {
  value       = "${azurerm_role_definition.application_gateway_readonly_definition}"
  description = "ReadOnly Definition for Application Gateway."
}

output "application_gateway_fullaccess" {
  value       = "${azurerm_role_definition.application_gateway_fullaccess_definition}"
  description = "FullAccess Definition for Application Gateway."
}

output "azure_ddos_protection_noaccess" {
  value       = "${azurerm_role_definition.azure_ddos_protection_noaccess_definition}"
  description = "NoAccess Definition for Azure DDoS Protection."
}

output "azure_ddos_protection_readonly" {
  value       = "${azurerm_role_definition.azure_ddos_protection_readonly_definition}"
  description = "ReadOnly Definition for Azure DDoS Protection."
}

output "azure_ddos_protection_fullaccess" {
  value       = "${azurerm_role_definition.azure_ddos_protection_fullaccess_definition}"
  description = "FullAccess Definition for Azure DDoS Protection."
}

output "network_watcher_noaccess" {
  value       = "${azurerm_role_definition.network_watcher_noaccess_definition}"
  description = "NoAccess Definition for Network Watcher."
}

output "network_watcher_readonly" {
  value       = "${azurerm_role_definition.network_watcher_readonly_definition}"
  description = "ReadOnly Definition for Network Watcher."
}

output "network_watcher_fullaccess" {
  value       = "${azurerm_role_definition.network_watcher_fullaccess_definition}"
  description = "FullAccess Definition for Network Watcher."
}

output "azure_firewall_noaccess" {
  value       = "${azurerm_role_definition.azure_firewall_noaccess_definition}"
  description = "NoAccess Definition for Azure Firewall."
}

output "azure_firewall_readonly" {
  value       = "${azurerm_role_definition.azure_firewall_readonly_definition}"
  description = "ReadOnly Definition for Azure Firewall."
}

output "azure_firewall_fullaccess" {
  value       = "${azurerm_role_definition.azure_firewall_fullaccess_definition}"
  description = "FullAccess Definition for Azure Firewall."
}

output "virtual_wan_noaccess" {
  value       = "${azurerm_role_definition.virtual_wan_noaccess_definition}"
  description = "NoAccess Definition for Virtual WAN."
}

output "virtual_wan_readonly" {
  value       = "${azurerm_role_definition.virtual_wan_readonly_definition}"
  description = "ReadOnly Definition for Virtual WAN."
}

output "virtual_wan_fullaccess" {
  value       = "${azurerm_role_definition.virtual_wan_fullaccess_definition}"
  description = "FullAccess Definition for Virtual WAN."
}

output "azure_front_door_service_noaccess" {
  value       = "${azurerm_role_definition.azure_front_door_service_noaccess_definition}"
  description = "NoAccess Definition for Azure Front Door Service."
}

output "azure_front_door_service_readonly" {
  value       = "${azurerm_role_definition.azure_front_door_service_readonly_definition}"
  description = "ReadOnly Definition for Azure Front Door Service."
}

output "azure_front_door_service_fullaccess" {
  value       = "${azurerm_role_definition.azure_front_door_service_fullaccess_definition}"
  description = "FullAccess Definition for Azure Front Door Service."
}

output "azure_bastion_noaccess" {
  value       = "${azurerm_role_definition.azure_bastion_noaccess_definition}"
  description = "NoAccess Definition for Azure Bastion."
}

output "azure_bastion_readonly" {
  value       = "${azurerm_role_definition.azure_bastion_readonly_definition}"
  description = "ReadOnly Definition for Azure Bastion."
}

output "azure_bastion_fullaccess" {
  value       = "${azurerm_role_definition.azure_bastion_fullaccess_definition}"
  description = "FullAccess Definition for Azure Bastion."
}

output "key_vault_noaccess" {
  value       = "${azurerm_role_definition.key_vault_noaccess_definition}"
  description = "NoAccess Definition for Key Vault."
}

output "key_vault_readonly" {
  value       = "${azurerm_role_definition.key_vault_readonly_definition}"
  description = "ReadOnly Definition for Key Vault."
}

output "key_vault_fullaccess" {
  value       = "${azurerm_role_definition.key_vault_fullaccess_definition}"
  description = "FullAccess Definition for Key Vault."
}

output "security_center_noaccess" {
  value       = "${azurerm_role_definition.security_center_noaccess_definition}"
  description = "NoAccess Definition for Security Center."
}

output "security_center_readonly" {
  value       = "${azurerm_role_definition.security_center_readonly_definition}"
  description = "ReadOnly Definition for Security Center."
}

output "security_center_fullaccess" {
  value       = "${azurerm_role_definition.security_center_fullaccess_definition}"
  description = "FullAccess Definition for Security Center."
}

output "azure_dedicated_hsm_noaccess" {
  value       = "${azurerm_role_definition.azure_dedicated_hsm_noaccess_definition}"
  description = "NoAccess Definition for Azure Dedicated HSM."
}

output "azure_dedicated_hsm_readonly" {
  value       = "${azurerm_role_definition.azure_dedicated_hsm_readonly_definition}"
  description = "ReadOnly Definition for Azure Dedicated HSM."
}

output "azure_dedicated_hsm_fullaccess" {
  value       = "${azurerm_role_definition.azure_dedicated_hsm_fullaccess_definition}"
  description = "FullAccess Definition for Azure Dedicated HSM."
}

output "storage_accounts_noaccess" {
  value       = "${azurerm_role_definition.storage_accounts_noaccess_definition}"
  description = "NoAccess Definition for Storage Accounts."
}

output "storage_accounts_readonly" {
  value       = "${azurerm_role_definition.storage_accounts_readonly_definition}"
  description = "ReadOnly Definition for Storage Accounts."
}

output "storage_accounts_fullaccess" {
  value       = "${azurerm_role_definition.storage_accounts_fullaccess_definition}"
  description = "FullAccess Definition for Storage Accounts."
}

output "storsimple_noaccess" {
  value       = "${azurerm_role_definition.storsimple_noaccess_definition}"
  description = "NoAccess Definition for StorSimple."
}

output "storsimple_readonly" {
  value       = "${azurerm_role_definition.storsimple_readonly_definition}"
  description = "ReadOnly Definition for StorSimple."
}

output "storsimple_fullaccess" {
  value       = "${azurerm_role_definition.storsimple_fullaccess_definition}"
  description = "FullAccess Definition for StorSimple."
}

output "azure_data_lake_storage_noaccess" {
  value       = "${azurerm_role_definition.azure_data_lake_storage_noaccess_definition}"
  description = "NoAccess Definition for Azure Data Lake Storage."
}

output "azure_data_lake_storage_readonly" {
  value       = "${azurerm_role_definition.azure_data_lake_storage_readonly_definition}"
  description = "ReadOnly Definition for Azure Data Lake Storage."
}

output "azure_data_lake_storage_fullaccess" {
  value       = "${azurerm_role_definition.azure_data_lake_storage_fullaccess_definition}"
  description = "FullAccess Definition for Azure Data Lake Storage."
}

output "file_storage_noaccess" {
  value       = "${azurerm_role_definition.file_storage_noaccess_definition}"
  description = "NoAccess Definition for File storage."
}

output "file_storage_readonly" {
  value       = "${azurerm_role_definition.file_storage_readonly_definition}"
  description = "ReadOnly Definition for File storage."
}

output "file_storage_fullaccess" {
  value       = "${azurerm_role_definition.file_storage_fullaccess_definition}"
  description = "FullAccess Definition for File storage."
}

output "data_box_noaccess" {
  value       = "${azurerm_role_definition.data_box_noaccess_definition}"
  description = "NoAccess Definition for Data Box."
}

output "data_box_readonly" {
  value       = "${azurerm_role_definition.data_box_readonly_definition}"
  description = "ReadOnly Definition for Data Box."
}

output "data_box_fullaccess" {
  value       = "${azurerm_role_definition.data_box_fullaccess_definition}"
  description = "FullAccess Definition for Data Box."
}

output "avere_vfxt_for_azure_noaccess" {
  value       = "${azurerm_role_definition.avere_vfxt_for_azure_noaccess_definition}"
  description = "NoAccess Definition for Avere vFXT for Azure."
}

output "avere_vfxt_for_azure_readonly" {
  value       = "${azurerm_role_definition.avere_vfxt_for_azure_readonly_definition}"
  description = "ReadOnly Definition for Avere vFXT for Azure."
}

output "avere_vfxt_for_azure_fullaccess" {
  value       = "${azurerm_role_definition.avere_vfxt_for_azure_fullaccess_definition}"
  description = "FullAccess Definition for Avere vFXT for Azure."
}

output "azure_netapp_files_noaccess" {
  value       = "${azurerm_role_definition.azure_netapp_files_noaccess_definition}"
  description = "NoAccess Definition for Azure NetApp Files."
}

output "azure_netapp_files_readonly" {
  value       = "${azurerm_role_definition.azure_netapp_files_readonly_definition}"
  description = "ReadOnly Definition for Azure NetApp Files."
}

output "azure_netapp_files_fullaccess" {
  value       = "${azurerm_role_definition.azure_netapp_files_fullaccess_definition}"
  description = "FullAccess Definition for Azure NetApp Files."
}

output "app_service_noaccess" {
  value       = "${azurerm_role_definition.app_service_noaccess_definition}"
  description = "NoAccess Definition for App Service."
}

output "app_service_readonly" {
  value       = "${azurerm_role_definition.app_service_readonly_definition}"
  description = "ReadOnly Definition for App Service."
}

output "app_service_fullaccess" {
  value       = "${azurerm_role_definition.app_service_fullaccess_definition}"
  description = "FullAccess Definition for App Service."
}

output "azure_signalr_service_noaccess" {
  value       = "${azurerm_role_definition.azure_signalr_service_noaccess_definition}"
  description = "NoAccess Definition for Azure SignalR Service."
}

output "azure_signalr_service_readonly" {
  value       = "${azurerm_role_definition.azure_signalr_service_readonly_definition}"
  description = "ReadOnly Definition for Azure SignalR Service."
}

output "azure_signalr_service_fullaccess" {
  value       = "${azurerm_role_definition.azure_signalr_service_fullaccess_definition}"
  description = "FullAccess Definition for Azure SignalR Service."
}

output "enterprise_graph_noaccess" {
  value       = "${azurerm_role_definition.enterprise_graph_noaccess_definition}"
  description = "NoAccess Definition for Enterprise Graph."
}

output "enterprise_graph_readonly" {
  value       = "${azurerm_role_definition.enterprise_graph_readonly_definition}"
  description = "ReadOnly Definition for Enterprise Graph."
}

output "enterprise_graph_fullaccess" {
  value       = "${azurerm_role_definition.enterprise_graph_fullaccess_definition}"
  description = "FullAccess Definition for Enterprise Graph."
}

output "customer_insights_noaccess" {
  value       = "${azurerm_role_definition.customer_insights_noaccess_definition}"
  description = "NoAccess Definition for Customer Insights."
}

output "customer_insights_readonly" {
  value       = "${azurerm_role_definition.customer_insights_readonly_definition}"
  description = "ReadOnly Definition for Customer Insights."
}

output "customer_insights_fullaccess" {
  value       = "${azurerm_role_definition.customer_insights_fullaccess_definition}"
  description = "FullAccess Definition for Customer Insights."
}

output "azure_import_export_noaccess" {
  value       = "${azurerm_role_definition.azure_import_export_noaccess_definition}"
  description = "NoAccess Definition for Azure Import/Export."
}

output "azure_import_export_readonly" {
  value       = "${azurerm_role_definition.azure_import_export_readonly_definition}"
  description = "ReadOnly Definition for Azure Import/Export."
}

output "azure_import_export_fullaccess" {
  value       = "${azurerm_role_definition.azure_import_export_fullaccess_definition}"
  description = "FullAccess Definition for Azure Import/Export."
}

output "azure_powerbi_noaccess" {
  value       = "${azurerm_role_definition.azure_powerbi_noaccess_definition}"
  description = "NoAccess Definition for Azure PowerBI."
}

output "azure_powerbi_readonly" {
  value       = "${azurerm_role_definition.azure_powerbi_readonly_definition}"
  description = "ReadOnly Definition for Azure PowerBI."
}

output "azure_powerbi_fullaccess" {
  value       = "${azurerm_role_definition.azure_powerbi_fullaccess_definition}"
  description = "FullAccess Definition for Azure PowerBI."
}

output "azure_relay_noaccess" {
  value       = "${azurerm_role_definition.azure_relay_noaccess_definition}"
  description = "NoAccess Definition for Azure Relay."
}

output "azure_relay_readonly" {
  value       = "${azurerm_role_definition.azure_relay_readonly_definition}"
  description = "ReadOnly Definition for Azure Relay."
}

output "azure_relay_fullaccess" {
  value       = "${azurerm_role_definition.azure_relay_fullaccess_definition}"
  description = "FullAccess Definition for Azure Relay."
}

output "graph_security_api_noaccess" {
  value       = "${azurerm_role_definition.graph_security_api_noaccess_definition}"
  description = "NoAccess Definition for Graph Security API."
}

output "graph_security_api_readonly" {
  value       = "${azurerm_role_definition.graph_security_api_readonly_definition}"
  description = "ReadOnly Definition for Graph Security API."
}

output "graph_security_api_fullaccess" {
  value       = "${azurerm_role_definition.graph_security_api_fullaccess_definition}"
  description = "FullAccess Definition for Graph Security API."
}

output "intune_noaccess" {
  value       = "${azurerm_role_definition.intune_noaccess_definition}"
  description = "NoAccess Definition for InTune."
}

output "intune_readonly" {
  value       = "${azurerm_role_definition.intune_readonly_definition}"
  description = "ReadOnly Definition for InTune."
}

output "intune_fullaccess" {
  value       = "${azurerm_role_definition.intune_fullaccess_definition}"
  description = "FullAccess Definition for InTune."
}

output "customer_lockbox_noaccess" {
  value       = "${azurerm_role_definition.customer_lockbox_noaccess_definition}"
  description = "NoAccess Definition for Customer LockBox."
}

output "customer_lockbox_readonly" {
  value       = "${azurerm_role_definition.customer_lockbox_readonly_definition}"
  description = "ReadOnly Definition for Customer LockBox."
}

output "customer_lockbox_fullaccess" {
  value       = "${azurerm_role_definition.customer_lockbox_fullaccess_definition}"
  description = "FullAccess Definition for Customer LockBox."
}

output "azure_api_for_fhir_noaccess" {
  value       = "${azurerm_role_definition.azure_api_for_fhir_noaccess_definition}"
  description = "NoAccess Definition for Azure API for FHIR."
}

output "azure_api_for_fhir_readonly" {
  value       = "${azurerm_role_definition.azure_api_for_fhir_readonly_definition}"
  description = "ReadOnly Definition for Azure API for FHIR."
}

output "azure_api_for_fhir_fullaccess" {
  value       = "${azurerm_role_definition.azure_api_for_fhir_fullaccess_definition}"
  description = "FullAccess Definition for Azure API for FHIR."
}

output "azure_custom_providers_noaccess" {
  value       = "${azurerm_role_definition.azure_custom_providers_noaccess_definition}"
  description = "NoAccess Definition for Azure Custom Providers."
}

output "azure_custom_providers_readonly" {
  value       = "${azurerm_role_definition.azure_custom_providers_readonly_definition}"
  description = "ReadOnly Definition for Azure Custom Providers."
}

output "azure_custom_providers_fullaccess" {
  value       = "${azurerm_role_definition.azure_custom_providers_fullaccess_definition}"
  description = "FullAccess Definition for Azure Custom Providers."
}
