# tf-azure-service-access-definitions
This is a terraform module for controlling access to services in Microsoft Azure.

## Dependencies
- Use: Terraform `>=0.11.11`.
- Build: Git, Terraform `>=0.11.11`, Windows Powershell, [ImportExcel](https://github.com/dfinke/ImportExcel) PowerShell Module.

## Usage
There are three role definitions created for each service as part of this module; `FullAccess`, `ReadOnly` and `NoAccess`.

This gives the flexibility to govern service access as a whole (by automatically assigning all new users to the `NoAccess` group), yet grant service access to a specific user/group/service principal via a change of assignment. **It is reccomended as part of your user creation pipeline to automatically assign all new users to the `NoAccess` groups.**

In the following example a subscription id is used as the top level scope. This is the only parameter for this module.

```hcl
module "azure_service_access_definitions" {
    source          = "git::https://gitlab.com/claranet-pcp/terraform/azure/tf-azure-service-access-definitions"
    subscription_id = "fd04759d-d5e8-4960-8bbe-f3b9b15a880a"
}
```

## Development
All the Azure services and permissions schemas are stored in the Excel file at `\source\azure-services-and-roles.xlsx`. These link to one another via the `ServiceId` column, and are used in the automatic build of this module.

To run a build, update the Excel file with the new service(s) and permission(s), then run `.\build\build.ps1` to automatically rebuild the code, docs, terraform fmt and git commit/push. You can use the `-CommitMessage` parameter against the build script to supply your own commit message instead of the default one.

To update either the terraform or readme boilerplates; be sure to edit these in the `build` folder as these are where the build gets the content from. Failure to do this will result in your changes being hosed the next time a build is run.

## Services
This is the catalog of Microsoft Azure services for which groups will be created by this module. Child services are services which use the same permissions set as the parent for access.

| Category | Service | Description | Child Services | Link |
|----------|---------|-------------|----------------|------|
| AI + Machine Learning | Azure Bot Service | A managed service purpose-built for bot development. |  | https://azure.microsoft.com/en-gb/services/bot-service/ |
| AI + Machine Learning | Azure Databricks | Fast, easy and collaborative Apache Spark-based analytics service. |  | https://azure.microsoft.com/en-gb/services/databricks/ |
| AI + Machine Learning | Azure Search | AI-powered cloud search service for mobile and web app development. |  | https://azure.microsoft.com/en-gb/services/search/ |
| AI + Machine Learning | Cognitive Services | Infuse your apps, websites and bots with intelligent algorithms to see, hear, speak, understand and interpret your user needs through natural methods of communication. | `Bing Autosuggest`, `Bing Custom Search`, `Bing Entity Search`, `Bing Image Search`, `Bing News Search`, `Bing Spell Check`, `Bing Video Search`, `Bing Visual Search`, `Bing Web Search`, `Custom Speech`, `Custom Vision`, `Translator Speech`, `Linguistic Analysis`, `Form Recogniser`, `Ink Recogniser`, `Personaliser`, `QnA Maker`, `Speaker Recognition`, `Speech translation`, `Speech-to-Text`, `Text to Speech`, `Translator Text`, `Anomaly detector`, `Azure Open Datasets`, `Immersive Reader` | https://azure.microsoft.com/en-gb/services/cognitive-services/ |
| AI + Machine Learning | Computer Vision | Extract rich information from images to categorise and process visual data and perform machine-assisted moderation of images to help curate your services. |  | https://azure.microsoft.com/en-gb/services/cognitive-services/computer-vision/ |
| AI + Machine Learning | Content moderator | Machine-assisted content moderation APIs and human review tool for images, text and videos. |  | https://azure.microsoft.com/en-gb/services/cognitive-services/content-moderator/ |
| AI + Machine Learning | Face | Detect and compare human faces, organise images into groups based on similarities and identify previously tagged people in images. |  | https://azure.microsoft.com/en-gb/services/cognitive-services/face/ |
| AI + Machine Learning | Azure Machine Learning service | Build models rapidly and operationalise at scale from cloud to edge. |  | https://azure.microsoft.com/en-gb/services/machine-learning-service/ |
| AI + Machine Learning | Machine Learning Studio | Machine Learning Studio is a powerfully simple browser-based, visual drag-and-drop authoring environment where no coding is necessary. |  | https://azure.microsoft.com/en-gb/services/machine-learning-studio/ |
| AI + Machine Learning | Language Understanding | A machine learning-based service for building natural language understanding into apps, bots and IoT devices. Quickly create enterprise-ready, custom models that continuously improve. |  | https://azure.microsoft.com/en-gb/services/cognitive-services/language-understanding-intelligent-service/ |
| AI + Machine Learning | Text Analytics | Detect sentiment, key phrases, named entities and language from your text. |  | https://azure.microsoft.com/en-gb/services/cognitive-services/text-analytics/ |
| Analytics | Azure Stream Analytics | Easily develop and run massively parallel real-time analytics on multiple IoT or non-IoT streams of data using simple SQL-like language. |  | https://azure.microsoft.com/en-gb/services/stream-analytics/ |
| Analytics | HDInsight | Easy, cost-effective, enterprise-grade service for open source analytics. |  | https://azure.microsoft.com/en-gb/services/hdinsight/ |
| Analytics | Data Factory | Hybrid data integration service that simplifies ETL at scale. |  | https://azure.microsoft.com/en-gb/services/data-factory/ |
| Analytics | Data Lake Analytics | An on-demand analytics job service to power intelligent action. |  | https://azure.microsoft.com/en-gb/services/data-lake-analytics/ |
| Analytics | Event Hubs | Simple, secure and scalable real-time data ingestion. |  | https://azure.microsoft.com/en-gb/services/event-hubs/ |
| Analytics | Azure Analysis Services | Proven analytics engine. | `Power BI Embedded`, `R Server for HDInsight` | https://azure.microsoft.com/en-gb/services/analysis-services/ |
| Analytics | Data Catalog | Get more value from your enterprise data assets. |  | https://azure.microsoft.com/en-gb/services/data-catalog/ |
| Analytics | Azure Data Explorer | Fast and highly scalable data analytics service. |  | https://azure.microsoft.com/en-gb/services/data-explorer/ |
| Blockchain | Azure Blockchain Service | Build, govern and expand consortium blockchain networks. | `Azure Blockchain Workbench` | https://azure.microsoft.com/en-gb/services/blockchain-service/ |
| Compute | Virtual Machines | Create Linux and Windows virtual machines in seconds. | `Data Science Virtual Machines`, `Linux Virtual Machines`, `Windows Virtual Desktop`, `Azure CycleCloud`, `SQL Server on virtual machines` | https://azure.microsoft.com/en-gb/services/virtual-machines/ |
| Compute | Batch | Cloud-scale job scheduling and compute management. |  | https://azure.microsoft.com/en-gb/services/batch/ |
| Compute | SAP HANA on Azure Large Instances | Run SAP solutions across development, testing and production scenarios in Azure. |  | https://azure.microsoft.com/en-gb/services/virtual-machines/sap-hana/ |
| Compute | Virtual Machine Scale Sets | Apply auto-scaling to virtual machines for high availability. |  | https://azure.microsoft.com/en-gb/services/virtual-machine-scale-sets/ |
| Compute | Azure VMware Solution by CloudSimple | Run your VMware workloads natively on Azure. |  | https://azure.microsoft.com/en-gb/services/azure-vmware-cloudsimple/ |
| Containers | Azure Kubernetes Service (AKS) | Highly available, secure and fully managed Kubernetes service. |  | https://azure.microsoft.com/en-gb/services/kubernetes-service/ |
| Containers | Service Fabric | Build and operate always-on, scalable, distributed apps. |  | https://azure.microsoft.com/en-gb/services/service-fabric/ |
| Containers | Container Instances | Easily run containers on Azure without managing servers. | `Azure functions` | https://azure.microsoft.com/en-gb/services/container-instances/ |
| Containers | Container Registry | Manage a Docker private registry as a first-class Azure resource. |  | https://azure.microsoft.com/en-gb/services/container-registry/ |
| Containers | Azure Red Hat OpenShift | Fully managed OpenShift service, jointly operated with Red Hat. |  | https://azure.microsoft.com/en-gb/services/openshift/ |
| Databases | Azure SQL Database | Managed, intelligent SQL in the cloud. | `SQL Data Warehouse`, `SQL Server Stretch Database`, `Table storage`, `Azure SQL Database Edge` | https://azure.microsoft.com/en-gb/services/sql-database/ |
| Databases | Azure Cosmos DB | Globally distributed, multi-model database service for any scale. |  | https://azure.microsoft.com/en-gb/services/cosmos-db/ |
| Databases | Azure Cache for Redis | Fully managed, open-source-compatible in-memory data store to power fast, scalable applications. |  | https://azure.microsoft.com/en-gb/services/cache/ |
| Databases | Azure Database for PostgreSQL | Enterprise-ready, fully managed community PostgreSQL. |  | https://azure.microsoft.com/en-gb/services/postgresql/ |
| Databases | Azure Database for MariaDB | Enterprise-ready, fully managed community MariaDB. |  | https://azure.microsoft.com/en-gb/services/mariadb/ |
| Databases | Azure Database for MySQL | Enterprise-ready, fully managed community MySQL. |  | https://azure.microsoft.com/en-gb/services/mysql/ |
| Developer Tools | Azure Lab Services | Computer labs in the cloud. |  | https://azure.microsoft.com/en-gb/services/lab-services/ |
| DevOps | Azure DevOps | Plan smarter, collaborate better and ship faster with a set of modern dev services. | `App Configuration`, `Azure Pipelines`, `Azure Boards`, `Azure Repos`, `Azure Artifacts`, `Azure Test Plans`, `DevOps tool integrations` | https://azure.microsoft.com/en-gb/services/devops/ |
| DevOps | Azure DevTest Labs | Fast, easy and lean dev-test environments. |  | https://azure.microsoft.com/en-gb/services/devtest-lab/ |
| Identity | Azure Active Directory | Get the reliability and scalability you need with identity services that work with your on-premises, cloud or hybrid environment. | `Azure Active Directory B2C` | https://azure.microsoft.com/en-gb/services/active-directory/ |
| Identity | Azure Active Directory Domain Services | Your domain controller as a service. |  | https://azure.microsoft.com/en-gb/services/active-directory-ds/ |
| Integration | Event Grid | Get reliable event delivery at massive scale. |  | https://azure.microsoft.com/en-gb/services/event-grid/ |
| Integration | Logic Apps | Quickly build powerful integration solutions. |  | https://azure.microsoft.com/en-gb/services/logic-apps/ |
| Integration | API Management | Publish, manage, secure and analyse your APIs in minutes. |  | https://azure.microsoft.com/en-gb/services/api-management/ |
| Integration | Service Bus | Reliable cloud messaging as a service (MaaS) and simple hybrid integration. |  | https://azure.microsoft.com/en-gb/services/service-bus/ |
| Internet of Things | Azure IoT Hub | Connect, monitor and manage billions of IoT assets. | `Azure IoT Edge`, `Azure IoT solution accelerators`, `Azure Sphere` | https://azure.microsoft.com/en-gb/services/iot-hub/ |
| Internet of Things | Azure IoT Central | Experience the simplicity of SaaS for IoT (Internet of Things), with no cloud expertise required. |  | https://azure.microsoft.com/en-gb/services/iot-central/ |
| Internet of Things | Azure Time Series Insights | Drive operational intelligence and transform your business with actionable, real-time insights about IoT-scale time-series data. |  | https://azure.microsoft.com/en-gb/services/time-series-insights/ |
| Internet of Things | Azure Maps | Geospatial APIs to add maps, spatial analytics and mobility solutions to your apps. |  | https://azure.microsoft.com/en-gb/services/azure-maps/ |
| Internet of Things | Windows 10 IoT Core Services | Long-term OS support and services to manage device updates and assess device health. |  | https://azure.microsoft.com/en-gb/services/windows-10-iot-core/ |
| Internet of Things | Notification Hubs | Send push notifications to any platform from any back-end. |  | https://azure.microsoft.com/en-gb/services/notification-hubs/ |
| Management and Governance | Azure Advisor | Your free, personalised guide to Azure best practices. |  | https://azure.microsoft.com/en-gb/services/advisor/ |
| Management and Governance | Scheduler | Run your jobs on simple or complex recurring schedules. |  | https://azure.microsoft.com/en-gb/services/scheduler/ |
| Management and Governance | Automation | Automate, configure and install updates across hybrid environments. |  | https://azure.microsoft.com/en-gb/services/automation/ |
| Management and Governance | Azure Monitor | Full observability into your applications, infrastructure and network. |  | https://azure.microsoft.com/en-gb/services/monitor/ |
| Management and Governance | Azure Service Health | Personalised alerts and guidance for Azure service issues. |  | https://azure.microsoft.com/en-gb/features/service-health/ |
| Management and Governance | Microsoft Azure portal | Build, manage and monitor everything from simple web apps to complex cloud applications in a single, unified console. | `Cloud Shell` | https://azure.microsoft.com/en-gb/features/azure-portal/ |
| Management and Governance | Azure Resource Manager | Simplify how you manage your app resources. |  | https://azure.microsoft.com/en-gb/features/resource-manager/ |
| Management and Governance | Azure Policy | Active control and governance at scale for your Azure resources. |  | https://azure.microsoft.com/en-gb/services/azure-policy/ |
| Management and Governance | Cost management | Optimise cloud costs while maximising cloud potential. |  | https://azure.microsoft.com/en-gb/services/cost-management/ |
| Management and Governance | Azure Managed Applications | Azure Managed Applications enable managed service providers (MSPs), independent software vendors (ISVs) and enterprise IT teams to deliver turnkey solutions through the Azure Marketplace or service catalogue. |  | https://azure.microsoft.com/en-gb/services/managed-applications/ |
| Management and Governance | Azure Blueprints | Enabling quick, repeatable creation of fully governed environments. |  | https://azure.microsoft.com/en-gb/services/blueprints/ |
| Media | Content Delivery Network | Secure and reliable global content delivery and acceleration. |  | https://azure.microsoft.com/en-gb/services/cdn/ |
| Media | Media Services | Azure Media Services makes it easy to use high-definition video encoding and streaming services to reach your audiences on today�s most popular devices. | `Encoding`, `Live and On-Demand Streaming`, `Azure Media Player`, `Content Protection`, `Media Analytics`, `Video Indexer` | https://azure.microsoft.com/en-gb/services/media-services/ |
| Microsoft Azure Stack | Azure Stack | An extension of Azure to consistently build and run hybrid applications across cloud boundaries. |  | https://azure.microsoft.com/en-gb/overview/azure-stack/ |
| Migration | Azure Site Recovery | Azure�s built-in disaster recovery as a service (DRaaS). | `Azure Backup` | https://azure.microsoft.com/en-gb/services/site-recovery/ |
| Migration | Azure Database Migration Service | Accelerate your transition to the cloud. |  | https://azure.microsoft.com/en-gb/services/database-migration/ |
| Migration | Azure Migrate | Easily discover, assess and migrate your on-premises virtual machines to Azure. |  | https://azure.microsoft.com/en-gb/services/azure-migrate/ |
| Mixed Reality | Spatial anchors | Create multi-user, spatially aware mixed reality experiences. | `Azure Digital Twins`, `Kinect DK`, `Remote rendering` | https://azure.microsoft.com/en-gb/services/spatial-anchors/ |
| Mobile | Visual Studio App Centre | Continuous Everything � Build, Test, Deploy, Engage, Repeat. | `Azure mobile app`, `Xamarin` | https://azure.microsoft.com/en-gb/services/app-center/ |
| Networking | ExpressRoute | Experience a faster private connection to Azure. |  | https://azure.microsoft.com/en-gb/services/expressroute/ |
| Networking | Azure DNS | Host your domain in Azure for outstanding performance and availability. |  | https://azure.microsoft.com/en-gb/services/dns/ |
| Networking | Virtual Network | Your private network in the cloud. |  | https://azure.microsoft.com/en-gb/services/virtual-network/ |
| Networking | Traffic Manager | Route incoming traffic for high performance and availability. |  | https://azure.microsoft.com/en-gb/services/traffic-manager/ |
| Networking | Load Balancer | Deliver high availability and network performance to your applications. |  | https://azure.microsoft.com/en-gb/services/load-balancer/ |
| Networking | VPN Gateway | Connecting your infrastructure to the cloud. |  | https://azure.microsoft.com/en-gb/services/vpn-gateway/ |
| Networking | Application Gateway | Platform-managed, scalable and highly available application delivery controller as a service. |  | https://azure.microsoft.com/en-gb/services/application-gateway/ |
| Networking | Azure DDoS Protection | Protect your Azure resources from Distributed Denial of Service (DDoS) attacks. |  | https://azure.microsoft.com/en-gb/services/ddos-protection/ |
| Networking | Network Watcher | Monitor, diagnose and gain insights to your network performance and health. |  | https://azure.microsoft.com/en-gb/services/network-watcher/ |
| Networking | Azure Firewall | Cloud-native network security to protect your Azure Virtual Network resources. |  | https://azure.microsoft.com/en-gb/services/azure-firewall/ |
| Networking | Virtual WAN | Simple, unified, global connectivity and security. |  | https://azure.microsoft.com/en-gb/services/virtual-wan/ |
| Networking | Azure Front Door Service | Scalable and secure entry point for fast delivery of your global applications. |  | https://azure.microsoft.com/en-gb/services/frontdoor/ |
| Networking | Azure Bastion | Private and fully managed RDP and SSH access to your virtual machines. |  | https://azure.microsoft.com/en-gb/services/azure-bastion/ |
| Security | Key Vault | Safeguard cryptographic keys and other secrets used by cloud apps and services. |  | https://azure.microsoft.com/en-gb/services/key-vault/ |
| Security | Security Center | Gain unmatched hybrid security management and threat protection. | `Azure Information Protection`, `Azure Sentinel` | https://azure.microsoft.com/en-gb/services/security-center/ |
| Security | Azure Dedicated HSM | Your hardware security module (HSM) in the cloud. |  | https://azure.microsoft.com/en-gb/services/azure-dedicated-hsm/ |
| Storage | Storage Accounts | Offload the heavy lifting of data centre management. | `Blob Storage`, `Disk storage`, `Managed Disks`, `Queue Storage`, `Archive Storage`, `Storage Explorer` | https://azure.microsoft.com/en-gb/services/storage/ |
| Storage | StorSimple | An enterprise hybrid cloud storage solution that lowers costs by up to 60%. |  | https://azure.microsoft.com/en-gb/services/storsimple/ |
| Storage | Azure Data Lake Storage | Massively scalable, secure data lake functionality built on Azure Blob Storage. |  | https://azure.microsoft.com/en-gb/services/storage/data-lake-storage/ |
| Storage | File storage | Simple, secure and fully managed cloud file shares. |  | https://azure.microsoft.com/en-gb/services/storage/files/ |
| Storage | Data Box | Move stored or in-flight data to Azure quickly and cost-effectively. |  | https://azure.microsoft.com/en-gb/services/databox/ |
| Storage | Avere vFXT for Azure | Faster, more accessible data storage for high-performance computing at the edge. | `Azure FXT Edge Filer` | https://azure.microsoft.com/en-gb/services/storage/avere-vfxt/ |
| Storage | Azure NetApp Files | Enterprise-grade Azure file shares, powered by NetApp. |  | https://azure.microsoft.com/en-gb/services/netapp/ |
| Web | App Service | Build, deploy and scale web apps on a fully managed platform. | `Web Apps`, `Mobile apps`, `API apps`, `Web App for Containers` | https://azure.microsoft.com/en-gb/services/app-service/ |
| Web | Azure SignalR Service | Easily add real-time web functionality to applications. |  | https://azure.microsoft.com/en-gb/services/signalr-service/ |
| Databases | Enterprise Graph | Enterprise Graph by Microsoft enables you to build knowledge graphs that provide deeper insight into your business data. |  | https://docs.microsoft.com/en-us/enterprise-graph/enterprise-graph-overview |
| Analytics | Customer Insights | Customer Insights enables organizations of all sizes to bring together diverse data sets and generate knowledge & insights to build a holistic 360� view of their customers. |  | https://azuremarketplace.microsoft.com/en-us/marketplace/apps/Microsoft.CustomerInsights?tab=Overview |
| Migration | Azure Import/Export | Fast, highly secured, large data migration to the cloud. |  | https://azure.microsoft.com/en-gb/services/storage/import-export/ |
| Analytics | Azure PowerBI | With Azure services and Power BI, you can turn your data processing efforts into analytics and reports that provide real-time insights into your business. |  | https://docs.microsoft.com/en-us/power-bi/service-azure-and-power-bi |
| Integration | Azure Relay | The Azure Relay service enables you to securely expose services that run in your corporate network to the public cloud. |  | https://docs.microsoft.com/en-us/azure/service-bus-relay/relay-what-is-it |
| Security | Graph Security API | Unify integration across multiple security solutions to reduce development time and effort. |  | https://www.microsoft.com/en-us/security/partnerships/graph-security-api |
| Security | InTune | Microsoft Intune is a cloud-based service in the enterprise mobility management (EMM) space that helps enable your workforce to be productive while keeping your corporate data protected. |  | https://docs.microsoft.com/en-us/intune/what-is-intune |
| Security | Customer LockBox | Customer Lockbox for Microsoft Azure provides an interface for customers to review and approve or reject customer data access requests. |  | https://docs.microsoft.com/en-us/azure/security/azure-customer-lockbox-overview |
| Medical | Azure API for FHIR | Easily create and deploy a FHIR service for health data solutions. |  | https://azure.microsoft.com/en-in/services/azure-api-for-fhir/ |
| Integration | Azure Custom Providers | With Azure Custom Providers, you can extend Azure to work with your service. |  | https://docs.microsoft.com/en-us/azure/managed-applications/custom-providers-overview |

## Outputs
| Category | Service | Property |
|----------|---------|----------|
| AI + Machine Learning | Azure Bot Service | `azure_bot_service_noaccess` |
| AI + Machine Learning | Azure Bot Service | `azure_bot_service_readonly` |
| AI + Machine Learning | Azure Bot Service | `azure_bot_service_fullaccess` |
| AI + Machine Learning | Azure Databricks | `azure_databricks_noaccess` |
| AI + Machine Learning | Azure Databricks | `azure_databricks_readonly` |
| AI + Machine Learning | Azure Databricks | `azure_databricks_fullaccess` |
| AI + Machine Learning | Azure Search | `azure_search_noaccess` |
| AI + Machine Learning | Azure Search | `azure_search_readonly` |
| AI + Machine Learning | Azure Search | `azure_search_fullaccess` |
| AI + Machine Learning | Cognitive Services | `cognitive_services_noaccess` |
| AI + Machine Learning | Cognitive Services | `cognitive_services_readonly` |
| AI + Machine Learning | Cognitive Services | `cognitive_services_fullaccess` |
| AI + Machine Learning | Computer Vision | `computer_vision_noaccess` |
| AI + Machine Learning | Computer Vision | `computer_vision_readonly` |
| AI + Machine Learning | Computer Vision | `computer_vision_fullaccess` |
| AI + Machine Learning | Content moderator | `content_moderator_noaccess` |
| AI + Machine Learning | Content moderator | `content_moderator_readonly` |
| AI + Machine Learning | Content moderator | `content_moderator_fullaccess` |
| AI + Machine Learning | Face | `face_noaccess` |
| AI + Machine Learning | Face | `face_readonly` |
| AI + Machine Learning | Face | `face_fullaccess` |
| AI + Machine Learning | Azure Machine Learning service | `azure_machine_learning_service_noaccess` |
| AI + Machine Learning | Azure Machine Learning service | `azure_machine_learning_service_readonly` |
| AI + Machine Learning | Azure Machine Learning service | `azure_machine_learning_service_fullaccess` |
| AI + Machine Learning | Machine Learning Studio | `machine_learning_studio_noaccess` |
| AI + Machine Learning | Machine Learning Studio | `machine_learning_studio_readonly` |
| AI + Machine Learning | Machine Learning Studio | `machine_learning_studio_fullaccess` |
| AI + Machine Learning | Language Understanding | `language_understanding_noaccess` |
| AI + Machine Learning | Language Understanding | `language_understanding_readonly` |
| AI + Machine Learning | Language Understanding | `language_understanding_fullaccess` |
| AI + Machine Learning | Text Analytics | `text_analytics_noaccess` |
| AI + Machine Learning | Text Analytics | `text_analytics_readonly` |
| AI + Machine Learning | Text Analytics | `text_analytics_fullaccess` |
| Analytics | Azure Stream Analytics | `azure_stream_analytics_noaccess` |
| Analytics | Azure Stream Analytics | `azure_stream_analytics_readonly` |
| Analytics | Azure Stream Analytics | `azure_stream_analytics_fullaccess` |
| Analytics | HDInsight | `hdinsight_noaccess` |
| Analytics | HDInsight | `hdinsight_readonly` |
| Analytics | HDInsight | `hdinsight_fullaccess` |
| Analytics | Data Factory | `data_factory_noaccess` |
| Analytics | Data Factory | `data_factory_readonly` |
| Analytics | Data Factory | `data_factory_fullaccess` |
| Analytics | Data Lake Analytics | `data_lake_analytics_noaccess` |
| Analytics | Data Lake Analytics | `data_lake_analytics_readonly` |
| Analytics | Data Lake Analytics | `data_lake_analytics_fullaccess` |
| Analytics | Event Hubs | `event_hubs_noaccess` |
| Analytics | Event Hubs | `event_hubs_readonly` |
| Analytics | Event Hubs | `event_hubs_fullaccess` |
| Analytics | Azure Analysis Services | `azure_analysis_services_noaccess` |
| Analytics | Azure Analysis Services | `azure_analysis_services_readonly` |
| Analytics | Azure Analysis Services | `azure_analysis_services_fullaccess` |
| Analytics | Data Catalog | `data_catalog_noaccess` |
| Analytics | Data Catalog | `data_catalog_readonly` |
| Analytics | Data Catalog | `data_catalog_fullaccess` |
| Analytics | Azure Data Explorer | `azure_data_explorer_noaccess` |
| Analytics | Azure Data Explorer | `azure_data_explorer_readonly` |
| Analytics | Azure Data Explorer | `azure_data_explorer_fullaccess` |
| Blockchain | Azure Blockchain Service | `azure_blockchain_service_noaccess` |
| Blockchain | Azure Blockchain Service | `azure_blockchain_service_readonly` |
| Blockchain | Azure Blockchain Service | `azure_blockchain_service_fullaccess` |
| Compute | Virtual Machines | `virtual_machines_noaccess` |
| Compute | Virtual Machines | `virtual_machines_readonly` |
| Compute | Virtual Machines | `virtual_machines_fullaccess` |
| Compute | Batch | `batch_noaccess` |
| Compute | Batch | `batch_readonly` |
| Compute | Batch | `batch_fullaccess` |
| Compute | SAP HANA on Azure Large Instances | `sap_hana_on_azure_large_instances_noaccess` |
| Compute | SAP HANA on Azure Large Instances | `sap_hana_on_azure_large_instances_readonly` |
| Compute | SAP HANA on Azure Large Instances | `sap_hana_on_azure_large_instances_fullaccess` |
| Compute | Virtual Machine Scale Sets | `virtual_machine_scale_sets_noaccess` |
| Compute | Virtual Machine Scale Sets | `virtual_machine_scale_sets_readonly` |
| Compute | Virtual Machine Scale Sets | `virtual_machine_scale_sets_fullaccess` |
| Compute | Azure VMware Solution by CloudSimple | `azure_vmware_solution_by_cloudsimple_noaccess` |
| Compute | Azure VMware Solution by CloudSimple | `azure_vmware_solution_by_cloudsimple_readonly` |
| Compute | Azure VMware Solution by CloudSimple | `azure_vmware_solution_by_cloudsimple_fullaccess` |
| Containers | Azure Kubernetes Service (AKS) | `azure_kubernetes_service_noaccess` |
| Containers | Azure Kubernetes Service (AKS) | `azure_kubernetes_service_readonly` |
| Containers | Azure Kubernetes Service (AKS) | `azure_kubernetes_service_fullaccess` |
| Containers | Service Fabric | `service_fabric_noaccess` |
| Containers | Service Fabric | `service_fabric_readonly` |
| Containers | Service Fabric | `service_fabric_fullaccess` |
| Containers | Container Instances | `container_instances_noaccess` |
| Containers | Container Instances | `container_instances_readonly` |
| Containers | Container Instances | `container_instances_fullaccess` |
| Containers | Container Registry | `container_registry_noaccess` |
| Containers | Container Registry | `container_registry_readonly` |
| Containers | Container Registry | `container_registry_fullaccess` |
| Containers | Azure Red Hat OpenShift | `azure_red_hat_openshift_noaccess` |
| Containers | Azure Red Hat OpenShift | `azure_red_hat_openshift_readonly` |
| Containers | Azure Red Hat OpenShift | `azure_red_hat_openshift_fullaccess` |
| Databases | Azure SQL Database | `azure_sql_database_noaccess` |
| Databases | Azure SQL Database | `azure_sql_database_readonly` |
| Databases | Azure SQL Database | `azure_sql_database_fullaccess` |
| Databases | Azure Cosmos DB | `azure_cosmos_db_noaccess` |
| Databases | Azure Cosmos DB | `azure_cosmos_db_readonly` |
| Databases | Azure Cosmos DB | `azure_cosmos_db_fullaccess` |
| Databases | Azure Cache for Redis | `azure_cache_for_redis_noaccess` |
| Databases | Azure Cache for Redis | `azure_cache_for_redis_readonly` |
| Databases | Azure Cache for Redis | `azure_cache_for_redis_fullaccess` |
| Databases | Azure Database for PostgreSQL | `azure_database_for_postgresql_noaccess` |
| Databases | Azure Database for PostgreSQL | `azure_database_for_postgresql_readonly` |
| Databases | Azure Database for PostgreSQL | `azure_database_for_postgresql_fullaccess` |
| Databases | Azure Database for MariaDB | `azure_database_for_mariadb_noaccess` |
| Databases | Azure Database for MariaDB | `azure_database_for_mariadb_readonly` |
| Databases | Azure Database for MariaDB | `azure_database_for_mariadb_fullaccess` |
| Databases | Azure Database for MySQL | `azure_database_for_mysql_noaccess` |
| Databases | Azure Database for MySQL | `azure_database_for_mysql_readonly` |
| Databases | Azure Database for MySQL | `azure_database_for_mysql_fullaccess` |
| Developer Tools | Azure Lab Services | `azure_lab_services_noaccess` |
| Developer Tools | Azure Lab Services | `azure_lab_services_readonly` |
| Developer Tools | Azure Lab Services | `azure_lab_services_fullaccess` |
| DevOps | Azure DevOps | `azure_devops_noaccess` |
| DevOps | Azure DevOps | `azure_devops_readonly` |
| DevOps | Azure DevOps | `azure_devops_fullaccess` |
| DevOps | Azure DevTest Labs | `azure_devtest_labs_noaccess` |
| DevOps | Azure DevTest Labs | `azure_devtest_labs_readonly` |
| DevOps | Azure DevTest Labs | `azure_devtest_labs_fullaccess` |
| Identity | Azure Active Directory | `azure_active_directory_noaccess` |
| Identity | Azure Active Directory | `azure_active_directory_readonly` |
| Identity | Azure Active Directory | `azure_active_directory_fullaccess` |
| Identity | Azure Active Directory Domain Services | `azure_active_directory_domain_services_noaccess` |
| Identity | Azure Active Directory Domain Services | `azure_active_directory_domain_services_readonly` |
| Identity | Azure Active Directory Domain Services | `azure_active_directory_domain_services_fullaccess` |
| Integration | Event Grid | `event_grid_noaccess` |
| Integration | Event Grid | `event_grid_readonly` |
| Integration | Event Grid | `event_grid_fullaccess` |
| Integration | Logic Apps | `logic_apps_noaccess` |
| Integration | Logic Apps | `logic_apps_readonly` |
| Integration | Logic Apps | `logic_apps_fullaccess` |
| Integration | API Management | `api_management_noaccess` |
| Integration | API Management | `api_management_readonly` |
| Integration | API Management | `api_management_fullaccess` |
| Integration | Service Bus | `service_bus_noaccess` |
| Integration | Service Bus | `service_bus_readonly` |
| Integration | Service Bus | `service_bus_fullaccess` |
| Internet of Things | Azure IoT Hub | `azure_iot_hub_noaccess` |
| Internet of Things | Azure IoT Hub | `azure_iot_hub_readonly` |
| Internet of Things | Azure IoT Hub | `azure_iot_hub_fullaccess` |
| Internet of Things | Azure IoT Central | `azure_iot_central_noaccess` |
| Internet of Things | Azure IoT Central | `azure_iot_central_readonly` |
| Internet of Things | Azure IoT Central | `azure_iot_central_fullaccess` |
| Internet of Things | Azure Time Series Insights | `azure_time_series_insights_noaccess` |
| Internet of Things | Azure Time Series Insights | `azure_time_series_insights_readonly` |
| Internet of Things | Azure Time Series Insights | `azure_time_series_insights_fullaccess` |
| Internet of Things | Azure Maps | `azure_maps_noaccess` |
| Internet of Things | Azure Maps | `azure_maps_readonly` |
| Internet of Things | Azure Maps | `azure_maps_fullaccess` |
| Internet of Things | Windows 10 IoT Core Services | `windows_10_iot_core_services_noaccess` |
| Internet of Things | Windows 10 IoT Core Services | `windows_10_iot_core_services_readonly` |
| Internet of Things | Windows 10 IoT Core Services | `windows_10_iot_core_services_fullaccess` |
| Internet of Things | Notification Hubs | `notification_hubs_noaccess` |
| Internet of Things | Notification Hubs | `notification_hubs_readonly` |
| Internet of Things | Notification Hubs | `notification_hubs_fullaccess` |
| Management and Governance | Azure Advisor | `azure_advisor_noaccess` |
| Management and Governance | Azure Advisor | `azure_advisor_readonly` |
| Management and Governance | Azure Advisor | `azure_advisor_fullaccess` |
| Management and Governance | Scheduler | `scheduler_noaccess` |
| Management and Governance | Scheduler | `scheduler_readonly` |
| Management and Governance | Scheduler | `scheduler_fullaccess` |
| Management and Governance | Automation | `automation_noaccess` |
| Management and Governance | Automation | `automation_readonly` |
| Management and Governance | Automation | `automation_fullaccess` |
| Management and Governance | Azure Monitor | `azure_monitor_noaccess` |
| Management and Governance | Azure Monitor | `azure_monitor_readonly` |
| Management and Governance | Azure Monitor | `azure_monitor_fullaccess` |
| Management and Governance | Azure Service Health | `azure_service_health_noaccess` |
| Management and Governance | Azure Service Health | `azure_service_health_readonly` |
| Management and Governance | Azure Service Health | `azure_service_health_fullaccess` |
| Management and Governance | Microsoft Azure portal | `microsoft_azure_portal_noaccess` |
| Management and Governance | Microsoft Azure portal | `microsoft_azure_portal_readonly` |
| Management and Governance | Microsoft Azure portal | `microsoft_azure_portal_fullaccess` |
| Management and Governance | Azure Resource Manager | `azure_resource_manager_noaccess` |
| Management and Governance | Azure Resource Manager | `azure_resource_manager_readonly` |
| Management and Governance | Azure Resource Manager | `azure_resource_manager_fullaccess` |
| Management and Governance | Azure Policy | `azure_policy_noaccess` |
| Management and Governance | Azure Policy | `azure_policy_readonly` |
| Management and Governance | Azure Policy | `azure_policy_fullaccess` |
| Management and Governance | Cost management | `cost_management_noaccess` |
| Management and Governance | Cost management | `cost_management_readonly` |
| Management and Governance | Cost management | `cost_management_fullaccess` |
| Management and Governance | Azure Managed Applications | `azure_managed_applications_noaccess` |
| Management and Governance | Azure Managed Applications | `azure_managed_applications_readonly` |
| Management and Governance | Azure Managed Applications | `azure_managed_applications_fullaccess` |
| Management and Governance | Azure Blueprints | `azure_blueprints_noaccess` |
| Management and Governance | Azure Blueprints | `azure_blueprints_readonly` |
| Management and Governance | Azure Blueprints | `azure_blueprints_fullaccess` |
| Media | Content Delivery Network | `content_delivery_network_noaccess` |
| Media | Content Delivery Network | `content_delivery_network_readonly` |
| Media | Content Delivery Network | `content_delivery_network_fullaccess` |
| Media | Media Services | `media_services_noaccess` |
| Media | Media Services | `media_services_readonly` |
| Media | Media Services | `media_services_fullaccess` |
| Microsoft Azure Stack | Azure Stack | `azure_stack_noaccess` |
| Microsoft Azure Stack | Azure Stack | `azure_stack_readonly` |
| Microsoft Azure Stack | Azure Stack | `azure_stack_fullaccess` |
| Migration | Azure Site Recovery | `azure_site_recovery_noaccess` |
| Migration | Azure Site Recovery | `azure_site_recovery_readonly` |
| Migration | Azure Site Recovery | `azure_site_recovery_fullaccess` |
| Migration | Azure Database Migration Service | `azure_database_migration_service_noaccess` |
| Migration | Azure Database Migration Service | `azure_database_migration_service_readonly` |
| Migration | Azure Database Migration Service | `azure_database_migration_service_fullaccess` |
| Migration | Azure Migrate | `azure_migrate_noaccess` |
| Migration | Azure Migrate | `azure_migrate_readonly` |
| Migration | Azure Migrate | `azure_migrate_fullaccess` |
| Mixed Reality | Spatial anchors | `spatial_anchors_noaccess` |
| Mixed Reality | Spatial anchors | `spatial_anchors_readonly` |
| Mixed Reality | Spatial anchors | `spatial_anchors_fullaccess` |
| Mobile | Visual Studio App Centre | `visual_studio_app_centre_noaccess` |
| Mobile | Visual Studio App Centre | `visual_studio_app_centre_readonly` |
| Mobile | Visual Studio App Centre | `visual_studio_app_centre_fullaccess` |
| Networking | ExpressRoute | `expressroute_noaccess` |
| Networking | ExpressRoute | `expressroute_readonly` |
| Networking | ExpressRoute | `expressroute_fullaccess` |
| Networking | Azure DNS | `azure_dns_noaccess` |
| Networking | Azure DNS | `azure_dns_readonly` |
| Networking | Azure DNS | `azure_dns_fullaccess` |
| Networking | Virtual Network | `virtual_network_noaccess` |
| Networking | Virtual Network | `virtual_network_readonly` |
| Networking | Virtual Network | `virtual_network_fullaccess` |
| Networking | Traffic Manager | `traffic_manager_noaccess` |
| Networking | Traffic Manager | `traffic_manager_readonly` |
| Networking | Traffic Manager | `traffic_manager_fullaccess` |
| Networking | Load Balancer | `load_balancer_noaccess` |
| Networking | Load Balancer | `load_balancer_readonly` |
| Networking | Load Balancer | `load_balancer_fullaccess` |
| Networking | VPN Gateway | `vpn_gateway_noaccess` |
| Networking | VPN Gateway | `vpn_gateway_readonly` |
| Networking | VPN Gateway | `vpn_gateway_fullaccess` |
| Networking | Application Gateway | `application_gateway_noaccess` |
| Networking | Application Gateway | `application_gateway_readonly` |
| Networking | Application Gateway | `application_gateway_fullaccess` |
| Networking | Azure DDoS Protection | `azure_ddos_protection_noaccess` |
| Networking | Azure DDoS Protection | `azure_ddos_protection_readonly` |
| Networking | Azure DDoS Protection | `azure_ddos_protection_fullaccess` |
| Networking | Network Watcher | `network_watcher_noaccess` |
| Networking | Network Watcher | `network_watcher_readonly` |
| Networking | Network Watcher | `network_watcher_fullaccess` |
| Networking | Azure Firewall | `azure_firewall_noaccess` |
| Networking | Azure Firewall | `azure_firewall_readonly` |
| Networking | Azure Firewall | `azure_firewall_fullaccess` |
| Networking | Virtual WAN | `virtual_wan_noaccess` |
| Networking | Virtual WAN | `virtual_wan_readonly` |
| Networking | Virtual WAN | `virtual_wan_fullaccess` |
| Networking | Azure Front Door Service | `azure_front_door_service_noaccess` |
| Networking | Azure Front Door Service | `azure_front_door_service_readonly` |
| Networking | Azure Front Door Service | `azure_front_door_service_fullaccess` |
| Networking | Azure Bastion | `azure_bastion_noaccess` |
| Networking | Azure Bastion | `azure_bastion_readonly` |
| Networking | Azure Bastion | `azure_bastion_fullaccess` |
| Security | Key Vault | `key_vault_noaccess` |
| Security | Key Vault | `key_vault_readonly` |
| Security | Key Vault | `key_vault_fullaccess` |
| Security | Security Center | `security_center_noaccess` |
| Security | Security Center | `security_center_readonly` |
| Security | Security Center | `security_center_fullaccess` |
| Security | Azure Dedicated HSM | `azure_dedicated_hsm_noaccess` |
| Security | Azure Dedicated HSM | `azure_dedicated_hsm_readonly` |
| Security | Azure Dedicated HSM | `azure_dedicated_hsm_fullaccess` |
| Storage | Storage Accounts | `storage_accounts_noaccess` |
| Storage | Storage Accounts | `storage_accounts_readonly` |
| Storage | Storage Accounts | `storage_accounts_fullaccess` |
| Storage | StorSimple | `storsimple_noaccess` |
| Storage | StorSimple | `storsimple_readonly` |
| Storage | StorSimple | `storsimple_fullaccess` |
| Storage | Azure Data Lake Storage | `azure_data_lake_storage_noaccess` |
| Storage | Azure Data Lake Storage | `azure_data_lake_storage_readonly` |
| Storage | Azure Data Lake Storage | `azure_data_lake_storage_fullaccess` |
| Storage | File storage | `file_storage_noaccess` |
| Storage | File storage | `file_storage_readonly` |
| Storage | File storage | `file_storage_fullaccess` |
| Storage | Data Box | `data_box_noaccess` |
| Storage | Data Box | `data_box_readonly` |
| Storage | Data Box | `data_box_fullaccess` |
| Storage | Avere vFXT for Azure | `avere_vfxt_for_azure_noaccess` |
| Storage | Avere vFXT for Azure | `avere_vfxt_for_azure_readonly` |
| Storage | Avere vFXT for Azure | `avere_vfxt_for_azure_fullaccess` |
| Storage | Azure NetApp Files | `azure_netapp_files_noaccess` |
| Storage | Azure NetApp Files | `azure_netapp_files_readonly` |
| Storage | Azure NetApp Files | `azure_netapp_files_fullaccess` |
| Web | App Service | `app_service_noaccess` |
| Web | App Service | `app_service_readonly` |
| Web | App Service | `app_service_fullaccess` |
| Web | Azure SignalR Service | `azure_signalr_service_noaccess` |
| Web | Azure SignalR Service | `azure_signalr_service_readonly` |
| Web | Azure SignalR Service | `azure_signalr_service_fullaccess` |
| Databases | Enterprise Graph | `enterprise_graph_noaccess` |
| Databases | Enterprise Graph | `enterprise_graph_readonly` |
| Databases | Enterprise Graph | `enterprise_graph_fullaccess` |
| Analytics | Customer Insights | `customer_insights_noaccess` |
| Analytics | Customer Insights | `customer_insights_readonly` |
| Analytics | Customer Insights | `customer_insights_fullaccess` |
| Migration | Azure Import/Export | `azure_import_export_noaccess` |
| Migration | Azure Import/Export | `azure_import_export_readonly` |
| Migration | Azure Import/Export | `azure_import_export_fullaccess` |
| Analytics | Azure PowerBI | `azure_powerbi_noaccess` |
| Analytics | Azure PowerBI | `azure_powerbi_readonly` |
| Analytics | Azure PowerBI | `azure_powerbi_fullaccess` |
| Integration | Azure Relay | `azure_relay_noaccess` |
| Integration | Azure Relay | `azure_relay_readonly` |
| Integration | Azure Relay | `azure_relay_fullaccess` |
| Security | Graph Security API | `graph_security_api_noaccess` |
| Security | Graph Security API | `graph_security_api_readonly` |
| Security | Graph Security API | `graph_security_api_fullaccess` |
| Security | InTune | `intune_noaccess` |
| Security | InTune | `intune_readonly` |
| Security | InTune | `intune_fullaccess` |
| Security | Customer LockBox | `customer_lockbox_noaccess` |
| Security | Customer LockBox | `customer_lockbox_readonly` |
| Security | Customer LockBox | `customer_lockbox_fullaccess` |
| Medical | Azure API for FHIR | `azure_api_for_fhir_noaccess` |
| Medical | Azure API for FHIR | `azure_api_for_fhir_readonly` |
| Medical | Azure API for FHIR | `azure_api_for_fhir_fullaccess` |
| Integration | Azure Custom Providers | `azure_custom_providers_noaccess` |
| Integration | Azure Custom Providers | `azure_custom_providers_readonly` |
| Integration | Azure Custom Providers | `azure_custom_providers_fullaccess` |
