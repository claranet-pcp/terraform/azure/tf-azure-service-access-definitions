variable "subscription_id" {
  description = "The ID of the primary subscription that the service roles should be placed in."
  type        = "string"
}
