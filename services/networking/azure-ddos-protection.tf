// FullAccess
resource "azurerm_role_definition" "azure_ddos_protection_fullaccess_definition" {
  name              = "azure-ddos-protection-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Network/ddosProtectionPlans/read",
      "Microsoft.Network/ddosProtectionPlans/write",
      "Microsoft.Network/ddosProtectionPlans/delete",
      "Microsoft.Network/ddosProtectionPlans/join/action",
      "Microsoft.Network/ddosCustomPolicies/read",
      "Microsoft.Network/ddosCustomPolicies/write",
      "Microsoft.Network/ddosCustomPolicies/delete",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "azure_ddos_protection_readonly_definition" {
  name              = "azure-ddos-protection-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Network/ddosProtectionPlans/read",
      "Microsoft.Network/ddosCustomPolicies/read",
    ]

    not_actions = [
      "Microsoft.Network/ddosProtectionPlans/write",
      "Microsoft.Network/ddosProtectionPlans/delete",
      "Microsoft.Network/ddosProtectionPlans/join/action",
      "Microsoft.Network/ddosCustomPolicies/write",
      "Microsoft.Network/ddosCustomPolicies/delete",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "azure_ddos_protection_noaccess_definition" {
  name              = "azure-ddos-protection-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.Network/ddosProtectionPlans/read",
      "Microsoft.Network/ddosProtectionPlans/write",
      "Microsoft.Network/ddosProtectionPlans/delete",
      "Microsoft.Network/ddosProtectionPlans/join/action",
      "Microsoft.Network/ddosCustomPolicies/read",
      "Microsoft.Network/ddosCustomPolicies/write",
      "Microsoft.Network/ddosCustomPolicies/delete",
    ]
  }
}
