// FullAccess
resource "azurerm_role_definition" "network_watcher_fullaccess_definition" {
  name              = "network-watcher-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Network/networkWatchers/read",
      "Microsoft.Network/networkWatchers/write",
      "Microsoft.Network/networkWatchers/delete",
      "Microsoft.Network/networkWatchers/configureFlowLog/action",
      "Microsoft.Network/networkWatchers/ipFlowVerify/action",
      "Microsoft.Network/networkWatchers/nextHop/action",
      "Microsoft.Network/networkWatchers/queryFlowLogStatus/action",
      "Microsoft.Network/networkWatchers/queryTroubleshootResult/action",
      "Microsoft.Network/networkWatchers/securityGroupView/action",
      "Microsoft.Network/networkWatchers/networkConfigurationDiagnostic/action",
      "Microsoft.Network/networkWatchers/queryConnectionMonitors/action",
      "Microsoft.Network/networkWatchers/topology/action",
      "Microsoft.Network/networkWatchers/troubleshoot/action",
      "Microsoft.Network/networkWatchers/connectivityCheck/action",
      "Microsoft.Network/networkWatchers/azureReachabilityReport/action",
      "Microsoft.Network/networkWatchers/availableProvidersList/action",
      "Microsoft.Network/networkWatchers/packetCaptures/queryStatus/action",
      "Microsoft.Network/networkWatchers/packetCaptures/stop/action",
      "Microsoft.Network/networkWatchers/packetCaptures/read",
      "Microsoft.Network/networkWatchers/packetCaptures/write",
      "Microsoft.Network/networkWatchers/packetCaptures/delete",
      "Microsoft.Network/networkWatchers/connectionMonitors/start/action",
      "Microsoft.Network/networkWatchers/connectionMonitors/stop/action",
      "Microsoft.Network/networkWatchers/connectionMonitors/query/action",
      "Microsoft.Network/networkWatchers/connectionMonitors/read",
      "Microsoft.Network/networkWatchers/connectionMonitors/write",
      "Microsoft.Network/networkWatchers/connectionMonitors/delete",
      "Microsoft.Network/networkWatchers/lenses/start/action",
      "Microsoft.Network/networkWatchers/lenses/stop/action",
      "Microsoft.Network/networkWatchers/lenses/query/action",
      "Microsoft.Network/networkWatchers/lenses/read",
      "Microsoft.Network/networkWatchers/lenses/write",
      "Microsoft.Network/networkWatchers/lenses/delete",
      "Microsoft.Network/networkWatchers/pingMeshes/start/action",
      "Microsoft.Network/networkWatchers/pingMeshes/stop/action",
      "Microsoft.Network/networkWatchers/pingMeshes/read",
      "Microsoft.Network/networkWatchers/pingMeshes/write",
      "Microsoft.Network/networkWatchers/pingMeshes/delete",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "network_watcher_readonly_definition" {
  name              = "network-watcher-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Network/networkWatchers/read",
      "Microsoft.Network/networkWatchers/packetCaptures/read",
      "Microsoft.Network/networkWatchers/connectionMonitors/read",
      "Microsoft.Network/networkWatchers/lenses/read",
      "Microsoft.Network/networkWatchers/pingMeshes/read",
    ]

    not_actions = [
      "Microsoft.Network/networkWatchers/write",
      "Microsoft.Network/networkWatchers/delete",
      "Microsoft.Network/networkWatchers/configureFlowLog/action",
      "Microsoft.Network/networkWatchers/ipFlowVerify/action",
      "Microsoft.Network/networkWatchers/nextHop/action",
      "Microsoft.Network/networkWatchers/queryFlowLogStatus/action",
      "Microsoft.Network/networkWatchers/queryTroubleshootResult/action",
      "Microsoft.Network/networkWatchers/securityGroupView/action",
      "Microsoft.Network/networkWatchers/networkConfigurationDiagnostic/action",
      "Microsoft.Network/networkWatchers/queryConnectionMonitors/action",
      "Microsoft.Network/networkWatchers/topology/action",
      "Microsoft.Network/networkWatchers/troubleshoot/action",
      "Microsoft.Network/networkWatchers/connectivityCheck/action",
      "Microsoft.Network/networkWatchers/azureReachabilityReport/action",
      "Microsoft.Network/networkWatchers/availableProvidersList/action",
      "Microsoft.Network/networkWatchers/packetCaptures/queryStatus/action",
      "Microsoft.Network/networkWatchers/packetCaptures/stop/action",
      "Microsoft.Network/networkWatchers/packetCaptures/write",
      "Microsoft.Network/networkWatchers/packetCaptures/delete",
      "Microsoft.Network/networkWatchers/connectionMonitors/start/action",
      "Microsoft.Network/networkWatchers/connectionMonitors/stop/action",
      "Microsoft.Network/networkWatchers/connectionMonitors/query/action",
      "Microsoft.Network/networkWatchers/connectionMonitors/write",
      "Microsoft.Network/networkWatchers/connectionMonitors/delete",
      "Microsoft.Network/networkWatchers/lenses/start/action",
      "Microsoft.Network/networkWatchers/lenses/stop/action",
      "Microsoft.Network/networkWatchers/lenses/query/action",
      "Microsoft.Network/networkWatchers/lenses/write",
      "Microsoft.Network/networkWatchers/lenses/delete",
      "Microsoft.Network/networkWatchers/pingMeshes/start/action",
      "Microsoft.Network/networkWatchers/pingMeshes/stop/action",
      "Microsoft.Network/networkWatchers/pingMeshes/write",
      "Microsoft.Network/networkWatchers/pingMeshes/delete",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "network_watcher_noaccess_definition" {
  name              = "network-watcher-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.Network/networkWatchers/read",
      "Microsoft.Network/networkWatchers/write",
      "Microsoft.Network/networkWatchers/delete",
      "Microsoft.Network/networkWatchers/configureFlowLog/action",
      "Microsoft.Network/networkWatchers/ipFlowVerify/action",
      "Microsoft.Network/networkWatchers/nextHop/action",
      "Microsoft.Network/networkWatchers/queryFlowLogStatus/action",
      "Microsoft.Network/networkWatchers/queryTroubleshootResult/action",
      "Microsoft.Network/networkWatchers/securityGroupView/action",
      "Microsoft.Network/networkWatchers/networkConfigurationDiagnostic/action",
      "Microsoft.Network/networkWatchers/queryConnectionMonitors/action",
      "Microsoft.Network/networkWatchers/topology/action",
      "Microsoft.Network/networkWatchers/troubleshoot/action",
      "Microsoft.Network/networkWatchers/connectivityCheck/action",
      "Microsoft.Network/networkWatchers/azureReachabilityReport/action",
      "Microsoft.Network/networkWatchers/availableProvidersList/action",
      "Microsoft.Network/networkWatchers/packetCaptures/queryStatus/action",
      "Microsoft.Network/networkWatchers/packetCaptures/stop/action",
      "Microsoft.Network/networkWatchers/packetCaptures/read",
      "Microsoft.Network/networkWatchers/packetCaptures/write",
      "Microsoft.Network/networkWatchers/packetCaptures/delete",
      "Microsoft.Network/networkWatchers/connectionMonitors/start/action",
      "Microsoft.Network/networkWatchers/connectionMonitors/stop/action",
      "Microsoft.Network/networkWatchers/connectionMonitors/query/action",
      "Microsoft.Network/networkWatchers/connectionMonitors/read",
      "Microsoft.Network/networkWatchers/connectionMonitors/write",
      "Microsoft.Network/networkWatchers/connectionMonitors/delete",
      "Microsoft.Network/networkWatchers/lenses/start/action",
      "Microsoft.Network/networkWatchers/lenses/stop/action",
      "Microsoft.Network/networkWatchers/lenses/query/action",
      "Microsoft.Network/networkWatchers/lenses/read",
      "Microsoft.Network/networkWatchers/lenses/write",
      "Microsoft.Network/networkWatchers/lenses/delete",
      "Microsoft.Network/networkWatchers/pingMeshes/start/action",
      "Microsoft.Network/networkWatchers/pingMeshes/stop/action",
      "Microsoft.Network/networkWatchers/pingMeshes/read",
      "Microsoft.Network/networkWatchers/pingMeshes/write",
      "Microsoft.Network/networkWatchers/pingMeshes/delete",
    ]
  }
}
