// FullAccess
resource "azurerm_role_definition" "azure_firewall_fullaccess_definition" {
  name              = "azure-firewall-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Network/networkSecurityGroups/read",
      "Microsoft.Network/networkSecurityGroups/write",
      "Microsoft.Network/networkSecurityGroups/delete",
      "Microsoft.Network/networkSecurityGroups/join/action",
      "Microsoft.Network/applicationSecurityGroups/joinIpConfiguration/action",
      "Microsoft.Network/applicationSecurityGroups/joinNetworkSecurityRule/action",
      "Microsoft.Network/applicationSecurityGroups/read",
      "Microsoft.Network/applicationSecurityGroups/write",
      "Microsoft.Network/applicationSecurityGroups/delete",
      "Microsoft.Network/applicationSecurityGroups/listIpConfigurations/action",
      "Microsoft.Network/networkSecurityGroups/defaultSecurityRules/read",
      "Microsoft.Network/networkSecurityGroups/securityRules/read",
      "Microsoft.Network/networkSecurityGroups/securityRules/write",
      "Microsoft.Network/networkSecurityGroups/securityRules/delete",
      "Microsoft.Network/azurefirewalls/read",
      "Microsoft.Network/azurefirewalls/write",
      "Microsoft.Network/azurefirewalls/delete",
      "Microsoft.Network/azureFirewallFqdnTags/read",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "azure_firewall_readonly_definition" {
  name              = "azure-firewall-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Network/networkSecurityGroups/read",
      "Microsoft.Network/applicationSecurityGroups/read",
      "Microsoft.Network/networkSecurityGroups/defaultSecurityRules/read",
      "Microsoft.Network/networkSecurityGroups/securityRules/read",
      "Microsoft.Network/azurefirewalls/read",
      "Microsoft.Network/azureFirewallFqdnTags/read",
    ]

    not_actions = [
      "Microsoft.Network/networkSecurityGroups/write",
      "Microsoft.Network/networkSecurityGroups/delete",
      "Microsoft.Network/networkSecurityGroups/join/action",
      "Microsoft.Network/applicationSecurityGroups/joinIpConfiguration/action",
      "Microsoft.Network/applicationSecurityGroups/joinNetworkSecurityRule/action",
      "Microsoft.Network/applicationSecurityGroups/write",
      "Microsoft.Network/applicationSecurityGroups/delete",
      "Microsoft.Network/applicationSecurityGroups/listIpConfigurations/action",
      "Microsoft.Network/networkSecurityGroups/securityRules/write",
      "Microsoft.Network/networkSecurityGroups/securityRules/delete",
      "Microsoft.Network/azurefirewalls/write",
      "Microsoft.Network/azurefirewalls/delete",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "azure_firewall_noaccess_definition" {
  name              = "azure-firewall-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.Network/networkSecurityGroups/read",
      "Microsoft.Network/networkSecurityGroups/write",
      "Microsoft.Network/networkSecurityGroups/delete",
      "Microsoft.Network/networkSecurityGroups/join/action",
      "Microsoft.Network/applicationSecurityGroups/joinIpConfiguration/action",
      "Microsoft.Network/applicationSecurityGroups/joinNetworkSecurityRule/action",
      "Microsoft.Network/applicationSecurityGroups/read",
      "Microsoft.Network/applicationSecurityGroups/write",
      "Microsoft.Network/applicationSecurityGroups/delete",
      "Microsoft.Network/applicationSecurityGroups/listIpConfigurations/action",
      "Microsoft.Network/networkSecurityGroups/defaultSecurityRules/read",
      "Microsoft.Network/networkSecurityGroups/securityRules/read",
      "Microsoft.Network/networkSecurityGroups/securityRules/write",
      "Microsoft.Network/networkSecurityGroups/securityRules/delete",
      "Microsoft.Network/azurefirewalls/read",
      "Microsoft.Network/azurefirewalls/write",
      "Microsoft.Network/azurefirewalls/delete",
      "Microsoft.Network/azureFirewallFqdnTags/read",
    ]
  }
}
