// FullAccess
resource "azurerm_role_definition" "expressroute_fullaccess_definition" {
  name              = "expressroute-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Network/expressRouteCircuits/read",
      "Microsoft.Network/expressRouteCircuits/write",
      "Microsoft.Network/expressRouteCircuits/join/action",
      "Microsoft.Network/expressRouteCircuits/delete",
      "Microsoft.Network/expressRouteCircuits/stats/read",
      "Microsoft.Network/expressRouteCircuits/authorizations/read",
      "Microsoft.Network/expressRouteCircuits/authorizations/write",
      "Microsoft.Network/expressRouteCircuits/authorizations/delete",
      "Microsoft.Network/expressRouteCircuits/peerings/read",
      "Microsoft.Network/expressRouteCircuits/peerings/write",
      "Microsoft.Network/expressRouteCircuits/peerings/delete",
      "Microsoft.Network/expressRouteCircuits/peerings/arpTables/action",
      "Microsoft.Network/expressRouteCircuits/peerings/routeTables/action",
      "Microsoft.Network/expressRouteCircuits/peerings/routeTablesSummary/action",
      "Microsoft.Network/expressRouteCircuits/peerings/stats/read",
      "Microsoft.Network/expressRouteCircuits/peerings/connections/read",
      "Microsoft.Network/expressRouteCircuits/peerings/connections/write",
      "Microsoft.Network/expressRouteCircuits/peerings/connections/delete",
      "Microsoft.Network/expressRouteServiceProviders/read",
      "Microsoft.Network/expressRouteCrossConnections/read",
      "Microsoft.Network/expressRouteCrossConnections/join/action",
      "Microsoft.Network/expressRouteCrossConnections/peerings/read",
      "Microsoft.Network/expressRouteCrossConnections/peerings/write",
      "Microsoft.Network/expressRouteCrossConnections/peerings/delete",
      "Microsoft.Network/expressRouteCrossConnections/peerings/arpTables/action",
      "Microsoft.Network/expressRouteCrossConnections/peerings/routeTables/action",
      "Microsoft.Network/expressRouteCrossConnections/peerings/routeTableSummary/action",
      "Microsoft.Network/expressRoutePorts/read",
      "Microsoft.Network/expressRoutePorts/write",
      "Microsoft.Network/expressRoutePorts/join/action",
      "Microsoft.Network/expressRoutePorts/delete",
      "Microsoft.Network/expressRoutePorts/links/read",
      "Microsoft.Network/expressRoutePortsLocations/read",
      "Microsoft.Network/expressRouteGateways/read",
      "Microsoft.Network/expressRouteGateways/join/action",
      "Microsoft.Network/expressRouteGateways/expressRouteConnections/read",
      "Microsoft.Network/expressRouteGateways/expressRouteConnections/write",
      "Microsoft.Network/expressRouteGateways/expressRouteConnections/delete",
      "Microsoft.Network/expressRouteCircuits/peerings/peerConnections/read",
      "Microsoft.Peering/register/action",
      "Microsoft.Peering/peeringLocations/read",
      "Microsoft.Peering/peerings/write",
      "Microsoft.Peering/peerings/read",
      "Microsoft.Peering/peerings/delete",
      "Microsoft.Peering/peerAsns/write",
      "Microsoft.Peering/peerAsns/read",
      "Microsoft.Peering/peerAsns/delete",
      "Microsoft.Peering/legacyPeerings/read",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "expressroute_readonly_definition" {
  name              = "expressroute-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Network/expressRouteCircuits/read",
      "Microsoft.Network/expressRouteCircuits/stats/read",
      "Microsoft.Network/expressRouteCircuits/authorizations/read",
      "Microsoft.Network/expressRouteCircuits/peerings/read",
      "Microsoft.Network/expressRouteCircuits/peerings/stats/read",
      "Microsoft.Network/expressRouteCircuits/peerings/connections/read",
      "Microsoft.Network/expressRouteServiceProviders/read",
      "Microsoft.Network/expressRouteCrossConnections/read",
      "Microsoft.Network/expressRouteCrossConnections/peerings/read",
      "Microsoft.Network/expressRoutePorts/read",
      "Microsoft.Network/expressRoutePorts/links/read",
      "Microsoft.Network/expressRoutePortsLocations/read",
      "Microsoft.Network/expressRouteGateways/read",
      "Microsoft.Network/expressRouteGateways/expressRouteConnections/read",
      "Microsoft.Network/expressRouteCircuits/peerings/peerConnections/read",
      "Microsoft.Peering/peeringLocations/read",
      "Microsoft.Peering/peerings/read",
      "Microsoft.Peering/peerAsns/read",
      "Microsoft.Peering/legacyPeerings/read",
    ]

    not_actions = [
      "Microsoft.Network/expressRouteCircuits/write",
      "Microsoft.Network/expressRouteCircuits/join/action",
      "Microsoft.Network/expressRouteCircuits/delete",
      "Microsoft.Network/expressRouteCircuits/authorizations/write",
      "Microsoft.Network/expressRouteCircuits/authorizations/delete",
      "Microsoft.Network/expressRouteCircuits/peerings/write",
      "Microsoft.Network/expressRouteCircuits/peerings/delete",
      "Microsoft.Network/expressRouteCircuits/peerings/arpTables/action",
      "Microsoft.Network/expressRouteCircuits/peerings/routeTables/action",
      "Microsoft.Network/expressRouteCircuits/peerings/routeTablesSummary/action",
      "Microsoft.Network/expressRouteCircuits/peerings/connections/write",
      "Microsoft.Network/expressRouteCircuits/peerings/connections/delete",
      "Microsoft.Network/expressRouteCrossConnections/join/action",
      "Microsoft.Network/expressRouteCrossConnections/peerings/write",
      "Microsoft.Network/expressRouteCrossConnections/peerings/delete",
      "Microsoft.Network/expressRouteCrossConnections/peerings/arpTables/action",
      "Microsoft.Network/expressRouteCrossConnections/peerings/routeTables/action",
      "Microsoft.Network/expressRouteCrossConnections/peerings/routeTableSummary/action",
      "Microsoft.Network/expressRoutePorts/write",
      "Microsoft.Network/expressRoutePorts/join/action",
      "Microsoft.Network/expressRoutePorts/delete",
      "Microsoft.Network/expressRouteGateways/join/action",
      "Microsoft.Network/expressRouteGateways/expressRouteConnections/write",
      "Microsoft.Network/expressRouteGateways/expressRouteConnections/delete",
      "Microsoft.Peering/register/action",
      "Microsoft.Peering/peerings/write",
      "Microsoft.Peering/peerings/delete",
      "Microsoft.Peering/peerAsns/write",
      "Microsoft.Peering/peerAsns/delete",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "expressroute_noaccess_definition" {
  name              = "expressroute-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.Network/expressRouteCircuits/read",
      "Microsoft.Network/expressRouteCircuits/write",
      "Microsoft.Network/expressRouteCircuits/join/action",
      "Microsoft.Network/expressRouteCircuits/delete",
      "Microsoft.Network/expressRouteCircuits/stats/read",
      "Microsoft.Network/expressRouteCircuits/authorizations/read",
      "Microsoft.Network/expressRouteCircuits/authorizations/write",
      "Microsoft.Network/expressRouteCircuits/authorizations/delete",
      "Microsoft.Network/expressRouteCircuits/peerings/read",
      "Microsoft.Network/expressRouteCircuits/peerings/write",
      "Microsoft.Network/expressRouteCircuits/peerings/delete",
      "Microsoft.Network/expressRouteCircuits/peerings/arpTables/action",
      "Microsoft.Network/expressRouteCircuits/peerings/routeTables/action",
      "Microsoft.Network/expressRouteCircuits/peerings/routeTablesSummary/action",
      "Microsoft.Network/expressRouteCircuits/peerings/stats/read",
      "Microsoft.Network/expressRouteCircuits/peerings/connections/read",
      "Microsoft.Network/expressRouteCircuits/peerings/connections/write",
      "Microsoft.Network/expressRouteCircuits/peerings/connections/delete",
      "Microsoft.Network/expressRouteServiceProviders/read",
      "Microsoft.Network/expressRouteCrossConnections/read",
      "Microsoft.Network/expressRouteCrossConnections/join/action",
      "Microsoft.Network/expressRouteCrossConnections/peerings/read",
      "Microsoft.Network/expressRouteCrossConnections/peerings/write",
      "Microsoft.Network/expressRouteCrossConnections/peerings/delete",
      "Microsoft.Network/expressRouteCrossConnections/peerings/arpTables/action",
      "Microsoft.Network/expressRouteCrossConnections/peerings/routeTables/action",
      "Microsoft.Network/expressRouteCrossConnections/peerings/routeTableSummary/action",
      "Microsoft.Network/expressRoutePorts/read",
      "Microsoft.Network/expressRoutePorts/write",
      "Microsoft.Network/expressRoutePorts/join/action",
      "Microsoft.Network/expressRoutePorts/delete",
      "Microsoft.Network/expressRoutePorts/links/read",
      "Microsoft.Network/expressRoutePortsLocations/read",
      "Microsoft.Network/expressRouteGateways/read",
      "Microsoft.Network/expressRouteGateways/join/action",
      "Microsoft.Network/expressRouteGateways/expressRouteConnections/read",
      "Microsoft.Network/expressRouteGateways/expressRouteConnections/write",
      "Microsoft.Network/expressRouteGateways/expressRouteConnections/delete",
      "Microsoft.Network/expressRouteCircuits/peerings/peerConnections/read",
      "Microsoft.Peering/register/action",
      "Microsoft.Peering/peeringLocations/read",
      "Microsoft.Peering/peerings/write",
      "Microsoft.Peering/peerings/read",
      "Microsoft.Peering/peerings/delete",
      "Microsoft.Peering/peerAsns/write",
      "Microsoft.Peering/peerAsns/read",
      "Microsoft.Peering/peerAsns/delete",
      "Microsoft.Peering/legacyPeerings/read",
    ]
  }
}
