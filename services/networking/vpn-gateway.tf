// FullAccess
resource "azurerm_role_definition" "vpn_gateway_fullaccess_definition" {
  name              = "vpn-gateway-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Network/virtualnetworkgateways/supportedvpndevices/action",
      "Microsoft.Network/virtualNetworkGateways/read",
      "Microsoft.Network/virtualNetworkGateways/write",
      "Microsoft.Network/virtualNetworkGateways/delete",
      "microsoft.network/virtualnetworkgateways/generatevpnclientpackage/action",
      "microsoft.network/virtualnetworkgateways/generatevpnprofile/action",
      "microsoft.network/virtualnetworkgateways/getvpnclientconnectionhealth/action",
      "microsoft.network/virtualnetworkgateways/getvpnprofilepackageurl/action",
      "microsoft.network/virtualnetworkgateways/setvpnclientipsecparameters/action",
      "microsoft.network/virtualnetworkgateways/getvpnclientipsecparameters/action",
      "microsoft.network/virtualnetworkgateways/resetvpnclientsharedkey/action",
      "microsoft.network/virtualnetworkgateways/reset/action",
      "microsoft.network/virtualnetworkgateways/getadvertisedroutes/action",
      "microsoft.network/virtualnetworkgateways/getbgppeerstatus/action",
      "microsoft.network/virtualnetworkgateways/getlearnedroutes/action",
      "microsoft.network/virtualnetworkgateways/connections/read",
      "Microsoft.Network/vpnsites/read",
      "Microsoft.Network/vpnsites/write",
      "Microsoft.Network/vpnsites/delete",
      "Microsoft.Network/vpnGateways/read",
      "Microsoft.Network/vpnGateways/write",
      "Microsoft.Network/vpnGateways/delete",
      "microsoft.network/vpngateways/reset/action",
      "microsoft.network/vpngateways/listvpnconnectionshealth/action",
      "microsoft.network/vpnGateways/vpnConnections/read",
      "microsoft.network/vpnGateways/vpnConnections/write",
      "microsoft.network/vpnGateways/vpnConnections/delete",
      "Microsoft.Network/p2sVpnGateways/read",
      "Microsoft.Network/p2sVpnGateways/write",
      "Microsoft.Network/p2sVpnGateways/delete",
      "Microsoft.Network/p2sVpnGateways/generatevpnprofile/action",
      "Microsoft.Network/p2sVpnGateways/getp2svpnconnectionhealth/action",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "vpn_gateway_readonly_definition" {
  name              = "vpn-gateway-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Network/virtualNetworkGateways/read",
      "microsoft.network/virtualnetworkgateways/connections/read",
      "Microsoft.Network/vpnsites/read",
      "Microsoft.Network/vpnGateways/read",
      "microsoft.network/vpnGateways/vpnConnections/read",
      "Microsoft.Network/p2sVpnGateways/read",
    ]

    not_actions = [
      "Microsoft.Network/virtualnetworkgateways/supportedvpndevices/action",
      "Microsoft.Network/virtualNetworkGateways/write",
      "Microsoft.Network/virtualNetworkGateways/delete",
      "microsoft.network/virtualnetworkgateways/generatevpnclientpackage/action",
      "microsoft.network/virtualnetworkgateways/generatevpnprofile/action",
      "microsoft.network/virtualnetworkgateways/getvpnclientconnectionhealth/action",
      "microsoft.network/virtualnetworkgateways/getvpnprofilepackageurl/action",
      "microsoft.network/virtualnetworkgateways/setvpnclientipsecparameters/action",
      "microsoft.network/virtualnetworkgateways/getvpnclientipsecparameters/action",
      "microsoft.network/virtualnetworkgateways/resetvpnclientsharedkey/action",
      "microsoft.network/virtualnetworkgateways/reset/action",
      "microsoft.network/virtualnetworkgateways/getadvertisedroutes/action",
      "microsoft.network/virtualnetworkgateways/getbgppeerstatus/action",
      "microsoft.network/virtualnetworkgateways/getlearnedroutes/action",
      "Microsoft.Network/vpnsites/write",
      "Microsoft.Network/vpnsites/delete",
      "Microsoft.Network/vpnGateways/write",
      "Microsoft.Network/vpnGateways/delete",
      "microsoft.network/vpngateways/reset/action",
      "microsoft.network/vpngateways/listvpnconnectionshealth/action",
      "microsoft.network/vpnGateways/vpnConnections/write",
      "microsoft.network/vpnGateways/vpnConnections/delete",
      "Microsoft.Network/p2sVpnGateways/write",
      "Microsoft.Network/p2sVpnGateways/delete",
      "Microsoft.Network/p2sVpnGateways/generatevpnprofile/action",
      "Microsoft.Network/p2sVpnGateways/getp2svpnconnectionhealth/action",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "vpn_gateway_noaccess_definition" {
  name              = "vpn-gateway-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.Network/virtualnetworkgateways/supportedvpndevices/action",
      "Microsoft.Network/virtualNetworkGateways/read",
      "Microsoft.Network/virtualNetworkGateways/write",
      "Microsoft.Network/virtualNetworkGateways/delete",
      "microsoft.network/virtualnetworkgateways/generatevpnclientpackage/action",
      "microsoft.network/virtualnetworkgateways/generatevpnprofile/action",
      "microsoft.network/virtualnetworkgateways/getvpnclientconnectionhealth/action",
      "microsoft.network/virtualnetworkgateways/getvpnprofilepackageurl/action",
      "microsoft.network/virtualnetworkgateways/setvpnclientipsecparameters/action",
      "microsoft.network/virtualnetworkgateways/getvpnclientipsecparameters/action",
      "microsoft.network/virtualnetworkgateways/resetvpnclientsharedkey/action",
      "microsoft.network/virtualnetworkgateways/reset/action",
      "microsoft.network/virtualnetworkgateways/getadvertisedroutes/action",
      "microsoft.network/virtualnetworkgateways/getbgppeerstatus/action",
      "microsoft.network/virtualnetworkgateways/getlearnedroutes/action",
      "microsoft.network/virtualnetworkgateways/connections/read",
      "Microsoft.Network/vpnsites/read",
      "Microsoft.Network/vpnsites/write",
      "Microsoft.Network/vpnsites/delete",
      "Microsoft.Network/vpnGateways/read",
      "Microsoft.Network/vpnGateways/write",
      "Microsoft.Network/vpnGateways/delete",
      "microsoft.network/vpngateways/reset/action",
      "microsoft.network/vpngateways/listvpnconnectionshealth/action",
      "microsoft.network/vpnGateways/vpnConnections/read",
      "microsoft.network/vpnGateways/vpnConnections/write",
      "microsoft.network/vpnGateways/vpnConnections/delete",
      "Microsoft.Network/p2sVpnGateways/read",
      "Microsoft.Network/p2sVpnGateways/write",
      "Microsoft.Network/p2sVpnGateways/delete",
      "Microsoft.Network/p2sVpnGateways/generatevpnprofile/action",
      "Microsoft.Network/p2sVpnGateways/getp2svpnconnectionhealth/action",
    ]
  }
}
