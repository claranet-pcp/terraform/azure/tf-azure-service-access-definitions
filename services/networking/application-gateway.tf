// FullAccess
resource "azurerm_role_definition" "application_gateway_fullaccess_definition" {
  name              = "application-gateway-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Network/applicationGateways/read",
      "Microsoft.Network/applicationGateways/write",
      "Microsoft.Network/applicationGateways/delete",
      "Microsoft.Network/applicationGateways/backendhealth/action",
      "Microsoft.Network/applicationGateways/start/action",
      "Microsoft.Network/applicationGateways/stop/action",
      "Microsoft.Network/applicationGateways/backendAddressPools/join/action",
      "Microsoft.Network/applicationGatewayAvailableWafRuleSets/read",
      "Microsoft.Network/applicationGatewayAvailableSslOptions/read",
      "Microsoft.Network/applicationGatewayAvailableSslOptions/predefinedPolicies/read",
      "Microsoft.Network/applicationGatewayAvailableServerVariables/read",
      "Microsoft.Network/applicationGatewayAvailableRequestHeaders/read",
      "Microsoft.Network/applicationGatewayAvailableResponseHeaders/read",
      "Microsoft.Network/ApplicationGatewayWebApplicationFirewallPolicies/read",
      "Microsoft.Network/ApplicationGatewayWebApplicationFirewallPolicies/write",
      "Microsoft.Network/ApplicationGatewayWebApplicationFirewallPolicies/delete",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "application_gateway_readonly_definition" {
  name              = "application-gateway-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Network/applicationGateways/read",
      "Microsoft.Network/applicationGatewayAvailableWafRuleSets/read",
      "Microsoft.Network/applicationGatewayAvailableSslOptions/read",
      "Microsoft.Network/applicationGatewayAvailableSslOptions/predefinedPolicies/read",
      "Microsoft.Network/applicationGatewayAvailableServerVariables/read",
      "Microsoft.Network/applicationGatewayAvailableRequestHeaders/read",
      "Microsoft.Network/applicationGatewayAvailableResponseHeaders/read",
      "Microsoft.Network/ApplicationGatewayWebApplicationFirewallPolicies/read",
    ]

    not_actions = [
      "Microsoft.Network/applicationGateways/write",
      "Microsoft.Network/applicationGateways/delete",
      "Microsoft.Network/applicationGateways/backendhealth/action",
      "Microsoft.Network/applicationGateways/start/action",
      "Microsoft.Network/applicationGateways/stop/action",
      "Microsoft.Network/applicationGateways/backendAddressPools/join/action",
      "Microsoft.Network/ApplicationGatewayWebApplicationFirewallPolicies/write",
      "Microsoft.Network/ApplicationGatewayWebApplicationFirewallPolicies/delete",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "application_gateway_noaccess_definition" {
  name              = "application-gateway-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.Network/applicationGateways/read",
      "Microsoft.Network/applicationGateways/write",
      "Microsoft.Network/applicationGateways/delete",
      "Microsoft.Network/applicationGateways/backendhealth/action",
      "Microsoft.Network/applicationGateways/start/action",
      "Microsoft.Network/applicationGateways/stop/action",
      "Microsoft.Network/applicationGateways/backendAddressPools/join/action",
      "Microsoft.Network/applicationGatewayAvailableWafRuleSets/read",
      "Microsoft.Network/applicationGatewayAvailableSslOptions/read",
      "Microsoft.Network/applicationGatewayAvailableSslOptions/predefinedPolicies/read",
      "Microsoft.Network/applicationGatewayAvailableServerVariables/read",
      "Microsoft.Network/applicationGatewayAvailableRequestHeaders/read",
      "Microsoft.Network/applicationGatewayAvailableResponseHeaders/read",
      "Microsoft.Network/ApplicationGatewayWebApplicationFirewallPolicies/read",
      "Microsoft.Network/ApplicationGatewayWebApplicationFirewallPolicies/write",
      "Microsoft.Network/ApplicationGatewayWebApplicationFirewallPolicies/delete",
    ]
  }
}
