// FullAccess
resource "azurerm_role_definition" "load_balancer_fullaccess_definition" {
  name              = "load-balancer-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Network/loadBalancers/read",
      "Microsoft.Network/loadBalancers/write",
      "Microsoft.Network/loadBalancers/delete",
      "Microsoft.Network/loadBalancers/virtualMachines/read",
      "Microsoft.Network/loadBalancers/networkInterfaces/read",
      "Microsoft.Network/loadBalancers/frontendIPConfigurations/read",
      "Microsoft.Network/loadBalancers/frontendIPConfigurations/join/action",
      "Microsoft.Network/loadBalancers/backendAddressPools/read",
      "Microsoft.Network/loadBalancers/backendAddressPools/join/action",
      "Microsoft.Network/loadBalancers/inboundNatRules/read",
      "Microsoft.Network/loadBalancers/inboundNatRules/write",
      "Microsoft.Network/loadBalancers/inboundNatRules/delete",
      "Microsoft.Network/loadBalancers/inboundNatRules/join/action",
      "Microsoft.Network/loadBalancers/inboundNatPools/read",
      "Microsoft.Network/loadBalancers/inboundNatPools/join/action",
      "Microsoft.Network/loadBalancers/loadBalancingRules/read",
      "Microsoft.Network/loadBalancers/probes/read",
      "Microsoft.Network/loadBalancers/probes/join/action",
      "Microsoft.Network/loadBalancers/outboundRules/read",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "load_balancer_readonly_definition" {
  name              = "load-balancer-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Network/loadBalancers/read",
      "Microsoft.Network/loadBalancers/virtualMachines/read",
      "Microsoft.Network/loadBalancers/networkInterfaces/read",
      "Microsoft.Network/loadBalancers/frontendIPConfigurations/read",
      "Microsoft.Network/loadBalancers/backendAddressPools/read",
      "Microsoft.Network/loadBalancers/inboundNatRules/read",
      "Microsoft.Network/loadBalancers/inboundNatPools/read",
      "Microsoft.Network/loadBalancers/loadBalancingRules/read",
      "Microsoft.Network/loadBalancers/probes/read",
      "Microsoft.Network/loadBalancers/outboundRules/read",
    ]

    not_actions = [
      "Microsoft.Network/loadBalancers/write",
      "Microsoft.Network/loadBalancers/delete",
      "Microsoft.Network/loadBalancers/frontendIPConfigurations/join/action",
      "Microsoft.Network/loadBalancers/backendAddressPools/join/action",
      "Microsoft.Network/loadBalancers/inboundNatRules/write",
      "Microsoft.Network/loadBalancers/inboundNatRules/delete",
      "Microsoft.Network/loadBalancers/inboundNatRules/join/action",
      "Microsoft.Network/loadBalancers/inboundNatPools/join/action",
      "Microsoft.Network/loadBalancers/probes/join/action",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "load_balancer_noaccess_definition" {
  name              = "load-balancer-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.Network/loadBalancers/read",
      "Microsoft.Network/loadBalancers/write",
      "Microsoft.Network/loadBalancers/delete",
      "Microsoft.Network/loadBalancers/virtualMachines/read",
      "Microsoft.Network/loadBalancers/networkInterfaces/read",
      "Microsoft.Network/loadBalancers/frontendIPConfigurations/read",
      "Microsoft.Network/loadBalancers/frontendIPConfigurations/join/action",
      "Microsoft.Network/loadBalancers/backendAddressPools/read",
      "Microsoft.Network/loadBalancers/backendAddressPools/join/action",
      "Microsoft.Network/loadBalancers/inboundNatRules/read",
      "Microsoft.Network/loadBalancers/inboundNatRules/write",
      "Microsoft.Network/loadBalancers/inboundNatRules/delete",
      "Microsoft.Network/loadBalancers/inboundNatRules/join/action",
      "Microsoft.Network/loadBalancers/inboundNatPools/read",
      "Microsoft.Network/loadBalancers/inboundNatPools/join/action",
      "Microsoft.Network/loadBalancers/loadBalancingRules/read",
      "Microsoft.Network/loadBalancers/probes/read",
      "Microsoft.Network/loadBalancers/probes/join/action",
      "Microsoft.Network/loadBalancers/outboundRules/read",
    ]
  }
}
