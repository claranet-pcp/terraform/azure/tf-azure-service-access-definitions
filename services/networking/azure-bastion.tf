// FullAccess
resource "azurerm_role_definition" "azure_bastion_fullaccess_definition" {
  name              = "azure-bastion-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Network/bastionHosts/read",
      "Microsoft.Network/bastionHosts/write",
      "Microsoft.Network/bastionHosts/delete",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "azure_bastion_readonly_definition" {
  name              = "azure-bastion-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Network/bastionHosts/read",
    ]

    not_actions = [
      "Microsoft.Network/bastionHosts/write",
      "Microsoft.Network/bastionHosts/delete",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "azure_bastion_noaccess_definition" {
  name              = "azure-bastion-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.Network/bastionHosts/read",
      "Microsoft.Network/bastionHosts/write",
      "Microsoft.Network/bastionHosts/delete",
    ]
  }
}
