// FullAccess
resource "azurerm_role_definition" "virtual_wan_fullaccess_definition" {
  name              = "virtual-wan-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Network/virtualWans/delete",
      "Microsoft.Network/virtualWans/read",
      "Microsoft.Network/virtualWans/write",
      "Microsoft.Network/virtualwans/vpnconfiguration/action",
      "Microsoft.Network/virtualWans/vpnSites/read",
      "Microsoft.Network/virtualWans/virtualHubs/read",
      "Microsoft.Network/virtualwans/supportedSecurityProviders/read",
      "Microsoft.Network/virtualWans/p2sVpnServerConfigurations/read",
      "Microsoft.network/virtualWans/p2sVpnServerConfigurations/write",
      "Microsoft.network/virtualWans/p2sVpnServerConfigurations/delete",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "virtual_wan_readonly_definition" {
  name              = "virtual-wan-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Network/virtualWans/read",
      "Microsoft.Network/virtualWans/vpnSites/read",
      "Microsoft.Network/virtualWans/virtualHubs/read",
      "Microsoft.Network/virtualwans/supportedSecurityProviders/read",
      "Microsoft.Network/virtualWans/p2sVpnServerConfigurations/read",
    ]

    not_actions = [
      "Microsoft.Network/virtualWans/delete",
      "Microsoft.Network/virtualWans/write",
      "Microsoft.Network/virtualwans/vpnconfiguration/action",
      "Microsoft.network/virtualWans/p2sVpnServerConfigurations/write",
      "Microsoft.network/virtualWans/p2sVpnServerConfigurations/delete",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "virtual_wan_noaccess_definition" {
  name              = "virtual-wan-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.Network/virtualWans/delete",
      "Microsoft.Network/virtualWans/read",
      "Microsoft.Network/virtualWans/write",
      "Microsoft.Network/virtualwans/vpnconfiguration/action",
      "Microsoft.Network/virtualWans/vpnSites/read",
      "Microsoft.Network/virtualWans/virtualHubs/read",
      "Microsoft.Network/virtualwans/supportedSecurityProviders/read",
      "Microsoft.Network/virtualWans/p2sVpnServerConfigurations/read",
      "Microsoft.network/virtualWans/p2sVpnServerConfigurations/write",
      "Microsoft.network/virtualWans/p2sVpnServerConfigurations/delete",
    ]
  }
}
