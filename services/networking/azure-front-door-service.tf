// FullAccess
resource "azurerm_role_definition" "azure_front_door_service_fullaccess_definition" {
  name              = "azure-front-door-service-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Network/frontDoors/read",
      "Microsoft.Network/frontDoors/write",
      "Microsoft.Network/frontDoors/delete",
      "Microsoft.Network/frontDoors/purge/action",
      "Microsoft.Network/frontDoors/validateCustomDomain/action",
      "Microsoft.Network/frontDoors/routingRules/read",
      "Microsoft.Network/frontDoors/routingRules/write",
      "Microsoft.Network/frontDoors/routingRules/delete",
      "Microsoft.Network/frontDoors/backendPools/read",
      "Microsoft.Network/frontDoors/backendPools/write",
      "Microsoft.Network/frontDoors/backendPools/delete",
      "Microsoft.Network/frontDoors/frontendEndpoints/read",
      "Microsoft.Network/frontDoors/frontendEndpoints/write",
      "Microsoft.Network/frontDoors/frontendEndpoints/delete",
      "Microsoft.Network/frontDoors/frontendEndpoints/enableHttps/action",
      "Microsoft.Network/frontDoors/frontendEndpoints/disableHttps/action",
      "Microsoft.Network/frontDoors/loadBalancingSettings/read",
      "Microsoft.Network/frontDoors/loadBalancingSettings/write",
      "Microsoft.Network/frontDoors/loadBalancingSettings/delete",
      "Microsoft.Network/frontDoors/healthProbeSettings/read",
      "Microsoft.Network/frontDoors/healthProbeSettings/write",
      "Microsoft.Network/frontDoors/healthProbeSettings/delete",
      "Microsoft.Network/frontDoorWebApplicationFirewallPolicies/read",
      "Microsoft.Network/frontDoorWebApplicationFirewallPolicies/write",
      "Microsoft.Network/frontDoorWebApplicationFirewallPolicies/delete",
      "Microsoft.Network/frontDoorWebApplicationFirewallManagedRuleSets/read",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "azure_front_door_service_readonly_definition" {
  name              = "azure-front-door-service-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Network/frontDoors/read",
      "Microsoft.Network/frontDoors/routingRules/read",
      "Microsoft.Network/frontDoors/backendPools/read",
      "Microsoft.Network/frontDoors/frontendEndpoints/read",
      "Microsoft.Network/frontDoors/loadBalancingSettings/read",
      "Microsoft.Network/frontDoors/healthProbeSettings/read",
      "Microsoft.Network/frontDoorWebApplicationFirewallPolicies/read",
      "Microsoft.Network/frontDoorWebApplicationFirewallManagedRuleSets/read",
    ]

    not_actions = [
      "Microsoft.Network/frontDoors/write",
      "Microsoft.Network/frontDoors/delete",
      "Microsoft.Network/frontDoors/purge/action",
      "Microsoft.Network/frontDoors/validateCustomDomain/action",
      "Microsoft.Network/frontDoors/routingRules/write",
      "Microsoft.Network/frontDoors/routingRules/delete",
      "Microsoft.Network/frontDoors/backendPools/write",
      "Microsoft.Network/frontDoors/backendPools/delete",
      "Microsoft.Network/frontDoors/frontendEndpoints/write",
      "Microsoft.Network/frontDoors/frontendEndpoints/delete",
      "Microsoft.Network/frontDoors/frontendEndpoints/enableHttps/action",
      "Microsoft.Network/frontDoors/frontendEndpoints/disableHttps/action",
      "Microsoft.Network/frontDoors/loadBalancingSettings/write",
      "Microsoft.Network/frontDoors/loadBalancingSettings/delete",
      "Microsoft.Network/frontDoors/healthProbeSettings/write",
      "Microsoft.Network/frontDoors/healthProbeSettings/delete",
      "Microsoft.Network/frontDoorWebApplicationFirewallPolicies/write",
      "Microsoft.Network/frontDoorWebApplicationFirewallPolicies/delete",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "azure_front_door_service_noaccess_definition" {
  name              = "azure-front-door-service-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.Network/frontDoors/read",
      "Microsoft.Network/frontDoors/write",
      "Microsoft.Network/frontDoors/delete",
      "Microsoft.Network/frontDoors/purge/action",
      "Microsoft.Network/frontDoors/validateCustomDomain/action",
      "Microsoft.Network/frontDoors/routingRules/read",
      "Microsoft.Network/frontDoors/routingRules/write",
      "Microsoft.Network/frontDoors/routingRules/delete",
      "Microsoft.Network/frontDoors/backendPools/read",
      "Microsoft.Network/frontDoors/backendPools/write",
      "Microsoft.Network/frontDoors/backendPools/delete",
      "Microsoft.Network/frontDoors/frontendEndpoints/read",
      "Microsoft.Network/frontDoors/frontendEndpoints/write",
      "Microsoft.Network/frontDoors/frontendEndpoints/delete",
      "Microsoft.Network/frontDoors/frontendEndpoints/enableHttps/action",
      "Microsoft.Network/frontDoors/frontendEndpoints/disableHttps/action",
      "Microsoft.Network/frontDoors/loadBalancingSettings/read",
      "Microsoft.Network/frontDoors/loadBalancingSettings/write",
      "Microsoft.Network/frontDoors/loadBalancingSettings/delete",
      "Microsoft.Network/frontDoors/healthProbeSettings/read",
      "Microsoft.Network/frontDoors/healthProbeSettings/write",
      "Microsoft.Network/frontDoors/healthProbeSettings/delete",
      "Microsoft.Network/frontDoorWebApplicationFirewallPolicies/read",
      "Microsoft.Network/frontDoorWebApplicationFirewallPolicies/write",
      "Microsoft.Network/frontDoorWebApplicationFirewallPolicies/delete",
      "Microsoft.Network/frontDoorWebApplicationFirewallManagedRuleSets/read",
    ]
  }
}
