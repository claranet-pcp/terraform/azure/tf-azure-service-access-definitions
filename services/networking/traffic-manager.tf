// FullAccess
resource "azurerm_role_definition" "traffic_manager_fullaccess_definition" {
  name              = "traffic-manager-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Network/trafficManagerProfiles/read",
      "Microsoft.Network/trafficManagerProfiles/write",
      "Microsoft.Network/trafficManagerProfiles/delete",
      "Microsoft.Network/trafficManagerGeographicHierarchies/read",
      "Microsoft.Network/trafficManagerProfiles/heatMaps/read",
      "Microsoft.Network/trafficManagerProfiles/azureEndpoints/read",
      "Microsoft.Network/trafficManagerProfiles/azureEndpoints/write",
      "Microsoft.Network/trafficManagerProfiles/azureEndpoints/delete",
      "Microsoft.Network/trafficManagerProfiles/externalEndpoints/read",
      "Microsoft.Network/trafficManagerProfiles/externalEndpoints/write",
      "Microsoft.Network/trafficManagerProfiles/externalEndpoints/delete",
      "Microsoft.Network/trafficManagerProfiles/nestedEndpoints/read",
      "Microsoft.Network/trafficManagerProfiles/nestedEndpoints/write",
      "Microsoft.Network/trafficManagerProfiles/nestedEndpoints/delete",
      "Microsoft.Network/trafficManagerUserMetricsKeys/read",
      "Microsoft.Network/trafficManagerUserMetricsKeys/write",
      "Microsoft.Network/trafficManagerUserMetricsKeys/delete",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "traffic_manager_readonly_definition" {
  name              = "traffic-manager-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Network/trafficManagerProfiles/read",
      "Microsoft.Network/trafficManagerGeographicHierarchies/read",
      "Microsoft.Network/trafficManagerProfiles/heatMaps/read",
      "Microsoft.Network/trafficManagerProfiles/azureEndpoints/read",
      "Microsoft.Network/trafficManagerProfiles/externalEndpoints/read",
      "Microsoft.Network/trafficManagerProfiles/nestedEndpoints/read",
      "Microsoft.Network/trafficManagerUserMetricsKeys/read",
    ]

    not_actions = [
      "Microsoft.Network/trafficManagerProfiles/write",
      "Microsoft.Network/trafficManagerProfiles/delete",
      "Microsoft.Network/trafficManagerProfiles/azureEndpoints/write",
      "Microsoft.Network/trafficManagerProfiles/azureEndpoints/delete",
      "Microsoft.Network/trafficManagerProfiles/externalEndpoints/write",
      "Microsoft.Network/trafficManagerProfiles/externalEndpoints/delete",
      "Microsoft.Network/trafficManagerProfiles/nestedEndpoints/write",
      "Microsoft.Network/trafficManagerProfiles/nestedEndpoints/delete",
      "Microsoft.Network/trafficManagerUserMetricsKeys/write",
      "Microsoft.Network/trafficManagerUserMetricsKeys/delete",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "traffic_manager_noaccess_definition" {
  name              = "traffic-manager-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.Network/trafficManagerProfiles/read",
      "Microsoft.Network/trafficManagerProfiles/write",
      "Microsoft.Network/trafficManagerProfiles/delete",
      "Microsoft.Network/trafficManagerGeographicHierarchies/read",
      "Microsoft.Network/trafficManagerProfiles/heatMaps/read",
      "Microsoft.Network/trafficManagerProfiles/azureEndpoints/read",
      "Microsoft.Network/trafficManagerProfiles/azureEndpoints/write",
      "Microsoft.Network/trafficManagerProfiles/azureEndpoints/delete",
      "Microsoft.Network/trafficManagerProfiles/externalEndpoints/read",
      "Microsoft.Network/trafficManagerProfiles/externalEndpoints/write",
      "Microsoft.Network/trafficManagerProfiles/externalEndpoints/delete",
      "Microsoft.Network/trafficManagerProfiles/nestedEndpoints/read",
      "Microsoft.Network/trafficManagerProfiles/nestedEndpoints/write",
      "Microsoft.Network/trafficManagerProfiles/nestedEndpoints/delete",
      "Microsoft.Network/trafficManagerUserMetricsKeys/read",
      "Microsoft.Network/trafficManagerUserMetricsKeys/write",
      "Microsoft.Network/trafficManagerUserMetricsKeys/delete",
    ]
  }
}
