// FullAccess
resource "azurerm_role_definition" "microsoft_azure_portal_fullaccess_definition" {
  name              = "microsoft-azure-portal-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Portal/register/action",
      "Microsoft.Portal/dashboards/read",
      "Microsoft.Portal/dashboards/write",
      "Microsoft.Portal/dashboards/delete",
      "Microsoft.Portal/consoles/delete",
      "Microsoft.Portal/consoles/write",
      "Microsoft.Portal/consoles/read",
      "Microsoft.Portal/usersettings/delete",
      "Microsoft.Portal/usersettings/write",
      "Microsoft.Portal/usersettings/read",
      "Microsoft.ResourceGraph/resources/read",
      "Microsoft.ResourceGraph/resourceChanges/read",
      "Microsoft.ResourceGraph/resourceChangeDetails/read",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "microsoft_azure_portal_readonly_definition" {
  name              = "microsoft-azure-portal-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Portal/dashboards/read",
      "Microsoft.Portal/consoles/read",
      "Microsoft.Portal/usersettings/read",
      "Microsoft.ResourceGraph/resources/read",
      "Microsoft.ResourceGraph/resourceChanges/read",
      "Microsoft.ResourceGraph/resourceChangeDetails/read",
    ]

    not_actions = [
      "Microsoft.Portal/register/action",
      "Microsoft.Portal/dashboards/write",
      "Microsoft.Portal/dashboards/delete",
      "Microsoft.Portal/consoles/delete",
      "Microsoft.Portal/consoles/write",
      "Microsoft.Portal/usersettings/delete",
      "Microsoft.Portal/usersettings/write",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "microsoft_azure_portal_noaccess_definition" {
  name              = "microsoft-azure-portal-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.Portal/register/action",
      "Microsoft.Portal/dashboards/read",
      "Microsoft.Portal/dashboards/write",
      "Microsoft.Portal/dashboards/delete",
      "Microsoft.Portal/consoles/delete",
      "Microsoft.Portal/consoles/write",
      "Microsoft.Portal/consoles/read",
      "Microsoft.Portal/usersettings/delete",
      "Microsoft.Portal/usersettings/write",
      "Microsoft.Portal/usersettings/read",
      "Microsoft.ResourceGraph/resources/read",
      "Microsoft.ResourceGraph/resourceChanges/read",
      "Microsoft.ResourceGraph/resourceChangeDetails/read",
    ]
  }
}
