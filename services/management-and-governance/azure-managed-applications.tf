// FullAccess
resource "azurerm_role_definition" "azure_managed_applications_fullaccess_definition" {
  name              = "azure-managed-applications-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Solutions/register/action",
      "Microsoft.Solutions/unregister/action",
      "Microsoft.Solutions/applicationDefinitions/read",
      "Microsoft.Solutions/applicationDefinitions/write",
      "Microsoft.Solutions/applicationDefinitions/delete",
      "Microsoft.Solutions/applications/read",
      "Microsoft.Solutions/applications/write",
      "Microsoft.Solutions/applications/delete",
      "Microsoft.Solutions/applications/refreshPermissions/action",
      "Microsoft.Solutions/applications/updateAccess/action",
      "Microsoft.Solutions/locations/operationStatuses/read",
      "Microsoft.Solutions/jitRequests/read",
      "Microsoft.Solutions/jitRequests/write",
      "Microsoft.Solutions/jitRequests/delete",
      "Microsoft.Solutions/applicationDefinitions/applicationArtifacts/read",
      "Microsoft.Solutions/applications/applicationArtifacts/read",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "azure_managed_applications_readonly_definition" {
  name              = "azure-managed-applications-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Solutions/applicationDefinitions/read",
      "Microsoft.Solutions/applications/read",
      "Microsoft.Solutions/locations/operationStatuses/read",
      "Microsoft.Solutions/jitRequests/read",
      "Microsoft.Solutions/applicationDefinitions/applicationArtifacts/read",
      "Microsoft.Solutions/applications/applicationArtifacts/read",
    ]

    not_actions = [
      "Microsoft.Solutions/register/action",
      "Microsoft.Solutions/unregister/action",
      "Microsoft.Solutions/applicationDefinitions/write",
      "Microsoft.Solutions/applicationDefinitions/delete",
      "Microsoft.Solutions/applications/write",
      "Microsoft.Solutions/applications/delete",
      "Microsoft.Solutions/applications/refreshPermissions/action",
      "Microsoft.Solutions/applications/updateAccess/action",
      "Microsoft.Solutions/jitRequests/write",
      "Microsoft.Solutions/jitRequests/delete",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "azure_managed_applications_noaccess_definition" {
  name              = "azure-managed-applications-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.Solutions/register/action",
      "Microsoft.Solutions/unregister/action",
      "Microsoft.Solutions/applicationDefinitions/read",
      "Microsoft.Solutions/applicationDefinitions/write",
      "Microsoft.Solutions/applicationDefinitions/delete",
      "Microsoft.Solutions/applications/read",
      "Microsoft.Solutions/applications/write",
      "Microsoft.Solutions/applications/delete",
      "Microsoft.Solutions/applications/refreshPermissions/action",
      "Microsoft.Solutions/applications/updateAccess/action",
      "Microsoft.Solutions/locations/operationStatuses/read",
      "Microsoft.Solutions/jitRequests/read",
      "Microsoft.Solutions/jitRequests/write",
      "Microsoft.Solutions/jitRequests/delete",
      "Microsoft.Solutions/applicationDefinitions/applicationArtifacts/read",
      "Microsoft.Solutions/applications/applicationArtifacts/read",
    ]
  }
}
