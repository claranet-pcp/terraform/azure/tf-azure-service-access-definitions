// FullAccess
resource "azurerm_role_definition" "azure_blueprints_fullaccess_definition" {
  name              = "azure-blueprints-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Blueprint/register/action",
      "Microsoft.Blueprint/blueprints/read",
      "Microsoft.Blueprint/blueprints/write",
      "Microsoft.Blueprint/blueprints/delete",
      "Microsoft.Blueprint/blueprints/artifacts/read",
      "Microsoft.Blueprint/blueprints/artifacts/write",
      "Microsoft.Blueprint/blueprints/artifacts/delete",
      "Microsoft.Blueprint/blueprints/versions/read",
      "Microsoft.Blueprint/blueprints/versions/write",
      "Microsoft.Blueprint/blueprints/versions/delete",
      "Microsoft.Blueprint/blueprints/versions/artifacts/read",
      "Microsoft.Blueprint/blueprintAssignments/read",
      "Microsoft.Blueprint/blueprintAssignments/write",
      "Microsoft.Blueprint/blueprintAssignments/delete",
      "Microsoft.Blueprint/blueprintAssignments/whoisblueprint/action",
      "Microsoft.Blueprint/blueprintAssignments/assignmentOperations/read",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "azure_blueprints_readonly_definition" {
  name              = "azure-blueprints-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Blueprint/blueprints/read",
      "Microsoft.Blueprint/blueprints/artifacts/read",
      "Microsoft.Blueprint/blueprints/versions/read",
      "Microsoft.Blueprint/blueprints/versions/artifacts/read",
      "Microsoft.Blueprint/blueprintAssignments/read",
      "Microsoft.Blueprint/blueprintAssignments/assignmentOperations/read",
    ]

    not_actions = [
      "Microsoft.Blueprint/register/action",
      "Microsoft.Blueprint/blueprints/write",
      "Microsoft.Blueprint/blueprints/delete",
      "Microsoft.Blueprint/blueprints/artifacts/write",
      "Microsoft.Blueprint/blueprints/artifacts/delete",
      "Microsoft.Blueprint/blueprints/versions/write",
      "Microsoft.Blueprint/blueprints/versions/delete",
      "Microsoft.Blueprint/blueprintAssignments/write",
      "Microsoft.Blueprint/blueprintAssignments/delete",
      "Microsoft.Blueprint/blueprintAssignments/whoisblueprint/action",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "azure_blueprints_noaccess_definition" {
  name              = "azure-blueprints-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.Blueprint/register/action",
      "Microsoft.Blueprint/blueprints/read",
      "Microsoft.Blueprint/blueprints/write",
      "Microsoft.Blueprint/blueprints/delete",
      "Microsoft.Blueprint/blueprints/artifacts/read",
      "Microsoft.Blueprint/blueprints/artifacts/write",
      "Microsoft.Blueprint/blueprints/artifacts/delete",
      "Microsoft.Blueprint/blueprints/versions/read",
      "Microsoft.Blueprint/blueprints/versions/write",
      "Microsoft.Blueprint/blueprints/versions/delete",
      "Microsoft.Blueprint/blueprints/versions/artifacts/read",
      "Microsoft.Blueprint/blueprintAssignments/read",
      "Microsoft.Blueprint/blueprintAssignments/write",
      "Microsoft.Blueprint/blueprintAssignments/delete",
      "Microsoft.Blueprint/blueprintAssignments/whoisblueprint/action",
      "Microsoft.Blueprint/blueprintAssignments/assignmentOperations/read",
    ]
  }
}
