// FullAccess
resource "azurerm_role_definition" "cost_management_fullaccess_definition" {
  name              = "cost-management-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Consumption/register/action",
      "Microsoft.Consumption/usageDetails/read",
      "Microsoft.Consumption/reservationSummaries/read",
      "Microsoft.Consumption/reservationDetails/read",
      "Microsoft.Consumption/reservationTransactions/read",
      "Microsoft.Consumption/balances/read",
      "Microsoft.Consumption/pricesheets/read",
      "Microsoft.Consumption/marketplaces/read",
      "Microsoft.Consumption/budgets/read",
      "Microsoft.Consumption/budgets/write",
      "Microsoft.Consumption/budgets/delete",
      "Microsoft.Consumption/tenants/register/action",
      "Microsoft.Consumption/tenants/read",
      "Microsoft.Consumption/terms/read",
      "Microsoft.Consumption/reservationRecommendations/read",
      "Microsoft.Consumption/operations/read",
      "Microsoft.Consumption/charges/read",
      "Microsoft.Consumption/credits/read",
      "Microsoft.Consumption/events/read",
      "Microsoft.Consumption/forecasts/read",
      "Microsoft.Consumption/lots/read",
      "Microsoft.Consumption/operationstatus/read",
      "Microsoft.Consumption/operationresults/read",
      "Microsoft.Consumption/tags/read",
      "Microsoft.CostManagement/query/action",
      "Microsoft.CostManagement/reports/action",
      "Microsoft.CostManagement/exports/action",
      "Microsoft.CostManagement/register/action",
      "Microsoft.CostManagement/dimensions/read",
      "Microsoft.CostManagement/query/read",
      "Microsoft.CostManagement/reports/read",
      "Microsoft.CostManagement/exports/read",
      "Microsoft.CostManagement/exports/write",
      "Microsoft.CostManagement/exports/delete",
      "Microsoft.CostManagement/exports/run/action",
      "Microsoft.CostManagement/tenants/register/action",
      "Microsoft.CostManagement/cloudConnectors/read",
      "Microsoft.CostManagement/cloudConnectors/write",
      "Microsoft.CostManagement/cloudConnectors/delete",
      "Microsoft.CostManagement/externalBillingAccounts/read",
      "Microsoft.CostManagement/externalSubscriptions/read",
      "Microsoft.CostManagement/externalSubscriptions/write",
      "Microsoft.CostManagement/externalBillingAccounts/externalSubscriptions/read",
      "Microsoft.CostManagementExports/register/action",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "cost_management_readonly_definition" {
  name              = "cost-management-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Consumption/usageDetails/read",
      "Microsoft.Consumption/reservationSummaries/read",
      "Microsoft.Consumption/reservationDetails/read",
      "Microsoft.Consumption/reservationTransactions/read",
      "Microsoft.Consumption/balances/read",
      "Microsoft.Consumption/pricesheets/read",
      "Microsoft.Consumption/marketplaces/read",
      "Microsoft.Consumption/budgets/read",
      "Microsoft.Consumption/tenants/read",
      "Microsoft.Consumption/terms/read",
      "Microsoft.Consumption/reservationRecommendations/read",
      "Microsoft.Consumption/operations/read",
      "Microsoft.Consumption/charges/read",
      "Microsoft.Consumption/credits/read",
      "Microsoft.Consumption/events/read",
      "Microsoft.Consumption/forecasts/read",
      "Microsoft.Consumption/lots/read",
      "Microsoft.Consumption/operationstatus/read",
      "Microsoft.Consumption/operationresults/read",
      "Microsoft.Consumption/tags/read",
      "Microsoft.CostManagement/dimensions/read",
      "Microsoft.CostManagement/query/read",
      "Microsoft.CostManagement/reports/read",
      "Microsoft.CostManagement/exports/read",
      "Microsoft.CostManagement/cloudConnectors/read",
      "Microsoft.CostManagement/externalBillingAccounts/read",
      "Microsoft.CostManagement/externalSubscriptions/read",
      "Microsoft.CostManagement/externalBillingAccounts/externalSubscriptions/read",
    ]

    not_actions = [
      "Microsoft.Consumption/register/action",
      "Microsoft.Consumption/budgets/write",
      "Microsoft.Consumption/budgets/delete",
      "Microsoft.Consumption/tenants/register/action",
      "Microsoft.CostManagement/query/action",
      "Microsoft.CostManagement/reports/action",
      "Microsoft.CostManagement/exports/action",
      "Microsoft.CostManagement/register/action",
      "Microsoft.CostManagement/exports/write",
      "Microsoft.CostManagement/exports/delete",
      "Microsoft.CostManagement/exports/run/action",
      "Microsoft.CostManagement/tenants/register/action",
      "Microsoft.CostManagement/cloudConnectors/write",
      "Microsoft.CostManagement/cloudConnectors/delete",
      "Microsoft.CostManagement/externalSubscriptions/write",
      "Microsoft.CostManagementExports/register/action",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "cost_management_noaccess_definition" {
  name              = "cost-management-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.Consumption/register/action",
      "Microsoft.Consumption/usageDetails/read",
      "Microsoft.Consumption/reservationSummaries/read",
      "Microsoft.Consumption/reservationDetails/read",
      "Microsoft.Consumption/reservationTransactions/read",
      "Microsoft.Consumption/balances/read",
      "Microsoft.Consumption/pricesheets/read",
      "Microsoft.Consumption/marketplaces/read",
      "Microsoft.Consumption/budgets/read",
      "Microsoft.Consumption/budgets/write",
      "Microsoft.Consumption/budgets/delete",
      "Microsoft.Consumption/tenants/register/action",
      "Microsoft.Consumption/tenants/read",
      "Microsoft.Consumption/terms/read",
      "Microsoft.Consumption/reservationRecommendations/read",
      "Microsoft.Consumption/operations/read",
      "Microsoft.Consumption/charges/read",
      "Microsoft.Consumption/credits/read",
      "Microsoft.Consumption/events/read",
      "Microsoft.Consumption/forecasts/read",
      "Microsoft.Consumption/lots/read",
      "Microsoft.Consumption/operationstatus/read",
      "Microsoft.Consumption/operationresults/read",
      "Microsoft.Consumption/tags/read",
      "Microsoft.CostManagement/query/action",
      "Microsoft.CostManagement/reports/action",
      "Microsoft.CostManagement/exports/action",
      "Microsoft.CostManagement/register/action",
      "Microsoft.CostManagement/dimensions/read",
      "Microsoft.CostManagement/query/read",
      "Microsoft.CostManagement/reports/read",
      "Microsoft.CostManagement/exports/read",
      "Microsoft.CostManagement/exports/write",
      "Microsoft.CostManagement/exports/delete",
      "Microsoft.CostManagement/exports/run/action",
      "Microsoft.CostManagement/tenants/register/action",
      "Microsoft.CostManagement/cloudConnectors/read",
      "Microsoft.CostManagement/cloudConnectors/write",
      "Microsoft.CostManagement/cloudConnectors/delete",
      "Microsoft.CostManagement/externalBillingAccounts/read",
      "Microsoft.CostManagement/externalSubscriptions/read",
      "Microsoft.CostManagement/externalSubscriptions/write",
      "Microsoft.CostManagement/externalBillingAccounts/externalSubscriptions/read",
      "Microsoft.CostManagementExports/register/action",
    ]
  }
}
