// FullAccess
resource "azurerm_role_definition" "azure_service_health_fullaccess_definition" {
  name              = "azure-service-health-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.ADHybridHealthService/configuration/action",
      "Microsoft.ADHybridHealthService/services/action",
      "Microsoft.ADHybridHealthService/addsservices/action",
      "Microsoft.ADHybridHealthService/register/action",
      "Microsoft.ADHybridHealthService/unregister/action",
      "Microsoft.ADHybridHealthService/configuration/write",
      "Microsoft.ADHybridHealthService/configuration/read",
      "Microsoft.ADHybridHealthService/services/write",
      "Microsoft.ADHybridHealthService/services/read",
      "Microsoft.ADHybridHealthService/services/delete",
      "Microsoft.ADHybridHealthService/services/servicemembers/action",
      "Microsoft.ADHybridHealthService/services/servicemembers/read",
      "Microsoft.ADHybridHealthService/services/servicemembers/delete",
      "Microsoft.ADHybridHealthService/services/servicemembers/alerts/read",
      "Microsoft.ADHybridHealthService/services/alerts/read",
      "Microsoft.ADHybridHealthService/services/alerts/read",
      "Microsoft.ADHybridHealthService/services/tenantwhitelisting/read",
      "Microsoft.ADHybridHealthService/services/servicemembers/serviceconfiguration/read",
      "Microsoft.ADHybridHealthService/services/servicemembers/metrics/groups/read",
      "Microsoft.ADHybridHealthService/services/servicemembers/exportstatus/read",
      "Microsoft.ADHybridHealthService/services/servicemembers/datafreshness/read",
      "Microsoft.ADHybridHealthService/services/servicemembers/credentials/read",
      "Microsoft.ADHybridHealthService/services/reports/details/read",
      "Microsoft.ADHybridHealthService/services/premiumcheck/read",
      "Microsoft.ADHybridHealthService/services/monitoringconfigurations/read",
      "Microsoft.ADHybridHealthService/services/monitoringconfigurations/write",
      "Microsoft.ADHybridHealthService/services/monitoringconfiguration/write",
      "Microsoft.ADHybridHealthService/services/metrics/groups/sum/read",
      "Microsoft.ADHybridHealthService/services/metrics/groups/read",
      "Microsoft.ADHybridHealthService/services/metrics/groups/average/read",
      "Microsoft.ADHybridHealthService/services/metricmetadata/read",
      "Microsoft.ADHybridHealthService/services/exportstatus/read",
      "Microsoft.ADHybridHealthService/services/exporterrors/read",
      "Microsoft.ADHybridHealthService/services/checkservicefeatureavailibility/read",
      "Microsoft.ADHybridHealthService/reports/updateselecteddeployment/read",
      "Microsoft.ADHybridHealthService/reports/tenantassigneddeployment/read",
      "Microsoft.ADHybridHealthService/reports/selecteddeployment/read",
      "Microsoft.ADHybridHealthService/reports/selectdevopstenant/read",
      "Microsoft.ADHybridHealthService/reports/isdevops/read",
      "Microsoft.ADHybridHealthService/reports/consentedtodevopstenants/read",
      "Microsoft.ADHybridHealthService/reports/badpassworduseridipfrequency/read",
      "Microsoft.ADHybridHealthService/reports/availabledeployments/read",
      "Microsoft.ADHybridHealthService/reports/badpassword/read",
      "Microsoft.ADHybridHealthService/logs/read",
      "Microsoft.ADHybridHealthService/logs/contents/read",
      "Microsoft.ADHybridHealthService/addsservices/write",
      "Microsoft.ADHybridHealthService/addsservices/servicemembers/action",
      "Microsoft.ADHybridHealthService/addsservices/read",
      "Microsoft.ADHybridHealthService/addsservices/delete",
      "Microsoft.ADHybridHealthService/addsservices/servicemembers/credentials/read",
      "Microsoft.ADHybridHealthService/addsservices/servicemembers/delete",
      "Microsoft.ADHybridHealthService/addsservices/replicationsummary/read",
      "Microsoft.ADHybridHealthService/addsservices/replicationstatus/read",
      "Microsoft.ADHybridHealthService/addsservices/premiumcheck/read",
      "Microsoft.ADHybridHealthService/addsservices/metrics/groups/read",
      "Microsoft.ADHybridHealthService/addsservices/metricmetadata/read",
      "Microsoft.ADHybridHealthService/addsservices/forestsummary/read",
      "Microsoft.ADHybridHealthService/addsservices/features/userpreference/read",
      "Microsoft.ADHybridHealthService/addsservices/dimensions/read",
      "Microsoft.ADHybridHealthService/addsservices/configuration/read",
      "Microsoft.ADHybridHealthService/addsservices/alerts/read",
      "Microsoft.ADHybridHealthService/services/feedbacktype/feedback/read",
      "Microsoft.ADHybridHealthService/addsservices/addomainservicemembers/read",
      "Microsoft.ADHybridHealthService/addsservices/replicationdetails/read",
      "Microsoft.ADHybridHealthService/operations/read",
      "Microsoft.ADHybridHealthService/services/servicemembers/metrics/read",
      "Microsoft.ADHybridHealthService/services/reports/blobUris/read",
      "Microsoft.ADHybridHealthService/services/reports/generateBlobUri/action",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "azure_service_health_readonly_definition" {
  name              = "azure-service-health-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.ADHybridHealthService/configuration/read",
      "Microsoft.ADHybridHealthService/services/read",
      "Microsoft.ADHybridHealthService/services/servicemembers/read",
      "Microsoft.ADHybridHealthService/services/servicemembers/alerts/read",
      "Microsoft.ADHybridHealthService/services/alerts/read",
      "Microsoft.ADHybridHealthService/services/alerts/read",
      "Microsoft.ADHybridHealthService/services/tenantwhitelisting/read",
      "Microsoft.ADHybridHealthService/services/servicemembers/serviceconfiguration/read",
      "Microsoft.ADHybridHealthService/services/servicemembers/metrics/groups/read",
      "Microsoft.ADHybridHealthService/services/servicemembers/exportstatus/read",
      "Microsoft.ADHybridHealthService/services/servicemembers/datafreshness/read",
      "Microsoft.ADHybridHealthService/services/servicemembers/credentials/read",
      "Microsoft.ADHybridHealthService/services/reports/details/read",
      "Microsoft.ADHybridHealthService/services/premiumcheck/read",
      "Microsoft.ADHybridHealthService/services/monitoringconfigurations/read",
      "Microsoft.ADHybridHealthService/services/metrics/groups/sum/read",
      "Microsoft.ADHybridHealthService/services/metrics/groups/read",
      "Microsoft.ADHybridHealthService/services/metrics/groups/average/read",
      "Microsoft.ADHybridHealthService/services/metricmetadata/read",
      "Microsoft.ADHybridHealthService/services/exportstatus/read",
      "Microsoft.ADHybridHealthService/services/exporterrors/read",
      "Microsoft.ADHybridHealthService/services/checkservicefeatureavailibility/read",
      "Microsoft.ADHybridHealthService/reports/updateselecteddeployment/read",
      "Microsoft.ADHybridHealthService/reports/tenantassigneddeployment/read",
      "Microsoft.ADHybridHealthService/reports/selecteddeployment/read",
      "Microsoft.ADHybridHealthService/reports/selectdevopstenant/read",
      "Microsoft.ADHybridHealthService/reports/isdevops/read",
      "Microsoft.ADHybridHealthService/reports/consentedtodevopstenants/read",
      "Microsoft.ADHybridHealthService/reports/badpassworduseridipfrequency/read",
      "Microsoft.ADHybridHealthService/reports/availabledeployments/read",
      "Microsoft.ADHybridHealthService/reports/badpassword/read",
      "Microsoft.ADHybridHealthService/logs/read",
      "Microsoft.ADHybridHealthService/logs/contents/read",
      "Microsoft.ADHybridHealthService/addsservices/read",
      "Microsoft.ADHybridHealthService/addsservices/servicemembers/credentials/read",
      "Microsoft.ADHybridHealthService/addsservices/replicationsummary/read",
      "Microsoft.ADHybridHealthService/addsservices/replicationstatus/read",
      "Microsoft.ADHybridHealthService/addsservices/premiumcheck/read",
      "Microsoft.ADHybridHealthService/addsservices/metrics/groups/read",
      "Microsoft.ADHybridHealthService/addsservices/metricmetadata/read",
      "Microsoft.ADHybridHealthService/addsservices/forestsummary/read",
      "Microsoft.ADHybridHealthService/addsservices/features/userpreference/read",
      "Microsoft.ADHybridHealthService/addsservices/dimensions/read",
      "Microsoft.ADHybridHealthService/addsservices/configuration/read",
      "Microsoft.ADHybridHealthService/addsservices/alerts/read",
      "Microsoft.ADHybridHealthService/services/feedbacktype/feedback/read",
      "Microsoft.ADHybridHealthService/addsservices/addomainservicemembers/read",
      "Microsoft.ADHybridHealthService/addsservices/replicationdetails/read",
      "Microsoft.ADHybridHealthService/operations/read",
      "Microsoft.ADHybridHealthService/services/servicemembers/metrics/read",
      "Microsoft.ADHybridHealthService/services/reports/blobUris/read",
    ]

    not_actions = [
      "Microsoft.ADHybridHealthService/configuration/action",
      "Microsoft.ADHybridHealthService/services/action",
      "Microsoft.ADHybridHealthService/addsservices/action",
      "Microsoft.ADHybridHealthService/register/action",
      "Microsoft.ADHybridHealthService/unregister/action",
      "Microsoft.ADHybridHealthService/configuration/write",
      "Microsoft.ADHybridHealthService/services/write",
      "Microsoft.ADHybridHealthService/services/delete",
      "Microsoft.ADHybridHealthService/services/servicemembers/action",
      "Microsoft.ADHybridHealthService/services/servicemembers/delete",
      "Microsoft.ADHybridHealthService/services/monitoringconfigurations/write",
      "Microsoft.ADHybridHealthService/services/monitoringconfiguration/write",
      "Microsoft.ADHybridHealthService/addsservices/write",
      "Microsoft.ADHybridHealthService/addsservices/servicemembers/action",
      "Microsoft.ADHybridHealthService/addsservices/delete",
      "Microsoft.ADHybridHealthService/addsservices/servicemembers/delete",
      "Microsoft.ADHybridHealthService/services/reports/generateBlobUri/action",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "azure_service_health_noaccess_definition" {
  name              = "azure-service-health-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.ADHybridHealthService/configuration/action",
      "Microsoft.ADHybridHealthService/services/action",
      "Microsoft.ADHybridHealthService/addsservices/action",
      "Microsoft.ADHybridHealthService/register/action",
      "Microsoft.ADHybridHealthService/unregister/action",
      "Microsoft.ADHybridHealthService/configuration/write",
      "Microsoft.ADHybridHealthService/configuration/read",
      "Microsoft.ADHybridHealthService/services/write",
      "Microsoft.ADHybridHealthService/services/read",
      "Microsoft.ADHybridHealthService/services/delete",
      "Microsoft.ADHybridHealthService/services/servicemembers/action",
      "Microsoft.ADHybridHealthService/services/servicemembers/read",
      "Microsoft.ADHybridHealthService/services/servicemembers/delete",
      "Microsoft.ADHybridHealthService/services/servicemembers/alerts/read",
      "Microsoft.ADHybridHealthService/services/alerts/read",
      "Microsoft.ADHybridHealthService/services/alerts/read",
      "Microsoft.ADHybridHealthService/services/tenantwhitelisting/read",
      "Microsoft.ADHybridHealthService/services/servicemembers/serviceconfiguration/read",
      "Microsoft.ADHybridHealthService/services/servicemembers/metrics/groups/read",
      "Microsoft.ADHybridHealthService/services/servicemembers/exportstatus/read",
      "Microsoft.ADHybridHealthService/services/servicemembers/datafreshness/read",
      "Microsoft.ADHybridHealthService/services/servicemembers/credentials/read",
      "Microsoft.ADHybridHealthService/services/reports/details/read",
      "Microsoft.ADHybridHealthService/services/premiumcheck/read",
      "Microsoft.ADHybridHealthService/services/monitoringconfigurations/read",
      "Microsoft.ADHybridHealthService/services/monitoringconfigurations/write",
      "Microsoft.ADHybridHealthService/services/monitoringconfiguration/write",
      "Microsoft.ADHybridHealthService/services/metrics/groups/sum/read",
      "Microsoft.ADHybridHealthService/services/metrics/groups/read",
      "Microsoft.ADHybridHealthService/services/metrics/groups/average/read",
      "Microsoft.ADHybridHealthService/services/metricmetadata/read",
      "Microsoft.ADHybridHealthService/services/exportstatus/read",
      "Microsoft.ADHybridHealthService/services/exporterrors/read",
      "Microsoft.ADHybridHealthService/services/checkservicefeatureavailibility/read",
      "Microsoft.ADHybridHealthService/reports/updateselecteddeployment/read",
      "Microsoft.ADHybridHealthService/reports/tenantassigneddeployment/read",
      "Microsoft.ADHybridHealthService/reports/selecteddeployment/read",
      "Microsoft.ADHybridHealthService/reports/selectdevopstenant/read",
      "Microsoft.ADHybridHealthService/reports/isdevops/read",
      "Microsoft.ADHybridHealthService/reports/consentedtodevopstenants/read",
      "Microsoft.ADHybridHealthService/reports/badpassworduseridipfrequency/read",
      "Microsoft.ADHybridHealthService/reports/availabledeployments/read",
      "Microsoft.ADHybridHealthService/reports/badpassword/read",
      "Microsoft.ADHybridHealthService/logs/read",
      "Microsoft.ADHybridHealthService/logs/contents/read",
      "Microsoft.ADHybridHealthService/addsservices/write",
      "Microsoft.ADHybridHealthService/addsservices/servicemembers/action",
      "Microsoft.ADHybridHealthService/addsservices/read",
      "Microsoft.ADHybridHealthService/addsservices/delete",
      "Microsoft.ADHybridHealthService/addsservices/servicemembers/credentials/read",
      "Microsoft.ADHybridHealthService/addsservices/servicemembers/delete",
      "Microsoft.ADHybridHealthService/addsservices/replicationsummary/read",
      "Microsoft.ADHybridHealthService/addsservices/replicationstatus/read",
      "Microsoft.ADHybridHealthService/addsservices/premiumcheck/read",
      "Microsoft.ADHybridHealthService/addsservices/metrics/groups/read",
      "Microsoft.ADHybridHealthService/addsservices/metricmetadata/read",
      "Microsoft.ADHybridHealthService/addsservices/forestsummary/read",
      "Microsoft.ADHybridHealthService/addsservices/features/userpreference/read",
      "Microsoft.ADHybridHealthService/addsservices/dimensions/read",
      "Microsoft.ADHybridHealthService/addsservices/configuration/read",
      "Microsoft.ADHybridHealthService/addsservices/alerts/read",
      "Microsoft.ADHybridHealthService/services/feedbacktype/feedback/read",
      "Microsoft.ADHybridHealthService/addsservices/addomainservicemembers/read",
      "Microsoft.ADHybridHealthService/addsservices/replicationdetails/read",
      "Microsoft.ADHybridHealthService/operations/read",
      "Microsoft.ADHybridHealthService/services/servicemembers/metrics/read",
      "Microsoft.ADHybridHealthService/services/reports/blobUris/read",
      "Microsoft.ADHybridHealthService/services/reports/generateBlobUri/action",
    ]
  }
}
