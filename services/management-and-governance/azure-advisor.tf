// FullAccess
resource "azurerm_role_definition" "azure_advisor_fullaccess_definition" {
  name              = "azure-advisor-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Advisor/generateRecommendations/action",
      "Microsoft.Advisor/register/action",
      "Microsoft.Advisor/unregister/action",
      "Microsoft.Advisor/recommendations/read",
      "Microsoft.Advisor/recommendations/available/action",
      "Microsoft.Advisor/generateRecommendations/read",
      "Microsoft.Advisor/suppressions/read",
      "Microsoft.Advisor/suppressions/write",
      "Microsoft.Advisor/suppressions/delete",
      "Microsoft.Advisor/recommendations/suppressions/read",
      "Microsoft.Advisor/recommendations/suppressions/write",
      "Microsoft.Advisor/recommendations/suppressions/delete",
      "Microsoft.Advisor/configurations/read",
      "Microsoft.Advisor/configurations/write",
      "Microsoft.Advisor/operations/read",
      "Microsoft.Advisor/metadata/read",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "azure_advisor_readonly_definition" {
  name              = "azure-advisor-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Advisor/recommendations/read",
      "Microsoft.Advisor/generateRecommendations/read",
      "Microsoft.Advisor/suppressions/read",
      "Microsoft.Advisor/recommendations/suppressions/read",
      "Microsoft.Advisor/configurations/read",
      "Microsoft.Advisor/operations/read",
      "Microsoft.Advisor/metadata/read",
    ]

    not_actions = [
      "Microsoft.Advisor/generateRecommendations/action",
      "Microsoft.Advisor/register/action",
      "Microsoft.Advisor/unregister/action",
      "Microsoft.Advisor/recommendations/available/action",
      "Microsoft.Advisor/suppressions/write",
      "Microsoft.Advisor/suppressions/delete",
      "Microsoft.Advisor/recommendations/suppressions/write",
      "Microsoft.Advisor/recommendations/suppressions/delete",
      "Microsoft.Advisor/configurations/write",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "azure_advisor_noaccess_definition" {
  name              = "azure-advisor-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.Advisor/generateRecommendations/action",
      "Microsoft.Advisor/register/action",
      "Microsoft.Advisor/unregister/action",
      "Microsoft.Advisor/recommendations/read",
      "Microsoft.Advisor/recommendations/available/action",
      "Microsoft.Advisor/generateRecommendations/read",
      "Microsoft.Advisor/suppressions/read",
      "Microsoft.Advisor/suppressions/write",
      "Microsoft.Advisor/suppressions/delete",
      "Microsoft.Advisor/recommendations/suppressions/read",
      "Microsoft.Advisor/recommendations/suppressions/write",
      "Microsoft.Advisor/recommendations/suppressions/delete",
      "Microsoft.Advisor/configurations/read",
      "Microsoft.Advisor/configurations/write",
      "Microsoft.Advisor/operations/read",
      "Microsoft.Advisor/metadata/read",
    ]
  }
}
