// FullAccess
resource "azurerm_role_definition" "scheduler_fullaccess_definition" {
  name              = "scheduler-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Scheduler/jobcollections/read",
      "Microsoft.Scheduler/jobcollections/write",
      "Microsoft.Scheduler/jobcollections/delete",
      "Microsoft.Scheduler/jobcollections/enable/action",
      "Microsoft.Scheduler/jobcollections/disable/action",
      "Microsoft.Scheduler/jobcollections/jobs/read",
      "Microsoft.Scheduler/jobcollections/jobs/write",
      "Microsoft.Scheduler/jobcollections/jobs/delete",
      "Microsoft.Scheduler/jobcollections/jobs/run/action",
      "Microsoft.Scheduler/jobcollections/jobs/generateLogicAppDefinition/action",
      "Microsoft.Scheduler/jobcollections/jobs/jobhistories/read",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "scheduler_readonly_definition" {
  name              = "scheduler-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Scheduler/jobcollections/read",
      "Microsoft.Scheduler/jobcollections/jobs/read",
      "Microsoft.Scheduler/jobcollections/jobs/jobhistories/read",
    ]

    not_actions = [
      "Microsoft.Scheduler/jobcollections/write",
      "Microsoft.Scheduler/jobcollections/delete",
      "Microsoft.Scheduler/jobcollections/enable/action",
      "Microsoft.Scheduler/jobcollections/disable/action",
      "Microsoft.Scheduler/jobcollections/jobs/write",
      "Microsoft.Scheduler/jobcollections/jobs/delete",
      "Microsoft.Scheduler/jobcollections/jobs/run/action",
      "Microsoft.Scheduler/jobcollections/jobs/generateLogicAppDefinition/action",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "scheduler_noaccess_definition" {
  name              = "scheduler-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.Scheduler/jobcollections/read",
      "Microsoft.Scheduler/jobcollections/write",
      "Microsoft.Scheduler/jobcollections/delete",
      "Microsoft.Scheduler/jobcollections/enable/action",
      "Microsoft.Scheduler/jobcollections/disable/action",
      "Microsoft.Scheduler/jobcollections/jobs/read",
      "Microsoft.Scheduler/jobcollections/jobs/write",
      "Microsoft.Scheduler/jobcollections/jobs/delete",
      "Microsoft.Scheduler/jobcollections/jobs/run/action",
      "Microsoft.Scheduler/jobcollections/jobs/generateLogicAppDefinition/action",
      "Microsoft.Scheduler/jobcollections/jobs/jobhistories/read",
    ]
  }
}
