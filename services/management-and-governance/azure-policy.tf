// FullAccess
resource "azurerm_role_definition" "azure_policy_fullaccess_definition" {
  name              = "azure-policy-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.PolicyInsights/register/action",
      "Microsoft.PolicyInsights/unregister/action",
      "Microsoft.PolicyInsights/policyEvents/queryResults/action",
      "Microsoft.PolicyInsights/policyStates/queryResults/action",
      "Microsoft.PolicyInsights/policyStates/summarize/action",
      "Microsoft.PolicyInsights/policyStates/triggerEvaluation/action",
      "Microsoft.PolicyInsights/asyncOperationResults/read",
      "Microsoft.PolicyInsights/policyEvents/queryResults/read",
      "Microsoft.PolicyInsights/policyStates/queryResults/read",
      "Microsoft.PolicyInsights/policyStates/summarize/read",
      "Microsoft.PolicyInsights/remediations/read",
      "Microsoft.PolicyInsights/remediations/write",
      "Microsoft.PolicyInsights/remediations/delete",
      "Microsoft.PolicyInsights/remediations/cancel/action",
      "Microsoft.PolicyInsights/remediations/listDeployments/read",
      "Microsoft.PolicyInsights/policyTrackedResources/queryResults/read",
      "Microsoft.PolicyInsights/operations/read",
      "Microsoft.GuestConfiguration/register/action",
      "Microsoft.GuestConfiguration/guestConfigurationAssignments/write",
      "Microsoft.GuestConfiguration/guestConfigurationAssignments/read",
      "Microsoft.GuestConfiguration/guestConfigurationAssignments/delete",
      "Microsoft.GuestConfiguration/guestConfigurationAssignments/reports/read",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "azure_policy_readonly_definition" {
  name              = "azure-policy-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.PolicyInsights/asyncOperationResults/read",
      "Microsoft.PolicyInsights/policyEvents/queryResults/read",
      "Microsoft.PolicyInsights/policyStates/queryResults/read",
      "Microsoft.PolicyInsights/policyStates/summarize/read",
      "Microsoft.PolicyInsights/remediations/read",
      "Microsoft.PolicyInsights/remediations/listDeployments/read",
      "Microsoft.PolicyInsights/policyTrackedResources/queryResults/read",
      "Microsoft.PolicyInsights/operations/read",
      "Microsoft.GuestConfiguration/guestConfigurationAssignments/read",
      "Microsoft.GuestConfiguration/guestConfigurationAssignments/reports/read",
    ]

    not_actions = [
      "Microsoft.PolicyInsights/register/action",
      "Microsoft.PolicyInsights/unregister/action",
      "Microsoft.PolicyInsights/policyEvents/queryResults/action",
      "Microsoft.PolicyInsights/policyStates/queryResults/action",
      "Microsoft.PolicyInsights/policyStates/summarize/action",
      "Microsoft.PolicyInsights/policyStates/triggerEvaluation/action",
      "Microsoft.PolicyInsights/remediations/write",
      "Microsoft.PolicyInsights/remediations/delete",
      "Microsoft.PolicyInsights/remediations/cancel/action",
      "Microsoft.GuestConfiguration/register/action",
      "Microsoft.GuestConfiguration/guestConfigurationAssignments/write",
      "Microsoft.GuestConfiguration/guestConfigurationAssignments/delete",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "azure_policy_noaccess_definition" {
  name              = "azure-policy-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.PolicyInsights/register/action",
      "Microsoft.PolicyInsights/unregister/action",
      "Microsoft.PolicyInsights/policyEvents/queryResults/action",
      "Microsoft.PolicyInsights/policyStates/queryResults/action",
      "Microsoft.PolicyInsights/policyStates/summarize/action",
      "Microsoft.PolicyInsights/policyStates/triggerEvaluation/action",
      "Microsoft.PolicyInsights/asyncOperationResults/read",
      "Microsoft.PolicyInsights/policyEvents/queryResults/read",
      "Microsoft.PolicyInsights/policyStates/queryResults/read",
      "Microsoft.PolicyInsights/policyStates/summarize/read",
      "Microsoft.PolicyInsights/remediations/read",
      "Microsoft.PolicyInsights/remediations/write",
      "Microsoft.PolicyInsights/remediations/delete",
      "Microsoft.PolicyInsights/remediations/cancel/action",
      "Microsoft.PolicyInsights/remediations/listDeployments/read",
      "Microsoft.PolicyInsights/policyTrackedResources/queryResults/read",
      "Microsoft.PolicyInsights/operations/read",
      "Microsoft.GuestConfiguration/register/action",
      "Microsoft.GuestConfiguration/guestConfigurationAssignments/write",
      "Microsoft.GuestConfiguration/guestConfigurationAssignments/read",
      "Microsoft.GuestConfiguration/guestConfigurationAssignments/delete",
      "Microsoft.GuestConfiguration/guestConfigurationAssignments/reports/read",
    ]
  }
}
