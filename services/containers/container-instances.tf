// FullAccess
resource "azurerm_role_definition" "container_instances_fullaccess_definition" {
  name              = "container-instances-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.ContainerInstance/register/action",
      "Microsoft.ContainerInstance/containerGroups/read",
      "Microsoft.ContainerInstance/containerGroups/write",
      "Microsoft.ContainerInstance/containerGroups/delete",
      "Microsoft.ContainerInstance/containerGroups/restart/action",
      "Microsoft.ContainerInstance/containerGroups/stop/action",
      "Microsoft.ContainerInstance/containerGroups/start/action",
      "Microsoft.ContainerInstance/containerGroups/containers/logs/read",
      "Microsoft.ContainerInstance/locations/cachedImages/read",
      "Microsoft.ContainerInstance/locations/deleteVirtualNetworkOrSubnets/action",
      "Microsoft.ContainerInstance/containerGroups/containers/exec/action",
      "Microsoft.ContainerInstance/locations/capabilities/read",
      "Microsoft.ContainerInstance/locations/usages/read",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "container_instances_readonly_definition" {
  name              = "container-instances-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.ContainerInstance/containerGroups/read",
      "Microsoft.ContainerInstance/containerGroups/containers/logs/read",
      "Microsoft.ContainerInstance/locations/cachedImages/read",
      "Microsoft.ContainerInstance/locations/capabilities/read",
      "Microsoft.ContainerInstance/locations/usages/read",
    ]

    not_actions = [
      "Microsoft.ContainerInstance/register/action",
      "Microsoft.ContainerInstance/containerGroups/write",
      "Microsoft.ContainerInstance/containerGroups/delete",
      "Microsoft.ContainerInstance/containerGroups/restart/action",
      "Microsoft.ContainerInstance/containerGroups/stop/action",
      "Microsoft.ContainerInstance/containerGroups/start/action",
      "Microsoft.ContainerInstance/locations/deleteVirtualNetworkOrSubnets/action",
      "Microsoft.ContainerInstance/containerGroups/containers/exec/action",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "container_instances_noaccess_definition" {
  name              = "container-instances-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.ContainerInstance/register/action",
      "Microsoft.ContainerInstance/containerGroups/read",
      "Microsoft.ContainerInstance/containerGroups/write",
      "Microsoft.ContainerInstance/containerGroups/delete",
      "Microsoft.ContainerInstance/containerGroups/restart/action",
      "Microsoft.ContainerInstance/containerGroups/stop/action",
      "Microsoft.ContainerInstance/containerGroups/start/action",
      "Microsoft.ContainerInstance/containerGroups/containers/logs/read",
      "Microsoft.ContainerInstance/locations/cachedImages/read",
      "Microsoft.ContainerInstance/locations/deleteVirtualNetworkOrSubnets/action",
      "Microsoft.ContainerInstance/containerGroups/containers/exec/action",
      "Microsoft.ContainerInstance/locations/capabilities/read",
      "Microsoft.ContainerInstance/locations/usages/read",
    ]
  }
}
