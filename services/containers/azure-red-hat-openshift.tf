// FullAccess
resource "azurerm_role_definition" "azure_red_hat_openshift_fullaccess_definition" {
  name              = "azure-red-hat-openshift-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "azure_red_hat_openshift_readonly_definition" {
  name              = "azure-red-hat-openshift-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "",
    ]

    not_actions = [
      "",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "azure_red_hat_openshift_noaccess_definition" {
  name              = "azure-red-hat-openshift-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "",
    ]
  }
}
