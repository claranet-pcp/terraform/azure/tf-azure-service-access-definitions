// FullAccess
resource "azurerm_role_definition" "azure_kubernetes_service_fullaccess_definition" {
  name              = "azure-kubernetes-service-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.ContainerService/register/action",
      "Microsoft.ContainerService/unregister/action",
      "Microsoft.ContainerService/containerServices/read",
      "Microsoft.ContainerService/containerServices/write",
      "Microsoft.ContainerService/containerServices/delete",
      "Microsoft.ContainerService/managedClusters/read",
      "Microsoft.ContainerService/managedClusters/write",
      "Microsoft.ContainerService/managedClusters/delete",
      "Microsoft.ContainerService/managedClusters/listClusterAdminCredential/action",
      "Microsoft.ContainerService/managedClusters/listClusterUserCredential/action",
      "Microsoft.ContainerService/managedClusters/resetServicePrincipalProfile/action",
      "Microsoft.ContainerService/managedClusters/resetAADProfile/action",
      "Microsoft.ContainerService/managedClusters/accessProfiles/read",
      "Microsoft.ContainerService/managedClusters/accessProfiles/listCredential/action",
      "Microsoft.ContainerService/locations/operations/read",
      "Microsoft.ContainerService/locations/orchestrators/read",
      "Microsoft.ContainerService/managedClusters/upgradeprofiles/read",
      "Microsoft.ContainerService/operations/read",
      "Microsoft.ContainerService/locations/operationresults/read",
      "Microsoft.ContainerService/openShiftManagedClusters/read",
      "Microsoft.ContainerService/openShiftManagedClusters/write",
      "Microsoft.ContainerService/openShiftManagedClusters/delete",
      "Microsoft.ContainerService/openShiftClusters/read",
      "Microsoft.ContainerService/openShiftClusters/write",
      "Microsoft.ContainerService/openShiftClusters/delete",
      "Microsoft.ContainerService/managedClusters/agentPools/read",
      "Microsoft.ContainerService/managedClusters/agentPools/write",
      "Microsoft.ContainerService/managedClusters/agentPools/delete",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "azure_kubernetes_service_readonly_definition" {
  name              = "azure-kubernetes-service-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.ContainerService/containerServices/read",
      "Microsoft.ContainerService/managedClusters/read",
      "Microsoft.ContainerService/managedClusters/accessProfiles/read",
      "Microsoft.ContainerService/locations/operations/read",
      "Microsoft.ContainerService/locations/orchestrators/read",
      "Microsoft.ContainerService/managedClusters/upgradeprofiles/read",
      "Microsoft.ContainerService/operations/read",
      "Microsoft.ContainerService/locations/operationresults/read",
      "Microsoft.ContainerService/openShiftManagedClusters/read",
      "Microsoft.ContainerService/openShiftClusters/read",
      "Microsoft.ContainerService/managedClusters/agentPools/read",
    ]

    not_actions = [
      "Microsoft.ContainerService/register/action",
      "Microsoft.ContainerService/unregister/action",
      "Microsoft.ContainerService/containerServices/write",
      "Microsoft.ContainerService/containerServices/delete",
      "Microsoft.ContainerService/managedClusters/write",
      "Microsoft.ContainerService/managedClusters/delete",
      "Microsoft.ContainerService/managedClusters/listClusterAdminCredential/action",
      "Microsoft.ContainerService/managedClusters/listClusterUserCredential/action",
      "Microsoft.ContainerService/managedClusters/resetServicePrincipalProfile/action",
      "Microsoft.ContainerService/managedClusters/resetAADProfile/action",
      "Microsoft.ContainerService/managedClusters/accessProfiles/listCredential/action",
      "Microsoft.ContainerService/openShiftManagedClusters/write",
      "Microsoft.ContainerService/openShiftManagedClusters/delete",
      "Microsoft.ContainerService/openShiftClusters/write",
      "Microsoft.ContainerService/openShiftClusters/delete",
      "Microsoft.ContainerService/managedClusters/agentPools/write",
      "Microsoft.ContainerService/managedClusters/agentPools/delete",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "azure_kubernetes_service_noaccess_definition" {
  name              = "azure-kubernetes-service-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.ContainerService/register/action",
      "Microsoft.ContainerService/unregister/action",
      "Microsoft.ContainerService/containerServices/read",
      "Microsoft.ContainerService/containerServices/write",
      "Microsoft.ContainerService/containerServices/delete",
      "Microsoft.ContainerService/managedClusters/read",
      "Microsoft.ContainerService/managedClusters/write",
      "Microsoft.ContainerService/managedClusters/delete",
      "Microsoft.ContainerService/managedClusters/listClusterAdminCredential/action",
      "Microsoft.ContainerService/managedClusters/listClusterUserCredential/action",
      "Microsoft.ContainerService/managedClusters/resetServicePrincipalProfile/action",
      "Microsoft.ContainerService/managedClusters/resetAADProfile/action",
      "Microsoft.ContainerService/managedClusters/accessProfiles/read",
      "Microsoft.ContainerService/managedClusters/accessProfiles/listCredential/action",
      "Microsoft.ContainerService/locations/operations/read",
      "Microsoft.ContainerService/locations/orchestrators/read",
      "Microsoft.ContainerService/managedClusters/upgradeprofiles/read",
      "Microsoft.ContainerService/operations/read",
      "Microsoft.ContainerService/locations/operationresults/read",
      "Microsoft.ContainerService/openShiftManagedClusters/read",
      "Microsoft.ContainerService/openShiftManagedClusters/write",
      "Microsoft.ContainerService/openShiftManagedClusters/delete",
      "Microsoft.ContainerService/openShiftClusters/read",
      "Microsoft.ContainerService/openShiftClusters/write",
      "Microsoft.ContainerService/openShiftClusters/delete",
      "Microsoft.ContainerService/managedClusters/agentPools/read",
      "Microsoft.ContainerService/managedClusters/agentPools/write",
      "Microsoft.ContainerService/managedClusters/agentPools/delete",
    ]
  }
}
