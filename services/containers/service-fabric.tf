// FullAccess
resource "azurerm_role_definition" "service_fabric_fullaccess_definition" {
  name              = "service-fabric-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.ServiceFabric/register/action",
      "Microsoft.ServiceFabric/clusters/read",
      "Microsoft.ServiceFabric/clusters/write",
      "Microsoft.ServiceFabric/clusters/delete",
      "Microsoft.ServiceFabric/clusters/applicationTypes/read",
      "Microsoft.ServiceFabric/clusters/applicationTypes/write",
      "Microsoft.ServiceFabric/clusters/applicationTypes/delete",
      "Microsoft.ServiceFabric/clusters/applicationTypes/versions/read",
      "Microsoft.ServiceFabric/clusters/applicationTypes/versions/write",
      "Microsoft.ServiceFabric/clusters/applicationTypes/versions/delete",
      "Microsoft.ServiceFabric/clusters/applications/read",
      "Microsoft.ServiceFabric/clusters/applications/write",
      "Microsoft.ServiceFabric/clusters/applications/delete",
      "Microsoft.ServiceFabric/clusters/applications/services/read",
      "Microsoft.ServiceFabric/clusters/applications/services/write",
      "Microsoft.ServiceFabric/clusters/applications/services/delete",
      "Microsoft.ServiceFabric/clusters/statuses/read",
      "Microsoft.ServiceFabric/clusters/nodes/read",
      "Microsoft.ServiceFabric/clusters/applications/services/statuses/read",
      "Microsoft.ServiceFabric/clusters/applications/services/partitions/read",
      "Microsoft.ServiceFabric/clusters/applications/services/partitions/replicas/read",
      "Microsoft.ServiceFabric/locations/operationresults/read",
      "Microsoft.ServiceFabric/locations/operations/read",
      "Microsoft.ServiceFabric/operations/read",
      "Microsoft.ServiceFabric/locations/clusterVersions/read",
      "Microsoft.ServiceFabric/locations/environments/clusterVersions/read",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "service_fabric_readonly_definition" {
  name              = "service-fabric-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.ServiceFabric/clusters/read",
      "Microsoft.ServiceFabric/clusters/applicationTypes/read",
      "Microsoft.ServiceFabric/clusters/applicationTypes/versions/read",
      "Microsoft.ServiceFabric/clusters/applications/read",
      "Microsoft.ServiceFabric/clusters/applications/services/read",
      "Microsoft.ServiceFabric/clusters/statuses/read",
      "Microsoft.ServiceFabric/clusters/nodes/read",
      "Microsoft.ServiceFabric/clusters/applications/services/statuses/read",
      "Microsoft.ServiceFabric/clusters/applications/services/partitions/read",
      "Microsoft.ServiceFabric/clusters/applications/services/partitions/replicas/read",
      "Microsoft.ServiceFabric/locations/operationresults/read",
      "Microsoft.ServiceFabric/locations/operations/read",
      "Microsoft.ServiceFabric/operations/read",
      "Microsoft.ServiceFabric/locations/clusterVersions/read",
      "Microsoft.ServiceFabric/locations/environments/clusterVersions/read",
    ]

    not_actions = [
      "Microsoft.ServiceFabric/register/action",
      "Microsoft.ServiceFabric/clusters/write",
      "Microsoft.ServiceFabric/clusters/delete",
      "Microsoft.ServiceFabric/clusters/applicationTypes/write",
      "Microsoft.ServiceFabric/clusters/applicationTypes/delete",
      "Microsoft.ServiceFabric/clusters/applicationTypes/versions/write",
      "Microsoft.ServiceFabric/clusters/applicationTypes/versions/delete",
      "Microsoft.ServiceFabric/clusters/applications/write",
      "Microsoft.ServiceFabric/clusters/applications/delete",
      "Microsoft.ServiceFabric/clusters/applications/services/write",
      "Microsoft.ServiceFabric/clusters/applications/services/delete",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "service_fabric_noaccess_definition" {
  name              = "service-fabric-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.ServiceFabric/register/action",
      "Microsoft.ServiceFabric/clusters/read",
      "Microsoft.ServiceFabric/clusters/write",
      "Microsoft.ServiceFabric/clusters/delete",
      "Microsoft.ServiceFabric/clusters/applicationTypes/read",
      "Microsoft.ServiceFabric/clusters/applicationTypes/write",
      "Microsoft.ServiceFabric/clusters/applicationTypes/delete",
      "Microsoft.ServiceFabric/clusters/applicationTypes/versions/read",
      "Microsoft.ServiceFabric/clusters/applicationTypes/versions/write",
      "Microsoft.ServiceFabric/clusters/applicationTypes/versions/delete",
      "Microsoft.ServiceFabric/clusters/applications/read",
      "Microsoft.ServiceFabric/clusters/applications/write",
      "Microsoft.ServiceFabric/clusters/applications/delete",
      "Microsoft.ServiceFabric/clusters/applications/services/read",
      "Microsoft.ServiceFabric/clusters/applications/services/write",
      "Microsoft.ServiceFabric/clusters/applications/services/delete",
      "Microsoft.ServiceFabric/clusters/statuses/read",
      "Microsoft.ServiceFabric/clusters/nodes/read",
      "Microsoft.ServiceFabric/clusters/applications/services/statuses/read",
      "Microsoft.ServiceFabric/clusters/applications/services/partitions/read",
      "Microsoft.ServiceFabric/clusters/applications/services/partitions/replicas/read",
      "Microsoft.ServiceFabric/locations/operationresults/read",
      "Microsoft.ServiceFabric/locations/operations/read",
      "Microsoft.ServiceFabric/operations/read",
      "Microsoft.ServiceFabric/locations/clusterVersions/read",
      "Microsoft.ServiceFabric/locations/environments/clusterVersions/read",
    ]
  }
}
