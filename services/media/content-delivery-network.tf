// FullAccess
resource "azurerm_role_definition" "content_delivery_network_fullaccess_definition" {
  name              = "content-delivery-network-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Cdn/register/action",
      "Microsoft.Cdn/CheckNameAvailability/action",
      "Microsoft.Cdn/ValidateProbe/action",
      "Microsoft.Cdn/CheckResourceUsage/action",
      "Microsoft.Cdn/operations/read",
      "Microsoft.Cdn/edgenodes/read",
      "Microsoft.Cdn/edgenodes/write",
      "Microsoft.Cdn/edgenodes/delete",
      "Microsoft.Cdn/profiles/read",
      "Microsoft.Cdn/profiles/write",
      "Microsoft.Cdn/profiles/delete",
      "Microsoft.Cdn/profiles/CheckResourceUsage/action",
      "Microsoft.Cdn/profiles/GenerateSsoUri/action",
      "Microsoft.Cdn/profiles/GetSupportedOptimizationTypes/action",
      "Microsoft.Cdn/profiles/endpoints/read",
      "Microsoft.Cdn/profiles/endpoints/write",
      "Microsoft.Cdn/profiles/endpoints/delete",
      "Microsoft.Cdn/profiles/endpoints/CheckResourceUsage/action",
      "Microsoft.Cdn/profiles/endpoints/Start/action",
      "Microsoft.Cdn/profiles/endpoints/Stop/action",
      "Microsoft.Cdn/profiles/endpoints/Purge/action",
      "Microsoft.Cdn/profiles/endpoints/Load/action",
      "Microsoft.Cdn/profiles/endpoints/ValidateCustomDomain/action",
      "Microsoft.Cdn/profiles/endpoints/customdomains/read",
      "Microsoft.Cdn/profiles/endpoints/customdomains/write",
      "Microsoft.Cdn/profiles/endpoints/customdomains/delete",
      "Microsoft.Cdn/profiles/endpoints/customdomains/DisableCustomHttps/action",
      "Microsoft.Cdn/profiles/endpoints/customdomains/EnableCustomHttps/action",
      "Microsoft.Cdn/profiles/endpoints/origins/read",
      "Microsoft.Cdn/profiles/endpoints/origins/write",
      "Microsoft.Cdn/profiles/endpoints/origins/delete",
      "Microsoft.Cdn/operationresults/read",
      "Microsoft.Cdn/operationresults/write",
      "Microsoft.Cdn/operationresults/delete",
      "Microsoft.Cdn/operationresults/profileresults/read",
      "Microsoft.Cdn/operationresults/profileresults/write",
      "Microsoft.Cdn/operationresults/profileresults/delete",
      "Microsoft.Cdn/operationresults/profileresults/CheckResourceUsage/action",
      "Microsoft.Cdn/operationresults/profileresults/GenerateSsoUri/action",
      "Microsoft.Cdn/operationresults/profileresults/GetSupportedOptimizationTypes/action",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/read",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/write",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/delete",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/CheckResourceUsage/action",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/Start/action",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/Stop/action",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/Purge/action",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/Load/action",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/ValidateCustomDomain/action",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/originresults/read",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/originresults/write",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/originresults/delete",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/customdomainresults/read",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/customdomainresults/write",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/customdomainresults/delete",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/customdomainresults/DisableCustomHttps/action",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/customdomainresults/EnableCustomHttps/action",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "content_delivery_network_readonly_definition" {
  name              = "content-delivery-network-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Cdn/operations/read",
      "Microsoft.Cdn/edgenodes/read",
      "Microsoft.Cdn/profiles/read",
      "Microsoft.Cdn/profiles/endpoints/read",
      "Microsoft.Cdn/profiles/endpoints/customdomains/read",
      "Microsoft.Cdn/profiles/endpoints/origins/read",
      "Microsoft.Cdn/operationresults/read",
      "Microsoft.Cdn/operationresults/profileresults/read",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/read",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/originresults/read",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/customdomainresults/read",
    ]

    not_actions = [
      "Microsoft.Cdn/register/action",
      "Microsoft.Cdn/CheckNameAvailability/action",
      "Microsoft.Cdn/ValidateProbe/action",
      "Microsoft.Cdn/CheckResourceUsage/action",
      "Microsoft.Cdn/edgenodes/write",
      "Microsoft.Cdn/edgenodes/delete",
      "Microsoft.Cdn/profiles/write",
      "Microsoft.Cdn/profiles/delete",
      "Microsoft.Cdn/profiles/CheckResourceUsage/action",
      "Microsoft.Cdn/profiles/GenerateSsoUri/action",
      "Microsoft.Cdn/profiles/GetSupportedOptimizationTypes/action",
      "Microsoft.Cdn/profiles/endpoints/write",
      "Microsoft.Cdn/profiles/endpoints/delete",
      "Microsoft.Cdn/profiles/endpoints/CheckResourceUsage/action",
      "Microsoft.Cdn/profiles/endpoints/Start/action",
      "Microsoft.Cdn/profiles/endpoints/Stop/action",
      "Microsoft.Cdn/profiles/endpoints/Purge/action",
      "Microsoft.Cdn/profiles/endpoints/Load/action",
      "Microsoft.Cdn/profiles/endpoints/ValidateCustomDomain/action",
      "Microsoft.Cdn/profiles/endpoints/customdomains/write",
      "Microsoft.Cdn/profiles/endpoints/customdomains/delete",
      "Microsoft.Cdn/profiles/endpoints/customdomains/DisableCustomHttps/action",
      "Microsoft.Cdn/profiles/endpoints/customdomains/EnableCustomHttps/action",
      "Microsoft.Cdn/profiles/endpoints/origins/write",
      "Microsoft.Cdn/profiles/endpoints/origins/delete",
      "Microsoft.Cdn/operationresults/write",
      "Microsoft.Cdn/operationresults/delete",
      "Microsoft.Cdn/operationresults/profileresults/write",
      "Microsoft.Cdn/operationresults/profileresults/delete",
      "Microsoft.Cdn/operationresults/profileresults/CheckResourceUsage/action",
      "Microsoft.Cdn/operationresults/profileresults/GenerateSsoUri/action",
      "Microsoft.Cdn/operationresults/profileresults/GetSupportedOptimizationTypes/action",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/write",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/delete",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/CheckResourceUsage/action",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/Start/action",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/Stop/action",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/Purge/action",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/Load/action",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/ValidateCustomDomain/action",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/originresults/write",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/originresults/delete",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/customdomainresults/write",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/customdomainresults/delete",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/customdomainresults/DisableCustomHttps/action",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/customdomainresults/EnableCustomHttps/action",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "content_delivery_network_noaccess_definition" {
  name              = "content-delivery-network-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.Cdn/register/action",
      "Microsoft.Cdn/CheckNameAvailability/action",
      "Microsoft.Cdn/ValidateProbe/action",
      "Microsoft.Cdn/CheckResourceUsage/action",
      "Microsoft.Cdn/operations/read",
      "Microsoft.Cdn/edgenodes/read",
      "Microsoft.Cdn/edgenodes/write",
      "Microsoft.Cdn/edgenodes/delete",
      "Microsoft.Cdn/profiles/read",
      "Microsoft.Cdn/profiles/write",
      "Microsoft.Cdn/profiles/delete",
      "Microsoft.Cdn/profiles/CheckResourceUsage/action",
      "Microsoft.Cdn/profiles/GenerateSsoUri/action",
      "Microsoft.Cdn/profiles/GetSupportedOptimizationTypes/action",
      "Microsoft.Cdn/profiles/endpoints/read",
      "Microsoft.Cdn/profiles/endpoints/write",
      "Microsoft.Cdn/profiles/endpoints/delete",
      "Microsoft.Cdn/profiles/endpoints/CheckResourceUsage/action",
      "Microsoft.Cdn/profiles/endpoints/Start/action",
      "Microsoft.Cdn/profiles/endpoints/Stop/action",
      "Microsoft.Cdn/profiles/endpoints/Purge/action",
      "Microsoft.Cdn/profiles/endpoints/Load/action",
      "Microsoft.Cdn/profiles/endpoints/ValidateCustomDomain/action",
      "Microsoft.Cdn/profiles/endpoints/customdomains/read",
      "Microsoft.Cdn/profiles/endpoints/customdomains/write",
      "Microsoft.Cdn/profiles/endpoints/customdomains/delete",
      "Microsoft.Cdn/profiles/endpoints/customdomains/DisableCustomHttps/action",
      "Microsoft.Cdn/profiles/endpoints/customdomains/EnableCustomHttps/action",
      "Microsoft.Cdn/profiles/endpoints/origins/read",
      "Microsoft.Cdn/profiles/endpoints/origins/write",
      "Microsoft.Cdn/profiles/endpoints/origins/delete",
      "Microsoft.Cdn/operationresults/read",
      "Microsoft.Cdn/operationresults/write",
      "Microsoft.Cdn/operationresults/delete",
      "Microsoft.Cdn/operationresults/profileresults/read",
      "Microsoft.Cdn/operationresults/profileresults/write",
      "Microsoft.Cdn/operationresults/profileresults/delete",
      "Microsoft.Cdn/operationresults/profileresults/CheckResourceUsage/action",
      "Microsoft.Cdn/operationresults/profileresults/GenerateSsoUri/action",
      "Microsoft.Cdn/operationresults/profileresults/GetSupportedOptimizationTypes/action",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/read",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/write",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/delete",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/CheckResourceUsage/action",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/Start/action",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/Stop/action",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/Purge/action",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/Load/action",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/ValidateCustomDomain/action",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/originresults/read",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/originresults/write",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/originresults/delete",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/customdomainresults/read",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/customdomainresults/write",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/customdomainresults/delete",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/customdomainresults/DisableCustomHttps/action",
      "Microsoft.Cdn/operationresults/profileresults/endpointresults/customdomainresults/EnableCustomHttps/action",
    ]
  }
}
