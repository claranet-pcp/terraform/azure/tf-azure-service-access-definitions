// FullAccess
resource "azurerm_role_definition" "azure_cache_for_redis_fullaccess_definition" {
  name              = "azure-cache-for-redis-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Cache/checknameavailability/action",
      "Microsoft.Cache/register/action",
      "Microsoft.Cache/unregister/action",
      "Microsoft.Cache/operations/read",
      "Microsoft.Cache/locations/operationresults/read",
      "Microsoft.Cache/redis/write",
      "Microsoft.Cache/redis/read",
      "Microsoft.Cache/redis/delete",
      "Microsoft.Cache/redis/listKeys/action",
      "Microsoft.Cache/redis/regenerateKey/action",
      "Microsoft.Cache/redis/import/action",
      "Microsoft.Cache/redis/export/action",
      "Microsoft.Cache/redis/forceReboot/action",
      "Microsoft.Cache/redis/stop/action",
      "Microsoft.Cache/redis/start/action",
      "Microsoft.Cache/redis/metricDefinitions/read",
      "Microsoft.Cache/redis/listUpgradeNotifications/read",
      "Microsoft.Cache/redis/patchSchedules/read",
      "Microsoft.Cache/redis/patchSchedules/write",
      "Microsoft.Cache/redis/patchSchedules/delete",
      "Microsoft.Cache/redis/firewallRules/read",
      "Microsoft.Cache/redis/firewallRules/write",
      "Microsoft.Cache/redis/firewallRules/delete",
      "Microsoft.Cache/redis/linkedservers/read",
      "Microsoft.Cache/redis/linkedservers/write",
      "Microsoft.Cache/redis/linkedservers/delete",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "azure_cache_for_redis_readonly_definition" {
  name              = "azure-cache-for-redis-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Cache/operations/read",
      "Microsoft.Cache/locations/operationresults/read",
      "Microsoft.Cache/redis/read",
      "Microsoft.Cache/redis/metricDefinitions/read",
      "Microsoft.Cache/redis/listUpgradeNotifications/read",
      "Microsoft.Cache/redis/patchSchedules/read",
      "Microsoft.Cache/redis/firewallRules/read",
      "Microsoft.Cache/redis/linkedservers/read",
    ]

    not_actions = [
      "Microsoft.Cache/checknameavailability/action",
      "Microsoft.Cache/register/action",
      "Microsoft.Cache/unregister/action",
      "Microsoft.Cache/redis/write",
      "Microsoft.Cache/redis/delete",
      "Microsoft.Cache/redis/listKeys/action",
      "Microsoft.Cache/redis/regenerateKey/action",
      "Microsoft.Cache/redis/import/action",
      "Microsoft.Cache/redis/export/action",
      "Microsoft.Cache/redis/forceReboot/action",
      "Microsoft.Cache/redis/stop/action",
      "Microsoft.Cache/redis/start/action",
      "Microsoft.Cache/redis/patchSchedules/write",
      "Microsoft.Cache/redis/patchSchedules/delete",
      "Microsoft.Cache/redis/firewallRules/write",
      "Microsoft.Cache/redis/firewallRules/delete",
      "Microsoft.Cache/redis/linkedservers/write",
      "Microsoft.Cache/redis/linkedservers/delete",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "azure_cache_for_redis_noaccess_definition" {
  name              = "azure-cache-for-redis-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.Cache/checknameavailability/action",
      "Microsoft.Cache/register/action",
      "Microsoft.Cache/unregister/action",
      "Microsoft.Cache/operations/read",
      "Microsoft.Cache/locations/operationresults/read",
      "Microsoft.Cache/redis/write",
      "Microsoft.Cache/redis/read",
      "Microsoft.Cache/redis/delete",
      "Microsoft.Cache/redis/listKeys/action",
      "Microsoft.Cache/redis/regenerateKey/action",
      "Microsoft.Cache/redis/import/action",
      "Microsoft.Cache/redis/export/action",
      "Microsoft.Cache/redis/forceReboot/action",
      "Microsoft.Cache/redis/stop/action",
      "Microsoft.Cache/redis/start/action",
      "Microsoft.Cache/redis/metricDefinitions/read",
      "Microsoft.Cache/redis/listUpgradeNotifications/read",
      "Microsoft.Cache/redis/patchSchedules/read",
      "Microsoft.Cache/redis/patchSchedules/write",
      "Microsoft.Cache/redis/patchSchedules/delete",
      "Microsoft.Cache/redis/firewallRules/read",
      "Microsoft.Cache/redis/firewallRules/write",
      "Microsoft.Cache/redis/firewallRules/delete",
      "Microsoft.Cache/redis/linkedservers/read",
      "Microsoft.Cache/redis/linkedservers/write",
      "Microsoft.Cache/redis/linkedservers/delete",
    ]
  }
}
