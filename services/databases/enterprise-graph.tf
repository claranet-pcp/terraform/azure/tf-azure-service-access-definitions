// FullAccess
resource "azurerm_role_definition" "enterprise_graph_fullaccess_definition" {
  name              = "enterprise-graph-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.EnterpriseKnowledgeGraph/services/delete",
      "Microsoft.EnterpriseKnowledgeGraph/Operations/read",
      "Microsoft.EnterpriseKnowledgeGraph/locations/operationresults/read",
      "Microsoft.EnterpriseKnowledgeGraph/services/conflation/read",
      "Microsoft.EnterpriseKnowledgeGraph/services/conflation/write",
      "Microsoft.EnterpriseKnowledgeGraph/services/sourceschema/read",
      "Microsoft.EnterpriseKnowledgeGraph/services/sourceschema/write",
      "Microsoft.EnterpriseKnowledgeGraph/services/knowledge/read",
      "Microsoft.EnterpriseKnowledgeGraph/services/knowledge/write",
      "Microsoft.EnterpriseKnowledgeGraph/services/intentclassification/read",
      "Microsoft.EnterpriseKnowledgeGraph/services/intentclassification/write",
      "Microsoft.EnterpriseKnowledgeGraph/services/ingestion/read",
      "Microsoft.EnterpriseKnowledgeGraph/services/ingestion/write",
      "Microsoft.EnterpriseKnowledgeGraph/services/ontology/read",
      "Microsoft.EnterpriseKnowledgeGraph/services/ontology/write",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "enterprise_graph_readonly_definition" {
  name              = "enterprise-graph-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.EnterpriseKnowledgeGraph/Operations/read",
      "Microsoft.EnterpriseKnowledgeGraph/locations/operationresults/read",
      "Microsoft.EnterpriseKnowledgeGraph/services/conflation/read",
      "Microsoft.EnterpriseKnowledgeGraph/services/sourceschema/read",
      "Microsoft.EnterpriseKnowledgeGraph/services/knowledge/read",
      "Microsoft.EnterpriseKnowledgeGraph/services/intentclassification/read",
      "Microsoft.EnterpriseKnowledgeGraph/services/ingestion/read",
      "Microsoft.EnterpriseKnowledgeGraph/services/ontology/read",
    ]

    not_actions = [
      "Microsoft.EnterpriseKnowledgeGraph/services/delete",
      "Microsoft.EnterpriseKnowledgeGraph/services/conflation/write",
      "Microsoft.EnterpriseKnowledgeGraph/services/sourceschema/write",
      "Microsoft.EnterpriseKnowledgeGraph/services/knowledge/write",
      "Microsoft.EnterpriseKnowledgeGraph/services/intentclassification/write",
      "Microsoft.EnterpriseKnowledgeGraph/services/ingestion/write",
      "Microsoft.EnterpriseKnowledgeGraph/services/ontology/write",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "enterprise_graph_noaccess_definition" {
  name              = "enterprise-graph-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.EnterpriseKnowledgeGraph/services/delete",
      "Microsoft.EnterpriseKnowledgeGraph/Operations/read",
      "Microsoft.EnterpriseKnowledgeGraph/locations/operationresults/read",
      "Microsoft.EnterpriseKnowledgeGraph/services/conflation/read",
      "Microsoft.EnterpriseKnowledgeGraph/services/conflation/write",
      "Microsoft.EnterpriseKnowledgeGraph/services/sourceschema/read",
      "Microsoft.EnterpriseKnowledgeGraph/services/sourceschema/write",
      "Microsoft.EnterpriseKnowledgeGraph/services/knowledge/read",
      "Microsoft.EnterpriseKnowledgeGraph/services/knowledge/write",
      "Microsoft.EnterpriseKnowledgeGraph/services/intentclassification/read",
      "Microsoft.EnterpriseKnowledgeGraph/services/intentclassification/write",
      "Microsoft.EnterpriseKnowledgeGraph/services/ingestion/read",
      "Microsoft.EnterpriseKnowledgeGraph/services/ingestion/write",
      "Microsoft.EnterpriseKnowledgeGraph/services/ontology/read",
      "Microsoft.EnterpriseKnowledgeGraph/services/ontology/write",
    ]
  }
}
