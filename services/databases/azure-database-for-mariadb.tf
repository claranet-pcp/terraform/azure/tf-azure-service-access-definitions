// FullAccess
resource "azurerm_role_definition" "azure_database_for_mariadb_fullaccess_definition" {
  name              = "azure-database-for-mariadb-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.DBforMariaDB/register/action",
      "Microsoft.DBforMariaDB/checkNameAvailability/action",
      "Microsoft.DBforMariaDB/locations/performanceTiers/read",
      "Microsoft.DBforMariaDB/servers/queryTexts/action",
      "Microsoft.DBforMariaDB/servers/queryTexts/action",
      "Microsoft.DBforMariaDB/servers/read",
      "Microsoft.DBforMariaDB/servers/write",
      "Microsoft.DBforMariaDB/servers/delete",
      "Microsoft.DBforMariaDB/servers/restart/action",
      "Microsoft.DBforMariaDB/servers/updateConfigurations/action",
      "Microsoft.DBforMariaDB/servers/configurations/read",
      "Microsoft.DBforMariaDB/servers/configurations/write",
      "Microsoft.DBforMariaDB/servers/virtualNetworkRules/read",
      "Microsoft.DBforMariaDB/servers/virtualNetworkRules/write",
      "Microsoft.DBforMariaDB/servers/virtualNetworkRules/delete",
      "Microsoft.DBforMariaDB/servers/firewallRules/read",
      "Microsoft.DBforMariaDB/servers/firewallRules/write",
      "Microsoft.DBforMariaDB/servers/firewallRules/delete",
      "Microsoft.DBforMariaDB/performanceTiers/read",
      "Microsoft.DBforMariaDB/servers/recoverableServers/read",
      "Microsoft.DBforMariaDB/servers/providers/Microsoft.Insights/metricDefinitions/read",
      "Microsoft.DBforMariaDB/servers/providers/Microsoft.Insights/diagnosticSettings/read",
      "Microsoft.DBforMariaDB/servers/providers/Microsoft.Insights/diagnosticSettings/write",
      "Microsoft.DBforMariaDB/servers/securityAlertPolicies/read",
      "Microsoft.DBforMariaDB/servers/securityAlertPolicies/write",
      "Microsoft.DBforMariaDB/servers/providers/Microsoft.Insights/logDefinitions/read",
      "Microsoft.DBforMariaDB/servers/topQueryStatistics/read",
      "Microsoft.DBforMariaDB/servers/topQueryStatistics/read",
      "Microsoft.DBforMariaDB/servers/waitStatistics/read",
      "Microsoft.DBforMariaDB/servers/waitStatistics/read",
      "Microsoft.DBforMariaDB/locations/operationResults/read",
      "Microsoft.DBforMariaDB/locations/operationResults/read",
      "Microsoft.DBforMariaDB/locations/securityAlertPoliciesAzureAsyncOperation/read",
      "Microsoft.DBforMariaDB/locations/securityAlertPoliciesOperationResults/read",
      "Microsoft.DBforMariaDB/servers/logFiles/read",
      "Microsoft.DBforMariaDB/locations/azureAsyncOperation/read",
      "Microsoft.DBforMariaDB/servers/databases/read",
      "Microsoft.DBforMariaDB/servers/databases/write",
      "Microsoft.DBforMariaDB/servers/databases/delete",
      "Microsoft.DBforMariaDB/operations/read",
      "Microsoft.DBforMariaDB/servers/advisors/read",
      "Microsoft.DBforMariaDB/servers/advisors/read",
      "Microsoft.DBforMariaDB/servers/advisors/createRecommendedActionSession/action",
      "Microsoft.DBforMariaDB/servers/advisors/recommendedActions/read",
      "Microsoft.DBforMariaDB/servers/advisors/recommendedActions/read",
      "Microsoft.DBforMariaDB/servers/advisors/recommendedActions/read",
      "Microsoft.DBforMariaDB/servers/administrators/read",
      "Microsoft.DBforMariaDB/servers/administrators/write",
      "Microsoft.DBforMariaDB/servers/administrators/delete",
      "Microsoft.DBforMariaDB/servers/replicas/read",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "azure_database_for_mariadb_readonly_definition" {
  name              = "azure-database-for-mariadb-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.DBforMariaDB/locations/performanceTiers/read",
      "Microsoft.DBforMariaDB/servers/read",
      "Microsoft.DBforMariaDB/servers/configurations/read",
      "Microsoft.DBforMariaDB/servers/virtualNetworkRules/read",
      "Microsoft.DBforMariaDB/servers/firewallRules/read",
      "Microsoft.DBforMariaDB/performanceTiers/read",
      "Microsoft.DBforMariaDB/servers/recoverableServers/read",
      "Microsoft.DBforMariaDB/servers/providers/Microsoft.Insights/metricDefinitions/read",
      "Microsoft.DBforMariaDB/servers/providers/Microsoft.Insights/diagnosticSettings/read",
      "Microsoft.DBforMariaDB/servers/securityAlertPolicies/read",
      "Microsoft.DBforMariaDB/servers/providers/Microsoft.Insights/logDefinitions/read",
      "Microsoft.DBforMariaDB/servers/topQueryStatistics/read",
      "Microsoft.DBforMariaDB/servers/topQueryStatistics/read",
      "Microsoft.DBforMariaDB/servers/waitStatistics/read",
      "Microsoft.DBforMariaDB/servers/waitStatistics/read",
      "Microsoft.DBforMariaDB/locations/operationResults/read",
      "Microsoft.DBforMariaDB/locations/operationResults/read",
      "Microsoft.DBforMariaDB/locations/securityAlertPoliciesAzureAsyncOperation/read",
      "Microsoft.DBforMariaDB/locations/securityAlertPoliciesOperationResults/read",
      "Microsoft.DBforMariaDB/servers/logFiles/read",
      "Microsoft.DBforMariaDB/locations/azureAsyncOperation/read",
      "Microsoft.DBforMariaDB/servers/databases/read",
      "Microsoft.DBforMariaDB/operations/read",
      "Microsoft.DBforMariaDB/servers/advisors/read",
      "Microsoft.DBforMariaDB/servers/advisors/read",
      "Microsoft.DBforMariaDB/servers/advisors/recommendedActions/read",
      "Microsoft.DBforMariaDB/servers/advisors/recommendedActions/read",
      "Microsoft.DBforMariaDB/servers/advisors/recommendedActions/read",
      "Microsoft.DBforMariaDB/servers/administrators/read",
      "Microsoft.DBforMariaDB/servers/replicas/read",
    ]

    not_actions = [
      "Microsoft.DBforMariaDB/register/action",
      "Microsoft.DBforMariaDB/checkNameAvailability/action",
      "Microsoft.DBforMariaDB/servers/queryTexts/action",
      "Microsoft.DBforMariaDB/servers/queryTexts/action",
      "Microsoft.DBforMariaDB/servers/write",
      "Microsoft.DBforMariaDB/servers/delete",
      "Microsoft.DBforMariaDB/servers/restart/action",
      "Microsoft.DBforMariaDB/servers/updateConfigurations/action",
      "Microsoft.DBforMariaDB/servers/configurations/write",
      "Microsoft.DBforMariaDB/servers/virtualNetworkRules/write",
      "Microsoft.DBforMariaDB/servers/virtualNetworkRules/delete",
      "Microsoft.DBforMariaDB/servers/firewallRules/write",
      "Microsoft.DBforMariaDB/servers/firewallRules/delete",
      "Microsoft.DBforMariaDB/servers/providers/Microsoft.Insights/diagnosticSettings/write",
      "Microsoft.DBforMariaDB/servers/securityAlertPolicies/write",
      "Microsoft.DBforMariaDB/servers/databases/write",
      "Microsoft.DBforMariaDB/servers/databases/delete",
      "Microsoft.DBforMariaDB/servers/advisors/createRecommendedActionSession/action",
      "Microsoft.DBforMariaDB/servers/administrators/write",
      "Microsoft.DBforMariaDB/servers/administrators/delete",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "azure_database_for_mariadb_noaccess_definition" {
  name              = "azure-database-for-mariadb-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.DBforMariaDB/register/action",
      "Microsoft.DBforMariaDB/checkNameAvailability/action",
      "Microsoft.DBforMariaDB/locations/performanceTiers/read",
      "Microsoft.DBforMariaDB/servers/queryTexts/action",
      "Microsoft.DBforMariaDB/servers/queryTexts/action",
      "Microsoft.DBforMariaDB/servers/read",
      "Microsoft.DBforMariaDB/servers/write",
      "Microsoft.DBforMariaDB/servers/delete",
      "Microsoft.DBforMariaDB/servers/restart/action",
      "Microsoft.DBforMariaDB/servers/updateConfigurations/action",
      "Microsoft.DBforMariaDB/servers/configurations/read",
      "Microsoft.DBforMariaDB/servers/configurations/write",
      "Microsoft.DBforMariaDB/servers/virtualNetworkRules/read",
      "Microsoft.DBforMariaDB/servers/virtualNetworkRules/write",
      "Microsoft.DBforMariaDB/servers/virtualNetworkRules/delete",
      "Microsoft.DBforMariaDB/servers/firewallRules/read",
      "Microsoft.DBforMariaDB/servers/firewallRules/write",
      "Microsoft.DBforMariaDB/servers/firewallRules/delete",
      "Microsoft.DBforMariaDB/performanceTiers/read",
      "Microsoft.DBforMariaDB/servers/recoverableServers/read",
      "Microsoft.DBforMariaDB/servers/providers/Microsoft.Insights/metricDefinitions/read",
      "Microsoft.DBforMariaDB/servers/providers/Microsoft.Insights/diagnosticSettings/read",
      "Microsoft.DBforMariaDB/servers/providers/Microsoft.Insights/diagnosticSettings/write",
      "Microsoft.DBforMariaDB/servers/securityAlertPolicies/read",
      "Microsoft.DBforMariaDB/servers/securityAlertPolicies/write",
      "Microsoft.DBforMariaDB/servers/providers/Microsoft.Insights/logDefinitions/read",
      "Microsoft.DBforMariaDB/servers/topQueryStatistics/read",
      "Microsoft.DBforMariaDB/servers/topQueryStatistics/read",
      "Microsoft.DBforMariaDB/servers/waitStatistics/read",
      "Microsoft.DBforMariaDB/servers/waitStatistics/read",
      "Microsoft.DBforMariaDB/locations/operationResults/read",
      "Microsoft.DBforMariaDB/locations/operationResults/read",
      "Microsoft.DBforMariaDB/locations/securityAlertPoliciesAzureAsyncOperation/read",
      "Microsoft.DBforMariaDB/locations/securityAlertPoliciesOperationResults/read",
      "Microsoft.DBforMariaDB/servers/logFiles/read",
      "Microsoft.DBforMariaDB/locations/azureAsyncOperation/read",
      "Microsoft.DBforMariaDB/servers/databases/read",
      "Microsoft.DBforMariaDB/servers/databases/write",
      "Microsoft.DBforMariaDB/servers/databases/delete",
      "Microsoft.DBforMariaDB/operations/read",
      "Microsoft.DBforMariaDB/servers/advisors/read",
      "Microsoft.DBforMariaDB/servers/advisors/read",
      "Microsoft.DBforMariaDB/servers/advisors/createRecommendedActionSession/action",
      "Microsoft.DBforMariaDB/servers/advisors/recommendedActions/read",
      "Microsoft.DBforMariaDB/servers/advisors/recommendedActions/read",
      "Microsoft.DBforMariaDB/servers/advisors/recommendedActions/read",
      "Microsoft.DBforMariaDB/servers/administrators/read",
      "Microsoft.DBforMariaDB/servers/administrators/write",
      "Microsoft.DBforMariaDB/servers/administrators/delete",
      "Microsoft.DBforMariaDB/servers/replicas/read",
    ]
  }
}
