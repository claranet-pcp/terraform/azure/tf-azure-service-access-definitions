// FullAccess
resource "azurerm_role_definition" "azure_database_for_mysql_fullaccess_definition" {
  name              = "azure-database-for-mysql-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.DBforMySQL/register/action",
      "Microsoft.DBforMySQL/checkNameAvailability/action",
      "Microsoft.DBforMySQL/locations/performanceTiers/read",
      "Microsoft.DBforMySQL/servers/virtualNetworkRules/read",
      "Microsoft.DBforMySQL/servers/virtualNetworkRules/write",
      "Microsoft.DBforMySQL/servers/virtualNetworkRules/delete",
      "Microsoft.DBforMySQL/servers/firewallRules/read",
      "Microsoft.DBforMySQL/servers/firewallRules/write",
      "Microsoft.DBforMySQL/servers/firewallRules/delete",
      "Microsoft.DBforMySQL/servers/queryTexts/action",
      "Microsoft.DBforMySQL/servers/queryTexts/action",
      "Microsoft.DBforMySQL/servers/read",
      "Microsoft.DBforMySQL/servers/write",
      "Microsoft.DBforMySQL/servers/delete",
      "Microsoft.DBforMySQL/servers/restart/action",
      "Microsoft.DBforMySQL/servers/updateConfigurations/action",
      "Microsoft.DBforMySQL/performanceTiers/read",
      "Microsoft.DBforMySQL/servers/recoverableServers/read",
      "Microsoft.DBforMySQL/servers/providers/Microsoft.Insights/metricDefinitions/read",
      "Microsoft.DBforMySQL/servers/providers/Microsoft.Insights/diagnosticSettings/read",
      "Microsoft.DBforMySQL/servers/providers/Microsoft.Insights/diagnosticSettings/write",
      "Microsoft.DBforMySQL/servers/configurations/read",
      "Microsoft.DBforMySQL/servers/configurations/write",
      "Microsoft.DBforMySQL/servers/securityAlertPolicies/read",
      "Microsoft.DBforMySQL/servers/securityAlertPolicies/write",
      "Microsoft.DBforMySQL/servers/providers/Microsoft.Insights/logDefinitions/read",
      "Microsoft.DBforMySQL/servers/topQueryStatistics/read",
      "Microsoft.DBforMySQL/servers/topQueryStatistics/read",
      "Microsoft.DBforMySQL/servers/waitStatistics/read",
      "Microsoft.DBforMySQL/servers/waitStatistics/read",
      "Microsoft.DBforMySQL/locations/operationResults/read",
      "Microsoft.DBforMySQL/locations/operationResults/read",
      "Microsoft.DBforMySQL/locations/securityAlertPoliciesAzureAsyncOperation/read",
      "Microsoft.DBforMySQL/locations/securityAlertPoliciesOperationResults/read",
      "Microsoft.DBforMySQL/servers/logFiles/read",
      "Microsoft.DBforMySQL/locations/azureAsyncOperation/read",
      "Microsoft.DBforMySQL/servers/databases/read",
      "Microsoft.DBforMySQL/servers/databases/write",
      "Microsoft.DBforMySQL/servers/databases/delete",
      "Microsoft.DBforMySQL/operations/read",
      "Microsoft.DBforMySQL/servers/advisors/read",
      "Microsoft.DBforMySQL/servers/advisors/read",
      "Microsoft.DBforMySQL/servers/advisors/createRecommendedActionSession/action",
      "Microsoft.DBforMySQL/servers/advisors/recommendedActions/read",
      "Microsoft.DBforMySQL/servers/advisors/recommendedActions/read",
      "Microsoft.DBforMySQL/servers/advisors/recommendedActions/read",
      "Microsoft.DBforMySQL/servers/administrators/read",
      "Microsoft.DBforMySQL/servers/administrators/write",
      "Microsoft.DBforMySQL/servers/administrators/delete",
      "Microsoft.DBforMySQL/servers/replicas/read",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "azure_database_for_mysql_readonly_definition" {
  name              = "azure-database-for-mysql-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.DBforMySQL/locations/performanceTiers/read",
      "Microsoft.DBforMySQL/servers/virtualNetworkRules/read",
      "Microsoft.DBforMySQL/servers/firewallRules/read",
      "Microsoft.DBforMySQL/servers/read",
      "Microsoft.DBforMySQL/performanceTiers/read",
      "Microsoft.DBforMySQL/servers/recoverableServers/read",
      "Microsoft.DBforMySQL/servers/providers/Microsoft.Insights/metricDefinitions/read",
      "Microsoft.DBforMySQL/servers/providers/Microsoft.Insights/diagnosticSettings/read",
      "Microsoft.DBforMySQL/servers/configurations/read",
      "Microsoft.DBforMySQL/servers/securityAlertPolicies/read",
      "Microsoft.DBforMySQL/servers/providers/Microsoft.Insights/logDefinitions/read",
      "Microsoft.DBforMySQL/servers/topQueryStatistics/read",
      "Microsoft.DBforMySQL/servers/topQueryStatistics/read",
      "Microsoft.DBforMySQL/servers/waitStatistics/read",
      "Microsoft.DBforMySQL/servers/waitStatistics/read",
      "Microsoft.DBforMySQL/locations/operationResults/read",
      "Microsoft.DBforMySQL/locations/operationResults/read",
      "Microsoft.DBforMySQL/locations/securityAlertPoliciesAzureAsyncOperation/read",
      "Microsoft.DBforMySQL/locations/securityAlertPoliciesOperationResults/read",
      "Microsoft.DBforMySQL/servers/logFiles/read",
      "Microsoft.DBforMySQL/locations/azureAsyncOperation/read",
      "Microsoft.DBforMySQL/servers/databases/read",
      "Microsoft.DBforMySQL/operations/read",
      "Microsoft.DBforMySQL/servers/advisors/read",
      "Microsoft.DBforMySQL/servers/advisors/read",
      "Microsoft.DBforMySQL/servers/advisors/recommendedActions/read",
      "Microsoft.DBforMySQL/servers/advisors/recommendedActions/read",
      "Microsoft.DBforMySQL/servers/advisors/recommendedActions/read",
      "Microsoft.DBforMySQL/servers/administrators/read",
      "Microsoft.DBforMySQL/servers/replicas/read",
    ]

    not_actions = [
      "Microsoft.DBforMySQL/register/action",
      "Microsoft.DBforMySQL/checkNameAvailability/action",
      "Microsoft.DBforMySQL/servers/virtualNetworkRules/write",
      "Microsoft.DBforMySQL/servers/virtualNetworkRules/delete",
      "Microsoft.DBforMySQL/servers/firewallRules/write",
      "Microsoft.DBforMySQL/servers/firewallRules/delete",
      "Microsoft.DBforMySQL/servers/queryTexts/action",
      "Microsoft.DBforMySQL/servers/queryTexts/action",
      "Microsoft.DBforMySQL/servers/write",
      "Microsoft.DBforMySQL/servers/delete",
      "Microsoft.DBforMySQL/servers/restart/action",
      "Microsoft.DBforMySQL/servers/updateConfigurations/action",
      "Microsoft.DBforMySQL/servers/providers/Microsoft.Insights/diagnosticSettings/write",
      "Microsoft.DBforMySQL/servers/configurations/write",
      "Microsoft.DBforMySQL/servers/securityAlertPolicies/write",
      "Microsoft.DBforMySQL/servers/databases/write",
      "Microsoft.DBforMySQL/servers/databases/delete",
      "Microsoft.DBforMySQL/servers/advisors/createRecommendedActionSession/action",
      "Microsoft.DBforMySQL/servers/administrators/write",
      "Microsoft.DBforMySQL/servers/administrators/delete",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "azure_database_for_mysql_noaccess_definition" {
  name              = "azure-database-for-mysql-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.DBforMySQL/register/action",
      "Microsoft.DBforMySQL/checkNameAvailability/action",
      "Microsoft.DBforMySQL/locations/performanceTiers/read",
      "Microsoft.DBforMySQL/servers/virtualNetworkRules/read",
      "Microsoft.DBforMySQL/servers/virtualNetworkRules/write",
      "Microsoft.DBforMySQL/servers/virtualNetworkRules/delete",
      "Microsoft.DBforMySQL/servers/firewallRules/read",
      "Microsoft.DBforMySQL/servers/firewallRules/write",
      "Microsoft.DBforMySQL/servers/firewallRules/delete",
      "Microsoft.DBforMySQL/servers/queryTexts/action",
      "Microsoft.DBforMySQL/servers/queryTexts/action",
      "Microsoft.DBforMySQL/servers/read",
      "Microsoft.DBforMySQL/servers/write",
      "Microsoft.DBforMySQL/servers/delete",
      "Microsoft.DBforMySQL/servers/restart/action",
      "Microsoft.DBforMySQL/servers/updateConfigurations/action",
      "Microsoft.DBforMySQL/performanceTiers/read",
      "Microsoft.DBforMySQL/servers/recoverableServers/read",
      "Microsoft.DBforMySQL/servers/providers/Microsoft.Insights/metricDefinitions/read",
      "Microsoft.DBforMySQL/servers/providers/Microsoft.Insights/diagnosticSettings/read",
      "Microsoft.DBforMySQL/servers/providers/Microsoft.Insights/diagnosticSettings/write",
      "Microsoft.DBforMySQL/servers/configurations/read",
      "Microsoft.DBforMySQL/servers/configurations/write",
      "Microsoft.DBforMySQL/servers/securityAlertPolicies/read",
      "Microsoft.DBforMySQL/servers/securityAlertPolicies/write",
      "Microsoft.DBforMySQL/servers/providers/Microsoft.Insights/logDefinitions/read",
      "Microsoft.DBforMySQL/servers/topQueryStatistics/read",
      "Microsoft.DBforMySQL/servers/topQueryStatistics/read",
      "Microsoft.DBforMySQL/servers/waitStatistics/read",
      "Microsoft.DBforMySQL/servers/waitStatistics/read",
      "Microsoft.DBforMySQL/locations/operationResults/read",
      "Microsoft.DBforMySQL/locations/operationResults/read",
      "Microsoft.DBforMySQL/locations/securityAlertPoliciesAzureAsyncOperation/read",
      "Microsoft.DBforMySQL/locations/securityAlertPoliciesOperationResults/read",
      "Microsoft.DBforMySQL/servers/logFiles/read",
      "Microsoft.DBforMySQL/locations/azureAsyncOperation/read",
      "Microsoft.DBforMySQL/servers/databases/read",
      "Microsoft.DBforMySQL/servers/databases/write",
      "Microsoft.DBforMySQL/servers/databases/delete",
      "Microsoft.DBforMySQL/operations/read",
      "Microsoft.DBforMySQL/servers/advisors/read",
      "Microsoft.DBforMySQL/servers/advisors/read",
      "Microsoft.DBforMySQL/servers/advisors/createRecommendedActionSession/action",
      "Microsoft.DBforMySQL/servers/advisors/recommendedActions/read",
      "Microsoft.DBforMySQL/servers/advisors/recommendedActions/read",
      "Microsoft.DBforMySQL/servers/advisors/recommendedActions/read",
      "Microsoft.DBforMySQL/servers/administrators/read",
      "Microsoft.DBforMySQL/servers/administrators/write",
      "Microsoft.DBforMySQL/servers/administrators/delete",
      "Microsoft.DBforMySQL/servers/replicas/read",
    ]
  }
}
