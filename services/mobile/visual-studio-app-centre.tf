// FullAccess
resource "azurerm_role_definition" "visual_studio_app_centre_fullaccess_definition" {
  name              = "visual-studio-app-centre-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "visual_studio_app_centre_readonly_definition" {
  name              = "visual-studio-app-centre-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "",
    ]

    not_actions = [
      "",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "visual_studio_app_centre_noaccess_definition" {
  name              = "visual-studio-app-centre-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "",
    ]
  }
}
