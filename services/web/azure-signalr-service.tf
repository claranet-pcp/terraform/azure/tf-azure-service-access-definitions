// FullAccess
resource "azurerm_role_definition" "azure_signalr_service_fullaccess_definition" {
  name              = "azure-signalr-service-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.SignalRService/register/action",
      "Microsoft.SignalRService/unregister/action",
      "Microsoft.SignalRService/SignalR/read",
      "Microsoft.SignalRService/SignalR/write",
      "Microsoft.SignalRService/SignalR/delete",
      "Microsoft.SignalRService/SignalR/listkeys/action",
      "Microsoft.SignalRService/SignalR/regeneratekey/action",
      "Microsoft.SignalRService/SignalR/restart/action",
      "Microsoft.SignalRService/operationresults/read",
      "Microsoft.SignalRService/locations/operationresults/signalr/read",
      "Microsoft.SignalRService/locations/checknameavailability/action",
      "Microsoft.SignalRService/locations/usages/read",
      "Microsoft.SignalRService/operationstatus/read",
      "Microsoft.SignalRService/locations/operationStatuses/operationId/read",
      "Microsoft.SignalRService/SignalR/eventGridFilters/read",
      "Microsoft.SignalRService/SignalR/eventGridFilters/write",
      "Microsoft.SignalRService/SignalR/eventGridFilters/delete",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "azure_signalr_service_readonly_definition" {
  name              = "azure-signalr-service-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.SignalRService/SignalR/read",
      "Microsoft.SignalRService/operationresults/read",
      "Microsoft.SignalRService/locations/operationresults/signalr/read",
      "Microsoft.SignalRService/locations/usages/read",
      "Microsoft.SignalRService/operationstatus/read",
      "Microsoft.SignalRService/locations/operationStatuses/operationId/read",
      "Microsoft.SignalRService/SignalR/eventGridFilters/read",
    ]

    not_actions = [
      "Microsoft.SignalRService/register/action",
      "Microsoft.SignalRService/unregister/action",
      "Microsoft.SignalRService/SignalR/write",
      "Microsoft.SignalRService/SignalR/delete",
      "Microsoft.SignalRService/SignalR/listkeys/action",
      "Microsoft.SignalRService/SignalR/regeneratekey/action",
      "Microsoft.SignalRService/SignalR/restart/action",
      "Microsoft.SignalRService/locations/checknameavailability/action",
      "Microsoft.SignalRService/SignalR/eventGridFilters/write",
      "Microsoft.SignalRService/SignalR/eventGridFilters/delete",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "azure_signalr_service_noaccess_definition" {
  name              = "azure-signalr-service-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.SignalRService/register/action",
      "Microsoft.SignalRService/unregister/action",
      "Microsoft.SignalRService/SignalR/read",
      "Microsoft.SignalRService/SignalR/write",
      "Microsoft.SignalRService/SignalR/delete",
      "Microsoft.SignalRService/SignalR/listkeys/action",
      "Microsoft.SignalRService/SignalR/regeneratekey/action",
      "Microsoft.SignalRService/SignalR/restart/action",
      "Microsoft.SignalRService/operationresults/read",
      "Microsoft.SignalRService/locations/operationresults/signalr/read",
      "Microsoft.SignalRService/locations/checknameavailability/action",
      "Microsoft.SignalRService/locations/usages/read",
      "Microsoft.SignalRService/operationstatus/read",
      "Microsoft.SignalRService/locations/operationStatuses/operationId/read",
      "Microsoft.SignalRService/SignalR/eventGridFilters/read",
      "Microsoft.SignalRService/SignalR/eventGridFilters/write",
      "Microsoft.SignalRService/SignalR/eventGridFilters/delete",
    ]
  }
}
