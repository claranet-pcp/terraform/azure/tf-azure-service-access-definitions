// FullAccess
resource "azurerm_role_definition" "azure_data_lake_storage_fullaccess_definition" {
  name              = "azure-data-lake-storage-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.DataLakeStore/register/action",
      "Microsoft.DataLakeStore/operations/read",
      "Microsoft.DataLakeStore/accounts/read",
      "Microsoft.DataLakeStore/accounts/write",
      "Microsoft.DataLakeStore/accounts/delete",
      "Microsoft.DataLakeStore/accounts/enableKeyVault/action",
      "Microsoft.DataLakeStore/accounts/Superuser/action",
      "Microsoft.DataLakeStore/accounts/operationResults/read",
      "Microsoft.DataLakeStore/accounts/firewallRules/read",
      "Microsoft.DataLakeStore/accounts/firewallRules/write",
      "Microsoft.DataLakeStore/accounts/firewallRules/delete",
      "Microsoft.DataLakeStore/accounts/trustedIdProviders/read",
      "Microsoft.DataLakeStore/accounts/trustedIdProviders/write",
      "Microsoft.DataLakeStore/accounts/trustedIdProviders/delete",
      "Microsoft.DataLakeStore/accounts/eventGridFilters/read",
      "Microsoft.DataLakeStore/accounts/eventGridFilters/write",
      "Microsoft.DataLakeStore/accounts/eventGridFilters/delete",
      "Microsoft.DataLakeStore/locations/capability/read",
      "Microsoft.DataLakeStore/locations/checkNameAvailability/action",
      "Microsoft.DataLakeStore/locations/operationResults/read",
      "Microsoft.DataLakeStore/locations/usages/read",
      "Microsoft.DataLakeStore/accounts/virtualNetworkRules/read",
      "Microsoft.DataLakeStore/accounts/virtualNetworkRules/write",
      "Microsoft.DataLakeStore/accounts/virtualNetworkRules/delete",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "azure_data_lake_storage_readonly_definition" {
  name              = "azure-data-lake-storage-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.DataLakeStore/operations/read",
      "Microsoft.DataLakeStore/accounts/read",
      "Microsoft.DataLakeStore/accounts/operationResults/read",
      "Microsoft.DataLakeStore/accounts/firewallRules/read",
      "Microsoft.DataLakeStore/accounts/trustedIdProviders/read",
      "Microsoft.DataLakeStore/accounts/eventGridFilters/read",
      "Microsoft.DataLakeStore/locations/capability/read",
      "Microsoft.DataLakeStore/locations/operationResults/read",
      "Microsoft.DataLakeStore/locations/usages/read",
      "Microsoft.DataLakeStore/accounts/virtualNetworkRules/read",
    ]

    not_actions = [
      "Microsoft.DataLakeStore/register/action",
      "Microsoft.DataLakeStore/accounts/write",
      "Microsoft.DataLakeStore/accounts/delete",
      "Microsoft.DataLakeStore/accounts/enableKeyVault/action",
      "Microsoft.DataLakeStore/accounts/Superuser/action",
      "Microsoft.DataLakeStore/accounts/firewallRules/write",
      "Microsoft.DataLakeStore/accounts/firewallRules/delete",
      "Microsoft.DataLakeStore/accounts/trustedIdProviders/write",
      "Microsoft.DataLakeStore/accounts/trustedIdProviders/delete",
      "Microsoft.DataLakeStore/accounts/eventGridFilters/write",
      "Microsoft.DataLakeStore/accounts/eventGridFilters/delete",
      "Microsoft.DataLakeStore/locations/checkNameAvailability/action",
      "Microsoft.DataLakeStore/accounts/virtualNetworkRules/write",
      "Microsoft.DataLakeStore/accounts/virtualNetworkRules/delete",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "azure_data_lake_storage_noaccess_definition" {
  name              = "azure-data-lake-storage-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.DataLakeStore/register/action",
      "Microsoft.DataLakeStore/operations/read",
      "Microsoft.DataLakeStore/accounts/read",
      "Microsoft.DataLakeStore/accounts/write",
      "Microsoft.DataLakeStore/accounts/delete",
      "Microsoft.DataLakeStore/accounts/enableKeyVault/action",
      "Microsoft.DataLakeStore/accounts/Superuser/action",
      "Microsoft.DataLakeStore/accounts/operationResults/read",
      "Microsoft.DataLakeStore/accounts/firewallRules/read",
      "Microsoft.DataLakeStore/accounts/firewallRules/write",
      "Microsoft.DataLakeStore/accounts/firewallRules/delete",
      "Microsoft.DataLakeStore/accounts/trustedIdProviders/read",
      "Microsoft.DataLakeStore/accounts/trustedIdProviders/write",
      "Microsoft.DataLakeStore/accounts/trustedIdProviders/delete",
      "Microsoft.DataLakeStore/accounts/eventGridFilters/read",
      "Microsoft.DataLakeStore/accounts/eventGridFilters/write",
      "Microsoft.DataLakeStore/accounts/eventGridFilters/delete",
      "Microsoft.DataLakeStore/locations/capability/read",
      "Microsoft.DataLakeStore/locations/checkNameAvailability/action",
      "Microsoft.DataLakeStore/locations/operationResults/read",
      "Microsoft.DataLakeStore/locations/usages/read",
      "Microsoft.DataLakeStore/accounts/virtualNetworkRules/read",
      "Microsoft.DataLakeStore/accounts/virtualNetworkRules/write",
      "Microsoft.DataLakeStore/accounts/virtualNetworkRules/delete",
    ]
  }
}
