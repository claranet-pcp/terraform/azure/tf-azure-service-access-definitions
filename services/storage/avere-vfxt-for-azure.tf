// FullAccess
resource "azurerm_role_definition" "avere_vfxt_for_azure_fullaccess_definition" {
  name              = "avere-vfxt-for-azure-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "avere_vfxt_for_azure_readonly_definition" {
  name              = "avere-vfxt-for-azure-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "",
    ]

    not_actions = [
      "",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "avere_vfxt_for_azure_noaccess_definition" {
  name              = "avere-vfxt-for-azure-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "",
    ]
  }
}
