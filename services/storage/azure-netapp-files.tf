// FullAccess
resource "azurerm_role_definition" "azure_netapp_files_fullaccess_definition" {
  name              = "azure-netapp-files-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.NetApp/netAppAccounts/capacityPools/volumes/read",
      "Microsoft.NetApp/netAppAccounts/capacityPools/volumes/write",
      "Microsoft.NetApp/netAppAccounts/capacityPools/volumes/delete",
      "Microsoft.NetApp/netAppAccounts/read",
      "Microsoft.NetApp/netAppAccounts/write",
      "Microsoft.NetApp/netAppAccounts/delete",
      "Microsoft.NetApp/netAppAccounts/capacityPools/read",
      "Microsoft.NetApp/netAppAccounts/capacityPools/write",
      "Microsoft.NetApp/netAppAccounts/capacityPools/delete",
      "Microsoft.NetApp/netAppAccounts/capacityPools/volumes/mountTargets/read",
      "Microsoft.NetApp/netAppAccounts/capacityPools/volumes/snapshots/read",
      "Microsoft.NetApp/netAppAccounts/capacityPools/volumes/snapshots/write",
      "Microsoft.NetApp/netAppAccounts/capacityPools/volumes/snapshots/delete",
      "Microsoft.NetApp/Operations/read",
      "Microsoft.NetApp/locations/operationresults/read",
      "Microsoft.NetApp/locations/read",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "azure_netapp_files_readonly_definition" {
  name              = "azure-netapp-files-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.NetApp/netAppAccounts/capacityPools/volumes/read",
      "Microsoft.NetApp/netAppAccounts/read",
      "Microsoft.NetApp/netAppAccounts/capacityPools/read",
      "Microsoft.NetApp/netAppAccounts/capacityPools/volumes/mountTargets/read",
      "Microsoft.NetApp/netAppAccounts/capacityPools/volumes/snapshots/read",
      "Microsoft.NetApp/Operations/read",
      "Microsoft.NetApp/locations/operationresults/read",
      "Microsoft.NetApp/locations/read",
    ]

    not_actions = [
      "Microsoft.NetApp/netAppAccounts/capacityPools/volumes/write",
      "Microsoft.NetApp/netAppAccounts/capacityPools/volumes/delete",
      "Microsoft.NetApp/netAppAccounts/write",
      "Microsoft.NetApp/netAppAccounts/delete",
      "Microsoft.NetApp/netAppAccounts/capacityPools/write",
      "Microsoft.NetApp/netAppAccounts/capacityPools/delete",
      "Microsoft.NetApp/netAppAccounts/capacityPools/volumes/snapshots/write",
      "Microsoft.NetApp/netAppAccounts/capacityPools/volumes/snapshots/delete",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "azure_netapp_files_noaccess_definition" {
  name              = "azure-netapp-files-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.NetApp/netAppAccounts/capacityPools/volumes/read",
      "Microsoft.NetApp/netAppAccounts/capacityPools/volumes/write",
      "Microsoft.NetApp/netAppAccounts/capacityPools/volumes/delete",
      "Microsoft.NetApp/netAppAccounts/read",
      "Microsoft.NetApp/netAppAccounts/write",
      "Microsoft.NetApp/netAppAccounts/delete",
      "Microsoft.NetApp/netAppAccounts/capacityPools/read",
      "Microsoft.NetApp/netAppAccounts/capacityPools/write",
      "Microsoft.NetApp/netAppAccounts/capacityPools/delete",
      "Microsoft.NetApp/netAppAccounts/capacityPools/volumes/mountTargets/read",
      "Microsoft.NetApp/netAppAccounts/capacityPools/volumes/snapshots/read",
      "Microsoft.NetApp/netAppAccounts/capacityPools/volumes/snapshots/write",
      "Microsoft.NetApp/netAppAccounts/capacityPools/volumes/snapshots/delete",
      "Microsoft.NetApp/Operations/read",
      "Microsoft.NetApp/locations/operationresults/read",
      "Microsoft.NetApp/locations/read",
    ]
  }
}
