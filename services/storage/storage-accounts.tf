// FullAccess
resource "azurerm_role_definition" "storage_accounts_fullaccess_definition" {
  name              = "storage-accounts-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Storage/register/action",
      "Microsoft.Storage/locations/deleteVirtualNetworkOrSubnets/action",
      "Microsoft.Storage/storageAccounts/blobServices/generateUserDelegationKey/action",
      "Microsoft.Storage/storageAccounts/blobServices/write",
      "Microsoft.Storage/storageAccounts/blobServices/read",
      "Microsoft.Storage/storageAccounts/blobServices/containers/blobs/read",
      "Microsoft.Storage/storageAccounts/blobServices/containers/blobs/write",
      "Microsoft.Storage/storageAccounts/blobServices/containers/blobs/delete",
      "Microsoft.Storage/storageAccounts/blobServices/containers/blobs/deleteAutomaticSnapshot/action",
      "Microsoft.Storage/storageAccounts/blobServices/containers/blobs/add/action",
      "Microsoft.Storage/storageAccounts/blobServices/containers/blobs/filter/action",
      "Microsoft.Storage/storageAccounts/blobServices/containers/write",
      "Microsoft.Storage/storageAccounts/blobServices/containers/delete",
      "Microsoft.Storage/storageAccounts/blobServices/containers/read",
      "Microsoft.Storage/storageAccounts/blobServices/containers/read",
      "Microsoft.Storage/storageAccounts/blobServices/containers/lease/action",
      "Microsoft.Storage/storageAccounts/blobServices/containers/write",
      "Microsoft.Storage/storageAccounts/blobServices/containers/clearLegalHold/action",
      "Microsoft.Storage/storageAccounts/blobServices/containers/setLegalHold/action",
      "Microsoft.Storage/storageAccounts/blobServices/containers/immutabilityPolicies/extend/action",
      "Microsoft.Storage/storageAccounts/blobServices/containers/immutabilityPolicies/delete",
      "Microsoft.Storage/storageAccounts/blobServices/containers/immutabilityPolicies/write",
      "Microsoft.Storage/storageAccounts/blobServices/containers/immutabilityPolicies/lock/action",
      "Microsoft.Storage/storageAccounts/blobServices/containers/immutabilityPolicies/read",
      "Microsoft.Storage/storageAccounts/queueServices/read",
      "Microsoft.Storage/storageAccounts/queueServices/read",
      "Microsoft.Storage/storageAccounts/queueServices/write",
      "Microsoft.Storage/storageAccounts/queueServices/queues/read",
      "Microsoft.Storage/storageAccounts/queueServices/queues/write",
      "Microsoft.Storage/storageAccounts/queueServices/queues/delete",
      "Microsoft.Storage/storageAccounts/queueServices/queues/messages/read",
      "Microsoft.Storage/storageAccounts/queueServices/queues/messages/write",
      "Microsoft.Storage/storageAccounts/queueServices/queues/messages/delete",
      "Microsoft.Storage/storageAccounts/queueServices/queues/messages/add/action",
      "Microsoft.Storage/storageAccounts/queueServices/queues/messages/process/action",
      "Microsoft.Storage/storageAccounts/restoreBlobRanges/action",
      "Microsoft.Storage/storageAccounts/PrivateEndpointConnectionsApproval/action",
      "Microsoft.Storage/storageAccounts/failover/action",
      "Microsoft.Storage/storageAccounts/listkeys/action",
      "Microsoft.Storage/storageAccounts/regeneratekey/action",
      "Microsoft.Storage/storageAccounts/revokeUserDelegationKeys/action",
      "Microsoft.Storage/storageAccounts/delete",
      "Microsoft.Storage/storageAccounts/read",
      "Microsoft.Storage/storageAccounts/listAccountSas/action",
      "Microsoft.Storage/storageAccounts/listServiceSas/action",
      "Microsoft.Storage/storageAccounts/write",
      "Microsoft.Storage/storageAccounts/services/diagnosticSettings/write",
      "Microsoft.Storage/skus/read",
      "Microsoft.Storage/operations/read",
      "Microsoft.Storage/checknameavailability/read",
      "Microsoft.Storage/locations/usages/read",
      "Microsoft.Storage/usages/read",
      "Microsoft.Storage/storageAccounts/blobServices/containers/blobs/tags/read",
      "Microsoft.Storage/storageAccounts/blobServices/containers/blobs/tags/write",
      "Microsoft.Storage/storageAccounts/managementPolicies/delete",
      "Microsoft.Storage/storageAccounts/managementPolicies/read",
      "Microsoft.Storage/storageAccounts/managementPolicies/write",
      "Microsoft.Storage/storageAccounts/fileServices/read",
      "Microsoft.Storage/storageAccounts/tableServices/read",
      "Microsoft.Storage/storageAccounts/fileServices/fileshares/files/read",
      "Microsoft.Storage/storageAccounts/fileServices/fileshares/files/write",
      "Microsoft.Storage/storageAccounts/fileServices/fileshares/files/delete",
      "Microsoft.Storage/storageAccounts/fileServices/fileshares/files/modifypermissions/action",
      "Microsoft.Storage/storageAccounts/fileServices/fileshares/files/actassuperuser/action",
      "Microsoft.Storage/storageAccounts/privateEndpointConnectionProxies/delete",
      "Microsoft.Storage/storageAccounts/privateEndpointConnectionProxies/write",
      "Microsoft.Storage/storageAccounts/privateEndpointConnections/delete",
      "Microsoft.Storage/storageAccounts/privateEndpointConnections/read",
      "Microsoft.Storage/storageAccounts/privateEndpointConnections/write",
      "Microsoft.Storage/storageAccounts/privateLinkResources/read",
      "Microsoft.Storage/locations/checknameavailability/read",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "storage_accounts_readonly_definition" {
  name              = "storage-accounts-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Storage/storageAccounts/blobServices/read",
      "Microsoft.Storage/storageAccounts/blobServices/containers/blobs/read",
      "Microsoft.Storage/storageAccounts/blobServices/containers/read",
      "Microsoft.Storage/storageAccounts/blobServices/containers/read",
      "Microsoft.Storage/storageAccounts/blobServices/containers/immutabilityPolicies/read",
      "Microsoft.Storage/storageAccounts/queueServices/read",
      "Microsoft.Storage/storageAccounts/queueServices/read",
      "Microsoft.Storage/storageAccounts/queueServices/queues/read",
      "Microsoft.Storage/storageAccounts/queueServices/queues/messages/read",
      "Microsoft.Storage/storageAccounts/read",
      "Microsoft.Storage/skus/read",
      "Microsoft.Storage/operations/read",
      "Microsoft.Storage/checknameavailability/read",
      "Microsoft.Storage/locations/usages/read",
      "Microsoft.Storage/usages/read",
      "Microsoft.Storage/storageAccounts/blobServices/containers/blobs/tags/read",
      "Microsoft.Storage/storageAccounts/managementPolicies/read",
      "Microsoft.Storage/storageAccounts/fileServices/read",
      "Microsoft.Storage/storageAccounts/tableServices/read",
      "Microsoft.Storage/storageAccounts/fileServices/fileshares/files/read",
      "Microsoft.Storage/storageAccounts/privateEndpointConnections/read",
      "Microsoft.Storage/storageAccounts/privateLinkResources/read",
      "Microsoft.Storage/locations/checknameavailability/read",
    ]

    not_actions = [
      "Microsoft.Storage/register/action",
      "Microsoft.Storage/locations/deleteVirtualNetworkOrSubnets/action",
      "Microsoft.Storage/storageAccounts/blobServices/generateUserDelegationKey/action",
      "Microsoft.Storage/storageAccounts/blobServices/write",
      "Microsoft.Storage/storageAccounts/blobServices/containers/blobs/write",
      "Microsoft.Storage/storageAccounts/blobServices/containers/blobs/delete",
      "Microsoft.Storage/storageAccounts/blobServices/containers/blobs/deleteAutomaticSnapshot/action",
      "Microsoft.Storage/storageAccounts/blobServices/containers/blobs/add/action",
      "Microsoft.Storage/storageAccounts/blobServices/containers/blobs/filter/action",
      "Microsoft.Storage/storageAccounts/blobServices/containers/write",
      "Microsoft.Storage/storageAccounts/blobServices/containers/delete",
      "Microsoft.Storage/storageAccounts/blobServices/containers/lease/action",
      "Microsoft.Storage/storageAccounts/blobServices/containers/write",
      "Microsoft.Storage/storageAccounts/blobServices/containers/clearLegalHold/action",
      "Microsoft.Storage/storageAccounts/blobServices/containers/setLegalHold/action",
      "Microsoft.Storage/storageAccounts/blobServices/containers/immutabilityPolicies/extend/action",
      "Microsoft.Storage/storageAccounts/blobServices/containers/immutabilityPolicies/delete",
      "Microsoft.Storage/storageAccounts/blobServices/containers/immutabilityPolicies/write",
      "Microsoft.Storage/storageAccounts/blobServices/containers/immutabilityPolicies/lock/action",
      "Microsoft.Storage/storageAccounts/queueServices/write",
      "Microsoft.Storage/storageAccounts/queueServices/queues/write",
      "Microsoft.Storage/storageAccounts/queueServices/queues/delete",
      "Microsoft.Storage/storageAccounts/queueServices/queues/messages/write",
      "Microsoft.Storage/storageAccounts/queueServices/queues/messages/delete",
      "Microsoft.Storage/storageAccounts/queueServices/queues/messages/add/action",
      "Microsoft.Storage/storageAccounts/queueServices/queues/messages/process/action",
      "Microsoft.Storage/storageAccounts/restoreBlobRanges/action",
      "Microsoft.Storage/storageAccounts/PrivateEndpointConnectionsApproval/action",
      "Microsoft.Storage/storageAccounts/failover/action",
      "Microsoft.Storage/storageAccounts/listkeys/action",
      "Microsoft.Storage/storageAccounts/regeneratekey/action",
      "Microsoft.Storage/storageAccounts/revokeUserDelegationKeys/action",
      "Microsoft.Storage/storageAccounts/delete",
      "Microsoft.Storage/storageAccounts/listAccountSas/action",
      "Microsoft.Storage/storageAccounts/listServiceSas/action",
      "Microsoft.Storage/storageAccounts/write",
      "Microsoft.Storage/storageAccounts/services/diagnosticSettings/write",
      "Microsoft.Storage/storageAccounts/blobServices/containers/blobs/tags/write",
      "Microsoft.Storage/storageAccounts/managementPolicies/delete",
      "Microsoft.Storage/storageAccounts/managementPolicies/write",
      "Microsoft.Storage/storageAccounts/fileServices/fileshares/files/write",
      "Microsoft.Storage/storageAccounts/fileServices/fileshares/files/delete",
      "Microsoft.Storage/storageAccounts/fileServices/fileshares/files/modifypermissions/action",
      "Microsoft.Storage/storageAccounts/fileServices/fileshares/files/actassuperuser/action",
      "Microsoft.Storage/storageAccounts/privateEndpointConnectionProxies/delete",
      "Microsoft.Storage/storageAccounts/privateEndpointConnectionProxies/write",
      "Microsoft.Storage/storageAccounts/privateEndpointConnections/delete",
      "Microsoft.Storage/storageAccounts/privateEndpointConnections/write",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "storage_accounts_noaccess_definition" {
  name              = "storage-accounts-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.Storage/register/action",
      "Microsoft.Storage/locations/deleteVirtualNetworkOrSubnets/action",
      "Microsoft.Storage/storageAccounts/blobServices/generateUserDelegationKey/action",
      "Microsoft.Storage/storageAccounts/blobServices/write",
      "Microsoft.Storage/storageAccounts/blobServices/read",
      "Microsoft.Storage/storageAccounts/blobServices/containers/blobs/read",
      "Microsoft.Storage/storageAccounts/blobServices/containers/blobs/write",
      "Microsoft.Storage/storageAccounts/blobServices/containers/blobs/delete",
      "Microsoft.Storage/storageAccounts/blobServices/containers/blobs/deleteAutomaticSnapshot/action",
      "Microsoft.Storage/storageAccounts/blobServices/containers/blobs/add/action",
      "Microsoft.Storage/storageAccounts/blobServices/containers/blobs/filter/action",
      "Microsoft.Storage/storageAccounts/blobServices/containers/write",
      "Microsoft.Storage/storageAccounts/blobServices/containers/delete",
      "Microsoft.Storage/storageAccounts/blobServices/containers/read",
      "Microsoft.Storage/storageAccounts/blobServices/containers/read",
      "Microsoft.Storage/storageAccounts/blobServices/containers/lease/action",
      "Microsoft.Storage/storageAccounts/blobServices/containers/write",
      "Microsoft.Storage/storageAccounts/blobServices/containers/clearLegalHold/action",
      "Microsoft.Storage/storageAccounts/blobServices/containers/setLegalHold/action",
      "Microsoft.Storage/storageAccounts/blobServices/containers/immutabilityPolicies/extend/action",
      "Microsoft.Storage/storageAccounts/blobServices/containers/immutabilityPolicies/delete",
      "Microsoft.Storage/storageAccounts/blobServices/containers/immutabilityPolicies/write",
      "Microsoft.Storage/storageAccounts/blobServices/containers/immutabilityPolicies/lock/action",
      "Microsoft.Storage/storageAccounts/blobServices/containers/immutabilityPolicies/read",
      "Microsoft.Storage/storageAccounts/queueServices/read",
      "Microsoft.Storage/storageAccounts/queueServices/read",
      "Microsoft.Storage/storageAccounts/queueServices/write",
      "Microsoft.Storage/storageAccounts/queueServices/queues/read",
      "Microsoft.Storage/storageAccounts/queueServices/queues/write",
      "Microsoft.Storage/storageAccounts/queueServices/queues/delete",
      "Microsoft.Storage/storageAccounts/queueServices/queues/messages/read",
      "Microsoft.Storage/storageAccounts/queueServices/queues/messages/write",
      "Microsoft.Storage/storageAccounts/queueServices/queues/messages/delete",
      "Microsoft.Storage/storageAccounts/queueServices/queues/messages/add/action",
      "Microsoft.Storage/storageAccounts/queueServices/queues/messages/process/action",
      "Microsoft.Storage/storageAccounts/restoreBlobRanges/action",
      "Microsoft.Storage/storageAccounts/PrivateEndpointConnectionsApproval/action",
      "Microsoft.Storage/storageAccounts/failover/action",
      "Microsoft.Storage/storageAccounts/listkeys/action",
      "Microsoft.Storage/storageAccounts/regeneratekey/action",
      "Microsoft.Storage/storageAccounts/revokeUserDelegationKeys/action",
      "Microsoft.Storage/storageAccounts/delete",
      "Microsoft.Storage/storageAccounts/read",
      "Microsoft.Storage/storageAccounts/listAccountSas/action",
      "Microsoft.Storage/storageAccounts/listServiceSas/action",
      "Microsoft.Storage/storageAccounts/write",
      "Microsoft.Storage/storageAccounts/services/diagnosticSettings/write",
      "Microsoft.Storage/skus/read",
      "Microsoft.Storage/operations/read",
      "Microsoft.Storage/checknameavailability/read",
      "Microsoft.Storage/locations/usages/read",
      "Microsoft.Storage/usages/read",
      "Microsoft.Storage/storageAccounts/blobServices/containers/blobs/tags/read",
      "Microsoft.Storage/storageAccounts/blobServices/containers/blobs/tags/write",
      "Microsoft.Storage/storageAccounts/managementPolicies/delete",
      "Microsoft.Storage/storageAccounts/managementPolicies/read",
      "Microsoft.Storage/storageAccounts/managementPolicies/write",
      "Microsoft.Storage/storageAccounts/fileServices/read",
      "Microsoft.Storage/storageAccounts/tableServices/read",
      "Microsoft.Storage/storageAccounts/fileServices/fileshares/files/read",
      "Microsoft.Storage/storageAccounts/fileServices/fileshares/files/write",
      "Microsoft.Storage/storageAccounts/fileServices/fileshares/files/delete",
      "Microsoft.Storage/storageAccounts/fileServices/fileshares/files/modifypermissions/action",
      "Microsoft.Storage/storageAccounts/fileServices/fileshares/files/actassuperuser/action",
      "Microsoft.Storage/storageAccounts/privateEndpointConnectionProxies/delete",
      "Microsoft.Storage/storageAccounts/privateEndpointConnectionProxies/write",
      "Microsoft.Storage/storageAccounts/privateEndpointConnections/delete",
      "Microsoft.Storage/storageAccounts/privateEndpointConnections/read",
      "Microsoft.Storage/storageAccounts/privateEndpointConnections/write",
      "Microsoft.Storage/storageAccounts/privateLinkResources/read",
      "Microsoft.Storage/locations/checknameavailability/read",
    ]
  }
}
