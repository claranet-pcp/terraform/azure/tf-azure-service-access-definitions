// FullAccess
resource "azurerm_role_definition" "file_storage_fullaccess_definition" {
  name              = "file-storage-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "microsoft.storagesync/storageSyncServices/read",
      "microsoft.storagesync/storageSyncServices/write",
      "microsoft.storagesync/storageSyncServices/delete",
      "microsoft.storagesync/locations/checkNameAvailability/action",
      "microsoft.storagesync/storageSyncServices/syncGroups/read",
      "microsoft.storagesync/storageSyncServices/syncGroups/write",
      "microsoft.storagesync/storageSyncServices/syncGroups/delete",
      "microsoft.storagesync/storageSyncServices/syncGroups/cloudEndpoints/read",
      "microsoft.storagesync/storageSyncServices/syncGroups/cloudEndpoints/write",
      "microsoft.storagesync/storageSyncServices/syncGroups/cloudEndpoints/delete",
      "microsoft.storagesync/storageSyncServices/syncGroups/cloudEndpoints/prebackup/action",
      "microsoft.storagesync/storageSyncServices/syncGroups/cloudEndpoints/postbackup/action",
      "microsoft.storagesync/storageSyncServices/syncGroups/cloudEndpoints/prerestore/action",
      "microsoft.storagesync/storageSyncServices/syncGroups/cloudEndpoints/postrestore/action",
      "microsoft.storagesync/storageSyncServices/syncGroups/cloudEndpoints/restoreheartbeat/action",
      "microsoft.storagesync/storageSyncServices/syncGroups/cloudEndpoints/operationresults/read",
      "microsoft.storagesync/storageSyncServices/syncGroups/serverEndpoints/read",
      "microsoft.storagesync/storageSyncServices/syncGroups/serverEndpoints/write",
      "microsoft.storagesync/storageSyncServices/syncGroups/serverEndpoints/delete",
      "microsoft.storagesync/storageSyncServices/syncGroups/serverEndpoints/recallAction/action",
      "microsoft.storagesync/storageSyncServices/registeredServers/read",
      "microsoft.storagesync/storageSyncServices/registeredServers/write",
      "microsoft.storagesync/storageSyncServices/registeredServers/delete",
      "microsoft.storagesync/storageSyncServices/workflows/read",
      "microsoft.storagesync/storageSyncServices/workflows/operationresults/read",
      "microsoft.storagesync/storageSyncServices/workflows/operations/read",
      "microsoft.storagesync/locations/workflows/operations/read",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "file_storage_readonly_definition" {
  name              = "file-storage-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "microsoft.storagesync/storageSyncServices/read",
      "microsoft.storagesync/storageSyncServices/syncGroups/read",
      "microsoft.storagesync/storageSyncServices/syncGroups/cloudEndpoints/read",
      "microsoft.storagesync/storageSyncServices/syncGroups/cloudEndpoints/operationresults/read",
      "microsoft.storagesync/storageSyncServices/syncGroups/serverEndpoints/read",
      "microsoft.storagesync/storageSyncServices/registeredServers/read",
      "microsoft.storagesync/storageSyncServices/workflows/read",
      "microsoft.storagesync/storageSyncServices/workflows/operationresults/read",
      "microsoft.storagesync/storageSyncServices/workflows/operations/read",
      "microsoft.storagesync/locations/workflows/operations/read",
    ]

    not_actions = [
      "microsoft.storagesync/storageSyncServices/write",
      "microsoft.storagesync/storageSyncServices/delete",
      "microsoft.storagesync/locations/checkNameAvailability/action",
      "microsoft.storagesync/storageSyncServices/syncGroups/write",
      "microsoft.storagesync/storageSyncServices/syncGroups/delete",
      "microsoft.storagesync/storageSyncServices/syncGroups/cloudEndpoints/write",
      "microsoft.storagesync/storageSyncServices/syncGroups/cloudEndpoints/delete",
      "microsoft.storagesync/storageSyncServices/syncGroups/cloudEndpoints/prebackup/action",
      "microsoft.storagesync/storageSyncServices/syncGroups/cloudEndpoints/postbackup/action",
      "microsoft.storagesync/storageSyncServices/syncGroups/cloudEndpoints/prerestore/action",
      "microsoft.storagesync/storageSyncServices/syncGroups/cloudEndpoints/postrestore/action",
      "microsoft.storagesync/storageSyncServices/syncGroups/cloudEndpoints/restoreheartbeat/action",
      "microsoft.storagesync/storageSyncServices/syncGroups/serverEndpoints/write",
      "microsoft.storagesync/storageSyncServices/syncGroups/serverEndpoints/delete",
      "microsoft.storagesync/storageSyncServices/syncGroups/serverEndpoints/recallAction/action",
      "microsoft.storagesync/storageSyncServices/registeredServers/write",
      "microsoft.storagesync/storageSyncServices/registeredServers/delete",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "file_storage_noaccess_definition" {
  name              = "file-storage-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "microsoft.storagesync/storageSyncServices/read",
      "microsoft.storagesync/storageSyncServices/write",
      "microsoft.storagesync/storageSyncServices/delete",
      "microsoft.storagesync/locations/checkNameAvailability/action",
      "microsoft.storagesync/storageSyncServices/syncGroups/read",
      "microsoft.storagesync/storageSyncServices/syncGroups/write",
      "microsoft.storagesync/storageSyncServices/syncGroups/delete",
      "microsoft.storagesync/storageSyncServices/syncGroups/cloudEndpoints/read",
      "microsoft.storagesync/storageSyncServices/syncGroups/cloudEndpoints/write",
      "microsoft.storagesync/storageSyncServices/syncGroups/cloudEndpoints/delete",
      "microsoft.storagesync/storageSyncServices/syncGroups/cloudEndpoints/prebackup/action",
      "microsoft.storagesync/storageSyncServices/syncGroups/cloudEndpoints/postbackup/action",
      "microsoft.storagesync/storageSyncServices/syncGroups/cloudEndpoints/prerestore/action",
      "microsoft.storagesync/storageSyncServices/syncGroups/cloudEndpoints/postrestore/action",
      "microsoft.storagesync/storageSyncServices/syncGroups/cloudEndpoints/restoreheartbeat/action",
      "microsoft.storagesync/storageSyncServices/syncGroups/cloudEndpoints/operationresults/read",
      "microsoft.storagesync/storageSyncServices/syncGroups/serverEndpoints/read",
      "microsoft.storagesync/storageSyncServices/syncGroups/serverEndpoints/write",
      "microsoft.storagesync/storageSyncServices/syncGroups/serverEndpoints/delete",
      "microsoft.storagesync/storageSyncServices/syncGroups/serverEndpoints/recallAction/action",
      "microsoft.storagesync/storageSyncServices/registeredServers/read",
      "microsoft.storagesync/storageSyncServices/registeredServers/write",
      "microsoft.storagesync/storageSyncServices/registeredServers/delete",
      "microsoft.storagesync/storageSyncServices/workflows/read",
      "microsoft.storagesync/storageSyncServices/workflows/operationresults/read",
      "microsoft.storagesync/storageSyncServices/workflows/operations/read",
      "microsoft.storagesync/locations/workflows/operations/read",
    ]
  }
}
