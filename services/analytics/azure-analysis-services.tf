// FullAccess
resource "azurerm_role_definition" "azure_analysis_services_fullaccess_definition" {
  name              = "azure-analysis-services-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.AnalysisServices/register/action",
      "Microsoft.AnalysisServices/servers/read",
      "Microsoft.AnalysisServices/servers/write",
      "Microsoft.AnalysisServices/servers/delete",
      "Microsoft.AnalysisServices/servers/suspend/action",
      "Microsoft.AnalysisServices/servers/resume/action",
      "Microsoft.AnalysisServices/servers/listGatewayStatus/action",
      "Microsoft.AnalysisServices/locations/checkNameAvailability/action",
      "Microsoft.AnalysisServices/locations/operationresults/read",
      "Microsoft.AnalysisServices/locations/operationstatuses/read",
      "Microsoft.AnalysisServices/operations/read",
      "Microsoft.AnalysisServices/skus/read",
      "Microsoft.AnalysisServices/servers/skus/read",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "azure_analysis_services_readonly_definition" {
  name              = "azure-analysis-services-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.AnalysisServices/servers/read",
      "Microsoft.AnalysisServices/locations/operationresults/read",
      "Microsoft.AnalysisServices/locations/operationstatuses/read",
      "Microsoft.AnalysisServices/operations/read",
      "Microsoft.AnalysisServices/skus/read",
      "Microsoft.AnalysisServices/servers/skus/read",
    ]

    not_actions = [
      "Microsoft.AnalysisServices/register/action",
      "Microsoft.AnalysisServices/servers/write",
      "Microsoft.AnalysisServices/servers/delete",
      "Microsoft.AnalysisServices/servers/suspend/action",
      "Microsoft.AnalysisServices/servers/resume/action",
      "Microsoft.AnalysisServices/servers/listGatewayStatus/action",
      "Microsoft.AnalysisServices/locations/checkNameAvailability/action",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "azure_analysis_services_noaccess_definition" {
  name              = "azure-analysis-services-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.AnalysisServices/register/action",
      "Microsoft.AnalysisServices/servers/read",
      "Microsoft.AnalysisServices/servers/write",
      "Microsoft.AnalysisServices/servers/delete",
      "Microsoft.AnalysisServices/servers/suspend/action",
      "Microsoft.AnalysisServices/servers/resume/action",
      "Microsoft.AnalysisServices/servers/listGatewayStatus/action",
      "Microsoft.AnalysisServices/locations/checkNameAvailability/action",
      "Microsoft.AnalysisServices/locations/operationresults/read",
      "Microsoft.AnalysisServices/locations/operationstatuses/read",
      "Microsoft.AnalysisServices/operations/read",
      "Microsoft.AnalysisServices/skus/read",
      "Microsoft.AnalysisServices/servers/skus/read",
    ]
  }
}
