// FullAccess
resource "azurerm_role_definition" "azure_data_explorer_fullaccess_definition" {
  name              = "azure-data-explorer-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Kusto/Register/action",
      "Microsoft.Kusto/Unregister/action",
      "Microsoft.Kusto/locations/operationresults/read",
      "Microsoft.Kusto/Clusters/read",
      "Microsoft.Kusto/Clusters/write",
      "Microsoft.Kusto/Clusters/delete",
      "Microsoft.Kusto/Clusters/Start/action",
      "Microsoft.Kusto/Clusters/Stop/action",
      "Microsoft.Kusto/Clusters/Activate/action",
      "Microsoft.Kusto/Clusters/Deactivate/action",
      "Microsoft.Kusto/Clusters/CheckNameAvailability/action",
      "Microsoft.Kusto/Clusters/Databases/read",
      "Microsoft.Kusto/Clusters/Databases/write",
      "Microsoft.Kusto/Clusters/Databases/delete",
      "Microsoft.Kusto/Clusters/Databases/ListPrincipals/action",
      "Microsoft.Kusto/Clusters/Databases/AddPrincipals/action",
      "Microsoft.Kusto/Clusters/Databases/RemovePrincipals/action",
      "Microsoft.Kusto/Clusters/Databases/DataConnectionValidation/action",
      "Microsoft.Kusto/Clusters/Databases/CheckNameAvailability/action",
      "Microsoft.Kusto/Clusters/Databases/EventHubConnectionValidation/action",
      "Microsoft.Kusto/Operations/read",
      "Microsoft.Kusto/Clusters/Databases/DataConnections/read",
      "Microsoft.Kusto/Clusters/Databases/DataConnections/write",
      "Microsoft.Kusto/Clusters/Databases/DataConnections/delete",
      "Microsoft.Kusto/Locations/CheckNameAvailability/action",
      "Microsoft.Kusto/Clusters/SKUs/read",
      "Microsoft.Kusto/SKUs/read",
      "Microsoft.Kusto/Clusters/Databases/EventHubConnections/read",
      "Microsoft.Kusto/Clusters/Databases/EventHubConnections/write",
      "Microsoft.Kusto/Clusters/Databases/EventHubConnections/delete",
      "Microsoft.Kusto/Clusters/AttachedDatabaseConfigurations/read",
      "Microsoft.Kusto/Clusters/AttachedDatabaseConfigurations/write",
      "Microsoft.Kusto/Clusters/AttachedDatabaseConfigurations/delete",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "azure_data_explorer_readonly_definition" {
  name              = "azure-data-explorer-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Kusto/locations/operationresults/read",
      "Microsoft.Kusto/Clusters/read",
      "Microsoft.Kusto/Clusters/Databases/read",
      "Microsoft.Kusto/Operations/read",
      "Microsoft.Kusto/Clusters/Databases/DataConnections/read",
      "Microsoft.Kusto/Clusters/SKUs/read",
      "Microsoft.Kusto/SKUs/read",
      "Microsoft.Kusto/Clusters/Databases/EventHubConnections/read",
      "Microsoft.Kusto/Clusters/AttachedDatabaseConfigurations/read",
    ]

    not_actions = [
      "Microsoft.Kusto/Register/action",
      "Microsoft.Kusto/Unregister/action",
      "Microsoft.Kusto/Clusters/write",
      "Microsoft.Kusto/Clusters/delete",
      "Microsoft.Kusto/Clusters/Start/action",
      "Microsoft.Kusto/Clusters/Stop/action",
      "Microsoft.Kusto/Clusters/Activate/action",
      "Microsoft.Kusto/Clusters/Deactivate/action",
      "Microsoft.Kusto/Clusters/CheckNameAvailability/action",
      "Microsoft.Kusto/Clusters/Databases/write",
      "Microsoft.Kusto/Clusters/Databases/delete",
      "Microsoft.Kusto/Clusters/Databases/ListPrincipals/action",
      "Microsoft.Kusto/Clusters/Databases/AddPrincipals/action",
      "Microsoft.Kusto/Clusters/Databases/RemovePrincipals/action",
      "Microsoft.Kusto/Clusters/Databases/DataConnectionValidation/action",
      "Microsoft.Kusto/Clusters/Databases/CheckNameAvailability/action",
      "Microsoft.Kusto/Clusters/Databases/EventHubConnectionValidation/action",
      "Microsoft.Kusto/Clusters/Databases/DataConnections/write",
      "Microsoft.Kusto/Clusters/Databases/DataConnections/delete",
      "Microsoft.Kusto/Locations/CheckNameAvailability/action",
      "Microsoft.Kusto/Clusters/Databases/EventHubConnections/write",
      "Microsoft.Kusto/Clusters/Databases/EventHubConnections/delete",
      "Microsoft.Kusto/Clusters/AttachedDatabaseConfigurations/write",
      "Microsoft.Kusto/Clusters/AttachedDatabaseConfigurations/delete",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "azure_data_explorer_noaccess_definition" {
  name              = "azure-data-explorer-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.Kusto/Register/action",
      "Microsoft.Kusto/Unregister/action",
      "Microsoft.Kusto/locations/operationresults/read",
      "Microsoft.Kusto/Clusters/read",
      "Microsoft.Kusto/Clusters/write",
      "Microsoft.Kusto/Clusters/delete",
      "Microsoft.Kusto/Clusters/Start/action",
      "Microsoft.Kusto/Clusters/Stop/action",
      "Microsoft.Kusto/Clusters/Activate/action",
      "Microsoft.Kusto/Clusters/Deactivate/action",
      "Microsoft.Kusto/Clusters/CheckNameAvailability/action",
      "Microsoft.Kusto/Clusters/Databases/read",
      "Microsoft.Kusto/Clusters/Databases/write",
      "Microsoft.Kusto/Clusters/Databases/delete",
      "Microsoft.Kusto/Clusters/Databases/ListPrincipals/action",
      "Microsoft.Kusto/Clusters/Databases/AddPrincipals/action",
      "Microsoft.Kusto/Clusters/Databases/RemovePrincipals/action",
      "Microsoft.Kusto/Clusters/Databases/DataConnectionValidation/action",
      "Microsoft.Kusto/Clusters/Databases/CheckNameAvailability/action",
      "Microsoft.Kusto/Clusters/Databases/EventHubConnectionValidation/action",
      "Microsoft.Kusto/Operations/read",
      "Microsoft.Kusto/Clusters/Databases/DataConnections/read",
      "Microsoft.Kusto/Clusters/Databases/DataConnections/write",
      "Microsoft.Kusto/Clusters/Databases/DataConnections/delete",
      "Microsoft.Kusto/Locations/CheckNameAvailability/action",
      "Microsoft.Kusto/Clusters/SKUs/read",
      "Microsoft.Kusto/SKUs/read",
      "Microsoft.Kusto/Clusters/Databases/EventHubConnections/read",
      "Microsoft.Kusto/Clusters/Databases/EventHubConnections/write",
      "Microsoft.Kusto/Clusters/Databases/EventHubConnections/delete",
      "Microsoft.Kusto/Clusters/AttachedDatabaseConfigurations/read",
      "Microsoft.Kusto/Clusters/AttachedDatabaseConfigurations/write",
      "Microsoft.Kusto/Clusters/AttachedDatabaseConfigurations/delete",
    ]
  }
}
