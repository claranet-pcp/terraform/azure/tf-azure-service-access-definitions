// FullAccess
resource "azurerm_role_definition" "azure_powerbi_fullaccess_definition" {
  name              = "azure-powerbi-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.PowerBIDedicated/register/action",
      "Microsoft.PowerBIDedicated/capacities/read",
      "Microsoft.PowerBIDedicated/capacities/write",
      "Microsoft.PowerBIDedicated/capacities/delete",
      "Microsoft.PowerBIDedicated/capacities/suspend/action",
      "Microsoft.PowerBIDedicated/capacities/resume/action",
      "Microsoft.PowerBIDedicated/locations/operationresults/read",
      "Microsoft.PowerBIDedicated/locations/operationstatuses/read",
      "Microsoft.PowerBIDedicated/operations/read",
      "Microsoft.PowerBIDedicated/skus/read",
      "Microsoft.PowerBIDedicated/capacities/skus/read",
      "Microsoft.PowerBIDedicated/locations/checkNameAvailability/action",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "azure_powerbi_readonly_definition" {
  name              = "azure-powerbi-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.PowerBIDedicated/capacities/read",
      "Microsoft.PowerBIDedicated/locations/operationresults/read",
      "Microsoft.PowerBIDedicated/locations/operationstatuses/read",
      "Microsoft.PowerBIDedicated/operations/read",
      "Microsoft.PowerBIDedicated/skus/read",
      "Microsoft.PowerBIDedicated/capacities/skus/read",
    ]

    not_actions = [
      "Microsoft.PowerBIDedicated/register/action",
      "Microsoft.PowerBIDedicated/capacities/write",
      "Microsoft.PowerBIDedicated/capacities/delete",
      "Microsoft.PowerBIDedicated/capacities/suspend/action",
      "Microsoft.PowerBIDedicated/capacities/resume/action",
      "Microsoft.PowerBIDedicated/locations/checkNameAvailability/action",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "azure_powerbi_noaccess_definition" {
  name              = "azure-powerbi-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.PowerBIDedicated/register/action",
      "Microsoft.PowerBIDedicated/capacities/read",
      "Microsoft.PowerBIDedicated/capacities/write",
      "Microsoft.PowerBIDedicated/capacities/delete",
      "Microsoft.PowerBIDedicated/capacities/suspend/action",
      "Microsoft.PowerBIDedicated/capacities/resume/action",
      "Microsoft.PowerBIDedicated/locations/operationresults/read",
      "Microsoft.PowerBIDedicated/locations/operationstatuses/read",
      "Microsoft.PowerBIDedicated/operations/read",
      "Microsoft.PowerBIDedicated/skus/read",
      "Microsoft.PowerBIDedicated/capacities/skus/read",
      "Microsoft.PowerBIDedicated/locations/checkNameAvailability/action",
    ]
  }
}
