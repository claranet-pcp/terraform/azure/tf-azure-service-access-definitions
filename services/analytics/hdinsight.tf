// FullAccess
resource "azurerm_role_definition" "hdinsight_fullaccess_definition" {
  name              = "hdinsight-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.HDInsight/clusters/write",
      "Microsoft.HDInsight/clusters/read",
      "Microsoft.HDInsight/clusters/delete",
      "Microsoft.HDInsight/clusters/changerdpsetting/action",
      "Microsoft.HDInsight/clusters/getGatewaySettings/action",
      "Microsoft.HDInsight/clusters/updateGatewaySettings/action",
      "Microsoft.HDInsight/clusters/configurations/action",
      "Microsoft.HDInsight/clusters/configurations/action",
      "Microsoft.HDInsight/clusters/roles/resize/action",
      "Microsoft.HDInsight/clusters/configurations/read",
      "Microsoft.HDInsight/locations/checkNameAvailability/read",
      "Microsoft.HDInsight/locations/capabilities/read",
      "Microsoft.HDInsight/clusters/providers/Microsoft.Insights/diagnosticSettings/read",
      "Microsoft.HDInsight/clusters/providers/Microsoft.Insights/diagnosticSettings/write",
      "Microsoft.HDInsight/clusters/providers/Microsoft.Insights/metricDefinitions/read",
      "Microsoft.HDInsight/clusters/applications/read",
      "Microsoft.HDInsight/clusters/applications/write",
      "Microsoft.HDInsight/clusters/applications/delete",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "hdinsight_readonly_definition" {
  name              = "hdinsight-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.HDInsight/clusters/read",
      "Microsoft.HDInsight/clusters/configurations/read",
      "Microsoft.HDInsight/locations/checkNameAvailability/read",
      "Microsoft.HDInsight/locations/capabilities/read",
      "Microsoft.HDInsight/clusters/providers/Microsoft.Insights/diagnosticSettings/read",
      "Microsoft.HDInsight/clusters/providers/Microsoft.Insights/metricDefinitions/read",
      "Microsoft.HDInsight/clusters/applications/read",
    ]

    not_actions = [
      "Microsoft.HDInsight/clusters/write",
      "Microsoft.HDInsight/clusters/delete",
      "Microsoft.HDInsight/clusters/changerdpsetting/action",
      "Microsoft.HDInsight/clusters/getGatewaySettings/action",
      "Microsoft.HDInsight/clusters/updateGatewaySettings/action",
      "Microsoft.HDInsight/clusters/configurations/action",
      "Microsoft.HDInsight/clusters/configurations/action",
      "Microsoft.HDInsight/clusters/roles/resize/action",
      "Microsoft.HDInsight/clusters/providers/Microsoft.Insights/diagnosticSettings/write",
      "Microsoft.HDInsight/clusters/applications/write",
      "Microsoft.HDInsight/clusters/applications/delete",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "hdinsight_noaccess_definition" {
  name              = "hdinsight-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.HDInsight/clusters/write",
      "Microsoft.HDInsight/clusters/read",
      "Microsoft.HDInsight/clusters/delete",
      "Microsoft.HDInsight/clusters/changerdpsetting/action",
      "Microsoft.HDInsight/clusters/getGatewaySettings/action",
      "Microsoft.HDInsight/clusters/updateGatewaySettings/action",
      "Microsoft.HDInsight/clusters/configurations/action",
      "Microsoft.HDInsight/clusters/configurations/action",
      "Microsoft.HDInsight/clusters/roles/resize/action",
      "Microsoft.HDInsight/clusters/configurations/read",
      "Microsoft.HDInsight/locations/checkNameAvailability/read",
      "Microsoft.HDInsight/locations/capabilities/read",
      "Microsoft.HDInsight/clusters/providers/Microsoft.Insights/diagnosticSettings/read",
      "Microsoft.HDInsight/clusters/providers/Microsoft.Insights/diagnosticSettings/write",
      "Microsoft.HDInsight/clusters/providers/Microsoft.Insights/metricDefinitions/read",
      "Microsoft.HDInsight/clusters/applications/read",
      "Microsoft.HDInsight/clusters/applications/write",
      "Microsoft.HDInsight/clusters/applications/delete",
    ]
  }
}
