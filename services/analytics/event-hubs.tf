// FullAccess
resource "azurerm_role_definition" "event_hubs_fullaccess_definition" {
  name              = "event-hubs-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.EventHub/checkNamespaceAvailability/action",
      "Microsoft.EventHub/checkNameAvailability/action",
      "Microsoft.EventHub/register/action",
      "Microsoft.EventHub/unregister/action",
      "Microsoft.EventHub/namespaces/write",
      "Microsoft.EventHub/namespaces/read",
      "Microsoft.EventHub/namespaces/Delete",
      "Microsoft.EventHub/namespaces/authorizationRules/action",
      "Microsoft.EventHub/namespaces/removeAcsNamepsace/action",
      "Microsoft.EventHub/namespaces/operationresults/read",
      "Microsoft.EventHub/namespaces/authorizationRules/read",
      "Microsoft.EventHub/namespaces/authorizationRules/write",
      "Microsoft.EventHub/namespaces/authorizationRules/delete",
      "Microsoft.EventHub/namespaces/authorizationRules/listkeys/action",
      "Microsoft.EventHub/namespaces/authorizationRules/regenerateKeys/action",
      "Microsoft.EventHub/namespaces/messagingPlan/read",
      "Microsoft.EventHub/namespaces/messagingPlan/write",
      "Microsoft.EventHub/namespaces/eventhubs/write",
      "Microsoft.EventHub/namespaces/eventhubs/read",
      "Microsoft.EventHub/namespaces/eventhubs/Delete",
      "Microsoft.EventHub/namespaces/eventhubs/authorizationRules/action",
      "Microsoft.EventHub/namespaces/eventhubs/authorizationRules/read",
      "Microsoft.EventHub/namespaces/eventhubs/authorizationRules/write",
      "Microsoft.EventHub/namespaces/eventhubs/authorizationRules/delete",
      "Microsoft.EventHub/namespaces/eventhubs/authorizationRules/listkeys/action",
      "Microsoft.EventHub/namespaces/eventhubs/authorizationRules/regenerateKeys/action",
      "Microsoft.EventHub/namespaces/eventHubs/consumergroups/write",
      "Microsoft.EventHub/namespaces/eventHubs/consumergroups/read",
      "Microsoft.EventHub/namespaces/eventHubs/consumergroups/Delete",
      "Microsoft.EventHub/sku/read",
      "Microsoft.EventHub/sku/regions/read",
      "Microsoft.EventHub/operations/read",
      "Microsoft.EventHub/namespaces/providers/Microsoft.Insights/metricDefinitions/read",
      "Microsoft.EventHub/clusters/providers/Microsoft.Insights/metricDefinitions/read",
      "Microsoft.EventHub/namespaces/providers/Microsoft.Insights/diagnosticSettings/read",
      "Microsoft.EventHub/namespaces/providers/Microsoft.Insights/diagnosticSettings/write",
      "Microsoft.EventHub/namespaces/providers/Microsoft.Insights/logDefinitions/read",
      "Microsoft.EventHub/namespaces/disasterrecoveryconfigs/checkNameAvailability/action",
      "Microsoft.EventHub/namespaces/disasterRecoveryConfigs/write",
      "Microsoft.EventHub/namespaces/disasterRecoveryConfigs/read",
      "Microsoft.EventHub/namespaces/disasterRecoveryConfigs/delete",
      "Microsoft.EventHub/namespaces/disasterRecoveryConfigs/breakPairing/action",
      "Microsoft.EventHub/namespaces/disasterRecoveryConfigs/failover/action",
      "Microsoft.EventHub/namespaces/disasterRecoveryConfigs/authorizationRules/read",
      "Microsoft.EventHub/namespaces/disasterRecoveryConfigs/authorizationRules/listkeys/action",
      "Microsoft.EventHub/namespaces/ipFilterRules/read",
      "Microsoft.EventHub/namespaces/ipFilterRules/write",
      "Microsoft.EventHub/namespaces/ipFilterRules/delete",
      "Microsoft.EventHub/namespaces/virtualNetworkRules/read",
      "Microsoft.EventHub/namespaces/virtualNetworkRules/write",
      "Microsoft.EventHub/namespaces/virtualNetworkRules/delete",
      "Microsoft.EventHub/locations/deleteVirtualNetworkOrSubnets/action",
      "Microsoft.EventHub/namespaces/networkrulesets/read",
      "Microsoft.EventHub/namespaces/networkrulesets/write",
      "Microsoft.EventHub/namespaces/networkrulesets/delete",
      "Microsoft.EventHub/namespaces/messages/send/action",
      "Microsoft.EventHub/namespaces/messages/receive/action",
      "Microsoft.EventHub/clusters/read",
      "Microsoft.EventHub/clusters/write",
      "Microsoft.EventHub/clusters/delete",
      "Microsoft.EventHub/availableClusterRegions/read",
      "Microsoft.EventHub/clusters/namespaces/read",
      "Microsoft.EventHub/clusters/operationresults/read",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "event_hubs_readonly_definition" {
  name              = "event-hubs-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.EventHub/namespaces/read",
      "Microsoft.EventHub/namespaces/operationresults/read",
      "Microsoft.EventHub/namespaces/authorizationRules/read",
      "Microsoft.EventHub/namespaces/messagingPlan/read",
      "Microsoft.EventHub/namespaces/eventhubs/read",
      "Microsoft.EventHub/namespaces/eventhubs/authorizationRules/read",
      "Microsoft.EventHub/namespaces/eventHubs/consumergroups/read",
      "Microsoft.EventHub/sku/read",
      "Microsoft.EventHub/sku/regions/read",
      "Microsoft.EventHub/operations/read",
      "Microsoft.EventHub/namespaces/providers/Microsoft.Insights/metricDefinitions/read",
      "Microsoft.EventHub/clusters/providers/Microsoft.Insights/metricDefinitions/read",
      "Microsoft.EventHub/namespaces/providers/Microsoft.Insights/diagnosticSettings/read",
      "Microsoft.EventHub/namespaces/providers/Microsoft.Insights/logDefinitions/read",
      "Microsoft.EventHub/namespaces/disasterRecoveryConfigs/read",
      "Microsoft.EventHub/namespaces/disasterRecoveryConfigs/authorizationRules/read",
      "Microsoft.EventHub/namespaces/ipFilterRules/read",
      "Microsoft.EventHub/namespaces/virtualNetworkRules/read",
      "Microsoft.EventHub/namespaces/networkrulesets/read",
      "Microsoft.EventHub/clusters/read",
      "Microsoft.EventHub/availableClusterRegions/read",
      "Microsoft.EventHub/clusters/namespaces/read",
      "Microsoft.EventHub/clusters/operationresults/read",
    ]

    not_actions = [
      "Microsoft.EventHub/checkNamespaceAvailability/action",
      "Microsoft.EventHub/checkNameAvailability/action",
      "Microsoft.EventHub/register/action",
      "Microsoft.EventHub/unregister/action",
      "Microsoft.EventHub/namespaces/write",
      "Microsoft.EventHub/namespaces/Delete",
      "Microsoft.EventHub/namespaces/authorizationRules/action",
      "Microsoft.EventHub/namespaces/removeAcsNamepsace/action",
      "Microsoft.EventHub/namespaces/authorizationRules/write",
      "Microsoft.EventHub/namespaces/authorizationRules/delete",
      "Microsoft.EventHub/namespaces/authorizationRules/listkeys/action",
      "Microsoft.EventHub/namespaces/authorizationRules/regenerateKeys/action",
      "Microsoft.EventHub/namespaces/messagingPlan/write",
      "Microsoft.EventHub/namespaces/eventhubs/write",
      "Microsoft.EventHub/namespaces/eventhubs/Delete",
      "Microsoft.EventHub/namespaces/eventhubs/authorizationRules/action",
      "Microsoft.EventHub/namespaces/eventhubs/authorizationRules/write",
      "Microsoft.EventHub/namespaces/eventhubs/authorizationRules/delete",
      "Microsoft.EventHub/namespaces/eventhubs/authorizationRules/listkeys/action",
      "Microsoft.EventHub/namespaces/eventhubs/authorizationRules/regenerateKeys/action",
      "Microsoft.EventHub/namespaces/eventHubs/consumergroups/write",
      "Microsoft.EventHub/namespaces/eventHubs/consumergroups/Delete",
      "Microsoft.EventHub/namespaces/providers/Microsoft.Insights/diagnosticSettings/write",
      "Microsoft.EventHub/namespaces/disasterrecoveryconfigs/checkNameAvailability/action",
      "Microsoft.EventHub/namespaces/disasterRecoveryConfigs/write",
      "Microsoft.EventHub/namespaces/disasterRecoveryConfigs/delete",
      "Microsoft.EventHub/namespaces/disasterRecoveryConfigs/breakPairing/action",
      "Microsoft.EventHub/namespaces/disasterRecoveryConfigs/failover/action",
      "Microsoft.EventHub/namespaces/disasterRecoveryConfigs/authorizationRules/listkeys/action",
      "Microsoft.EventHub/namespaces/ipFilterRules/write",
      "Microsoft.EventHub/namespaces/ipFilterRules/delete",
      "Microsoft.EventHub/namespaces/virtualNetworkRules/write",
      "Microsoft.EventHub/namespaces/virtualNetworkRules/delete",
      "Microsoft.EventHub/locations/deleteVirtualNetworkOrSubnets/action",
      "Microsoft.EventHub/namespaces/networkrulesets/write",
      "Microsoft.EventHub/namespaces/networkrulesets/delete",
      "Microsoft.EventHub/namespaces/messages/send/action",
      "Microsoft.EventHub/namespaces/messages/receive/action",
      "Microsoft.EventHub/clusters/write",
      "Microsoft.EventHub/clusters/delete",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "event_hubs_noaccess_definition" {
  name              = "event-hubs-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.EventHub/checkNamespaceAvailability/action",
      "Microsoft.EventHub/checkNameAvailability/action",
      "Microsoft.EventHub/register/action",
      "Microsoft.EventHub/unregister/action",
      "Microsoft.EventHub/namespaces/write",
      "Microsoft.EventHub/namespaces/read",
      "Microsoft.EventHub/namespaces/Delete",
      "Microsoft.EventHub/namespaces/authorizationRules/action",
      "Microsoft.EventHub/namespaces/removeAcsNamepsace/action",
      "Microsoft.EventHub/namespaces/operationresults/read",
      "Microsoft.EventHub/namespaces/authorizationRules/read",
      "Microsoft.EventHub/namespaces/authorizationRules/write",
      "Microsoft.EventHub/namespaces/authorizationRules/delete",
      "Microsoft.EventHub/namespaces/authorizationRules/listkeys/action",
      "Microsoft.EventHub/namespaces/authorizationRules/regenerateKeys/action",
      "Microsoft.EventHub/namespaces/messagingPlan/read",
      "Microsoft.EventHub/namespaces/messagingPlan/write",
      "Microsoft.EventHub/namespaces/eventhubs/write",
      "Microsoft.EventHub/namespaces/eventhubs/read",
      "Microsoft.EventHub/namespaces/eventhubs/Delete",
      "Microsoft.EventHub/namespaces/eventhubs/authorizationRules/action",
      "Microsoft.EventHub/namespaces/eventhubs/authorizationRules/read",
      "Microsoft.EventHub/namespaces/eventhubs/authorizationRules/write",
      "Microsoft.EventHub/namespaces/eventhubs/authorizationRules/delete",
      "Microsoft.EventHub/namespaces/eventhubs/authorizationRules/listkeys/action",
      "Microsoft.EventHub/namespaces/eventhubs/authorizationRules/regenerateKeys/action",
      "Microsoft.EventHub/namespaces/eventHubs/consumergroups/write",
      "Microsoft.EventHub/namespaces/eventHubs/consumergroups/read",
      "Microsoft.EventHub/namespaces/eventHubs/consumergroups/Delete",
      "Microsoft.EventHub/sku/read",
      "Microsoft.EventHub/sku/regions/read",
      "Microsoft.EventHub/operations/read",
      "Microsoft.EventHub/namespaces/providers/Microsoft.Insights/metricDefinitions/read",
      "Microsoft.EventHub/clusters/providers/Microsoft.Insights/metricDefinitions/read",
      "Microsoft.EventHub/namespaces/providers/Microsoft.Insights/diagnosticSettings/read",
      "Microsoft.EventHub/namespaces/providers/Microsoft.Insights/diagnosticSettings/write",
      "Microsoft.EventHub/namespaces/providers/Microsoft.Insights/logDefinitions/read",
      "Microsoft.EventHub/namespaces/disasterrecoveryconfigs/checkNameAvailability/action",
      "Microsoft.EventHub/namespaces/disasterRecoveryConfigs/write",
      "Microsoft.EventHub/namespaces/disasterRecoveryConfigs/read",
      "Microsoft.EventHub/namespaces/disasterRecoveryConfigs/delete",
      "Microsoft.EventHub/namespaces/disasterRecoveryConfigs/breakPairing/action",
      "Microsoft.EventHub/namespaces/disasterRecoveryConfigs/failover/action",
      "Microsoft.EventHub/namespaces/disasterRecoveryConfigs/authorizationRules/read",
      "Microsoft.EventHub/namespaces/disasterRecoveryConfigs/authorizationRules/listkeys/action",
      "Microsoft.EventHub/namespaces/ipFilterRules/read",
      "Microsoft.EventHub/namespaces/ipFilterRules/write",
      "Microsoft.EventHub/namespaces/ipFilterRules/delete",
      "Microsoft.EventHub/namespaces/virtualNetworkRules/read",
      "Microsoft.EventHub/namespaces/virtualNetworkRules/write",
      "Microsoft.EventHub/namespaces/virtualNetworkRules/delete",
      "Microsoft.EventHub/locations/deleteVirtualNetworkOrSubnets/action",
      "Microsoft.EventHub/namespaces/networkrulesets/read",
      "Microsoft.EventHub/namespaces/networkrulesets/write",
      "Microsoft.EventHub/namespaces/networkrulesets/delete",
      "Microsoft.EventHub/namespaces/messages/send/action",
      "Microsoft.EventHub/namespaces/messages/receive/action",
      "Microsoft.EventHub/clusters/read",
      "Microsoft.EventHub/clusters/write",
      "Microsoft.EventHub/clusters/delete",
      "Microsoft.EventHub/availableClusterRegions/read",
      "Microsoft.EventHub/clusters/namespaces/read",
      "Microsoft.EventHub/clusters/operationresults/read",
    ]
  }
}
