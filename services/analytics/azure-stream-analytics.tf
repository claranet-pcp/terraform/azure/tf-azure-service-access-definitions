// FullAccess
resource "azurerm_role_definition" "azure_stream_analytics_fullaccess_definition" {
  name              = "azure-stream-analytics-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.StreamAnalytics/Register/action",
      "Microsoft.StreamAnalytics/streamingjobs/Delete",
      "Microsoft.StreamAnalytics/streamingjobs/Read",
      "Microsoft.StreamAnalytics/streamingjobs/Start/action",
      "Microsoft.StreamAnalytics/streamingjobs/Stop/action",
      "Microsoft.StreamAnalytics/streamingjobs/Write",
      "Microsoft.StreamAnalytics/streamingjobs/functions/Delete",
      "Microsoft.StreamAnalytics/streamingjobs/functions/Read",
      "Microsoft.StreamAnalytics/streamingjobs/functions/RetrieveDefaultDefinition/action",
      "Microsoft.StreamAnalytics/streamingjobs/functions/Test/action",
      "Microsoft.StreamAnalytics/streamingjobs/functions/Write",
      "Microsoft.StreamAnalytics/streamingjobs/inputs/Delete",
      "Microsoft.StreamAnalytics/streamingjobs/inputs/Read",
      "Microsoft.StreamAnalytics/streamingjobs/inputs/Sample/action",
      "Microsoft.StreamAnalytics/streamingjobs/inputs/Test/action",
      "Microsoft.StreamAnalytics/streamingjobs/inputs/Write",
      "Microsoft.StreamAnalytics/streamingjobs/outputs/Delete",
      "Microsoft.StreamAnalytics/streamingjobs/outputs/Read",
      "Microsoft.StreamAnalytics/streamingjobs/outputs/Test/action",
      "Microsoft.StreamAnalytics/streamingjobs/outputs/Write",
      "Microsoft.StreamAnalytics/streamingjobs/providers/Microsoft.Insights/logDefinitions/read",
      "Microsoft.StreamAnalytics/streamingjobs/providers/Microsoft.Insights/metricDefinitions/read",
      "Microsoft.StreamAnalytics/streamingjobs/transformations/Delete",
      "Microsoft.StreamAnalytics/streamingjobs/transformations/Read",
      "Microsoft.StreamAnalytics/streamingjobs/transformations/Write",
      "Microsoft.StreamAnalytics/locations/quotas/Read",
      "Microsoft.StreamAnalytics/operations/Read",
      "Microsoft.StreamAnalytics/streamingjobs/functions/operationresults/Read",
      "Microsoft.StreamAnalytics/streamingjobs/inputs/operationresults/Read",
      "Microsoft.StreamAnalytics/streamingjobs/metricdefinitions/Read",
      "Microsoft.StreamAnalytics/streamingjobs/operationresults/Read",
      "Microsoft.StreamAnalytics/streamingjobs/outputs/operationresults/Read",
      "Microsoft.StreamAnalytics/streamingjobs/providers/Microsoft.Insights/diagnosticSettings/read",
      "Microsoft.StreamAnalytics/streamingjobs/providers/Microsoft.Insights/diagnosticSettings/write",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "azure_stream_analytics_readonly_definition" {
  name              = "azure-stream-analytics-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.StreamAnalytics/streamingjobs/Read",
      "Microsoft.StreamAnalytics/streamingjobs/functions/Read",
      "Microsoft.StreamAnalytics/streamingjobs/inputs/Read",
      "Microsoft.StreamAnalytics/streamingjobs/outputs/Read",
      "Microsoft.StreamAnalytics/streamingjobs/providers/Microsoft.Insights/logDefinitions/read",
      "Microsoft.StreamAnalytics/streamingjobs/providers/Microsoft.Insights/metricDefinitions/read",
      "Microsoft.StreamAnalytics/streamingjobs/transformations/Read",
      "Microsoft.StreamAnalytics/locations/quotas/Read",
      "Microsoft.StreamAnalytics/operations/Read",
      "Microsoft.StreamAnalytics/streamingjobs/functions/operationresults/Read",
      "Microsoft.StreamAnalytics/streamingjobs/inputs/operationresults/Read",
      "Microsoft.StreamAnalytics/streamingjobs/metricdefinitions/Read",
      "Microsoft.StreamAnalytics/streamingjobs/operationresults/Read",
      "Microsoft.StreamAnalytics/streamingjobs/outputs/operationresults/Read",
      "Microsoft.StreamAnalytics/streamingjobs/providers/Microsoft.Insights/diagnosticSettings/read",
    ]

    not_actions = [
      "Microsoft.StreamAnalytics/Register/action",
      "Microsoft.StreamAnalytics/streamingjobs/Delete",
      "Microsoft.StreamAnalytics/streamingjobs/Start/action",
      "Microsoft.StreamAnalytics/streamingjobs/Stop/action",
      "Microsoft.StreamAnalytics/streamingjobs/Write",
      "Microsoft.StreamAnalytics/streamingjobs/functions/Delete",
      "Microsoft.StreamAnalytics/streamingjobs/functions/RetrieveDefaultDefinition/action",
      "Microsoft.StreamAnalytics/streamingjobs/functions/Test/action",
      "Microsoft.StreamAnalytics/streamingjobs/functions/Write",
      "Microsoft.StreamAnalytics/streamingjobs/inputs/Delete",
      "Microsoft.StreamAnalytics/streamingjobs/inputs/Sample/action",
      "Microsoft.StreamAnalytics/streamingjobs/inputs/Test/action",
      "Microsoft.StreamAnalytics/streamingjobs/inputs/Write",
      "Microsoft.StreamAnalytics/streamingjobs/outputs/Delete",
      "Microsoft.StreamAnalytics/streamingjobs/outputs/Test/action",
      "Microsoft.StreamAnalytics/streamingjobs/outputs/Write",
      "Microsoft.StreamAnalytics/streamingjobs/transformations/Delete",
      "Microsoft.StreamAnalytics/streamingjobs/transformations/Write",
      "Microsoft.StreamAnalytics/streamingjobs/providers/Microsoft.Insights/diagnosticSettings/write",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "azure_stream_analytics_noaccess_definition" {
  name              = "azure-stream-analytics-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.StreamAnalytics/Register/action",
      "Microsoft.StreamAnalytics/streamingjobs/Delete",
      "Microsoft.StreamAnalytics/streamingjobs/Read",
      "Microsoft.StreamAnalytics/streamingjobs/Start/action",
      "Microsoft.StreamAnalytics/streamingjobs/Stop/action",
      "Microsoft.StreamAnalytics/streamingjobs/Write",
      "Microsoft.StreamAnalytics/streamingjobs/functions/Delete",
      "Microsoft.StreamAnalytics/streamingjobs/functions/Read",
      "Microsoft.StreamAnalytics/streamingjobs/functions/RetrieveDefaultDefinition/action",
      "Microsoft.StreamAnalytics/streamingjobs/functions/Test/action",
      "Microsoft.StreamAnalytics/streamingjobs/functions/Write",
      "Microsoft.StreamAnalytics/streamingjobs/inputs/Delete",
      "Microsoft.StreamAnalytics/streamingjobs/inputs/Read",
      "Microsoft.StreamAnalytics/streamingjobs/inputs/Sample/action",
      "Microsoft.StreamAnalytics/streamingjobs/inputs/Test/action",
      "Microsoft.StreamAnalytics/streamingjobs/inputs/Write",
      "Microsoft.StreamAnalytics/streamingjobs/outputs/Delete",
      "Microsoft.StreamAnalytics/streamingjobs/outputs/Read",
      "Microsoft.StreamAnalytics/streamingjobs/outputs/Test/action",
      "Microsoft.StreamAnalytics/streamingjobs/outputs/Write",
      "Microsoft.StreamAnalytics/streamingjobs/providers/Microsoft.Insights/logDefinitions/read",
      "Microsoft.StreamAnalytics/streamingjobs/providers/Microsoft.Insights/metricDefinitions/read",
      "Microsoft.StreamAnalytics/streamingjobs/transformations/Delete",
      "Microsoft.StreamAnalytics/streamingjobs/transformations/Read",
      "Microsoft.StreamAnalytics/streamingjobs/transformations/Write",
      "Microsoft.StreamAnalytics/locations/quotas/Read",
      "Microsoft.StreamAnalytics/operations/Read",
      "Microsoft.StreamAnalytics/streamingjobs/functions/operationresults/Read",
      "Microsoft.StreamAnalytics/streamingjobs/inputs/operationresults/Read",
      "Microsoft.StreamAnalytics/streamingjobs/metricdefinitions/Read",
      "Microsoft.StreamAnalytics/streamingjobs/operationresults/Read",
      "Microsoft.StreamAnalytics/streamingjobs/outputs/operationresults/Read",
      "Microsoft.StreamAnalytics/streamingjobs/providers/Microsoft.Insights/diagnosticSettings/read",
      "Microsoft.StreamAnalytics/streamingjobs/providers/Microsoft.Insights/diagnosticSettings/write",
    ]
  }
}
