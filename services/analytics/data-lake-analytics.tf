// FullAccess
resource "azurerm_role_definition" "data_lake_analytics_fullaccess_definition" {
  name              = "data-lake-analytics-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.DataLakeAnalytics/register/action",
      "Microsoft.DataLakeAnalytics/operations/read",
      "Microsoft.DataLakeAnalytics/accounts/read",
      "Microsoft.DataLakeAnalytics/accounts/write",
      "Microsoft.DataLakeAnalytics/accounts/delete",
      "Microsoft.DataLakeAnalytics/accounts/transferAnalyticsUnits/action",
      "Microsoft.DataLakeAnalytics/accounts/TakeOwnership/action",
      "Microsoft.DataLakeAnalytics/accounts/operationResults/read",
      "Microsoft.DataLakeAnalytics/accounts/firewallRules/read",
      "Microsoft.DataLakeAnalytics/accounts/firewallRules/write",
      "Microsoft.DataLakeAnalytics/accounts/firewallRules/delete",
      "Microsoft.DataLakeAnalytics/accounts/dataLakeStoreAccounts/read",
      "Microsoft.DataLakeAnalytics/accounts/dataLakeStoreAccounts/write",
      "Microsoft.DataLakeAnalytics/accounts/dataLakeStoreAccounts/delete",
      "Microsoft.DataLakeAnalytics/accounts/storageAccounts/read",
      "Microsoft.DataLakeAnalytics/accounts/storageAccounts/write",
      "Microsoft.DataLakeAnalytics/accounts/storageAccounts/delete",
      "Microsoft.DataLakeAnalytics/accounts/storageAccounts/Containers/read",
      "Microsoft.DataLakeAnalytics/accounts/storageAccounts/Containers/listSasTokens/action",
      "Microsoft.DataLakeAnalytics/accounts/computePolicies/read",
      "Microsoft.DataLakeAnalytics/accounts/computePolicies/write",
      "Microsoft.DataLakeAnalytics/accounts/computePolicies/delete",
      "Microsoft.DataLakeAnalytics/locations/capability/read",
      "Microsoft.DataLakeAnalytics/locations/checkNameAvailability/action",
      "Microsoft.DataLakeAnalytics/locations/operationResults/read",
      "Microsoft.DataLakeAnalytics/locations/usages/read",
      "Microsoft.DataLakeAnalytics/accounts/virtualNetworkRules/read",
      "Microsoft.DataLakeAnalytics/accounts/virtualNetworkRules/write",
      "Microsoft.DataLakeAnalytics/accounts/virtualNetworkRules/delete",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "data_lake_analytics_readonly_definition" {
  name              = "data-lake-analytics-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.DataLakeAnalytics/operations/read",
      "Microsoft.DataLakeAnalytics/accounts/read",
      "Microsoft.DataLakeAnalytics/accounts/operationResults/read",
      "Microsoft.DataLakeAnalytics/accounts/firewallRules/read",
      "Microsoft.DataLakeAnalytics/accounts/dataLakeStoreAccounts/read",
      "Microsoft.DataLakeAnalytics/accounts/storageAccounts/read",
      "Microsoft.DataLakeAnalytics/accounts/storageAccounts/Containers/read",
      "Microsoft.DataLakeAnalytics/accounts/computePolicies/read",
      "Microsoft.DataLakeAnalytics/locations/capability/read",
      "Microsoft.DataLakeAnalytics/locations/operationResults/read",
      "Microsoft.DataLakeAnalytics/locations/usages/read",
      "Microsoft.DataLakeAnalytics/accounts/virtualNetworkRules/read",
    ]

    not_actions = [
      "Microsoft.DataLakeAnalytics/register/action",
      "Microsoft.DataLakeAnalytics/accounts/write",
      "Microsoft.DataLakeAnalytics/accounts/delete",
      "Microsoft.DataLakeAnalytics/accounts/transferAnalyticsUnits/action",
      "Microsoft.DataLakeAnalytics/accounts/TakeOwnership/action",
      "Microsoft.DataLakeAnalytics/accounts/firewallRules/write",
      "Microsoft.DataLakeAnalytics/accounts/firewallRules/delete",
      "Microsoft.DataLakeAnalytics/accounts/dataLakeStoreAccounts/write",
      "Microsoft.DataLakeAnalytics/accounts/dataLakeStoreAccounts/delete",
      "Microsoft.DataLakeAnalytics/accounts/storageAccounts/write",
      "Microsoft.DataLakeAnalytics/accounts/storageAccounts/delete",
      "Microsoft.DataLakeAnalytics/accounts/storageAccounts/Containers/listSasTokens/action",
      "Microsoft.DataLakeAnalytics/accounts/computePolicies/write",
      "Microsoft.DataLakeAnalytics/accounts/computePolicies/delete",
      "Microsoft.DataLakeAnalytics/locations/checkNameAvailability/action",
      "Microsoft.DataLakeAnalytics/accounts/virtualNetworkRules/write",
      "Microsoft.DataLakeAnalytics/accounts/virtualNetworkRules/delete",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "data_lake_analytics_noaccess_definition" {
  name              = "data-lake-analytics-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.DataLakeAnalytics/register/action",
      "Microsoft.DataLakeAnalytics/operations/read",
      "Microsoft.DataLakeAnalytics/accounts/read",
      "Microsoft.DataLakeAnalytics/accounts/write",
      "Microsoft.DataLakeAnalytics/accounts/delete",
      "Microsoft.DataLakeAnalytics/accounts/transferAnalyticsUnits/action",
      "Microsoft.DataLakeAnalytics/accounts/TakeOwnership/action",
      "Microsoft.DataLakeAnalytics/accounts/operationResults/read",
      "Microsoft.DataLakeAnalytics/accounts/firewallRules/read",
      "Microsoft.DataLakeAnalytics/accounts/firewallRules/write",
      "Microsoft.DataLakeAnalytics/accounts/firewallRules/delete",
      "Microsoft.DataLakeAnalytics/accounts/dataLakeStoreAccounts/read",
      "Microsoft.DataLakeAnalytics/accounts/dataLakeStoreAccounts/write",
      "Microsoft.DataLakeAnalytics/accounts/dataLakeStoreAccounts/delete",
      "Microsoft.DataLakeAnalytics/accounts/storageAccounts/read",
      "Microsoft.DataLakeAnalytics/accounts/storageAccounts/write",
      "Microsoft.DataLakeAnalytics/accounts/storageAccounts/delete",
      "Microsoft.DataLakeAnalytics/accounts/storageAccounts/Containers/read",
      "Microsoft.DataLakeAnalytics/accounts/storageAccounts/Containers/listSasTokens/action",
      "Microsoft.DataLakeAnalytics/accounts/computePolicies/read",
      "Microsoft.DataLakeAnalytics/accounts/computePolicies/write",
      "Microsoft.DataLakeAnalytics/accounts/computePolicies/delete",
      "Microsoft.DataLakeAnalytics/locations/capability/read",
      "Microsoft.DataLakeAnalytics/locations/checkNameAvailability/action",
      "Microsoft.DataLakeAnalytics/locations/operationResults/read",
      "Microsoft.DataLakeAnalytics/locations/usages/read",
      "Microsoft.DataLakeAnalytics/accounts/virtualNetworkRules/read",
      "Microsoft.DataLakeAnalytics/accounts/virtualNetworkRules/write",
      "Microsoft.DataLakeAnalytics/accounts/virtualNetworkRules/delete",
    ]
  }
}
