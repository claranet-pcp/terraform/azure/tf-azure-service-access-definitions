// FullAccess
resource "azurerm_role_definition" "data_catalog_fullaccess_definition" {
  name              = "data-catalog-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.DataCatalog/register/action",
      "Microsoft.DataCatalog/unregister/action",
      "Microsoft.DataCatalog/operations/read",
      "Microsoft.DataCatalog/datacatalogs/read",
      "Microsoft.DataCatalog/datacatalogs/write",
      "Microsoft.DataCatalog/datacatalogs/delete",
      "Microsoft.DataCatalog/catalogs/read",
      "Microsoft.DataCatalog/catalogs/write",
      "Microsoft.DataCatalog/catalogs/delete",
      "Microsoft.DataCatalog/checkNameAvailability/read",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "data_catalog_readonly_definition" {
  name              = "data-catalog-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.DataCatalog/operations/read",
      "Microsoft.DataCatalog/datacatalogs/read",
      "Microsoft.DataCatalog/catalogs/read",
      "Microsoft.DataCatalog/checkNameAvailability/read",
    ]

    not_actions = [
      "Microsoft.DataCatalog/register/action",
      "Microsoft.DataCatalog/unregister/action",
      "Microsoft.DataCatalog/datacatalogs/write",
      "Microsoft.DataCatalog/datacatalogs/delete",
      "Microsoft.DataCatalog/catalogs/write",
      "Microsoft.DataCatalog/catalogs/delete",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "data_catalog_noaccess_definition" {
  name              = "data-catalog-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.DataCatalog/register/action",
      "Microsoft.DataCatalog/unregister/action",
      "Microsoft.DataCatalog/operations/read",
      "Microsoft.DataCatalog/datacatalogs/read",
      "Microsoft.DataCatalog/datacatalogs/write",
      "Microsoft.DataCatalog/datacatalogs/delete",
      "Microsoft.DataCatalog/catalogs/read",
      "Microsoft.DataCatalog/catalogs/write",
      "Microsoft.DataCatalog/catalogs/delete",
      "Microsoft.DataCatalog/checkNameAvailability/read",
    ]
  }
}
