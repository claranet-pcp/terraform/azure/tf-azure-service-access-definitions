// FullAccess
resource "azurerm_role_definition" "azure_blockchain_service_fullaccess_definition" {
  name              = "azure-blockchain-service-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Blockchain/register/action",
      "Microsoft.Blockchain/blockchainMembers/read",
      "Microsoft.Blockchain/blockchainMembers/write",
      "Microsoft.Blockchain/blockchainMembers/delete",
      "Microsoft.Blockchain/blockchainMembers/listApiKeys/action",
      "Microsoft.Blockchain/blockchainMembers/transactionNodes/read",
      "Microsoft.Blockchain/blockchainMembers/transactionNodes/write",
      "Microsoft.Blockchain/blockchainMembers/transactionNodes/delete",
      "Microsoft.Blockchain/blockchainMembers/transactionNodes/listApiKeys/action",
      "Microsoft.Blockchain/blockchainMembers/transactionNodes/connect/action",
      "Microsoft.Blockchain/locations/blockchainMemberOperationResults/read",
      "Microsoft.Blockchain/locations/checkNameAvailability/action",
      "Microsoft.Blockchain/operations/read",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "azure_blockchain_service_readonly_definition" {
  name              = "azure-blockchain-service-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Blockchain/blockchainMembers/read",
      "Microsoft.Blockchain/blockchainMembers/transactionNodes/read",
      "Microsoft.Blockchain/locations/blockchainMemberOperationResults/read",
      "Microsoft.Blockchain/operations/read",
    ]

    not_actions = [
      "Microsoft.Blockchain/register/action",
      "Microsoft.Blockchain/blockchainMembers/write",
      "Microsoft.Blockchain/blockchainMembers/delete",
      "Microsoft.Blockchain/blockchainMembers/listApiKeys/action",
      "Microsoft.Blockchain/blockchainMembers/transactionNodes/write",
      "Microsoft.Blockchain/blockchainMembers/transactionNodes/delete",
      "Microsoft.Blockchain/blockchainMembers/transactionNodes/listApiKeys/action",
      "Microsoft.Blockchain/blockchainMembers/transactionNodes/connect/action",
      "Microsoft.Blockchain/locations/checkNameAvailability/action",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "azure_blockchain_service_noaccess_definition" {
  name              = "azure-blockchain-service-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.Blockchain/register/action",
      "Microsoft.Blockchain/blockchainMembers/read",
      "Microsoft.Blockchain/blockchainMembers/write",
      "Microsoft.Blockchain/blockchainMembers/delete",
      "Microsoft.Blockchain/blockchainMembers/listApiKeys/action",
      "Microsoft.Blockchain/blockchainMembers/transactionNodes/read",
      "Microsoft.Blockchain/blockchainMembers/transactionNodes/write",
      "Microsoft.Blockchain/blockchainMembers/transactionNodes/delete",
      "Microsoft.Blockchain/blockchainMembers/transactionNodes/listApiKeys/action",
      "Microsoft.Blockchain/blockchainMembers/transactionNodes/connect/action",
      "Microsoft.Blockchain/locations/blockchainMemberOperationResults/read",
      "Microsoft.Blockchain/locations/checkNameAvailability/action",
      "Microsoft.Blockchain/operations/read",
    ]
  }
}
