// FullAccess
resource "azurerm_role_definition" "spatial_anchors_fullaccess_definition" {
  name              = "spatial-anchors-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.MixedReality/register/action",
      "Microsoft.MixedReality/SpatialAnchorsAccounts/providers/Microsoft.Insights/metricDefinitions/read",
      "Microsoft.MixedReality/SpatialAnchorsAccounts/providers/Microsoft.Insights/diagnosticSettings/read",
      "Microsoft.MixedReality/SpatialAnchorsAccounts/providers/Microsoft.Insights/diagnosticSettings/write",
      "Microsoft.MixedReality/SpatialAnchorsAccounts/create/action",
      "Microsoft.MixedReality/SpatialAnchorsAccounts/delete",
      "Microsoft.MixedReality/SpatialAnchorsAccounts/write",
      "Microsoft.MixedReality/SpatialAnchorsAccounts/discovery/read",
      "Microsoft.MixedReality/SpatialAnchorsAccounts/properties/read",
      "Microsoft.MixedReality/SpatialAnchorsAccounts/query/read",
      "Microsoft.MixedReality/SpatialAnchorsAccounts/submitdiag/read",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "spatial_anchors_readonly_definition" {
  name              = "spatial-anchors-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.MixedReality/SpatialAnchorsAccounts/providers/Microsoft.Insights/metricDefinitions/read",
      "Microsoft.MixedReality/SpatialAnchorsAccounts/providers/Microsoft.Insights/diagnosticSettings/read",
      "Microsoft.MixedReality/SpatialAnchorsAccounts/discovery/read",
      "Microsoft.MixedReality/SpatialAnchorsAccounts/properties/read",
      "Microsoft.MixedReality/SpatialAnchorsAccounts/query/read",
      "Microsoft.MixedReality/SpatialAnchorsAccounts/submitdiag/read",
    ]

    not_actions = [
      "Microsoft.MixedReality/register/action",
      "Microsoft.MixedReality/SpatialAnchorsAccounts/providers/Microsoft.Insights/diagnosticSettings/write",
      "Microsoft.MixedReality/SpatialAnchorsAccounts/create/action",
      "Microsoft.MixedReality/SpatialAnchorsAccounts/delete",
      "Microsoft.MixedReality/SpatialAnchorsAccounts/write",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "spatial_anchors_noaccess_definition" {
  name              = "spatial-anchors-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.MixedReality/register/action",
      "Microsoft.MixedReality/SpatialAnchorsAccounts/providers/Microsoft.Insights/metricDefinitions/read",
      "Microsoft.MixedReality/SpatialAnchorsAccounts/providers/Microsoft.Insights/diagnosticSettings/read",
      "Microsoft.MixedReality/SpatialAnchorsAccounts/providers/Microsoft.Insights/diagnosticSettings/write",
      "Microsoft.MixedReality/SpatialAnchorsAccounts/create/action",
      "Microsoft.MixedReality/SpatialAnchorsAccounts/delete",
      "Microsoft.MixedReality/SpatialAnchorsAccounts/write",
      "Microsoft.MixedReality/SpatialAnchorsAccounts/discovery/read",
      "Microsoft.MixedReality/SpatialAnchorsAccounts/properties/read",
      "Microsoft.MixedReality/SpatialAnchorsAccounts/query/read",
      "Microsoft.MixedReality/SpatialAnchorsAccounts/submitdiag/read",
    ]
  }
}
