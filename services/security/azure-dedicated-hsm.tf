// FullAccess
resource "azurerm_role_definition" "azure_dedicated_hsm_fullaccess_definition" {
  name              = "azure-dedicated-hsm-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.HardwareSecurityModules/register/action",
      "Microsoft.HardwareSecurityModules/unregister/action",
      "Microsoft.HardwareSecurityModules/dedicatedhsms/read",
      "Microsoft.HardwareSecurityModules/dedicatedhsms/write",
      "Microsoft.HardwareSecurityModules/dedicatedhsms/delete",
      "Microsoft.HardwareSecurityModules/operations/read",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "azure_dedicated_hsm_readonly_definition" {
  name              = "azure-dedicated-hsm-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.HardwareSecurityModules/dedicatedhsms/read",
      "Microsoft.HardwareSecurityModules/operations/read",
    ]

    not_actions = [
      "Microsoft.HardwareSecurityModules/register/action",
      "Microsoft.HardwareSecurityModules/unregister/action",
      "Microsoft.HardwareSecurityModules/dedicatedhsms/write",
      "Microsoft.HardwareSecurityModules/dedicatedhsms/delete",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "azure_dedicated_hsm_noaccess_definition" {
  name              = "azure-dedicated-hsm-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.HardwareSecurityModules/register/action",
      "Microsoft.HardwareSecurityModules/unregister/action",
      "Microsoft.HardwareSecurityModules/dedicatedhsms/read",
      "Microsoft.HardwareSecurityModules/dedicatedhsms/write",
      "Microsoft.HardwareSecurityModules/dedicatedhsms/delete",
      "Microsoft.HardwareSecurityModules/operations/read",
    ]
  }
}
