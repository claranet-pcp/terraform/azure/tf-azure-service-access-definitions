// FullAccess
resource "azurerm_role_definition" "graph_security_api_fullaccess_definition" {
  name              = "graph-security-api-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.SecurityGraph/diagnosticsettings/write",
      "Microsoft.SecurityGraph/diagnosticsettings/read",
      "Microsoft.SecurityGraph/diagnosticsettings/delete",
      "Microsoft.SecurityGraph/diagnosticsettingscategories/read",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "graph_security_api_readonly_definition" {
  name              = "graph-security-api-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.SecurityGraph/diagnosticsettings/read",
      "Microsoft.SecurityGraph/diagnosticsettingscategories/read",
    ]

    not_actions = [
      "Microsoft.SecurityGraph/diagnosticsettings/write",
      "Microsoft.SecurityGraph/diagnosticsettings/delete",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "graph_security_api_noaccess_definition" {
  name              = "graph-security-api-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.SecurityGraph/diagnosticsettings/write",
      "Microsoft.SecurityGraph/diagnosticsettings/read",
      "Microsoft.SecurityGraph/diagnosticsettings/delete",
      "Microsoft.SecurityGraph/diagnosticsettingscategories/read",
    ]
  }
}
