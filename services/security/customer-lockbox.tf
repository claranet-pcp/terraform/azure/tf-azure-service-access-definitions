// FullAccess
resource "azurerm_role_definition" "customer_lockbox_fullaccess_definition" {
  name              = "customer-lockbox-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.CustomerLockbox/register/action",
      "Microsoft.CustomerLockbox/operations/read",
      "Microsoft.CustomerLockbox/requests/activitylog/CreateLockboxRequest/action",
      "Microsoft.CustomerLockbox/requests/activitylog/ApproveLockboxRequest/action",
      "Microsoft.CustomerLockbox/requests/activitylog/DenyLockboxRequest/action",
      "Microsoft.CustomerLockbox/requests/activitylog/ExpireLockboxRequest/action",
      "Microsoft.CustomerLockbox/requests/activitylog/CancelLockboxRequest/action",
      "Microsoft.CustomerLockbox/requests/activitylog/AutoApproveLockboxRequest/action",
      "Microsoft.CustomerLockbox/requests/activitylog/AutoDenyLockboxRequest/action",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "customer_lockbox_readonly_definition" {
  name              = "customer-lockbox-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.CustomerLockbox/operations/read",
    ]

    not_actions = [
      "Microsoft.CustomerLockbox/register/action",
      "Microsoft.CustomerLockbox/requests/activitylog/CreateLockboxRequest/action",
      "Microsoft.CustomerLockbox/requests/activitylog/ApproveLockboxRequest/action",
      "Microsoft.CustomerLockbox/requests/activitylog/DenyLockboxRequest/action",
      "Microsoft.CustomerLockbox/requests/activitylog/ExpireLockboxRequest/action",
      "Microsoft.CustomerLockbox/requests/activitylog/CancelLockboxRequest/action",
      "Microsoft.CustomerLockbox/requests/activitylog/AutoApproveLockboxRequest/action",
      "Microsoft.CustomerLockbox/requests/activitylog/AutoDenyLockboxRequest/action",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "customer_lockbox_noaccess_definition" {
  name              = "customer-lockbox-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.CustomerLockbox/register/action",
      "Microsoft.CustomerLockbox/operations/read",
      "Microsoft.CustomerLockbox/requests/activitylog/CreateLockboxRequest/action",
      "Microsoft.CustomerLockbox/requests/activitylog/ApproveLockboxRequest/action",
      "Microsoft.CustomerLockbox/requests/activitylog/DenyLockboxRequest/action",
      "Microsoft.CustomerLockbox/requests/activitylog/ExpireLockboxRequest/action",
      "Microsoft.CustomerLockbox/requests/activitylog/CancelLockboxRequest/action",
      "Microsoft.CustomerLockbox/requests/activitylog/AutoApproveLockboxRequest/action",
      "Microsoft.CustomerLockbox/requests/activitylog/AutoDenyLockboxRequest/action",
    ]
  }
}
