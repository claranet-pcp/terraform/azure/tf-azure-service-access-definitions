// FullAccess
resource "azurerm_role_definition" "key_vault_fullaccess_definition" {
  name              = "key-vault-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.KeyVault/register/action",
      "Microsoft.KeyVault/unregister/action",
      "Microsoft.KeyVault/vaults/read",
      "Microsoft.KeyVault/vaults/write",
      "Microsoft.KeyVault/vaults/delete",
      "Microsoft.KeyVault/vaults/deploy/action",
      "Microsoft.KeyVault/vaults/secrets/read",
      "Microsoft.KeyVault/vaults/secrets/write",
      "Microsoft.KeyVault/vaults/accessPolicies/write",
      "Microsoft.KeyVault/operations/read",
      "Microsoft.KeyVault/checkNameAvailability/read",
      "Microsoft.KeyVault/deletedVaults/read",
      "Microsoft.KeyVault/locations/deletedVaults/read",
      "Microsoft.KeyVault/locations/deletedVaults/purge/action",
      "Microsoft.KeyVault/locations/operationResults/read",
      "Microsoft.KeyVault/locations/deleteVirtualNetworkOrSubnets/action",
      "Microsoft.KeyVault/hsmPools/read",
      "Microsoft.KeyVault/hsmPools/write",
      "Microsoft.KeyVault/hsmPools/delete",
      "Microsoft.KeyVault/hsmPools/joinVault/action",
      "Microsoft.KeyVault/vaults/eventGridFilters/read",
      "Microsoft.KeyVault/vaults/eventGridFilters/write",
      "Microsoft.KeyVault/vaults/eventGridFilters/delete",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "key_vault_readonly_definition" {
  name              = "key-vault-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.KeyVault/vaults/read",
      "Microsoft.KeyVault/vaults/secrets/read",
      "Microsoft.KeyVault/operations/read",
      "Microsoft.KeyVault/checkNameAvailability/read",
      "Microsoft.KeyVault/deletedVaults/read",
      "Microsoft.KeyVault/locations/deletedVaults/read",
      "Microsoft.KeyVault/locations/operationResults/read",
      "Microsoft.KeyVault/hsmPools/read",
      "Microsoft.KeyVault/vaults/eventGridFilters/read",
    ]

    not_actions = [
      "Microsoft.KeyVault/register/action",
      "Microsoft.KeyVault/unregister/action",
      "Microsoft.KeyVault/vaults/write",
      "Microsoft.KeyVault/vaults/delete",
      "Microsoft.KeyVault/vaults/deploy/action",
      "Microsoft.KeyVault/vaults/secrets/write",
      "Microsoft.KeyVault/vaults/accessPolicies/write",
      "Microsoft.KeyVault/locations/deletedVaults/purge/action",
      "Microsoft.KeyVault/locations/deleteVirtualNetworkOrSubnets/action",
      "Microsoft.KeyVault/hsmPools/write",
      "Microsoft.KeyVault/hsmPools/delete",
      "Microsoft.KeyVault/hsmPools/joinVault/action",
      "Microsoft.KeyVault/vaults/eventGridFilters/write",
      "Microsoft.KeyVault/vaults/eventGridFilters/delete",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "key_vault_noaccess_definition" {
  name              = "key-vault-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.KeyVault/register/action",
      "Microsoft.KeyVault/unregister/action",
      "Microsoft.KeyVault/vaults/read",
      "Microsoft.KeyVault/vaults/write",
      "Microsoft.KeyVault/vaults/delete",
      "Microsoft.KeyVault/vaults/deploy/action",
      "Microsoft.KeyVault/vaults/secrets/read",
      "Microsoft.KeyVault/vaults/secrets/write",
      "Microsoft.KeyVault/vaults/accessPolicies/write",
      "Microsoft.KeyVault/operations/read",
      "Microsoft.KeyVault/checkNameAvailability/read",
      "Microsoft.KeyVault/deletedVaults/read",
      "Microsoft.KeyVault/locations/deletedVaults/read",
      "Microsoft.KeyVault/locations/deletedVaults/purge/action",
      "Microsoft.KeyVault/locations/operationResults/read",
      "Microsoft.KeyVault/locations/deleteVirtualNetworkOrSubnets/action",
      "Microsoft.KeyVault/hsmPools/read",
      "Microsoft.KeyVault/hsmPools/write",
      "Microsoft.KeyVault/hsmPools/delete",
      "Microsoft.KeyVault/hsmPools/joinVault/action",
      "Microsoft.KeyVault/vaults/eventGridFilters/read",
      "Microsoft.KeyVault/vaults/eventGridFilters/write",
      "Microsoft.KeyVault/vaults/eventGridFilters/delete",
    ]
  }
}
