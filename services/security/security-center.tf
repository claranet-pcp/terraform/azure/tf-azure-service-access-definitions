// FullAccess
resource "azurerm_role_definition" "security_center_fullaccess_definition" {
  name              = "security-center-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Security/register/action",
      "Microsoft.Security/unregister/action",
      "Microsoft.Security/tasks/read",
      "Microsoft.Security/locations/tasks/read",
      "Microsoft.Security/locations/tasks/start/action",
      "Microsoft.Security/locations/tasks/resolve/action",
      "Microsoft.Security/locations/tasks/activate/action",
      "Microsoft.Security/locations/tasks/dismiss/action",
      "Microsoft.Security/securityStatuses/read",
      "Microsoft.Security/policies/read",
      "Microsoft.Security/policies/write",
      "Microsoft.Security/workspaceSettings/read",
      "Microsoft.Security/workspaceSettings/write",
      "Microsoft.Security/workspaceSettings/delete",
      "Microsoft.Security/workspaceSettings/connect/action",
      "Microsoft.Security/pricings/read",
      "Microsoft.Security/pricings/write",
      "Microsoft.Security/pricings/delete",
      "Microsoft.Security/securityContacts/read",
      "Microsoft.Security/securityContacts/write",
      "Microsoft.Security/securityContacts/delete",
      "Microsoft.Security/complianceResults/read",
      "Microsoft.Security/locations/read",
      "Microsoft.Security/securityStatusesSummaries/read",
      "Microsoft.Security/alerts/read",
      "Microsoft.Security/locations/alerts/read",
      "Microsoft.Security/locations/alerts/dismiss/action",
      "Microsoft.Security/locations/alerts/activate/action",
      "Microsoft.Security/locations/jitNetworkAccessPolicies/read",
      "Microsoft.Security/locations/jitNetworkAccessPolicies/write",
      "Microsoft.Security/locations/jitNetworkAccessPolicies/delete",
      "Microsoft.Security/locations/jitNetworkAccessPolicies/initiate/action",
      "Microsoft.Security/applicationWhitelistings/read",
      "Microsoft.Security/applicationWhitelistings/write",
      "Microsoft.Security/securitySolutionsReferenceData/read",
      "Microsoft.Security/securitySolutions/read",
      "Microsoft.Security/securitySolutions/write",
      "Microsoft.Security/securitySolutions/delete",
      "Microsoft.Security/webApplicationFirewalls/read",
      "Microsoft.Security/webApplicationFirewalls/write",
      "Microsoft.Security/webApplicationFirewalls/delete",
      "Microsoft.Security/advancedThreatProtectionSettings/read",
      "Microsoft.Security/advancedThreatProtectionSettings/write",
      "Microsoft.Security/settings/read",
      "Microsoft.Security/settings/write",
      "Microsoft.Security/informationProtectionPolicies/read",
      "Microsoft.Security/informationProtectionPolicies/write",
      "Microsoft.Security/adaptiveNetworkHardenings/read",
      "Microsoft.Security/adaptiveNetworkHardenings/enforce/action",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "security_center_readonly_definition" {
  name              = "security-center-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Security/tasks/read",
      "Microsoft.Security/locations/tasks/read",
      "Microsoft.Security/securityStatuses/read",
      "Microsoft.Security/policies/read",
      "Microsoft.Security/workspaceSettings/read",
      "Microsoft.Security/pricings/read",
      "Microsoft.Security/securityContacts/read",
      "Microsoft.Security/complianceResults/read",
      "Microsoft.Security/locations/read",
      "Microsoft.Security/securityStatusesSummaries/read",
      "Microsoft.Security/alerts/read",
      "Microsoft.Security/locations/alerts/read",
      "Microsoft.Security/locations/jitNetworkAccessPolicies/read",
      "Microsoft.Security/applicationWhitelistings/read",
      "Microsoft.Security/securitySolutionsReferenceData/read",
      "Microsoft.Security/securitySolutions/read",
      "Microsoft.Security/webApplicationFirewalls/read",
      "Microsoft.Security/advancedThreatProtectionSettings/read",
      "Microsoft.Security/settings/read",
      "Microsoft.Security/informationProtectionPolicies/read",
      "Microsoft.Security/adaptiveNetworkHardenings/read",
    ]

    not_actions = [
      "Microsoft.Security/register/action",
      "Microsoft.Security/unregister/action",
      "Microsoft.Security/locations/tasks/start/action",
      "Microsoft.Security/locations/tasks/resolve/action",
      "Microsoft.Security/locations/tasks/activate/action",
      "Microsoft.Security/locations/tasks/dismiss/action",
      "Microsoft.Security/policies/write",
      "Microsoft.Security/workspaceSettings/write",
      "Microsoft.Security/workspaceSettings/delete",
      "Microsoft.Security/workspaceSettings/connect/action",
      "Microsoft.Security/pricings/write",
      "Microsoft.Security/pricings/delete",
      "Microsoft.Security/securityContacts/write",
      "Microsoft.Security/securityContacts/delete",
      "Microsoft.Security/locations/alerts/dismiss/action",
      "Microsoft.Security/locations/alerts/activate/action",
      "Microsoft.Security/locations/jitNetworkAccessPolicies/write",
      "Microsoft.Security/locations/jitNetworkAccessPolicies/delete",
      "Microsoft.Security/locations/jitNetworkAccessPolicies/initiate/action",
      "Microsoft.Security/applicationWhitelistings/write",
      "Microsoft.Security/securitySolutions/write",
      "Microsoft.Security/securitySolutions/delete",
      "Microsoft.Security/webApplicationFirewalls/write",
      "Microsoft.Security/webApplicationFirewalls/delete",
      "Microsoft.Security/advancedThreatProtectionSettings/write",
      "Microsoft.Security/settings/write",
      "Microsoft.Security/informationProtectionPolicies/write",
      "Microsoft.Security/adaptiveNetworkHardenings/enforce/action",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "security_center_noaccess_definition" {
  name              = "security-center-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.Security/register/action",
      "Microsoft.Security/unregister/action",
      "Microsoft.Security/tasks/read",
      "Microsoft.Security/locations/tasks/read",
      "Microsoft.Security/locations/tasks/start/action",
      "Microsoft.Security/locations/tasks/resolve/action",
      "Microsoft.Security/locations/tasks/activate/action",
      "Microsoft.Security/locations/tasks/dismiss/action",
      "Microsoft.Security/securityStatuses/read",
      "Microsoft.Security/policies/read",
      "Microsoft.Security/policies/write",
      "Microsoft.Security/workspaceSettings/read",
      "Microsoft.Security/workspaceSettings/write",
      "Microsoft.Security/workspaceSettings/delete",
      "Microsoft.Security/workspaceSettings/connect/action",
      "Microsoft.Security/pricings/read",
      "Microsoft.Security/pricings/write",
      "Microsoft.Security/pricings/delete",
      "Microsoft.Security/securityContacts/read",
      "Microsoft.Security/securityContacts/write",
      "Microsoft.Security/securityContacts/delete",
      "Microsoft.Security/complianceResults/read",
      "Microsoft.Security/locations/read",
      "Microsoft.Security/securityStatusesSummaries/read",
      "Microsoft.Security/alerts/read",
      "Microsoft.Security/locations/alerts/read",
      "Microsoft.Security/locations/alerts/dismiss/action",
      "Microsoft.Security/locations/alerts/activate/action",
      "Microsoft.Security/locations/jitNetworkAccessPolicies/read",
      "Microsoft.Security/locations/jitNetworkAccessPolicies/write",
      "Microsoft.Security/locations/jitNetworkAccessPolicies/delete",
      "Microsoft.Security/locations/jitNetworkAccessPolicies/initiate/action",
      "Microsoft.Security/applicationWhitelistings/read",
      "Microsoft.Security/applicationWhitelistings/write",
      "Microsoft.Security/securitySolutionsReferenceData/read",
      "Microsoft.Security/securitySolutions/read",
      "Microsoft.Security/securitySolutions/write",
      "Microsoft.Security/securitySolutions/delete",
      "Microsoft.Security/webApplicationFirewalls/read",
      "Microsoft.Security/webApplicationFirewalls/write",
      "Microsoft.Security/webApplicationFirewalls/delete",
      "Microsoft.Security/advancedThreatProtectionSettings/read",
      "Microsoft.Security/advancedThreatProtectionSettings/write",
      "Microsoft.Security/settings/read",
      "Microsoft.Security/settings/write",
      "Microsoft.Security/informationProtectionPolicies/read",
      "Microsoft.Security/informationProtectionPolicies/write",
      "Microsoft.Security/adaptiveNetworkHardenings/read",
      "Microsoft.Security/adaptiveNetworkHardenings/enforce/action",
    ]
  }
}
