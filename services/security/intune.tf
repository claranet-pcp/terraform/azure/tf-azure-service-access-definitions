// FullAccess
resource "azurerm_role_definition" "intune_fullaccess_definition" {
  name              = "intune-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Intune/diagnosticsettings/write",
      "Microsoft.Intune/diagnosticsettings/read",
      "Microsoft.Intune/diagnosticsettings/delete",
      "Microsoft.Intune/diagnosticsettingscategories/read",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "intune_readonly_definition" {
  name              = "intune-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Intune/diagnosticsettings/read",
      "Microsoft.Intune/diagnosticsettingscategories/read",
    ]

    not_actions = [
      "Microsoft.Intune/diagnosticsettings/write",
      "Microsoft.Intune/diagnosticsettings/delete",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "intune_noaccess_definition" {
  name              = "intune-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.Intune/diagnosticsettings/write",
      "Microsoft.Intune/diagnosticsettings/read",
      "Microsoft.Intune/diagnosticsettings/delete",
      "Microsoft.Intune/diagnosticsettingscategories/read",
    ]
  }
}
