// FullAccess
resource "azurerm_role_definition" "azure_api_for_fhir_fullaccess_definition" {
  name              = "azure-api-for-fhir-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.HealthcareApis/register/action",
      "Microsoft.HealthcareApis/services/read",
      "Microsoft.HealthcareApis/services/write",
      "Microsoft.HealthcareApis/services/delete",
      "Microsoft.HealthcareApis/locations/operationresults/read",
      "Microsoft.HealthcareApis/Operations/read",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "azure_api_for_fhir_readonly_definition" {
  name              = "azure-api-for-fhir-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.HealthcareApis/services/read",
      "Microsoft.HealthcareApis/locations/operationresults/read",
      "Microsoft.HealthcareApis/Operations/read",
    ]

    not_actions = [
      "Microsoft.HealthcareApis/register/action",
      "Microsoft.HealthcareApis/services/write",
      "Microsoft.HealthcareApis/services/delete",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "azure_api_for_fhir_noaccess_definition" {
  name              = "azure-api-for-fhir-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.HealthcareApis/register/action",
      "Microsoft.HealthcareApis/services/read",
      "Microsoft.HealthcareApis/services/write",
      "Microsoft.HealthcareApis/services/delete",
      "Microsoft.HealthcareApis/locations/operationresults/read",
      "Microsoft.HealthcareApis/Operations/read",
    ]
  }
}
