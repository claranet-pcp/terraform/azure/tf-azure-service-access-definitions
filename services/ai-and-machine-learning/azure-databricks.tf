// FullAccess
resource "azurerm_role_definition" "azure_databricks_fullaccess_definition" {
  name              = "azure-databricks-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Databricks/register/action",
      "Microsoft.Databricks/workspaces/read",
      "Microsoft.Databricks/workspaces/write",
      "Microsoft.Databricks/workspaces/delete",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "azure_databricks_readonly_definition" {
  name              = "azure-databricks-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Databricks/workspaces/read",
    ]

    not_actions = [
      "Microsoft.Databricks/register/action",
      "Microsoft.Databricks/workspaces/write",
      "Microsoft.Databricks/workspaces/delete",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "azure_databricks_noaccess_definition" {
  name              = "azure-databricks-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.Databricks/register/action",
      "Microsoft.Databricks/workspaces/read",
      "Microsoft.Databricks/workspaces/write",
      "Microsoft.Databricks/workspaces/delete",
    ]
  }
}
