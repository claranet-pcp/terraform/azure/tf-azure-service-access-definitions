// FullAccess
resource "azurerm_role_definition" "language_understanding_fullaccess_definition" {
  name              = "language-understanding-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.CognitiveServices/accounts/LUIS/predict/action",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "language_understanding_readonly_definition" {
  name              = "language-understanding-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "",
    ]

    not_actions = [
      "Microsoft.CognitiveServices/accounts/LUIS/predict/action",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "language_understanding_noaccess_definition" {
  name              = "language-understanding-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.CognitiveServices/accounts/LUIS/predict/action",
    ]
  }
}
