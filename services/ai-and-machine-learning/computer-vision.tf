// FullAccess
resource "azurerm_role_definition" "computer_vision_fullaccess_definition" {
  name              = "computer-vision-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.CognitiveServices/accounts/ComputerVision/analyze/action",
      "Microsoft.CognitiveServices/accounts/ComputerVision/describe/action",
      "Microsoft.CognitiveServices/accounts/ComputerVision/generatethumbnail/action",
      "Microsoft.CognitiveServices/accounts/ComputerVision/ocr/action",
      "Microsoft.CognitiveServices/accounts/ComputerVision/recognizetext/action",
      "Microsoft.CognitiveServices/accounts/ComputerVision/tag/action",
      "Microsoft.CognitiveServices/accounts/ComputerVision/areaofinterest/action",
      "Microsoft.CognitiveServices/accounts/ComputerVision/detect/action",
      "Microsoft.CognitiveServices/accounts/ComputerVision/textoperations/read",
      "Microsoft.CognitiveServices/accounts/ComputerVision/models/read",
      "Microsoft.CognitiveServices/accounts/ComputerVision/models/analyze/action",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "computer_vision_readonly_definition" {
  name              = "computer-vision-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.CognitiveServices/accounts/ComputerVision/textoperations/read",
      "Microsoft.CognitiveServices/accounts/ComputerVision/models/read",
    ]

    not_actions = [
      "Microsoft.CognitiveServices/accounts/ComputerVision/analyze/action",
      "Microsoft.CognitiveServices/accounts/ComputerVision/describe/action",
      "Microsoft.CognitiveServices/accounts/ComputerVision/generatethumbnail/action",
      "Microsoft.CognitiveServices/accounts/ComputerVision/ocr/action",
      "Microsoft.CognitiveServices/accounts/ComputerVision/recognizetext/action",
      "Microsoft.CognitiveServices/accounts/ComputerVision/tag/action",
      "Microsoft.CognitiveServices/accounts/ComputerVision/areaofinterest/action",
      "Microsoft.CognitiveServices/accounts/ComputerVision/detect/action",
      "Microsoft.CognitiveServices/accounts/ComputerVision/models/analyze/action",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "computer_vision_noaccess_definition" {
  name              = "computer-vision-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.CognitiveServices/accounts/ComputerVision/analyze/action",
      "Microsoft.CognitiveServices/accounts/ComputerVision/describe/action",
      "Microsoft.CognitiveServices/accounts/ComputerVision/generatethumbnail/action",
      "Microsoft.CognitiveServices/accounts/ComputerVision/ocr/action",
      "Microsoft.CognitiveServices/accounts/ComputerVision/recognizetext/action",
      "Microsoft.CognitiveServices/accounts/ComputerVision/tag/action",
      "Microsoft.CognitiveServices/accounts/ComputerVision/areaofinterest/action",
      "Microsoft.CognitiveServices/accounts/ComputerVision/detect/action",
      "Microsoft.CognitiveServices/accounts/ComputerVision/textoperations/read",
      "Microsoft.CognitiveServices/accounts/ComputerVision/models/read",
      "Microsoft.CognitiveServices/accounts/ComputerVision/models/analyze/action",
    ]
  }
}
