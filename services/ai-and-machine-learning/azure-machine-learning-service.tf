// FullAccess
resource "azurerm_role_definition" "azure_machine_learning_service_fullaccess_definition" {
  name              = "azure-machine-learning-service-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.MachineLearningCompute/register/action",
      "Microsoft.MachineLearningCompute/operationalizationClusters/read",
      "Microsoft.MachineLearningCompute/operationalizationClusters/write",
      "Microsoft.MachineLearningCompute/operationalizationClusters/delete",
      "Microsoft.MachineLearningCompute/operationalizationClusters/listKeys/action",
      "Microsoft.MachineLearningCompute/operationalizationClusters/checkUpdate/action",
      "Microsoft.MachineLearningCompute/operationalizationClusters/updateSystem/action",
      "Microsoft.MachineLearningServices/register/action",
      "Microsoft.MachineLearningServices/workspaces/read",
      "Microsoft.MachineLearningServices/workspaces/write",
      "Microsoft.MachineLearningServices/workspaces/delete",
      "Microsoft.MachineLearningServices/workspaces/listKeys/action",
      "Microsoft.MachineLearningServices/workspaces/computes/read",
      "Microsoft.MachineLearningServices/workspaces/computes/write",
      "Microsoft.MachineLearningServices/workspaces/computes/delete",
      "Microsoft.MachineLearningServices/workspaces/computes/listKeys/action",
      "Microsoft.MachineLearningServices/workspaces/computes/listNodes/action",
      "Microsoft.MachineLearningServices/locations/vmsizes/read",
      "Microsoft.MachineLearningServices/locations/usages/read",
      "Microsoft.MachineLearningServices/locations/workspaceOperationsStatus/read",
      "Microsoft.MachineLearningServices/locations/computeoperationsstatus/read",
      "Microsoft.MachineLearningServices/workspaces/experiments/write",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "azure_machine_learning_service_readonly_definition" {
  name              = "azure-machine-learning-service-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.MachineLearningCompute/operationalizationClusters/read",
      "Microsoft.MachineLearningServices/workspaces/read",
      "Microsoft.MachineLearningServices/workspaces/computes/read",
      "Microsoft.MachineLearningServices/locations/vmsizes/read",
      "Microsoft.MachineLearningServices/locations/usages/read",
      "Microsoft.MachineLearningServices/locations/workspaceOperationsStatus/read",
      "Microsoft.MachineLearningServices/locations/computeoperationsstatus/read",
    ]

    not_actions = [
      "Microsoft.MachineLearningCompute/register/action",
      "Microsoft.MachineLearningCompute/operationalizationClusters/write",
      "Microsoft.MachineLearningCompute/operationalizationClusters/delete",
      "Microsoft.MachineLearningCompute/operationalizationClusters/listKeys/action",
      "Microsoft.MachineLearningCompute/operationalizationClusters/checkUpdate/action",
      "Microsoft.MachineLearningCompute/operationalizationClusters/updateSystem/action",
      "Microsoft.MachineLearningServices/register/action",
      "Microsoft.MachineLearningServices/workspaces/write",
      "Microsoft.MachineLearningServices/workspaces/delete",
      "Microsoft.MachineLearningServices/workspaces/listKeys/action",
      "Microsoft.MachineLearningServices/workspaces/computes/write",
      "Microsoft.MachineLearningServices/workspaces/computes/delete",
      "Microsoft.MachineLearningServices/workspaces/computes/listKeys/action",
      "Microsoft.MachineLearningServices/workspaces/computes/listNodes/action",
      "Microsoft.MachineLearningServices/workspaces/experiments/write",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "azure_machine_learning_service_noaccess_definition" {
  name              = "azure-machine-learning-service-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.MachineLearningCompute/register/action",
      "Microsoft.MachineLearningCompute/operationalizationClusters/read",
      "Microsoft.MachineLearningCompute/operationalizationClusters/write",
      "Microsoft.MachineLearningCompute/operationalizationClusters/delete",
      "Microsoft.MachineLearningCompute/operationalizationClusters/listKeys/action",
      "Microsoft.MachineLearningCompute/operationalizationClusters/checkUpdate/action",
      "Microsoft.MachineLearningCompute/operationalizationClusters/updateSystem/action",
      "Microsoft.MachineLearningServices/register/action",
      "Microsoft.MachineLearningServices/workspaces/read",
      "Microsoft.MachineLearningServices/workspaces/write",
      "Microsoft.MachineLearningServices/workspaces/delete",
      "Microsoft.MachineLearningServices/workspaces/listKeys/action",
      "Microsoft.MachineLearningServices/workspaces/computes/read",
      "Microsoft.MachineLearningServices/workspaces/computes/write",
      "Microsoft.MachineLearningServices/workspaces/computes/delete",
      "Microsoft.MachineLearningServices/workspaces/computes/listKeys/action",
      "Microsoft.MachineLearningServices/workspaces/computes/listNodes/action",
      "Microsoft.MachineLearningServices/locations/vmsizes/read",
      "Microsoft.MachineLearningServices/locations/usages/read",
      "Microsoft.MachineLearningServices/locations/workspaceOperationsStatus/read",
      "Microsoft.MachineLearningServices/locations/computeoperationsstatus/read",
      "Microsoft.MachineLearningServices/workspaces/experiments/write",
    ]
  }
}
