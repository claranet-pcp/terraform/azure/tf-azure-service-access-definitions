// FullAccess
resource "azurerm_role_definition" "azure_search_fullaccess_definition" {
  name              = "azure-search-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Search/register/action",
      "Microsoft.Search/checkNameAvailability/action",
      "Microsoft.Search/operations/read",
      "Microsoft.Search/searchServices/write",
      "Microsoft.Search/searchServices/read",
      "Microsoft.Search/searchServices/delete",
      "Microsoft.Search/searchServices/start/action",
      "Microsoft.Search/searchServices/stop/action",
      "Microsoft.Search/searchServices/listAdminKeys/action",
      "Microsoft.Search/searchServices/regenerateAdminKey/action",
      "Microsoft.Search/searchServices/createQueryKey/action",
      "Microsoft.Search/searchServices/listQueryKeys/read",
      "Microsoft.Search/searchServices/deleteQueryKey/delete",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "azure_search_readonly_definition" {
  name              = "azure-search-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Search/operations/read",
      "Microsoft.Search/searchServices/read",
      "Microsoft.Search/searchServices/listQueryKeys/read",
    ]

    not_actions = [
      "Microsoft.Search/register/action",
      "Microsoft.Search/checkNameAvailability/action",
      "Microsoft.Search/searchServices/write",
      "Microsoft.Search/searchServices/delete",
      "Microsoft.Search/searchServices/start/action",
      "Microsoft.Search/searchServices/stop/action",
      "Microsoft.Search/searchServices/listAdminKeys/action",
      "Microsoft.Search/searchServices/regenerateAdminKey/action",
      "Microsoft.Search/searchServices/createQueryKey/action",
      "Microsoft.Search/searchServices/deleteQueryKey/delete",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "azure_search_noaccess_definition" {
  name              = "azure-search-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.Search/register/action",
      "Microsoft.Search/checkNameAvailability/action",
      "Microsoft.Search/operations/read",
      "Microsoft.Search/searchServices/write",
      "Microsoft.Search/searchServices/read",
      "Microsoft.Search/searchServices/delete",
      "Microsoft.Search/searchServices/start/action",
      "Microsoft.Search/searchServices/stop/action",
      "Microsoft.Search/searchServices/listAdminKeys/action",
      "Microsoft.Search/searchServices/regenerateAdminKey/action",
      "Microsoft.Search/searchServices/createQueryKey/action",
      "Microsoft.Search/searchServices/listQueryKeys/read",
      "Microsoft.Search/searchServices/deleteQueryKey/delete",
    ]
  }
}
