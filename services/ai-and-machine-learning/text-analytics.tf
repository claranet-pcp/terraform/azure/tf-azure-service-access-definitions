// FullAccess
resource "azurerm_role_definition" "text_analytics_fullaccess_definition" {
  name              = "text-analytics-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.CognitiveServices/accounts/TextAnalytics/languages/action",
      "Microsoft.CognitiveServices/accounts/TextAnalytics/entities/action",
      "Microsoft.CognitiveServices/accounts/TextAnalytics/keyphrases/action",
      "Microsoft.CognitiveServices/accounts/TextAnalytics/sentiment/action",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "text_analytics_readonly_definition" {
  name              = "text-analytics-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "",
    ]

    not_actions = [
      "Microsoft.CognitiveServices/accounts/TextAnalytics/languages/action",
      "Microsoft.CognitiveServices/accounts/TextAnalytics/entities/action",
      "Microsoft.CognitiveServices/accounts/TextAnalytics/keyphrases/action",
      "Microsoft.CognitiveServices/accounts/TextAnalytics/sentiment/action",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "text_analytics_noaccess_definition" {
  name              = "text-analytics-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.CognitiveServices/accounts/TextAnalytics/languages/action",
      "Microsoft.CognitiveServices/accounts/TextAnalytics/entities/action",
      "Microsoft.CognitiveServices/accounts/TextAnalytics/keyphrases/action",
      "Microsoft.CognitiveServices/accounts/TextAnalytics/sentiment/action",
    ]
  }
}
