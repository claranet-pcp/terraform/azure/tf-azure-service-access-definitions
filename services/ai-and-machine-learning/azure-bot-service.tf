// FullAccess
resource "azurerm_role_definition" "azure_bot_service_fullaccess_definition" {
  name              = "azure-bot-service-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.BotService/botServices/read",
      "Microsoft.BotService/botServices/write",
      "Microsoft.BotService/botServices/delete",
      "Microsoft.BotService/botServices/channels/read",
      "Microsoft.BotService/botServices/channels/write",
      "Microsoft.BotService/botServices/channels/delete",
      "Microsoft.BotService/botServices/connections/read",
      "Microsoft.BotService/botServices/connections/write",
      "Microsoft.BotService/botServices/connections/delete",
      "Microsoft.BotService/Operations/read",
      "Microsoft.BotService/locations/operationresults/read",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "azure_bot_service_readonly_definition" {
  name              = "azure-bot-service-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.BotService/botServices/read",
      "Microsoft.BotService/botServices/channels/read",
      "Microsoft.BotService/botServices/connections/read",
      "Microsoft.BotService/Operations/read",
      "Microsoft.BotService/locations/operationresults/read",
    ]

    not_actions = [
      "Microsoft.BotService/botServices/write",
      "Microsoft.BotService/botServices/delete",
      "Microsoft.BotService/botServices/channels/write",
      "Microsoft.BotService/botServices/channels/delete",
      "Microsoft.BotService/botServices/connections/write",
      "Microsoft.BotService/botServices/connections/delete",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "azure_bot_service_noaccess_definition" {
  name              = "azure-bot-service-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.BotService/botServices/read",
      "Microsoft.BotService/botServices/write",
      "Microsoft.BotService/botServices/delete",
      "Microsoft.BotService/botServices/channels/read",
      "Microsoft.BotService/botServices/channels/write",
      "Microsoft.BotService/botServices/channels/delete",
      "Microsoft.BotService/botServices/connections/read",
      "Microsoft.BotService/botServices/connections/write",
      "Microsoft.BotService/botServices/connections/delete",
      "Microsoft.BotService/Operations/read",
      "Microsoft.BotService/locations/operationresults/read",
    ]
  }
}
