// FullAccess
resource "azurerm_role_definition" "face_fullaccess_definition" {
  name              = "face-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.CognitiveServices/accounts/Face/detect/action",
      "Microsoft.CognitiveServices/accounts/Face/findsimilars/action",
      "Microsoft.CognitiveServices/accounts/Face/group/action",
      "Microsoft.CognitiveServices/accounts/Face/identify/action",
      "Microsoft.CognitiveServices/accounts/Face/verify/action",
      "Microsoft.CognitiveServices/accounts/Face/facelists/write",
      "Microsoft.CognitiveServices/accounts/Face/facelists/delete",
      "Microsoft.CognitiveServices/accounts/Face/facelists/read",
      "Microsoft.CognitiveServices/accounts/Face/facelists/persistedfaces/write",
      "Microsoft.CognitiveServices/accounts/Face/facelists/persistedfaces/delete",
      "Microsoft.CognitiveServices/accounts/Face/largefacelists/write",
      "Microsoft.CognitiveServices/accounts/Face/largefacelists/delete",
      "Microsoft.CognitiveServices/accounts/Face/largefacelists/read",
      "Microsoft.CognitiveServices/accounts/Face/largefacelists/train/action",
      "Microsoft.CognitiveServices/accounts/Face/largefacelists/persistedfaces/write",
      "Microsoft.CognitiveServices/accounts/Face/largefacelists/persistedfaces/delete",
      "Microsoft.CognitiveServices/accounts/Face/largefacelists/persistedfaces/read",
      "Microsoft.CognitiveServices/accounts/Face/largefacelists/training/read",
      "Microsoft.CognitiveServices/accounts/Face/largepersongroups/write",
      "Microsoft.CognitiveServices/accounts/Face/largepersongroups/delete",
      "Microsoft.CognitiveServices/accounts/Face/largepersongroups/read",
      "Microsoft.CognitiveServices/accounts/Face/largepersongroups/train/action",
      "Microsoft.CognitiveServices/accounts/Face/largepersongroups/persons/action",
      "Microsoft.CognitiveServices/accounts/Face/largepersongroups/training/read",
      "Microsoft.CognitiveServices/accounts/Face/largepersongroups/persons/delete",
      "Microsoft.CognitiveServices/accounts/Face/largepersongroups/persons/read",
      "Microsoft.CognitiveServices/accounts/Face/largepersongroups/persons/write",
      "Microsoft.CognitiveServices/accounts/Face/largepersongroups/persons/persistedfaces/write",
      "Microsoft.CognitiveServices/accounts/Face/largepersongroups/persons/persistedfaces/delete",
      "Microsoft.CognitiveServices/accounts/Face/largepersongroups/persons/persistedfaces/read",
      "Microsoft.CognitiveServices/accounts/Face/persongroups/write",
      "Microsoft.CognitiveServices/accounts/Face/persongroups/delete",
      "Microsoft.CognitiveServices/accounts/Face/persongroups/read",
      "Microsoft.CognitiveServices/accounts/Face/persongroups/train/action",
      "Microsoft.CognitiveServices/accounts/Face/persongroups/persons/action",
      "Microsoft.CognitiveServices/accounts/Face/persongroups/training/read",
      "Microsoft.CognitiveServices/accounts/Face/persongroups/persons/delete",
      "Microsoft.CognitiveServices/accounts/Face/persongroups/persons/read",
      "Microsoft.CognitiveServices/accounts/Face/persongroups/persons/write",
      "Microsoft.CognitiveServices/accounts/Face/persongroups/persons/persistedfaces/write",
      "Microsoft.CognitiveServices/accounts/Face/persongroups/persons/persistedfaces/delete",
      "Microsoft.CognitiveServices/accounts/Face/persongroups/persons/persistedfaces/read",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "face_readonly_definition" {
  name              = "face-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.CognitiveServices/accounts/Face/facelists/read",
      "Microsoft.CognitiveServices/accounts/Face/largefacelists/read",
      "Microsoft.CognitiveServices/accounts/Face/largefacelists/persistedfaces/read",
      "Microsoft.CognitiveServices/accounts/Face/largefacelists/training/read",
      "Microsoft.CognitiveServices/accounts/Face/largepersongroups/read",
      "Microsoft.CognitiveServices/accounts/Face/largepersongroups/training/read",
      "Microsoft.CognitiveServices/accounts/Face/largepersongroups/persons/read",
      "Microsoft.CognitiveServices/accounts/Face/largepersongroups/persons/persistedfaces/read",
      "Microsoft.CognitiveServices/accounts/Face/persongroups/read",
      "Microsoft.CognitiveServices/accounts/Face/persongroups/training/read",
      "Microsoft.CognitiveServices/accounts/Face/persongroups/persons/read",
      "Microsoft.CognitiveServices/accounts/Face/persongroups/persons/persistedfaces/read",
    ]

    not_actions = [
      "Microsoft.CognitiveServices/accounts/Face/detect/action",
      "Microsoft.CognitiveServices/accounts/Face/findsimilars/action",
      "Microsoft.CognitiveServices/accounts/Face/group/action",
      "Microsoft.CognitiveServices/accounts/Face/identify/action",
      "Microsoft.CognitiveServices/accounts/Face/verify/action",
      "Microsoft.CognitiveServices/accounts/Face/facelists/write",
      "Microsoft.CognitiveServices/accounts/Face/facelists/delete",
      "Microsoft.CognitiveServices/accounts/Face/facelists/persistedfaces/write",
      "Microsoft.CognitiveServices/accounts/Face/facelists/persistedfaces/delete",
      "Microsoft.CognitiveServices/accounts/Face/largefacelists/write",
      "Microsoft.CognitiveServices/accounts/Face/largefacelists/delete",
      "Microsoft.CognitiveServices/accounts/Face/largefacelists/train/action",
      "Microsoft.CognitiveServices/accounts/Face/largefacelists/persistedfaces/write",
      "Microsoft.CognitiveServices/accounts/Face/largefacelists/persistedfaces/delete",
      "Microsoft.CognitiveServices/accounts/Face/largepersongroups/write",
      "Microsoft.CognitiveServices/accounts/Face/largepersongroups/delete",
      "Microsoft.CognitiveServices/accounts/Face/largepersongroups/train/action",
      "Microsoft.CognitiveServices/accounts/Face/largepersongroups/persons/action",
      "Microsoft.CognitiveServices/accounts/Face/largepersongroups/persons/delete",
      "Microsoft.CognitiveServices/accounts/Face/largepersongroups/persons/write",
      "Microsoft.CognitiveServices/accounts/Face/largepersongroups/persons/persistedfaces/write",
      "Microsoft.CognitiveServices/accounts/Face/largepersongroups/persons/persistedfaces/delete",
      "Microsoft.CognitiveServices/accounts/Face/persongroups/write",
      "Microsoft.CognitiveServices/accounts/Face/persongroups/delete",
      "Microsoft.CognitiveServices/accounts/Face/persongroups/train/action",
      "Microsoft.CognitiveServices/accounts/Face/persongroups/persons/action",
      "Microsoft.CognitiveServices/accounts/Face/persongroups/persons/delete",
      "Microsoft.CognitiveServices/accounts/Face/persongroups/persons/write",
      "Microsoft.CognitiveServices/accounts/Face/persongroups/persons/persistedfaces/write",
      "Microsoft.CognitiveServices/accounts/Face/persongroups/persons/persistedfaces/delete",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "face_noaccess_definition" {
  name              = "face-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.CognitiveServices/accounts/Face/detect/action",
      "Microsoft.CognitiveServices/accounts/Face/findsimilars/action",
      "Microsoft.CognitiveServices/accounts/Face/group/action",
      "Microsoft.CognitiveServices/accounts/Face/identify/action",
      "Microsoft.CognitiveServices/accounts/Face/verify/action",
      "Microsoft.CognitiveServices/accounts/Face/facelists/write",
      "Microsoft.CognitiveServices/accounts/Face/facelists/delete",
      "Microsoft.CognitiveServices/accounts/Face/facelists/read",
      "Microsoft.CognitiveServices/accounts/Face/facelists/persistedfaces/write",
      "Microsoft.CognitiveServices/accounts/Face/facelists/persistedfaces/delete",
      "Microsoft.CognitiveServices/accounts/Face/largefacelists/write",
      "Microsoft.CognitiveServices/accounts/Face/largefacelists/delete",
      "Microsoft.CognitiveServices/accounts/Face/largefacelists/read",
      "Microsoft.CognitiveServices/accounts/Face/largefacelists/train/action",
      "Microsoft.CognitiveServices/accounts/Face/largefacelists/persistedfaces/write",
      "Microsoft.CognitiveServices/accounts/Face/largefacelists/persistedfaces/delete",
      "Microsoft.CognitiveServices/accounts/Face/largefacelists/persistedfaces/read",
      "Microsoft.CognitiveServices/accounts/Face/largefacelists/training/read",
      "Microsoft.CognitiveServices/accounts/Face/largepersongroups/write",
      "Microsoft.CognitiveServices/accounts/Face/largepersongroups/delete",
      "Microsoft.CognitiveServices/accounts/Face/largepersongroups/read",
      "Microsoft.CognitiveServices/accounts/Face/largepersongroups/train/action",
      "Microsoft.CognitiveServices/accounts/Face/largepersongroups/persons/action",
      "Microsoft.CognitiveServices/accounts/Face/largepersongroups/training/read",
      "Microsoft.CognitiveServices/accounts/Face/largepersongroups/persons/delete",
      "Microsoft.CognitiveServices/accounts/Face/largepersongroups/persons/read",
      "Microsoft.CognitiveServices/accounts/Face/largepersongroups/persons/write",
      "Microsoft.CognitiveServices/accounts/Face/largepersongroups/persons/persistedfaces/write",
      "Microsoft.CognitiveServices/accounts/Face/largepersongroups/persons/persistedfaces/delete",
      "Microsoft.CognitiveServices/accounts/Face/largepersongroups/persons/persistedfaces/read",
      "Microsoft.CognitiveServices/accounts/Face/persongroups/write",
      "Microsoft.CognitiveServices/accounts/Face/persongroups/delete",
      "Microsoft.CognitiveServices/accounts/Face/persongroups/read",
      "Microsoft.CognitiveServices/accounts/Face/persongroups/train/action",
      "Microsoft.CognitiveServices/accounts/Face/persongroups/persons/action",
      "Microsoft.CognitiveServices/accounts/Face/persongroups/training/read",
      "Microsoft.CognitiveServices/accounts/Face/persongroups/persons/delete",
      "Microsoft.CognitiveServices/accounts/Face/persongroups/persons/read",
      "Microsoft.CognitiveServices/accounts/Face/persongroups/persons/write",
      "Microsoft.CognitiveServices/accounts/Face/persongroups/persons/persistedfaces/write",
      "Microsoft.CognitiveServices/accounts/Face/persongroups/persons/persistedfaces/delete",
      "Microsoft.CognitiveServices/accounts/Face/persongroups/persons/persistedfaces/read",
    ]
  }
}
