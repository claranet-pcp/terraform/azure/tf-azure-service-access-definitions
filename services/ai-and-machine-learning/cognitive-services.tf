// FullAccess
resource "azurerm_role_definition" "cognitive_services_fullaccess_definition" {
  name              = "cognitive-services-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.CognitiveServices/register/action",
      "Microsoft.CognitiveServices/accounts/read",
      "Microsoft.CognitiveServices/accounts/write",
      "Microsoft.CognitiveServices/accounts/delete",
      "Microsoft.CognitiveServices/accounts/listKeys/action",
      "Microsoft.CognitiveServices/accounts/regenerateKey/action",
      "Microsoft.CognitiveServices/accounts/usages/read",
      "Microsoft.CognitiveServices/accounts/skus/read",
      "Microsoft.CognitiveServices/Operations/read",
      "Microsoft.CognitiveServices/locations/checkSkuAvailability/action",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "cognitive_services_readonly_definition" {
  name              = "cognitive-services-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.CognitiveServices/accounts/read",
      "Microsoft.CognitiveServices/accounts/usages/read",
      "Microsoft.CognitiveServices/accounts/skus/read",
      "Microsoft.CognitiveServices/Operations/read",
    ]

    not_actions = [
      "Microsoft.CognitiveServices/register/action",
      "Microsoft.CognitiveServices/accounts/write",
      "Microsoft.CognitiveServices/accounts/delete",
      "Microsoft.CognitiveServices/accounts/listKeys/action",
      "Microsoft.CognitiveServices/accounts/regenerateKey/action",
      "Microsoft.CognitiveServices/locations/checkSkuAvailability/action",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "cognitive_services_noaccess_definition" {
  name              = "cognitive-services-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.CognitiveServices/register/action",
      "Microsoft.CognitiveServices/accounts/read",
      "Microsoft.CognitiveServices/accounts/write",
      "Microsoft.CognitiveServices/accounts/delete",
      "Microsoft.CognitiveServices/accounts/listKeys/action",
      "Microsoft.CognitiveServices/accounts/regenerateKey/action",
      "Microsoft.CognitiveServices/accounts/usages/read",
      "Microsoft.CognitiveServices/accounts/skus/read",
      "Microsoft.CognitiveServices/Operations/read",
      "Microsoft.CognitiveServices/locations/checkSkuAvailability/action",
    ]
  }
}
