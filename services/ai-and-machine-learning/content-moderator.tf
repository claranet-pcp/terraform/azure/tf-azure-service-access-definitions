// FullAccess
resource "azurerm_role_definition" "content_moderator_fullaccess_definition" {
  name              = "content-moderator-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.ContentModerator/updateCommunicationPreference/action",
      "Microsoft.ContentModerator/listCommunicationPreference/action",
      "Microsoft.ContentModerator/operations/read",
      "Microsoft.ContentModerator/applications/read",
      "Microsoft.ContentModerator/applications/write",
      "Microsoft.ContentModerator/applications/write",
      "Microsoft.ContentModerator/applications/delete",
      "Microsoft.ContentModerator/applications/listSecrets/action",
      "Microsoft.ContentModerator/applications/listSingleSignOnToken/action",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "content_moderator_readonly_definition" {
  name              = "content-moderator-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.ContentModerator/operations/read",
      "Microsoft.ContentModerator/applications/read",
    ]

    not_actions = [
      "Microsoft.ContentModerator/updateCommunicationPreference/action",
      "Microsoft.ContentModerator/listCommunicationPreference/action",
      "Microsoft.ContentModerator/applications/write",
      "Microsoft.ContentModerator/applications/write",
      "Microsoft.ContentModerator/applications/delete",
      "Microsoft.ContentModerator/applications/listSecrets/action",
      "Microsoft.ContentModerator/applications/listSingleSignOnToken/action",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "content_moderator_noaccess_definition" {
  name              = "content-moderator-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.ContentModerator/updateCommunicationPreference/action",
      "Microsoft.ContentModerator/listCommunicationPreference/action",
      "Microsoft.ContentModerator/operations/read",
      "Microsoft.ContentModerator/applications/read",
      "Microsoft.ContentModerator/applications/write",
      "Microsoft.ContentModerator/applications/write",
      "Microsoft.ContentModerator/applications/delete",
      "Microsoft.ContentModerator/applications/listSecrets/action",
      "Microsoft.ContentModerator/applications/listSingleSignOnToken/action",
    ]
  }
}
