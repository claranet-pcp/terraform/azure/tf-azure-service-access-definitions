// FullAccess
resource "azurerm_role_definition" "machine_learning_studio_fullaccess_definition" {
  name              = "machine-learning-studio-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.MachineLearning/register/action",
      "Microsoft.MachineLearning/webServices/action",
      "Microsoft.MachineLearning/webServices/read",
      "Microsoft.MachineLearning/webServices/write",
      "Microsoft.MachineLearning/webServices/delete",
      "Microsoft.MachineLearning/webServices/listkeys/read",
      "Microsoft.MachineLearning/Workspaces/read",
      "Microsoft.MachineLearning/Workspaces/write",
      "Microsoft.MachineLearning/Workspaces/delete",
      "Microsoft.MachineLearning/Workspaces/listworkspacekeys/action",
      "Microsoft.MachineLearning/Workspaces/resyncstoragekeys/action",
      "Microsoft.MachineLearning/commitmentPlans/read",
      "Microsoft.MachineLearning/commitmentPlans/write",
      "Microsoft.MachineLearning/commitmentPlans/delete",
      "Microsoft.MachineLearning/commitmentPlans/join/action",
      "Microsoft.MachineLearning/commitmentPlans/commitmentAssociations/read",
      "Microsoft.MachineLearning/commitmentPlans/commitmentAssociations/move/action",
      "Microsoft.MachineLearning/skus/read",
      "Microsoft.MachineLearning/operations/read",
      "Microsoft.MachineLearning/locations/operationsstatus/read",
      "Microsoft.MachineLearning/locations/operationresults/read",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "machine_learning_studio_readonly_definition" {
  name              = "machine-learning-studio-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.MachineLearning/webServices/read",
      "Microsoft.MachineLearning/webServices/listkeys/read",
      "Microsoft.MachineLearning/Workspaces/read",
      "Microsoft.MachineLearning/commitmentPlans/read",
      "Microsoft.MachineLearning/commitmentPlans/commitmentAssociations/read",
      "Microsoft.MachineLearning/skus/read",
      "Microsoft.MachineLearning/operations/read",
      "Microsoft.MachineLearning/locations/operationsstatus/read",
      "Microsoft.MachineLearning/locations/operationresults/read",
    ]

    not_actions = [
      "Microsoft.MachineLearning/register/action",
      "Microsoft.MachineLearning/webServices/action",
      "Microsoft.MachineLearning/webServices/write",
      "Microsoft.MachineLearning/webServices/delete",
      "Microsoft.MachineLearning/Workspaces/write",
      "Microsoft.MachineLearning/Workspaces/delete",
      "Microsoft.MachineLearning/Workspaces/listworkspacekeys/action",
      "Microsoft.MachineLearning/Workspaces/resyncstoragekeys/action",
      "Microsoft.MachineLearning/commitmentPlans/write",
      "Microsoft.MachineLearning/commitmentPlans/delete",
      "Microsoft.MachineLearning/commitmentPlans/join/action",
      "Microsoft.MachineLearning/commitmentPlans/commitmentAssociations/move/action",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "machine_learning_studio_noaccess_definition" {
  name              = "machine-learning-studio-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.MachineLearning/register/action",
      "Microsoft.MachineLearning/webServices/action",
      "Microsoft.MachineLearning/webServices/read",
      "Microsoft.MachineLearning/webServices/write",
      "Microsoft.MachineLearning/webServices/delete",
      "Microsoft.MachineLearning/webServices/listkeys/read",
      "Microsoft.MachineLearning/Workspaces/read",
      "Microsoft.MachineLearning/Workspaces/write",
      "Microsoft.MachineLearning/Workspaces/delete",
      "Microsoft.MachineLearning/Workspaces/listworkspacekeys/action",
      "Microsoft.MachineLearning/Workspaces/resyncstoragekeys/action",
      "Microsoft.MachineLearning/commitmentPlans/read",
      "Microsoft.MachineLearning/commitmentPlans/write",
      "Microsoft.MachineLearning/commitmentPlans/delete",
      "Microsoft.MachineLearning/commitmentPlans/join/action",
      "Microsoft.MachineLearning/commitmentPlans/commitmentAssociations/read",
      "Microsoft.MachineLearning/commitmentPlans/commitmentAssociations/move/action",
      "Microsoft.MachineLearning/skus/read",
      "Microsoft.MachineLearning/operations/read",
      "Microsoft.MachineLearning/locations/operationsstatus/read",
      "Microsoft.MachineLearning/locations/operationresults/read",
    ]
  }
}
