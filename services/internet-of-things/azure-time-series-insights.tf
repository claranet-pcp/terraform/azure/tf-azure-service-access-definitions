// FullAccess
resource "azurerm_role_definition" "azure_time_series_insights_fullaccess_definition" {
  name              = "azure-time-series-insights-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.TimeSeriesInsights/register/action",
      "Microsoft.TimeSeriesInsights/environments/read",
      "Microsoft.TimeSeriesInsights/environments/write",
      "Microsoft.TimeSeriesInsights/environments/delete",
      "Microsoft.TimeSeriesInsights/environments/status/read",
      "Microsoft.TimeSeriesInsights/environments/eventsources/read",
      "Microsoft.TimeSeriesInsights/environments/eventsources/write",
      "Microsoft.TimeSeriesInsights/environments/eventsources/delete",
      "Microsoft.TimeSeriesInsights/environments/referencedatasets/read",
      "Microsoft.TimeSeriesInsights/environments/referencedatasets/write",
      "Microsoft.TimeSeriesInsights/environments/referencedatasets/delete",
      "Microsoft.TimeSeriesInsights/environments/accesspolicies/read",
      "Microsoft.TimeSeriesInsights/environments/accesspolicies/write",
      "Microsoft.TimeSeriesInsights/environments/accesspolicies/delete",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "azure_time_series_insights_readonly_definition" {
  name              = "azure-time-series-insights-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.TimeSeriesInsights/environments/read",
      "Microsoft.TimeSeriesInsights/environments/status/read",
      "Microsoft.TimeSeriesInsights/environments/eventsources/read",
      "Microsoft.TimeSeriesInsights/environments/referencedatasets/read",
      "Microsoft.TimeSeriesInsights/environments/accesspolicies/read",
    ]

    not_actions = [
      "Microsoft.TimeSeriesInsights/register/action",
      "Microsoft.TimeSeriesInsights/environments/write",
      "Microsoft.TimeSeriesInsights/environments/delete",
      "Microsoft.TimeSeriesInsights/environments/eventsources/write",
      "Microsoft.TimeSeriesInsights/environments/eventsources/delete",
      "Microsoft.TimeSeriesInsights/environments/referencedatasets/write",
      "Microsoft.TimeSeriesInsights/environments/referencedatasets/delete",
      "Microsoft.TimeSeriesInsights/environments/accesspolicies/write",
      "Microsoft.TimeSeriesInsights/environments/accesspolicies/delete",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "azure_time_series_insights_noaccess_definition" {
  name              = "azure-time-series-insights-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.TimeSeriesInsights/register/action",
      "Microsoft.TimeSeriesInsights/environments/read",
      "Microsoft.TimeSeriesInsights/environments/write",
      "Microsoft.TimeSeriesInsights/environments/delete",
      "Microsoft.TimeSeriesInsights/environments/status/read",
      "Microsoft.TimeSeriesInsights/environments/eventsources/read",
      "Microsoft.TimeSeriesInsights/environments/eventsources/write",
      "Microsoft.TimeSeriesInsights/environments/eventsources/delete",
      "Microsoft.TimeSeriesInsights/environments/referencedatasets/read",
      "Microsoft.TimeSeriesInsights/environments/referencedatasets/write",
      "Microsoft.TimeSeriesInsights/environments/referencedatasets/delete",
      "Microsoft.TimeSeriesInsights/environments/accesspolicies/read",
      "Microsoft.TimeSeriesInsights/environments/accesspolicies/write",
      "Microsoft.TimeSeriesInsights/environments/accesspolicies/delete",
    ]
  }
}
