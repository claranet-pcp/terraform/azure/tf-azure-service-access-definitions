// FullAccess
resource "azurerm_role_definition" "azure_maps_fullaccess_definition" {
  name              = "azure-maps-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.BingMaps/Operations/read",
      "Microsoft.BingMaps/mapApis/Read",
      "Microsoft.BingMaps/mapApis/Write",
      "Microsoft.BingMaps/mapApis/Delete",
      "Microsoft.BingMaps/mapApis/regenerateKey/action",
      "Microsoft.BingMaps/mapApis/listSecrets/action",
      "Microsoft.BingMaps/mapApis/listSingleSignOnToken/action",
      "Microsoft.Maps/register/action",
      "Microsoft.Maps/accounts/write",
      "Microsoft.Maps/accounts/read",
      "Microsoft.Maps/accounts/delete",
      "Microsoft.Maps/accounts/listKeys/action",
      "Microsoft.Maps/accounts/regenerateKey/action",
      "Microsoft.Maps/accounts/data/read",
      "Microsoft.Maps/accounts/eventGridFilters/delete",
      "Microsoft.Maps/accounts/eventGridFilters/read",
      "Microsoft.Maps/accounts/eventGridFilters/write",
      "Microsoft.Maps/operations/read",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "azure_maps_readonly_definition" {
  name              = "azure-maps-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.BingMaps/Operations/read",
      "Microsoft.BingMaps/mapApis/Read",
      "Microsoft.Maps/accounts/read",
      "Microsoft.Maps/accounts/data/read",
      "Microsoft.Maps/accounts/eventGridFilters/read",
      "Microsoft.Maps/operations/read",
    ]

    not_actions = [
      "Microsoft.BingMaps/mapApis/Write",
      "Microsoft.BingMaps/mapApis/Delete",
      "Microsoft.BingMaps/mapApis/regenerateKey/action",
      "Microsoft.BingMaps/mapApis/listSecrets/action",
      "Microsoft.BingMaps/mapApis/listSingleSignOnToken/action",
      "Microsoft.Maps/register/action",
      "Microsoft.Maps/accounts/write",
      "Microsoft.Maps/accounts/delete",
      "Microsoft.Maps/accounts/listKeys/action",
      "Microsoft.Maps/accounts/regenerateKey/action",
      "Microsoft.Maps/accounts/eventGridFilters/delete",
      "Microsoft.Maps/accounts/eventGridFilters/write",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "azure_maps_noaccess_definition" {
  name              = "azure-maps-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.BingMaps/Operations/read",
      "Microsoft.BingMaps/mapApis/Read",
      "Microsoft.BingMaps/mapApis/Write",
      "Microsoft.BingMaps/mapApis/Delete",
      "Microsoft.BingMaps/mapApis/regenerateKey/action",
      "Microsoft.BingMaps/mapApis/listSecrets/action",
      "Microsoft.BingMaps/mapApis/listSingleSignOnToken/action",
      "Microsoft.Maps/register/action",
      "Microsoft.Maps/accounts/write",
      "Microsoft.Maps/accounts/read",
      "Microsoft.Maps/accounts/delete",
      "Microsoft.Maps/accounts/listKeys/action",
      "Microsoft.Maps/accounts/regenerateKey/action",
      "Microsoft.Maps/accounts/data/read",
      "Microsoft.Maps/accounts/eventGridFilters/delete",
      "Microsoft.Maps/accounts/eventGridFilters/read",
      "Microsoft.Maps/accounts/eventGridFilters/write",
      "Microsoft.Maps/operations/read",
    ]
  }
}
