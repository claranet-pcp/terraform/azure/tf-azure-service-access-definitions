// FullAccess
resource "azurerm_role_definition" "azure_iot_central_fullaccess_definition" {
  name              = "azure-iot-central-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.IoTCentral/checkNameAvailability/action",
      "Microsoft.IoTCentral/checkSubdomainAvailability/action",
      "Microsoft.IoTCentral/appTemplates/action",
      "Microsoft.IoTCentral/register/action",
      "Microsoft.IoTCentral/IoTApps/read",
      "Microsoft.IoTCentral/IoTApps/write",
      "Microsoft.IoTCentral/IoTApps/delete",
      "Microsoft.IoTCentral/operations/read",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "azure_iot_central_readonly_definition" {
  name              = "azure-iot-central-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.IoTCentral/IoTApps/read",
      "Microsoft.IoTCentral/operations/read",
    ]

    not_actions = [
      "Microsoft.IoTCentral/checkNameAvailability/action",
      "Microsoft.IoTCentral/checkSubdomainAvailability/action",
      "Microsoft.IoTCentral/appTemplates/action",
      "Microsoft.IoTCentral/register/action",
      "Microsoft.IoTCentral/IoTApps/write",
      "Microsoft.IoTCentral/IoTApps/delete",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "azure_iot_central_noaccess_definition" {
  name              = "azure-iot-central-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.IoTCentral/checkNameAvailability/action",
      "Microsoft.IoTCentral/checkSubdomainAvailability/action",
      "Microsoft.IoTCentral/appTemplates/action",
      "Microsoft.IoTCentral/register/action",
      "Microsoft.IoTCentral/IoTApps/read",
      "Microsoft.IoTCentral/IoTApps/write",
      "Microsoft.IoTCentral/IoTApps/delete",
      "Microsoft.IoTCentral/operations/read",
    ]
  }
}
