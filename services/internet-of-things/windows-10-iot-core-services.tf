// FullAccess
resource "azurerm_role_definition" "windows_10_iot_core_services_fullaccess_definition" {
  name              = "windows-10-iot-core-services-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.WindowsIoT/DeviceServices/read",
      "Microsoft.WindowsIoT/DeviceServices/write",
      "Microsoft.WindowsIoT/DeviceServices/delete",
      "Microsoft.WindowsIoT/Operations/read",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "windows_10_iot_core_services_readonly_definition" {
  name              = "windows-10-iot-core-services-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.WindowsIoT/DeviceServices/read",
      "Microsoft.WindowsIoT/Operations/read",
    ]

    not_actions = [
      "Microsoft.WindowsIoT/DeviceServices/write",
      "Microsoft.WindowsIoT/DeviceServices/delete",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "windows_10_iot_core_services_noaccess_definition" {
  name              = "windows-10-iot-core-services-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.WindowsIoT/DeviceServices/read",
      "Microsoft.WindowsIoT/DeviceServices/write",
      "Microsoft.WindowsIoT/DeviceServices/delete",
      "Microsoft.WindowsIoT/Operations/read",
    ]
  }
}
