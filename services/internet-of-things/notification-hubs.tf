// FullAccess
resource "azurerm_role_definition" "notification_hubs_fullaccess_definition" {
  name              = "notification-hubs-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.NotificationHubs/register/action",
      "Microsoft.NotificationHubs/unregister/action",
      "Microsoft.NotificationHubs/CheckNamespaceAvailability/action",
      "Microsoft.NotificationHubs/Namespaces/write",
      "Microsoft.NotificationHubs/Namespaces/read",
      "Microsoft.NotificationHubs/Namespaces/Delete",
      "Microsoft.NotificationHubs/Namespaces/authorizationRules/action",
      "Microsoft.NotificationHubs/Namespaces/CheckNotificationHubAvailability/action",
      "Microsoft.NotificationHubs/Namespaces/authorizationRules/write",
      "Microsoft.NotificationHubs/Namespaces/authorizationRules/read",
      "Microsoft.NotificationHubs/Namespaces/authorizationRules/delete",
      "Microsoft.NotificationHubs/Namespaces/authorizationRules/listkeys/action",
      "Microsoft.NotificationHubs/Namespaces/authorizationRules/regenerateKeys/action",
      "Microsoft.NotificationHubs/Namespaces/NotificationHubs/write",
      "Microsoft.NotificationHubs/Namespaces/NotificationHubs/read",
      "Microsoft.NotificationHubs/Namespaces/NotificationHubs/Delete",
      "Microsoft.NotificationHubs/Namespaces/NotificationHubs/authorizationRules/action",
      "Microsoft.NotificationHubs/Namespaces/NotificationHubs/pnsCredentials/action",
      "Microsoft.NotificationHubs/Namespaces/NotificationHubs/debugSend/action",
      "Microsoft.NotificationHubs/Namespaces/NotificationHubs/authorizationRules/write",
      "Microsoft.NotificationHubs/Namespaces/NotificationHubs/authorizationRules/read",
      "Microsoft.NotificationHubs/Namespaces/NotificationHubs/authorizationRules/delete",
      "Microsoft.NotificationHubs/Namespaces/NotificationHubs/authorizationRules/listkeys/action",
      "Microsoft.NotificationHubs/Namespaces/NotificationHubs/authorizationRules/regenerateKeys/action",
      "Microsoft.NotificationHubs/Namespaces/NotificationHubs/metricDefinitions/read",
      "Microsoft.NotificationHubs/operations/read",
      "Microsoft.NotificationHubs/operationResults/read",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "notification_hubs_readonly_definition" {
  name              = "notification-hubs-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.NotificationHubs/Namespaces/read",
      "Microsoft.NotificationHubs/Namespaces/authorizationRules/read",
      "Microsoft.NotificationHubs/Namespaces/NotificationHubs/read",
      "Microsoft.NotificationHubs/Namespaces/NotificationHubs/authorizationRules/read",
      "Microsoft.NotificationHubs/Namespaces/NotificationHubs/metricDefinitions/read",
      "Microsoft.NotificationHubs/operations/read",
      "Microsoft.NotificationHubs/operationResults/read",
    ]

    not_actions = [
      "Microsoft.NotificationHubs/register/action",
      "Microsoft.NotificationHubs/unregister/action",
      "Microsoft.NotificationHubs/CheckNamespaceAvailability/action",
      "Microsoft.NotificationHubs/Namespaces/write",
      "Microsoft.NotificationHubs/Namespaces/Delete",
      "Microsoft.NotificationHubs/Namespaces/authorizationRules/action",
      "Microsoft.NotificationHubs/Namespaces/CheckNotificationHubAvailability/action",
      "Microsoft.NotificationHubs/Namespaces/authorizationRules/write",
      "Microsoft.NotificationHubs/Namespaces/authorizationRules/delete",
      "Microsoft.NotificationHubs/Namespaces/authorizationRules/listkeys/action",
      "Microsoft.NotificationHubs/Namespaces/authorizationRules/regenerateKeys/action",
      "Microsoft.NotificationHubs/Namespaces/NotificationHubs/write",
      "Microsoft.NotificationHubs/Namespaces/NotificationHubs/Delete",
      "Microsoft.NotificationHubs/Namespaces/NotificationHubs/authorizationRules/action",
      "Microsoft.NotificationHubs/Namespaces/NotificationHubs/pnsCredentials/action",
      "Microsoft.NotificationHubs/Namespaces/NotificationHubs/debugSend/action",
      "Microsoft.NotificationHubs/Namespaces/NotificationHubs/authorizationRules/write",
      "Microsoft.NotificationHubs/Namespaces/NotificationHubs/authorizationRules/delete",
      "Microsoft.NotificationHubs/Namespaces/NotificationHubs/authorizationRules/listkeys/action",
      "Microsoft.NotificationHubs/Namespaces/NotificationHubs/authorizationRules/regenerateKeys/action",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "notification_hubs_noaccess_definition" {
  name              = "notification-hubs-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.NotificationHubs/register/action",
      "Microsoft.NotificationHubs/unregister/action",
      "Microsoft.NotificationHubs/CheckNamespaceAvailability/action",
      "Microsoft.NotificationHubs/Namespaces/write",
      "Microsoft.NotificationHubs/Namespaces/read",
      "Microsoft.NotificationHubs/Namespaces/Delete",
      "Microsoft.NotificationHubs/Namespaces/authorizationRules/action",
      "Microsoft.NotificationHubs/Namespaces/CheckNotificationHubAvailability/action",
      "Microsoft.NotificationHubs/Namespaces/authorizationRules/write",
      "Microsoft.NotificationHubs/Namespaces/authorizationRules/read",
      "Microsoft.NotificationHubs/Namespaces/authorizationRules/delete",
      "Microsoft.NotificationHubs/Namespaces/authorizationRules/listkeys/action",
      "Microsoft.NotificationHubs/Namespaces/authorizationRules/regenerateKeys/action",
      "Microsoft.NotificationHubs/Namespaces/NotificationHubs/write",
      "Microsoft.NotificationHubs/Namespaces/NotificationHubs/read",
      "Microsoft.NotificationHubs/Namespaces/NotificationHubs/Delete",
      "Microsoft.NotificationHubs/Namespaces/NotificationHubs/authorizationRules/action",
      "Microsoft.NotificationHubs/Namespaces/NotificationHubs/pnsCredentials/action",
      "Microsoft.NotificationHubs/Namespaces/NotificationHubs/debugSend/action",
      "Microsoft.NotificationHubs/Namespaces/NotificationHubs/authorizationRules/write",
      "Microsoft.NotificationHubs/Namespaces/NotificationHubs/authorizationRules/read",
      "Microsoft.NotificationHubs/Namespaces/NotificationHubs/authorizationRules/delete",
      "Microsoft.NotificationHubs/Namespaces/NotificationHubs/authorizationRules/listkeys/action",
      "Microsoft.NotificationHubs/Namespaces/NotificationHubs/authorizationRules/regenerateKeys/action",
      "Microsoft.NotificationHubs/Namespaces/NotificationHubs/metricDefinitions/read",
      "Microsoft.NotificationHubs/operations/read",
      "Microsoft.NotificationHubs/operationResults/read",
    ]
  }
}
