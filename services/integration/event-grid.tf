// FullAccess
resource "azurerm_role_definition" "event_grid_fullaccess_definition" {
  name              = "event-grid-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.EventGrid/register/action",
      "Microsoft.EventGrid/unregister/action",
      "Microsoft.EventGrid/eventSubscriptions/write",
      "Microsoft.EventGrid/eventSubscriptions/read",
      "Microsoft.EventGrid/eventSubscriptions/delete",
      "Microsoft.EventGrid/eventSubscriptions/getFullUrl/action",
      "Microsoft.EventGrid/topics/write",
      "Microsoft.EventGrid/topics/read",
      "Microsoft.EventGrid/topics/delete",
      "Microsoft.EventGrid/topics/listKeys/action",
      "Microsoft.EventGrid/topics/regenerateKey/action",
      "Microsoft.EventGrid/extensionTopics/providers/Microsoft.Insights/metricDefinitions/read",
      "Microsoft.EventGrid/topictypes/read",
      "Microsoft.EventGrid/topictypes/eventtypes/read",
      "Microsoft.EventGrid/operationResults/read",
      "Microsoft.EventGrid/locations/operationResults/read",
      "Microsoft.EventGrid/operationsStatus/read",
      "Microsoft.EventGrid/locations/operationsStatus/read",
      "Microsoft.EventGrid/topictypes/eventSubscriptions/read",
      "Microsoft.EventGrid/locations/eventSubscriptions/read",
      "Microsoft.EventGrid/locations/topictypes/eventSubscriptions/read",
      "Microsoft.EventGrid/operations/read",
      "Microsoft.EventGrid/extensionTopics/read",
      "Microsoft.EventGrid/domains/write",
      "Microsoft.EventGrid/domains/read",
      "Microsoft.EventGrid/domains/delete",
      "Microsoft.EventGrid/domains/listKeys/action",
      "Microsoft.EventGrid/domains/regenerateKey/action",
      "Microsoft.EventGrid/domains/topics/read",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "event_grid_readonly_definition" {
  name              = "event-grid-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.EventGrid/eventSubscriptions/read",
      "Microsoft.EventGrid/topics/read",
      "Microsoft.EventGrid/extensionTopics/providers/Microsoft.Insights/metricDefinitions/read",
      "Microsoft.EventGrid/topictypes/read",
      "Microsoft.EventGrid/topictypes/eventtypes/read",
      "Microsoft.EventGrid/operationResults/read",
      "Microsoft.EventGrid/locations/operationResults/read",
      "Microsoft.EventGrid/operationsStatus/read",
      "Microsoft.EventGrid/locations/operationsStatus/read",
      "Microsoft.EventGrid/topictypes/eventSubscriptions/read",
      "Microsoft.EventGrid/locations/eventSubscriptions/read",
      "Microsoft.EventGrid/locations/topictypes/eventSubscriptions/read",
      "Microsoft.EventGrid/operations/read",
      "Microsoft.EventGrid/extensionTopics/read",
      "Microsoft.EventGrid/domains/read",
      "Microsoft.EventGrid/domains/topics/read",
    ]

    not_actions = [
      "Microsoft.EventGrid/register/action",
      "Microsoft.EventGrid/unregister/action",
      "Microsoft.EventGrid/eventSubscriptions/write",
      "Microsoft.EventGrid/eventSubscriptions/delete",
      "Microsoft.EventGrid/eventSubscriptions/getFullUrl/action",
      "Microsoft.EventGrid/topics/write",
      "Microsoft.EventGrid/topics/delete",
      "Microsoft.EventGrid/topics/listKeys/action",
      "Microsoft.EventGrid/topics/regenerateKey/action",
      "Microsoft.EventGrid/domains/write",
      "Microsoft.EventGrid/domains/delete",
      "Microsoft.EventGrid/domains/listKeys/action",
      "Microsoft.EventGrid/domains/regenerateKey/action",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "event_grid_noaccess_definition" {
  name              = "event-grid-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.EventGrid/register/action",
      "Microsoft.EventGrid/unregister/action",
      "Microsoft.EventGrid/eventSubscriptions/write",
      "Microsoft.EventGrid/eventSubscriptions/read",
      "Microsoft.EventGrid/eventSubscriptions/delete",
      "Microsoft.EventGrid/eventSubscriptions/getFullUrl/action",
      "Microsoft.EventGrid/topics/write",
      "Microsoft.EventGrid/topics/read",
      "Microsoft.EventGrid/topics/delete",
      "Microsoft.EventGrid/topics/listKeys/action",
      "Microsoft.EventGrid/topics/regenerateKey/action",
      "Microsoft.EventGrid/extensionTopics/providers/Microsoft.Insights/metricDefinitions/read",
      "Microsoft.EventGrid/topictypes/read",
      "Microsoft.EventGrid/topictypes/eventtypes/read",
      "Microsoft.EventGrid/operationResults/read",
      "Microsoft.EventGrid/locations/operationResults/read",
      "Microsoft.EventGrid/operationsStatus/read",
      "Microsoft.EventGrid/locations/operationsStatus/read",
      "Microsoft.EventGrid/topictypes/eventSubscriptions/read",
      "Microsoft.EventGrid/locations/eventSubscriptions/read",
      "Microsoft.EventGrid/locations/topictypes/eventSubscriptions/read",
      "Microsoft.EventGrid/operations/read",
      "Microsoft.EventGrid/extensionTopics/read",
      "Microsoft.EventGrid/domains/write",
      "Microsoft.EventGrid/domains/read",
      "Microsoft.EventGrid/domains/delete",
      "Microsoft.EventGrid/domains/listKeys/action",
      "Microsoft.EventGrid/domains/regenerateKey/action",
      "Microsoft.EventGrid/domains/topics/read",
    ]
  }
}
