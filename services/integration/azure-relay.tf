// FullAccess
resource "azurerm_role_definition" "azure_relay_fullaccess_definition" {
  name              = "azure-relay-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Relay/checkNamespaceAvailability/action",
      "Microsoft.Relay/checkNameAvailability/action",
      "Microsoft.Relay/register/action",
      "Microsoft.Relay/unregister/action",
      "Microsoft.Relay/namespaces/write",
      "Microsoft.Relay/namespaces/read",
      "Microsoft.Relay/namespaces/Delete",
      "Microsoft.Relay/namespaces/authorizationRules/action",
      "Microsoft.Relay/namespaces/removeAcsNamepsace/action",
      "Microsoft.Relay/namespaces/operationresults/read",
      "Microsoft.Relay/namespaces/authorizationRules/read",
      "Microsoft.Relay/namespaces/authorizationRules/write",
      "Microsoft.Relay/namespaces/authorizationRules/delete",
      "Microsoft.Relay/namespaces/authorizationRules/listkeys/action",
      "Microsoft.Relay/namespaces/authorizationRules/regenerateKeys/action",
      "Microsoft.Relay/namespaces/messagingPlan/read",
      "Microsoft.Relay/namespaces/messagingPlan/write",
      "Microsoft.Relay/namespaces/HybridConnections/write",
      "Microsoft.Relay/namespaces/HybridConnections/read",
      "Microsoft.Relay/namespaces/HybridConnections/Delete",
      "Microsoft.Relay/namespaces/HybridConnections/authorizationRules/action",
      "Microsoft.Relay/namespaces/HybridConnections/authorizationRules/read",
      "Microsoft.Relay/namespaces/HybridConnections/authorizationRules/write",
      "Microsoft.Relay/namespaces/HybridConnections/authorizationRules/delete",
      "Microsoft.Relay/namespaces/HybridConnections/authorizationRules/listkeys/action",
      "Microsoft.Relay/namespaces/HybridConnections/authorizationRules/regeneratekeys/action",
      "Microsoft.Relay/namespaces/WcfRelays/write",
      "Microsoft.Relay/namespaces/WcfRelays/read",
      "Microsoft.Relay/namespaces/WcfRelays/Delete",
      "Microsoft.Relay/namespaces/WcfRelays/authorizationRules/action",
      "Microsoft.Relay/namespaces/WcfRelays/authorizationRules/read",
      "Microsoft.Relay/namespaces/WcfRelays/authorizationRules/write",
      "Microsoft.Relay/namespaces/WcfRelays/authorizationRules/delete",
      "Microsoft.Relay/namespaces/WcfRelays/authorizationRules/listkeys/action",
      "Microsoft.Relay/namespaces/WcfRelays/authorizationRules/regeneratekeys/action",
      "Microsoft.Relay/operations/read",
      "Microsoft.Relay/namespaces/providers/Microsoft.Insights/metricDefinitions/read",
      "Microsoft.Relay/namespaces/disasterrecoveryconfigs/checkNameAvailability/action",
      "Microsoft.Relay/namespaces/disasterRecoveryConfigs/write",
      "Microsoft.Relay/namespaces/disasterRecoveryConfigs/read",
      "Microsoft.Relay/namespaces/disasterRecoveryConfigs/delete",
      "Microsoft.Relay/namespaces/disasterRecoveryConfigs/breakPairing/action",
      "Microsoft.Relay/namespaces/disasterRecoveryConfigs/failover/action",
      "Microsoft.Relay/namespaces/disasterRecoveryConfigs/authorizationRules/read",
      "Microsoft.Relay/namespaces/disasterRecoveryConfigs/authorizationRules/listkeys/action",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "azure_relay_readonly_definition" {
  name              = "azure-relay-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Relay/namespaces/read",
      "Microsoft.Relay/namespaces/operationresults/read",
      "Microsoft.Relay/namespaces/authorizationRules/read",
      "Microsoft.Relay/namespaces/messagingPlan/read",
      "Microsoft.Relay/namespaces/HybridConnections/read",
      "Microsoft.Relay/namespaces/HybridConnections/authorizationRules/read",
      "Microsoft.Relay/namespaces/WcfRelays/read",
      "Microsoft.Relay/namespaces/WcfRelays/authorizationRules/read",
      "Microsoft.Relay/operations/read",
      "Microsoft.Relay/namespaces/providers/Microsoft.Insights/metricDefinitions/read",
      "Microsoft.Relay/namespaces/disasterRecoveryConfigs/read",
      "Microsoft.Relay/namespaces/disasterRecoveryConfigs/authorizationRules/read",
    ]

    not_actions = [
      "Microsoft.Relay/checkNamespaceAvailability/action",
      "Microsoft.Relay/checkNameAvailability/action",
      "Microsoft.Relay/register/action",
      "Microsoft.Relay/unregister/action",
      "Microsoft.Relay/namespaces/write",
      "Microsoft.Relay/namespaces/Delete",
      "Microsoft.Relay/namespaces/authorizationRules/action",
      "Microsoft.Relay/namespaces/removeAcsNamepsace/action",
      "Microsoft.Relay/namespaces/authorizationRules/write",
      "Microsoft.Relay/namespaces/authorizationRules/delete",
      "Microsoft.Relay/namespaces/authorizationRules/listkeys/action",
      "Microsoft.Relay/namespaces/authorizationRules/regenerateKeys/action",
      "Microsoft.Relay/namespaces/messagingPlan/write",
      "Microsoft.Relay/namespaces/HybridConnections/write",
      "Microsoft.Relay/namespaces/HybridConnections/Delete",
      "Microsoft.Relay/namespaces/HybridConnections/authorizationRules/action",
      "Microsoft.Relay/namespaces/HybridConnections/authorizationRules/write",
      "Microsoft.Relay/namespaces/HybridConnections/authorizationRules/delete",
      "Microsoft.Relay/namespaces/HybridConnections/authorizationRules/listkeys/action",
      "Microsoft.Relay/namespaces/HybridConnections/authorizationRules/regeneratekeys/action",
      "Microsoft.Relay/namespaces/WcfRelays/write",
      "Microsoft.Relay/namespaces/WcfRelays/Delete",
      "Microsoft.Relay/namespaces/WcfRelays/authorizationRules/action",
      "Microsoft.Relay/namespaces/WcfRelays/authorizationRules/write",
      "Microsoft.Relay/namespaces/WcfRelays/authorizationRules/delete",
      "Microsoft.Relay/namespaces/WcfRelays/authorizationRules/listkeys/action",
      "Microsoft.Relay/namespaces/WcfRelays/authorizationRules/regeneratekeys/action",
      "Microsoft.Relay/namespaces/disasterrecoveryconfigs/checkNameAvailability/action",
      "Microsoft.Relay/namespaces/disasterRecoveryConfigs/write",
      "Microsoft.Relay/namespaces/disasterRecoveryConfigs/delete",
      "Microsoft.Relay/namespaces/disasterRecoveryConfigs/breakPairing/action",
      "Microsoft.Relay/namespaces/disasterRecoveryConfigs/failover/action",
      "Microsoft.Relay/namespaces/disasterRecoveryConfigs/authorizationRules/listkeys/action",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "azure_relay_noaccess_definition" {
  name              = "azure-relay-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.Relay/checkNamespaceAvailability/action",
      "Microsoft.Relay/checkNameAvailability/action",
      "Microsoft.Relay/register/action",
      "Microsoft.Relay/unregister/action",
      "Microsoft.Relay/namespaces/write",
      "Microsoft.Relay/namespaces/read",
      "Microsoft.Relay/namespaces/Delete",
      "Microsoft.Relay/namespaces/authorizationRules/action",
      "Microsoft.Relay/namespaces/removeAcsNamepsace/action",
      "Microsoft.Relay/namespaces/operationresults/read",
      "Microsoft.Relay/namespaces/authorizationRules/read",
      "Microsoft.Relay/namespaces/authorizationRules/write",
      "Microsoft.Relay/namespaces/authorizationRules/delete",
      "Microsoft.Relay/namespaces/authorizationRules/listkeys/action",
      "Microsoft.Relay/namespaces/authorizationRules/regenerateKeys/action",
      "Microsoft.Relay/namespaces/messagingPlan/read",
      "Microsoft.Relay/namespaces/messagingPlan/write",
      "Microsoft.Relay/namespaces/HybridConnections/write",
      "Microsoft.Relay/namespaces/HybridConnections/read",
      "Microsoft.Relay/namespaces/HybridConnections/Delete",
      "Microsoft.Relay/namespaces/HybridConnections/authorizationRules/action",
      "Microsoft.Relay/namespaces/HybridConnections/authorizationRules/read",
      "Microsoft.Relay/namespaces/HybridConnections/authorizationRules/write",
      "Microsoft.Relay/namespaces/HybridConnections/authorizationRules/delete",
      "Microsoft.Relay/namespaces/HybridConnections/authorizationRules/listkeys/action",
      "Microsoft.Relay/namespaces/HybridConnections/authorizationRules/regeneratekeys/action",
      "Microsoft.Relay/namespaces/WcfRelays/write",
      "Microsoft.Relay/namespaces/WcfRelays/read",
      "Microsoft.Relay/namespaces/WcfRelays/Delete",
      "Microsoft.Relay/namespaces/WcfRelays/authorizationRules/action",
      "Microsoft.Relay/namespaces/WcfRelays/authorizationRules/read",
      "Microsoft.Relay/namespaces/WcfRelays/authorizationRules/write",
      "Microsoft.Relay/namespaces/WcfRelays/authorizationRules/delete",
      "Microsoft.Relay/namespaces/WcfRelays/authorizationRules/listkeys/action",
      "Microsoft.Relay/namespaces/WcfRelays/authorizationRules/regeneratekeys/action",
      "Microsoft.Relay/operations/read",
      "Microsoft.Relay/namespaces/providers/Microsoft.Insights/metricDefinitions/read",
      "Microsoft.Relay/namespaces/disasterrecoveryconfigs/checkNameAvailability/action",
      "Microsoft.Relay/namespaces/disasterRecoveryConfigs/write",
      "Microsoft.Relay/namespaces/disasterRecoveryConfigs/read",
      "Microsoft.Relay/namespaces/disasterRecoveryConfigs/delete",
      "Microsoft.Relay/namespaces/disasterRecoveryConfigs/breakPairing/action",
      "Microsoft.Relay/namespaces/disasterRecoveryConfigs/failover/action",
      "Microsoft.Relay/namespaces/disasterRecoveryConfigs/authorizationRules/read",
      "Microsoft.Relay/namespaces/disasterRecoveryConfigs/authorizationRules/listkeys/action",
    ]
  }
}
