// FullAccess
resource "azurerm_role_definition" "azure_custom_providers_fullaccess_definition" {
  name              = "azure-custom-providers-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.CustomProviders/register/action",
      "Microsoft.CustomProviders/unregister/action",
      "Microsoft.CustomProviders/resourceproviders/read",
      "Microsoft.CustomProviders/resourceproviders/write",
      "Microsoft.CustomProviders/resourceproviders/delete",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "azure_custom_providers_readonly_definition" {
  name              = "azure-custom-providers-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.CustomProviders/resourceproviders/read",
    ]

    not_actions = [
      "Microsoft.CustomProviders/register/action",
      "Microsoft.CustomProviders/unregister/action",
      "Microsoft.CustomProviders/resourceproviders/write",
      "Microsoft.CustomProviders/resourceproviders/delete",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "azure_custom_providers_noaccess_definition" {
  name              = "azure-custom-providers-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.CustomProviders/register/action",
      "Microsoft.CustomProviders/unregister/action",
      "Microsoft.CustomProviders/resourceproviders/read",
      "Microsoft.CustomProviders/resourceproviders/write",
      "Microsoft.CustomProviders/resourceproviders/delete",
    ]
  }
}
