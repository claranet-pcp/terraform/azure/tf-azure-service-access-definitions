// FullAccess
resource "azurerm_role_definition" "azure_devops_fullaccess_definition" {
  name              = "azure-devops-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.VisualStudio/Register/Action",
      "Microsoft.VisualStudio/Account/Write",
      "Microsoft.VisualStudio/Account/Delete",
      "Microsoft.VisualStudio/Account/Read",
      "Microsoft.VisualStudio/Project/Write",
      "Microsoft.VisualStudio/Project/Delete",
      "Microsoft.VisualStudio/Project/Read",
      "Microsoft.VisualStudio/Extension/Write",
      "Microsoft.VisualStudio/Extension/Delete",
      "Microsoft.VisualStudio/Extension/Read",
      "Microsoft.VisualStudio/Account/Project/Read",
      "Microsoft.VisualStudio/Account/Project/Write",
      "Microsoft.VisualStudio/Account/Extension/Read",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "azure_devops_readonly_definition" {
  name              = "azure-devops-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.VisualStudio/Account/Read",
      "Microsoft.VisualStudio/Project/Read",
      "Microsoft.VisualStudio/Extension/Read",
      "Microsoft.VisualStudio/Account/Project/Read",
      "Microsoft.VisualStudio/Account/Extension/Read",
    ]

    not_actions = [
      "Microsoft.VisualStudio/Register/Action",
      "Microsoft.VisualStudio/Account/Write",
      "Microsoft.VisualStudio/Account/Delete",
      "Microsoft.VisualStudio/Project/Write",
      "Microsoft.VisualStudio/Project/Delete",
      "Microsoft.VisualStudio/Extension/Write",
      "Microsoft.VisualStudio/Extension/Delete",
      "Microsoft.VisualStudio/Account/Project/Write",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "azure_devops_noaccess_definition" {
  name              = "azure-devops-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.VisualStudio/Register/Action",
      "Microsoft.VisualStudio/Account/Write",
      "Microsoft.VisualStudio/Account/Delete",
      "Microsoft.VisualStudio/Account/Read",
      "Microsoft.VisualStudio/Project/Write",
      "Microsoft.VisualStudio/Project/Delete",
      "Microsoft.VisualStudio/Project/Read",
      "Microsoft.VisualStudio/Extension/Write",
      "Microsoft.VisualStudio/Extension/Delete",
      "Microsoft.VisualStudio/Extension/Read",
      "Microsoft.VisualStudio/Account/Project/Read",
      "Microsoft.VisualStudio/Account/Project/Write",
      "Microsoft.VisualStudio/Account/Extension/Read",
    ]
  }
}
