// FullAccess
resource "azurerm_role_definition" "azure_stack_fullaccess_definition" {
  name              = "azure-stack-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.AzureStack/register/action",
      "Microsoft.AzureStack/registrations/read",
      "Microsoft.AzureStack/registrations/write",
      "Microsoft.AzureStack/registrations/delete",
      "Microsoft.AzureStack/registrations/getActivationKey/action",
      "Microsoft.AzureStack/registrations/products/read",
      "Microsoft.AzureStack/registrations/products/listDetails/action",
      "Microsoft.AzureStack/registrations/customerSubscriptions/read",
      "Microsoft.AzureStack/registrations/customerSubscriptions/write",
      "Microsoft.AzureStack/registrations/customerSubscriptions/delete",
      "Microsoft.AzureStack/Operations/read",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "azure_stack_readonly_definition" {
  name              = "azure-stack-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.AzureStack/registrations/read",
      "Microsoft.AzureStack/registrations/products/read",
      "Microsoft.AzureStack/registrations/customerSubscriptions/read",
      "Microsoft.AzureStack/Operations/read",
    ]

    not_actions = [
      "Microsoft.AzureStack/register/action",
      "Microsoft.AzureStack/registrations/write",
      "Microsoft.AzureStack/registrations/delete",
      "Microsoft.AzureStack/registrations/getActivationKey/action",
      "Microsoft.AzureStack/registrations/products/listDetails/action",
      "Microsoft.AzureStack/registrations/customerSubscriptions/write",
      "Microsoft.AzureStack/registrations/customerSubscriptions/delete",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "azure_stack_noaccess_definition" {
  name              = "azure-stack-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.AzureStack/register/action",
      "Microsoft.AzureStack/registrations/read",
      "Microsoft.AzureStack/registrations/write",
      "Microsoft.AzureStack/registrations/delete",
      "Microsoft.AzureStack/registrations/getActivationKey/action",
      "Microsoft.AzureStack/registrations/products/read",
      "Microsoft.AzureStack/registrations/products/listDetails/action",
      "Microsoft.AzureStack/registrations/customerSubscriptions/read",
      "Microsoft.AzureStack/registrations/customerSubscriptions/write",
      "Microsoft.AzureStack/registrations/customerSubscriptions/delete",
      "Microsoft.AzureStack/Operations/read",
    ]
  }
}
