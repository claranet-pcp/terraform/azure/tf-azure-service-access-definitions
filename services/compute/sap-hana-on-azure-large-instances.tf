// FullAccess
resource "azurerm_role_definition" "sap_hana_on_azure_large_instances_fullaccess_definition" {
  name              = "sap-hana-on-azure-large-instances-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.HanaOnAzure/register/action",
      "Microsoft.HanaOnAzure/hanaInstances/read",
      "Microsoft.HanaOnAzure/hanaInstances/restart/action",
      "Microsoft.HanaOnAzure/locations/operations/read",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "sap_hana_on_azure_large_instances_readonly_definition" {
  name              = "sap-hana-on-azure-large-instances-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.HanaOnAzure/hanaInstances/read",
      "Microsoft.HanaOnAzure/locations/operations/read",
    ]

    not_actions = [
      "Microsoft.HanaOnAzure/register/action",
      "Microsoft.HanaOnAzure/hanaInstances/restart/action",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "sap_hana_on_azure_large_instances_noaccess_definition" {
  name              = "sap-hana-on-azure-large-instances-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.HanaOnAzure/register/action",
      "Microsoft.HanaOnAzure/hanaInstances/read",
      "Microsoft.HanaOnAzure/hanaInstances/restart/action",
      "Microsoft.HanaOnAzure/locations/operations/read",
    ]
  }
}
