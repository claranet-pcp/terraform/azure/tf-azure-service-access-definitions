// FullAccess
resource "azurerm_role_definition" "virtual_machine_scale_sets_fullaccess_definition" {
  name              = "virtual-machine-scale-sets-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Compute/virtualMachineScaleSets/extensions/read",
      "Microsoft.Compute/virtualMachineScaleSets/extensions/write",
      "Microsoft.Compute/virtualMachineScaleSets/extensions/delete",
      "Microsoft.Compute/virtualMachineScaleSets/read",
      "Microsoft.Compute/virtualMachineScaleSets/write",
      "Microsoft.Compute/virtualMachineScaleSets/delete",
      "Microsoft.Compute/virtualMachineScaleSets/delete/action",
      "Microsoft.Compute/virtualMachineScaleSets/start/action",
      "Microsoft.Compute/virtualMachineScaleSets/powerOff/action",
      "Microsoft.Compute/virtualMachineScaleSets/restart/action",
      "Microsoft.Compute/virtualMachineScaleSets/deallocate/action",
      "Microsoft.Compute/virtualMachineScaleSets/manualUpgrade/action",
      "Microsoft.Compute/virtualMachineScaleSets/reimage/action",
      "Microsoft.Compute/virtualMachineScaleSets/reimageAll/action",
      "Microsoft.Compute/virtualMachineScaleSets/redeploy/action",
      "Microsoft.Compute/virtualMachineScaleSets/performMaintenance/action",
      "Microsoft.Compute/virtualMachineScaleSets/scale/action",
      "Microsoft.Compute/virtualMachineScaleSets/forceRecoveryServiceFabricPlatformUpdateDomainWalk/action",
      "Microsoft.Compute/virtualMachineScaleSets/osRollingUpgrade/action",
      "Microsoft.Compute/virtualMachineScaleSets/instanceView/read",
      "Microsoft.Compute/virtualMachineScaleSets/skus/read",
      "Microsoft.Compute/virtualMachineScaleSets/rollingUpgrades/read",
      "Microsoft.Compute/virtualMachineScaleSets/rollingUpgrades/cancel/action",
      "Microsoft.Compute/virtualMachineScaleSets/osUpgradeHistory/read",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/read",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/write",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/delete",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/start/action",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/powerOff/action",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/restart/action",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/deallocate/action",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/reimage/action",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/reimageAll/action",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/redeploy/action",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/performMaintenance/action",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/runCommand/action",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/instanceView/read",
      "Microsoft.Compute/virtualMachineScaleSets/networkInterfaces/read",
      "Microsoft.Compute/virtualMachineScaleSets/publicIPAddresses/read",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/networkInterfaces/read",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/networkInterfaces/ipConfigurations/publicIPAddresses/read",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/networkInterfaces/ipConfigurations/read",
      "Microsoft.Compute/virtualMachineScaleSets/vmSizes/read",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "virtual_machine_scale_sets_readonly_definition" {
  name              = "virtual-machine-scale-sets-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Compute/virtualMachineScaleSets/extensions/read",
      "Microsoft.Compute/virtualMachineScaleSets/read",
      "Microsoft.Compute/virtualMachineScaleSets/instanceView/read",
      "Microsoft.Compute/virtualMachineScaleSets/skus/read",
      "Microsoft.Compute/virtualMachineScaleSets/rollingUpgrades/read",
      "Microsoft.Compute/virtualMachineScaleSets/osUpgradeHistory/read",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/read",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/instanceView/read",
      "Microsoft.Compute/virtualMachineScaleSets/networkInterfaces/read",
      "Microsoft.Compute/virtualMachineScaleSets/publicIPAddresses/read",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/networkInterfaces/read",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/networkInterfaces/ipConfigurations/publicIPAddresses/read",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/networkInterfaces/ipConfigurations/read",
      "Microsoft.Compute/virtualMachineScaleSets/vmSizes/read",
    ]

    not_actions = [
      "Microsoft.Compute/virtualMachineScaleSets/extensions/write",
      "Microsoft.Compute/virtualMachineScaleSets/extensions/delete",
      "Microsoft.Compute/virtualMachineScaleSets/write",
      "Microsoft.Compute/virtualMachineScaleSets/delete",
      "Microsoft.Compute/virtualMachineScaleSets/delete/action",
      "Microsoft.Compute/virtualMachineScaleSets/start/action",
      "Microsoft.Compute/virtualMachineScaleSets/powerOff/action",
      "Microsoft.Compute/virtualMachineScaleSets/restart/action",
      "Microsoft.Compute/virtualMachineScaleSets/deallocate/action",
      "Microsoft.Compute/virtualMachineScaleSets/manualUpgrade/action",
      "Microsoft.Compute/virtualMachineScaleSets/reimage/action",
      "Microsoft.Compute/virtualMachineScaleSets/reimageAll/action",
      "Microsoft.Compute/virtualMachineScaleSets/redeploy/action",
      "Microsoft.Compute/virtualMachineScaleSets/performMaintenance/action",
      "Microsoft.Compute/virtualMachineScaleSets/scale/action",
      "Microsoft.Compute/virtualMachineScaleSets/forceRecoveryServiceFabricPlatformUpdateDomainWalk/action",
      "Microsoft.Compute/virtualMachineScaleSets/osRollingUpgrade/action",
      "Microsoft.Compute/virtualMachineScaleSets/rollingUpgrades/cancel/action",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/write",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/delete",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/start/action",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/powerOff/action",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/restart/action",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/deallocate/action",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/reimage/action",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/reimageAll/action",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/redeploy/action",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/performMaintenance/action",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/runCommand/action",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "virtual_machine_scale_sets_noaccess_definition" {
  name              = "virtual-machine-scale-sets-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.Compute/virtualMachineScaleSets/extensions/read",
      "Microsoft.Compute/virtualMachineScaleSets/extensions/write",
      "Microsoft.Compute/virtualMachineScaleSets/extensions/delete",
      "Microsoft.Compute/virtualMachineScaleSets/read",
      "Microsoft.Compute/virtualMachineScaleSets/write",
      "Microsoft.Compute/virtualMachineScaleSets/delete",
      "Microsoft.Compute/virtualMachineScaleSets/delete/action",
      "Microsoft.Compute/virtualMachineScaleSets/start/action",
      "Microsoft.Compute/virtualMachineScaleSets/powerOff/action",
      "Microsoft.Compute/virtualMachineScaleSets/restart/action",
      "Microsoft.Compute/virtualMachineScaleSets/deallocate/action",
      "Microsoft.Compute/virtualMachineScaleSets/manualUpgrade/action",
      "Microsoft.Compute/virtualMachineScaleSets/reimage/action",
      "Microsoft.Compute/virtualMachineScaleSets/reimageAll/action",
      "Microsoft.Compute/virtualMachineScaleSets/redeploy/action",
      "Microsoft.Compute/virtualMachineScaleSets/performMaintenance/action",
      "Microsoft.Compute/virtualMachineScaleSets/scale/action",
      "Microsoft.Compute/virtualMachineScaleSets/forceRecoveryServiceFabricPlatformUpdateDomainWalk/action",
      "Microsoft.Compute/virtualMachineScaleSets/osRollingUpgrade/action",
      "Microsoft.Compute/virtualMachineScaleSets/instanceView/read",
      "Microsoft.Compute/virtualMachineScaleSets/skus/read",
      "Microsoft.Compute/virtualMachineScaleSets/rollingUpgrades/read",
      "Microsoft.Compute/virtualMachineScaleSets/rollingUpgrades/cancel/action",
      "Microsoft.Compute/virtualMachineScaleSets/osUpgradeHistory/read",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/read",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/write",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/delete",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/start/action",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/powerOff/action",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/restart/action",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/deallocate/action",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/reimage/action",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/reimageAll/action",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/redeploy/action",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/performMaintenance/action",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/runCommand/action",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/instanceView/read",
      "Microsoft.Compute/virtualMachineScaleSets/networkInterfaces/read",
      "Microsoft.Compute/virtualMachineScaleSets/publicIPAddresses/read",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/networkInterfaces/read",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/networkInterfaces/ipConfigurations/publicIPAddresses/read",
      "Microsoft.Compute/virtualMachineScaleSets/virtualMachines/networkInterfaces/ipConfigurations/read",
      "Microsoft.Compute/virtualMachineScaleSets/vmSizes/read",
    ]
  }
}
