// FullAccess
resource "azurerm_role_definition" "azure_vmware_solution_by_cloudsimple_fullaccess_definition" {
  name              = "azure-vmware-solution-by-cloudsimple-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.VMwareCloudSimple/register/action",
      "Microsoft.VMwareCloudSimple/virtualMachines/write",
      "Microsoft.VMwareCloudSimple/virtualMachines/delete",
      "Microsoft.VMwareCloudSimple/virtualMachines/read",
      "Microsoft.VMwareCloudSimple/virtualMachines/start/action",
      "Microsoft.VMwareCloudSimple/virtualMachines/stop/action",
      "Microsoft.VMwareCloudSimple/locations/privateClouds/read",
      "Microsoft.VMwareCloudSimple/locations/privateClouds/virtualNetworks/read",
      "Microsoft.VMwareCloudSimple/locations/privateClouds/virtualMachineTemplates/read",
      "Microsoft.VMwareCloudSimple/locations/privateClouds/resourcePools/read",
      "Microsoft.VMwareCloudSimple/locations/operationresults/read",
      "Microsoft.VMwareCloudSimple/dedicatedCloudNodes/write",
      "Microsoft.VMwareCloudSimple/dedicatedCloudNodes/delete",
      "Microsoft.VMwareCloudSimple/dedicatedCloudNodes/read",
      "Microsoft.VMwareCloudSimple/dedicatedCloudServices/write",
      "Microsoft.VMwareCloudSimple/dedicatedCloudServices/delete",
      "Microsoft.VMwareCloudSimple/dedicatedCloudServices/read",
      "Microsoft.VMwareCloudSimple/locations/availabilities/read",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "azure_vmware_solution_by_cloudsimple_readonly_definition" {
  name              = "azure-vmware-solution-by-cloudsimple-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.VMwareCloudSimple/virtualMachines/read",
      "Microsoft.VMwareCloudSimple/locations/privateClouds/read",
      "Microsoft.VMwareCloudSimple/locations/privateClouds/virtualNetworks/read",
      "Microsoft.VMwareCloudSimple/locations/privateClouds/virtualMachineTemplates/read",
      "Microsoft.VMwareCloudSimple/locations/privateClouds/resourcePools/read",
      "Microsoft.VMwareCloudSimple/locations/operationresults/read",
      "Microsoft.VMwareCloudSimple/dedicatedCloudNodes/read",
      "Microsoft.VMwareCloudSimple/dedicatedCloudServices/read",
      "Microsoft.VMwareCloudSimple/locations/availabilities/read",
    ]

    not_actions = [
      "Microsoft.VMwareCloudSimple/register/action",
      "Microsoft.VMwareCloudSimple/virtualMachines/write",
      "Microsoft.VMwareCloudSimple/virtualMachines/delete",
      "Microsoft.VMwareCloudSimple/virtualMachines/start/action",
      "Microsoft.VMwareCloudSimple/virtualMachines/stop/action",
      "Microsoft.VMwareCloudSimple/dedicatedCloudNodes/write",
      "Microsoft.VMwareCloudSimple/dedicatedCloudNodes/delete",
      "Microsoft.VMwareCloudSimple/dedicatedCloudServices/write",
      "Microsoft.VMwareCloudSimple/dedicatedCloudServices/delete",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "azure_vmware_solution_by_cloudsimple_noaccess_definition" {
  name              = "azure-vmware-solution-by-cloudsimple-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.VMwareCloudSimple/register/action",
      "Microsoft.VMwareCloudSimple/virtualMachines/write",
      "Microsoft.VMwareCloudSimple/virtualMachines/delete",
      "Microsoft.VMwareCloudSimple/virtualMachines/read",
      "Microsoft.VMwareCloudSimple/virtualMachines/start/action",
      "Microsoft.VMwareCloudSimple/virtualMachines/stop/action",
      "Microsoft.VMwareCloudSimple/locations/privateClouds/read",
      "Microsoft.VMwareCloudSimple/locations/privateClouds/virtualNetworks/read",
      "Microsoft.VMwareCloudSimple/locations/privateClouds/virtualMachineTemplates/read",
      "Microsoft.VMwareCloudSimple/locations/privateClouds/resourcePools/read",
      "Microsoft.VMwareCloudSimple/locations/operationresults/read",
      "Microsoft.VMwareCloudSimple/dedicatedCloudNodes/write",
      "Microsoft.VMwareCloudSimple/dedicatedCloudNodes/delete",
      "Microsoft.VMwareCloudSimple/dedicatedCloudNodes/read",
      "Microsoft.VMwareCloudSimple/dedicatedCloudServices/write",
      "Microsoft.VMwareCloudSimple/dedicatedCloudServices/delete",
      "Microsoft.VMwareCloudSimple/dedicatedCloudServices/read",
      "Microsoft.VMwareCloudSimple/locations/availabilities/read",
    ]
  }
}
