// FullAccess
resource "azurerm_role_definition" "batch_fullaccess_definition" {
  name              = "batch-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Batch/register/action",
      "Microsoft.Batch/unregister/action",
      "Microsoft.Batch/batchAccounts/read",
      "Microsoft.Batch/batchAccounts/write",
      "Microsoft.Batch/batchAccounts/delete",
      "Microsoft.Batch/batchAccounts/listkeys/action",
      "Microsoft.Batch/batchAccounts/regeneratekeys/action",
      "Microsoft.Batch/batchAccounts/syncAutoStorageKeys/action",
      "Microsoft.Batch/locations/quotas/read",
      "Microsoft.Batch/locations/checkNameAvailability/action",
      "Microsoft.Batch/batchAccounts/operationResults/read",
      "Microsoft.Batch/batchAccounts/applications/read",
      "Microsoft.Batch/batchAccounts/applications/write",
      "Microsoft.Batch/batchAccounts/applications/delete",
      "Microsoft.Batch/batchAccounts/applications/versions/read",
      "Microsoft.Batch/batchAccounts/applications/versions/write",
      "Microsoft.Batch/batchAccounts/applications/versions/delete",
      "Microsoft.Batch/batchAccounts/applications/versions/activate/action",
      "Microsoft.Batch/batchAccounts/certificates/read",
      "Microsoft.Batch/batchAccounts/certificates/write",
      "Microsoft.Batch/batchAccounts/certificates/delete",
      "Microsoft.Batch/batchAccounts/certificates/cancelDelete/action",
      "Microsoft.Batch/batchAccounts/certificateOperationResults/read",
      "Microsoft.Batch/batchAccounts/pools/read",
      "Microsoft.Batch/batchAccounts/pools/write",
      "Microsoft.Batch/batchAccounts/pools/delete",
      "Microsoft.Batch/batchAccounts/pools/stopResize/action",
      "Microsoft.Batch/batchAccounts/pools/disableAutoscale/action",
      "Microsoft.Batch/batchAccounts/poolOperationResults/read",
      "Microsoft.Batch/operations/read",
      "Microsoft.Batch/locations/accountOperationResults/read",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "batch_readonly_definition" {
  name              = "batch-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.Batch/batchAccounts/read",
      "Microsoft.Batch/locations/quotas/read",
      "Microsoft.Batch/batchAccounts/operationResults/read",
      "Microsoft.Batch/batchAccounts/applications/read",
      "Microsoft.Batch/batchAccounts/applications/versions/read",
      "Microsoft.Batch/batchAccounts/certificates/read",
      "Microsoft.Batch/batchAccounts/certificateOperationResults/read",
      "Microsoft.Batch/batchAccounts/pools/read",
      "Microsoft.Batch/batchAccounts/poolOperationResults/read",
      "Microsoft.Batch/operations/read",
      "Microsoft.Batch/locations/accountOperationResults/read",
    ]

    not_actions = [
      "Microsoft.Batch/register/action",
      "Microsoft.Batch/unregister/action",
      "Microsoft.Batch/batchAccounts/write",
      "Microsoft.Batch/batchAccounts/delete",
      "Microsoft.Batch/batchAccounts/listkeys/action",
      "Microsoft.Batch/batchAccounts/regeneratekeys/action",
      "Microsoft.Batch/batchAccounts/syncAutoStorageKeys/action",
      "Microsoft.Batch/locations/checkNameAvailability/action",
      "Microsoft.Batch/batchAccounts/applications/write",
      "Microsoft.Batch/batchAccounts/applications/delete",
      "Microsoft.Batch/batchAccounts/applications/versions/write",
      "Microsoft.Batch/batchAccounts/applications/versions/delete",
      "Microsoft.Batch/batchAccounts/applications/versions/activate/action",
      "Microsoft.Batch/batchAccounts/certificates/write",
      "Microsoft.Batch/batchAccounts/certificates/delete",
      "Microsoft.Batch/batchAccounts/certificates/cancelDelete/action",
      "Microsoft.Batch/batchAccounts/pools/write",
      "Microsoft.Batch/batchAccounts/pools/delete",
      "Microsoft.Batch/batchAccounts/pools/stopResize/action",
      "Microsoft.Batch/batchAccounts/pools/disableAutoscale/action",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "batch_noaccess_definition" {
  name              = "batch-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.Batch/register/action",
      "Microsoft.Batch/unregister/action",
      "Microsoft.Batch/batchAccounts/read",
      "Microsoft.Batch/batchAccounts/write",
      "Microsoft.Batch/batchAccounts/delete",
      "Microsoft.Batch/batchAccounts/listkeys/action",
      "Microsoft.Batch/batchAccounts/regeneratekeys/action",
      "Microsoft.Batch/batchAccounts/syncAutoStorageKeys/action",
      "Microsoft.Batch/locations/quotas/read",
      "Microsoft.Batch/locations/checkNameAvailability/action",
      "Microsoft.Batch/batchAccounts/operationResults/read",
      "Microsoft.Batch/batchAccounts/applications/read",
      "Microsoft.Batch/batchAccounts/applications/write",
      "Microsoft.Batch/batchAccounts/applications/delete",
      "Microsoft.Batch/batchAccounts/applications/versions/read",
      "Microsoft.Batch/batchAccounts/applications/versions/write",
      "Microsoft.Batch/batchAccounts/applications/versions/delete",
      "Microsoft.Batch/batchAccounts/applications/versions/activate/action",
      "Microsoft.Batch/batchAccounts/certificates/read",
      "Microsoft.Batch/batchAccounts/certificates/write",
      "Microsoft.Batch/batchAccounts/certificates/delete",
      "Microsoft.Batch/batchAccounts/certificates/cancelDelete/action",
      "Microsoft.Batch/batchAccounts/certificateOperationResults/read",
      "Microsoft.Batch/batchAccounts/pools/read",
      "Microsoft.Batch/batchAccounts/pools/write",
      "Microsoft.Batch/batchAccounts/pools/delete",
      "Microsoft.Batch/batchAccounts/pools/stopResize/action",
      "Microsoft.Batch/batchAccounts/pools/disableAutoscale/action",
      "Microsoft.Batch/batchAccounts/poolOperationResults/read",
      "Microsoft.Batch/operations/read",
      "Microsoft.Batch/locations/accountOperationResults/read",
    ]
  }
}
