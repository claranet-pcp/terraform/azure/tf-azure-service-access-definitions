// FullAccess
resource "azurerm_role_definition" "azure_active_directory_domain_services_fullaccess_definition" {
  name              = "azure-active-directory-domain-services-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.AAD/unregister/action",
      "Microsoft.AAD/register/action",
      "Microsoft.AAD/domainServices/read",
      "Microsoft.AAD/domainServices/write",
      "Microsoft.AAD/domainServices/delete",
      "Microsoft.AAD/locations/operationresults/read",
      "Microsoft.AAD/Operations/read",
      "Microsoft.AAD/domainServices/oucontainer/read",
      "Microsoft.AAD/domainServices/oucontainer/write",
      "Microsoft.AAD/domainServices/oucontainer/delete",
      "Microsoft.AAD/domainServices/replicaSets/read",
      "Microsoft.AAD/domainServices/replicaSets/write",
      "Microsoft.AAD/domainServices/replicaSets/delete",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "azure_active_directory_domain_services_readonly_definition" {
  name              = "azure-active-directory-domain-services-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.AAD/domainServices/read",
      "Microsoft.AAD/locations/operationresults/read",
      "Microsoft.AAD/Operations/read",
      "Microsoft.AAD/domainServices/oucontainer/read",
      "Microsoft.AAD/domainServices/replicaSets/read",
    ]

    not_actions = [
      "Microsoft.AAD/unregister/action",
      "Microsoft.AAD/register/action",
      "Microsoft.AAD/domainServices/write",
      "Microsoft.AAD/domainServices/delete",
      "Microsoft.AAD/domainServices/oucontainer/write",
      "Microsoft.AAD/domainServices/oucontainer/delete",
      "Microsoft.AAD/domainServices/replicaSets/write",
      "Microsoft.AAD/domainServices/replicaSets/delete",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "azure_active_directory_domain_services_noaccess_definition" {
  name              = "azure-active-directory-domain-services-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.AAD/unregister/action",
      "Microsoft.AAD/register/action",
      "Microsoft.AAD/domainServices/read",
      "Microsoft.AAD/domainServices/write",
      "Microsoft.AAD/domainServices/delete",
      "Microsoft.AAD/locations/operationresults/read",
      "Microsoft.AAD/Operations/read",
      "Microsoft.AAD/domainServices/oucontainer/read",
      "Microsoft.AAD/domainServices/oucontainer/write",
      "Microsoft.AAD/domainServices/oucontainer/delete",
      "Microsoft.AAD/domainServices/replicaSets/read",
      "Microsoft.AAD/domainServices/replicaSets/write",
      "Microsoft.AAD/domainServices/replicaSets/delete",
    ]
  }
}
