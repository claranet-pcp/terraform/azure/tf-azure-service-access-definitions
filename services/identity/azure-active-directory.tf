// FullAccess
resource "azurerm_role_definition" "azure_active_directory_fullaccess_definition" {
  name              = "azure-active-directory-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.AzureActiveDirectory/register/action",
      "Microsoft.AzureActiveDirectory/b2cDirectories/write",
      "Microsoft.AzureActiveDirectory/b2cDirectories/read",
      "Microsoft.AzureActiveDirectory/b2cDirectories/delete",
      "Microsoft.AzureActiveDirectory/operations/read",
      "Microsoft.AzureActiveDirectory/b2ctenants/read",
      "Microsoft.ManagedIdentity/register/action",
      "Microsoft.ManagedIdentity/userAssignedIdentities/read",
      "Microsoft.ManagedIdentity/userAssignedIdentities/write",
      "Microsoft.ManagedIdentity/userAssignedIdentities/delete",
      "Microsoft.ManagedIdentity/userAssignedIdentities/assign/action",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "azure_active_directory_readonly_definition" {
  name              = "azure-active-directory-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.AzureActiveDirectory/b2cDirectories/read",
      "Microsoft.AzureActiveDirectory/operations/read",
      "Microsoft.AzureActiveDirectory/b2ctenants/read",
      "Microsoft.ManagedIdentity/userAssignedIdentities/read",
    ]

    not_actions = [
      "Microsoft.AzureActiveDirectory/register/action",
      "Microsoft.AzureActiveDirectory/b2cDirectories/write",
      "Microsoft.AzureActiveDirectory/b2cDirectories/delete",
      "Microsoft.ManagedIdentity/register/action",
      "Microsoft.ManagedIdentity/userAssignedIdentities/write",
      "Microsoft.ManagedIdentity/userAssignedIdentities/delete",
      "Microsoft.ManagedIdentity/userAssignedIdentities/assign/action",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "azure_active_directory_noaccess_definition" {
  name              = "azure-active-directory-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.AzureActiveDirectory/register/action",
      "Microsoft.AzureActiveDirectory/b2cDirectories/write",
      "Microsoft.AzureActiveDirectory/b2cDirectories/read",
      "Microsoft.AzureActiveDirectory/b2cDirectories/delete",
      "Microsoft.AzureActiveDirectory/operations/read",
      "Microsoft.AzureActiveDirectory/b2ctenants/read",
      "Microsoft.ManagedIdentity/register/action",
      "Microsoft.ManagedIdentity/userAssignedIdentities/read",
      "Microsoft.ManagedIdentity/userAssignedIdentities/write",
      "Microsoft.ManagedIdentity/userAssignedIdentities/delete",
      "Microsoft.ManagedIdentity/userAssignedIdentities/assign/action",
    ]
  }
}
