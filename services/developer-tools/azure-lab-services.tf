// FullAccess
resource "azurerm_role_definition" "azure_lab_services_fullaccess_definition" {
  name              = "azure-lab-services-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.LabServices/register/action",
      "Microsoft.LabServices/labAccounts/delete",
      "Microsoft.LabServices/labAccounts/read",
      "Microsoft.LabServices/labAccounts/write",
      "Microsoft.LabServices/labAccounts/CreateLab/action",
      "Microsoft.LabServices/labAccounts/GetRegionalAvailability/action",
      "Microsoft.LabServices/labAccounts/labs/delete",
      "Microsoft.LabServices/labAccounts/labs/read",
      "Microsoft.LabServices/labAccounts/labs/write",
      "Microsoft.LabServices/labAccounts/labs/Register/action",
      "Microsoft.LabServices/labAccounts/labs/AddUsers/action",
      "Microsoft.LabServices/labAccounts/labs/SendEmail/action",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/delete",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/read",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/write",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/ClaimAny/action",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/Publish/action",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/Start/action",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/Stop/action",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/SaveImage/action",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/ResetPassword/action",
      "Microsoft.LabServices/locations/operations/read",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/environments/delete",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/environments/read",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/environments/write",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/environments/Claim/action",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/environments/Start/action",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/environments/Stop/action",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/environments/ResetPassword/action",
      "Microsoft.LabServices/labAccounts/labs/users/delete",
      "Microsoft.LabServices/labAccounts/labs/users/read",
      "Microsoft.LabServices/labAccounts/labs/users/write",
      "Microsoft.LabServices/users/Register/action",
      "Microsoft.LabServices/users/ListAllEnvironments/action",
      "Microsoft.LabServices/users/ListEnvironments/action",
      "Microsoft.LabServices/users/ListLabs/action",
      "Microsoft.LabServices/users/StartEnvironment/action",
      "Microsoft.LabServices/users/StopEnvironment/action",
      "Microsoft.LabServices/users/ResetPassword/action",
      "Microsoft.LabServices/users/GetEnvironment/action",
      "Microsoft.LabServices/users/GetOperationStatus/action",
      "Microsoft.LabServices/users/GetOperationBatchStatus/action",
      "Microsoft.LabServices/users/GetPersonalPreferences/action",
      "Microsoft.LabServices/labAccounts/galleryImages/delete",
      "Microsoft.LabServices/labAccounts/galleryImages/read",
      "Microsoft.LabServices/labAccounts/galleryImages/write",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/schedules/delete",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/schedules/read",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/schedules/write",
      "Microsoft.LabServices/labAccounts/sharedImages/delete",
      "Microsoft.LabServices/labAccounts/sharedImages/read",
      "Microsoft.LabServices/labAccounts/sharedImages/write",
      "Microsoft.LabServices/labAccounts/sharedGalleries/delete",
      "Microsoft.LabServices/labAccounts/sharedGalleries/read",
      "Microsoft.LabServices/labAccounts/sharedGalleries/write",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "azure_lab_services_readonly_definition" {
  name              = "azure-lab-services-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.LabServices/labAccounts/read",
      "Microsoft.LabServices/labAccounts/labs/read",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/read",
      "Microsoft.LabServices/locations/operations/read",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/environments/read",
      "Microsoft.LabServices/labAccounts/labs/users/read",
      "Microsoft.LabServices/labAccounts/galleryImages/read",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/schedules/read",
      "Microsoft.LabServices/labAccounts/sharedImages/read",
      "Microsoft.LabServices/labAccounts/sharedGalleries/read",
    ]

    not_actions = [
      "Microsoft.LabServices/register/action",
      "Microsoft.LabServices/labAccounts/delete",
      "Microsoft.LabServices/labAccounts/write",
      "Microsoft.LabServices/labAccounts/CreateLab/action",
      "Microsoft.LabServices/labAccounts/GetRegionalAvailability/action",
      "Microsoft.LabServices/labAccounts/labs/delete",
      "Microsoft.LabServices/labAccounts/labs/write",
      "Microsoft.LabServices/labAccounts/labs/Register/action",
      "Microsoft.LabServices/labAccounts/labs/AddUsers/action",
      "Microsoft.LabServices/labAccounts/labs/SendEmail/action",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/delete",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/write",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/ClaimAny/action",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/Publish/action",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/Start/action",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/Stop/action",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/SaveImage/action",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/ResetPassword/action",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/environments/delete",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/environments/write",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/environments/Claim/action",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/environments/Start/action",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/environments/Stop/action",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/environments/ResetPassword/action",
      "Microsoft.LabServices/labAccounts/labs/users/delete",
      "Microsoft.LabServices/labAccounts/labs/users/write",
      "Microsoft.LabServices/users/Register/action",
      "Microsoft.LabServices/users/ListAllEnvironments/action",
      "Microsoft.LabServices/users/ListEnvironments/action",
      "Microsoft.LabServices/users/ListLabs/action",
      "Microsoft.LabServices/users/StartEnvironment/action",
      "Microsoft.LabServices/users/StopEnvironment/action",
      "Microsoft.LabServices/users/ResetPassword/action",
      "Microsoft.LabServices/users/GetEnvironment/action",
      "Microsoft.LabServices/users/GetOperationStatus/action",
      "Microsoft.LabServices/users/GetOperationBatchStatus/action",
      "Microsoft.LabServices/users/GetPersonalPreferences/action",
      "Microsoft.LabServices/labAccounts/galleryImages/delete",
      "Microsoft.LabServices/labAccounts/galleryImages/write",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/schedules/delete",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/schedules/write",
      "Microsoft.LabServices/labAccounts/sharedImages/delete",
      "Microsoft.LabServices/labAccounts/sharedImages/write",
      "Microsoft.LabServices/labAccounts/sharedGalleries/delete",
      "Microsoft.LabServices/labAccounts/sharedGalleries/write",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "azure_lab_services_noaccess_definition" {
  name              = "azure-lab-services-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.LabServices/register/action",
      "Microsoft.LabServices/labAccounts/delete",
      "Microsoft.LabServices/labAccounts/read",
      "Microsoft.LabServices/labAccounts/write",
      "Microsoft.LabServices/labAccounts/CreateLab/action",
      "Microsoft.LabServices/labAccounts/GetRegionalAvailability/action",
      "Microsoft.LabServices/labAccounts/labs/delete",
      "Microsoft.LabServices/labAccounts/labs/read",
      "Microsoft.LabServices/labAccounts/labs/write",
      "Microsoft.LabServices/labAccounts/labs/Register/action",
      "Microsoft.LabServices/labAccounts/labs/AddUsers/action",
      "Microsoft.LabServices/labAccounts/labs/SendEmail/action",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/delete",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/read",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/write",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/ClaimAny/action",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/Publish/action",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/Start/action",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/Stop/action",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/SaveImage/action",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/ResetPassword/action",
      "Microsoft.LabServices/locations/operations/read",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/environments/delete",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/environments/read",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/environments/write",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/environments/Claim/action",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/environments/Start/action",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/environments/Stop/action",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/environments/ResetPassword/action",
      "Microsoft.LabServices/labAccounts/labs/users/delete",
      "Microsoft.LabServices/labAccounts/labs/users/read",
      "Microsoft.LabServices/labAccounts/labs/users/write",
      "Microsoft.LabServices/users/Register/action",
      "Microsoft.LabServices/users/ListAllEnvironments/action",
      "Microsoft.LabServices/users/ListEnvironments/action",
      "Microsoft.LabServices/users/ListLabs/action",
      "Microsoft.LabServices/users/StartEnvironment/action",
      "Microsoft.LabServices/users/StopEnvironment/action",
      "Microsoft.LabServices/users/ResetPassword/action",
      "Microsoft.LabServices/users/GetEnvironment/action",
      "Microsoft.LabServices/users/GetOperationStatus/action",
      "Microsoft.LabServices/users/GetOperationBatchStatus/action",
      "Microsoft.LabServices/users/GetPersonalPreferences/action",
      "Microsoft.LabServices/labAccounts/galleryImages/delete",
      "Microsoft.LabServices/labAccounts/galleryImages/read",
      "Microsoft.LabServices/labAccounts/galleryImages/write",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/schedules/delete",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/schedules/read",
      "Microsoft.LabServices/labAccounts/labs/environmentSettings/schedules/write",
      "Microsoft.LabServices/labAccounts/sharedImages/delete",
      "Microsoft.LabServices/labAccounts/sharedImages/read",
      "Microsoft.LabServices/labAccounts/sharedImages/write",
      "Microsoft.LabServices/labAccounts/sharedGalleries/delete",
      "Microsoft.LabServices/labAccounts/sharedGalleries/read",
      "Microsoft.LabServices/labAccounts/sharedGalleries/write",
    ]
  }
}
