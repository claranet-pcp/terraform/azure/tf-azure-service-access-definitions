// FullAccess
resource "azurerm_role_definition" "azure_import_export_fullaccess_definition" {
  name              = "azure-import-export-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.ImportExport/register/action",
      "Microsoft.ImportExport/locations/read",
      "Microsoft.ImportExport/jobs/write",
      "Microsoft.ImportExport/jobs/read",
      "Microsoft.ImportExport/jobs/listBitLockerKeys/action",
      "Microsoft.ImportExport/jobs/delete",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "azure_import_export_readonly_definition" {
  name              = "azure-import-export-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.ImportExport/locations/read",
      "Microsoft.ImportExport/jobs/read",
    ]

    not_actions = [
      "Microsoft.ImportExport/register/action",
      "Microsoft.ImportExport/jobs/write",
      "Microsoft.ImportExport/jobs/listBitLockerKeys/action",
      "Microsoft.ImportExport/jobs/delete",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "azure_import_export_noaccess_definition" {
  name              = "azure-import-export-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.ImportExport/register/action",
      "Microsoft.ImportExport/locations/read",
      "Microsoft.ImportExport/jobs/write",
      "Microsoft.ImportExport/jobs/read",
      "Microsoft.ImportExport/jobs/listBitLockerKeys/action",
      "Microsoft.ImportExport/jobs/delete",
    ]
  }
}
