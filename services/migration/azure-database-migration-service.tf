// FullAccess
resource "azurerm_role_definition" "azure_database_migration_service_fullaccess_definition" {
  name              = "azure-database-migration-service-fullaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.DataMigration/register/action",
      "Microsoft.DataMigration/locations/operationResults/read",
      "Microsoft.DataMigration/locations/operationStatuses/read",
      "Microsoft.DataMigration/skus/read",
      "Microsoft.DataMigration/services/read",
      "Microsoft.DataMigration/services/write",
      "Microsoft.DataMigration/services/delete",
      "Microsoft.DataMigration/services/stop/action",
      "Microsoft.DataMigration/services/start/action",
      "Microsoft.DataMigration/services/checkStatus/action",
      "Microsoft.DataMigration/services/configureWorker/action",
      "Microsoft.DataMigration/services/addWorker/action",
      "Microsoft.DataMigration/services/removeWorker/action",
      "Microsoft.DataMigration/services/updateAgentConfig/action",
      "Microsoft.DataMigration/services/projects/read",
      "Microsoft.DataMigration/services/projects/write",
      "Microsoft.DataMigration/services/projects/delete",
      "Microsoft.DataMigration/services/projects/accessArtifacts/action",
      "Microsoft.DataMigration/services/projects/tasks/read",
      "Microsoft.DataMigration/services/projects/tasks/write",
      "Microsoft.DataMigration/services/projects/tasks/delete",
      "Microsoft.DataMigration/services/projects/tasks/cancel/action",
      "Microsoft.DataMigration/services/slots/read",
      "Microsoft.DataMigration/services/slots/write",
      "Microsoft.DataMigration/services/slots/delete",
      "Microsoft.DataMigration/services/projects/files/read",
      "Microsoft.DataMigration/services/projects/files/write",
      "Microsoft.DataMigration/services/projects/files/delete",
      "Microsoft.DataMigration/services/projects/files/read/action",
      "Microsoft.DataMigration/services/projects/files/readWrite/action",
      "Microsoft.DataMigration/services/serviceTasks/read",
      "Microsoft.DataMigration/services/serviceTasks/write",
      "Microsoft.DataMigration/services/serviceTasks/delete",
      "Microsoft.DataMigration/services/serviceTasks/cancel/action",
    ]

    not_actions = []
  }
}

// ReadOnly
resource "azurerm_role_definition" "azure_database_migration_service_readonly_definition" {
  name              = "azure-database-migration-service-readonly-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = [
      "Microsoft.DataMigration/locations/operationResults/read",
      "Microsoft.DataMigration/locations/operationStatuses/read",
      "Microsoft.DataMigration/skus/read",
      "Microsoft.DataMigration/services/read",
      "Microsoft.DataMigration/services/projects/read",
      "Microsoft.DataMigration/services/projects/tasks/read",
      "Microsoft.DataMigration/services/slots/read",
      "Microsoft.DataMigration/services/projects/files/read",
      "Microsoft.DataMigration/services/serviceTasks/read",
    ]

    not_actions = [
      "Microsoft.DataMigration/register/action",
      "Microsoft.DataMigration/services/write",
      "Microsoft.DataMigration/services/delete",
      "Microsoft.DataMigration/services/stop/action",
      "Microsoft.DataMigration/services/start/action",
      "Microsoft.DataMigration/services/checkStatus/action",
      "Microsoft.DataMigration/services/configureWorker/action",
      "Microsoft.DataMigration/services/addWorker/action",
      "Microsoft.DataMigration/services/removeWorker/action",
      "Microsoft.DataMigration/services/updateAgentConfig/action",
      "Microsoft.DataMigration/services/projects/write",
      "Microsoft.DataMigration/services/projects/delete",
      "Microsoft.DataMigration/services/projects/accessArtifacts/action",
      "Microsoft.DataMigration/services/projects/tasks/write",
      "Microsoft.DataMigration/services/projects/tasks/delete",
      "Microsoft.DataMigration/services/projects/tasks/cancel/action",
      "Microsoft.DataMigration/services/slots/write",
      "Microsoft.DataMigration/services/slots/delete",
      "Microsoft.DataMigration/services/projects/files/write",
      "Microsoft.DataMigration/services/projects/files/delete",
      "Microsoft.DataMigration/services/projects/files/read/action",
      "Microsoft.DataMigration/services/projects/files/readWrite/action",
      "Microsoft.DataMigration/services/serviceTasks/write",
      "Microsoft.DataMigration/services/serviceTasks/delete",
      "Microsoft.DataMigration/services/serviceTasks/cancel/action",
    ]
  }
}

// NoAccess
resource "azurerm_role_definition" "azure_database_migration_service_noaccess_definition" {
  name              = "azure-database-migration-service-noaccess-definition"
  scope             = "/subscriptions/${var.subscription_id}"
  assignable_scopes = "/subscriptions/${var.subscription_id}"

  permissions {
    actions = []

    not_actions = [
      "Microsoft.DataMigration/register/action",
      "Microsoft.DataMigration/locations/operationResults/read",
      "Microsoft.DataMigration/locations/operationStatuses/read",
      "Microsoft.DataMigration/skus/read",
      "Microsoft.DataMigration/services/read",
      "Microsoft.DataMigration/services/write",
      "Microsoft.DataMigration/services/delete",
      "Microsoft.DataMigration/services/stop/action",
      "Microsoft.DataMigration/services/start/action",
      "Microsoft.DataMigration/services/checkStatus/action",
      "Microsoft.DataMigration/services/configureWorker/action",
      "Microsoft.DataMigration/services/addWorker/action",
      "Microsoft.DataMigration/services/removeWorker/action",
      "Microsoft.DataMigration/services/updateAgentConfig/action",
      "Microsoft.DataMigration/services/projects/read",
      "Microsoft.DataMigration/services/projects/write",
      "Microsoft.DataMigration/services/projects/delete",
      "Microsoft.DataMigration/services/projects/accessArtifacts/action",
      "Microsoft.DataMigration/services/projects/tasks/read",
      "Microsoft.DataMigration/services/projects/tasks/write",
      "Microsoft.DataMigration/services/projects/tasks/delete",
      "Microsoft.DataMigration/services/projects/tasks/cancel/action",
      "Microsoft.DataMigration/services/slots/read",
      "Microsoft.DataMigration/services/slots/write",
      "Microsoft.DataMigration/services/slots/delete",
      "Microsoft.DataMigration/services/projects/files/read",
      "Microsoft.DataMigration/services/projects/files/write",
      "Microsoft.DataMigration/services/projects/files/delete",
      "Microsoft.DataMigration/services/projects/files/read/action",
      "Microsoft.DataMigration/services/projects/files/readWrite/action",
      "Microsoft.DataMigration/services/serviceTasks/read",
      "Microsoft.DataMigration/services/serviceTasks/write",
      "Microsoft.DataMigration/services/serviceTasks/delete",
      "Microsoft.DataMigration/services/serviceTasks/cancel/action",
    ]
  }
}
