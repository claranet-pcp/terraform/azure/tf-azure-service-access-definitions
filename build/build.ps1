[Cmdletbinding()]
Param(
    [Parameter(Mandatory=$False)]
    [ValidateNotNullOrEmpty()]
    [String]$CommitMessage = "Automatic rebuild including docs + terraform fmt."
)

$ErrorActionPreference = "Stop";
Write-Host "Performing tf-azure-service-access-definitions build.." -ForegroundColor Yellow;

# Filepaths
$RootFolder     = "$PSScriptRoot\..";
$SourceFile     = "$RootFolder\source\azure-services-and-roles.xlsx";
$ReadmeFile     = "$RootFolder\README.md";
$ServicesFolder = "$RootFolder\services";
$OutputFile     = "$RootFolder\outputs.tf";

# Source data
$Services    = Import-Excel $SourceFile -WorksheetName "Azure Services";
$Permissions = Import-Excel $SourceFile -WorksheetName "Permissions";

# Build vars
$Spacer     = "      ";
$Joiner     = "`",`r`n$Spacer`"";
$OutputRows = @();

# Cleanup
Get-ChildItem $ServicesFolder -Recurse | Remove-Item -Force -Recurse;
Remove-Item $ReadmeFile -Force -ErrorAction SilentlyContinue;
Remove-Item $OutputFile -Force -ErrorAction SilentlyContinue;

# The Boilerplates
$TerraformBoilerplate  = Get-Content "$RootFolder\build\terraform.boilerplate" -Raw;
$ReadmeBoilerplate     = Get-Content "$RootFolder\build\readme.boilerplate" -Raw;
$OutputBoilerplate     = Get-Content "$RootFolder\build\output.boilerplate" -Raw;

# Get our counters organised
$Count = 0;
$Total = $Services.Where({$_.IsAddressableService}).Count;

# Enum the addressable services
$Services.Where({$_.IsAddressableService}) | %{

    # Get the pipeline
    $Service = $_;

    # Write progress
    $Count++;
    $Percentage = [Math]::Round((($Count / $Total) * 100), 2).ToString() + "%";
    Write-Host "Building Terraform: Service $Count of $Total $Percentage ($($Service.Name))" -ForegroundColor Cyan;

    # Get the single properties
    $SNAKE_NAME          = $Service.Name.ToLower().Replace(" ","_").Replace("\","_").Replace("/","_").Replace("_(aks)","");
    $KEBAB_NAME          = $Service.Name.ToLower().Replace(" ","-").Replace("\","-").Replace("/","-").Replace("-(aks)","");
    $PROPER_NAME         = $Service.Name;
    $SERVICE_DESCRIPTION = $Service.ServiceDescription;

    # Get the service children
    $CHILD_SERVICES = $Services.Where({!$_.IsAddressableService -and $_.PartOfService -eq $Service.ServiceId});

    # Get the schemas for this service
    $SchemasForThisService = $Permissions | ?{$_.ServiceId -eq $Service.ServiceId};
    
    # Split out the schemas for FULLACCESS
    $FULLACCESS_ACTIONS = $SchemasForThisService.Operation;

    # Split out the schemas for READONLY
    $READONLY_ACTIONS    = $SchemasForThisService.Operation.Where({$_.Split("/")[-1] -eq "read"});
    $READONLY_NOTACTIONS = $SchemasForThisService.Operation.Where({$READONLY_ACTIONS -notcontains $_});

    # Split out the schemas for NOACCESS
    $NOACCESS_NOTACTIONS = $SchemasForThisService.Operation;
    
    # Do the first batch of boilerplate replacements
    $Boilerplate = $TerraformBoilerplate.Replace("<<SNAKE_NAME>>", $SNAKE_NAME);
    $Boilerplate = $Boilerplate.Replace("<<KEBAB_NAME>>", $KEBAB_NAME);
    $Boilerplate = $Boilerplate.Replace("<<PROPER_NAME>>", $PROPER_NAME);

    # FullAccess role
    $BoilerPlate = $Boilerplate.Replace("<<FULLACCESS_ACTIONS>>",'"'+$($FULLACCESS_ACTIONS -join $Joiner)+'"');

    # ReadOnly role
    $BoilerPlate = $Boilerplate.Replace("<<READONLY_ACTIONS>>",'"'+$($READONLY_ACTIONS -join $Joiner)+'"');
    $BoilerPlate = $Boilerplate.Replace("<<READONLY_NOTACTIONS>>",'"'+$($READONLY_NOTACTIONS -join $Joiner)+'"');

    # NoAccess role
    $BoilerPlate = $Boilerplate.Replace("<<NOACCESS_NOTACTIONS>>",'"'+$($NOACCESS_NOTACTIONS -join $Joiner)+'"');

    # Create the section if not exists
    $Category = $Service.Category.ToLower().Replace(" ","-").Replace("+","and").Replace("\","-").Replace("/","-");
    $SectionPath = "$ServicesFolder\$Category";
    if (!(Test-Path $SectionPath)) {
        [Void](New-Item $SectionPath -ItemType Directory);
    }

    # Create the main terraform stanza file
    Set-Content -Path "$SectionPath\$KEBAB_NAME`.tf" -Value $Boilerplate -NoNewline;

    # Template up the output boilerplate
    "NoAccess","ReadOnly","FullAccess" | %{

        $AclName = $_;
        $LowerName = $AclName.ToLower();
        $OutputName = $SNAKE_NAME + "_" + $LowerName;
        $OutputDescription = "$AclName Definition for $PROPER_NAME.";
        $OutputValueProperty = "azurerm_role_definition.$($SNAKE_NAME)_$($LowerName)_definition";

        $Output = $OutputBoilerplate.Replace("<<SNAKE_NAME>>", $OutputName);
        $Output = $Output.Replace("<<TF_OBJECT_NAME>>", $OutputValueProperty);
        $Output = $Output.Replace("<<TF_OUTPUT_DESCRIPTION>>", $OutputDescription);

        $OutputRows += "| {0} | {1} | {2} |" -f $Service.Category,$Service.Name,$("``" + $OutputName + "``");

        Add-Content -Path $OutputFile -Value $Output;

        Start-Sleep -Milliseconds 250;
    }

    # Quick wait for Set-Content to catch up
    Start-Sleep -Milliseconds 250;
}

Write-Host "Building tf-azure-service-access-definitions readme.." -ForegroundColor Yellow;

# Build the readme content
$ServiceRows = ($Services.Where({$_.IsAddressableService}) | Select Category,Name,ServiceDescription, `
@{Name="Children";Expression={
    $ServiceId = $_.ServiceId;
    ($Services | ?{$_.PartOfService -eq $ServiceId} | `
    Select @{Name="Name";Expression={"``" + $_.Name + "``"}}).Name;
}}, Link | Select @{Name="Output";Expression={
    "| {0} | {1} | {2} | {3} | {4} |" -f $_.Category,$_.Name,$_.ServiceDescription,$($_.Children -join ", "),$_.Link;
}}).Output -join "`r`n";

# Set the readme content
$ReadmeContent = $ReadmeBoilerplate.Replace("<<SERVICE_ROWS>>", $ServiceRows);
$ReadmeContent = $ReadmeContent.Replace("<<OUTPUT_ROWS>>", $($OutputRows -join "`r`n"));
Set-Content -Path $ReadmeFile -Value $ReadmeContent;
Set-Location $RootFolder;

# Terraform fmt
Write-Host "Running terraform format.." -ForegroundColor Yellow;
terraform fmt

# Git commit
Write-Host "Creating build commit.." -ForegroundColor Yellow;
$Branch = git branch;
$Branch = $Branch.Replace("* ","");
git add .
git commit -m "Completed new build from source." -m "$CommitMessage"
git push -u origin $Branch

Write-Host "Building completed" -ForegroundColor Green;